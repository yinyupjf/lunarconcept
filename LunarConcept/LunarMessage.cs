﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SHomeWorkshop.LunarConcept
{
    internal class LunarMessage
    {
        /// <summary>
        /// 显示一个信息框。
        /// </summary>
        /// <param name="message">消息文本。</param>
        public static void Warning(string message)
        {
            if (string.IsNullOrEmpty(message)) return;

            MessageBox.Show(message, Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public static void Information(string message)
        {
            if (string.IsNullOrEmpty(message)) return;

            MessageBox.Show(message, Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
