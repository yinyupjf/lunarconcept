﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Converters
{
    /// <summary>
    /// 创建时间：2012年1月3日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：Text的最大宽度的转换器。转换规则：页面编辑器的横边减200。
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    public class TextMaxWidthConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return 800;

            return Math.Max(0, (double)value - 200);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value + 200;
        }

        #endregion
    }
}
