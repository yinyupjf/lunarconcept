﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SHomeWorkshop.LunarConcept.Converters
{
    /// <summary>
    /// 创建时间：2012年1月2日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：TextArea使用的上下标字号绑定的转换器。
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    public class HalfTextSizeConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value) / 2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value) * 2;
        }

        #endregion
    }
}
