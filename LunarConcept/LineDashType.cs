﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// 创建时间：2012年1月4日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：为线型部件定义线型。
    /// </summary>
    public class LineDashType
    {
        public enum DashType { Dash, DashDot, DashDotDot, Dot, Solid }

        public readonly static DoubleCollection dashCollection = new DoubleCollection() { 4, 2 };

        public readonly static DoubleCollection dashDotCollection = new DoubleCollection() { 4, 2, 1, 2 };

        public readonly static DoubleCollection dashDotDotCollection = new DoubleCollection() { 4, 2, 1, 2, 1, 2 };

        public readonly static DoubleCollection dotCollection = new DoubleCollection() { 1, 2 };

        public readonly static DoubleCollection solidCollection = null;

        public static DoubleCollection GetDashCollection(DashType dt)
        {
            switch (dt)
            {
                case DashType.Dash: return dashCollection;
                case DashType.DashDot: return dashDotCollection;
                case DashType.DashDotDot: return dashDotDotCollection;
                case DashType.Dot: return dotCollection;
                //case DashType.Solid: 
                default:
                    return solidCollection;
            }
        }
    }
}
