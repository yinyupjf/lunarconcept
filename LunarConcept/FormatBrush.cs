﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept
{
    public class FormatBrush
    {
        public WidgetStyle DefaultWidgetStyle { get; set; }

        private bool onlyFormatOnceTime = false;
        /// <summary>
        /// [读写]是否只格式化一次。
        /// </summary>
        public bool OnlyFormatOnceTime
        {
            get { return onlyFormatOnceTime; }
            set { onlyFormatOnceTime = value; }
        }

        /// <summary>
        /// 表示格式刷中的默认样式是从哪个类型的部件上取来的。这适用于支持多形态的部件（矩形、菱形、三角形等）。
        /// </summary>
        public Enums.WidgetTypes SourceWidgetType { get; set; } = Enums.WidgetTypes.None;
    }
}
