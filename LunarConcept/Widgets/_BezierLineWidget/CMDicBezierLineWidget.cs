﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicBezierLineWidget
    {
        private void CMDicBezierLineWidget_Opened(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            MiBezierLine.IsChecked = MiMindMapLinkLine.IsChecked = false;

            foreach (UIElement ue in pe.Children)
            {
                SHomeWorkshop.LunarConcept.Widgets.BezierLineWidget bw =
                    ue as SHomeWorkshop.LunarConcept.Widgets.BezierLineWidget;
                if (bw != null)
                {
                    if (bw.IsMainSelected)
                    {
                        MiBezierLine.IsChecked = (bw.LineForm == Enums.BezierLineForms.BezierLine);
                        MiMindMapLinkLine.IsChecked = (bw.LineForm == Enums.BezierLineForms.MindMapLinkLine);
                    }
                }
            }
        }
        
        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.BezierLineWidget).Name));
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void MiBezierLine_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBezierLineFormCommand.Execute(Enums.BezierLineForms.BezierLine));
        }

        private void MiMindMapLinkLine_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBezierLineFormCommand.Execute(Enums.BezierLineForms.MindMapLinkLine));
        }

        private void SdRadius_DragCompleted(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRadiusCommand.Execute(SdRadius.Value));
        }

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
