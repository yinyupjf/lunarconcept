﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Enums;
using SHomeWorkshop.LunarConcept.Tools;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    /// <summary>
    /// 创建时间：2012年1月16日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：作为所有单线（带首尾箭头）的LineWidget的基类。
    /// </summary>
    public abstract class ArrowLineWidget : LineWidget, IWidgetPadding, IRadius
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// 构造方法。
        /// </summary>
        public ArrowLineWidget(PageEditor masterEditor)
            : base(masterEditor)
        {
            this.mainTextPanel.Margin = defaultBorderWidth;//取消Widget中设置的对于ArrowLineWidget的文本区域来说多余的margin。
            this.mainBorder.Child = this.mainTextPanel;
            this.mainCanvas.Children.Add(this.mainBorder);
            Canvas.SetZIndex(this.mainBorder, 2);

            this.mainBorder.PreviewMouseLeftButtonDown += mainBorder_PreviewMouseLeftButtonDown;
            this.mainBorder.PreviewMouseLeftButtonUp += mainBorder_PreviewMouseLeftButtonUp;

            this.layerIndex = WidgetLayerIndex.ArrowLineWidget;
            RefreshLayerIndex();
        }

        #endregion


        #region 字段与属性===================================================================================================


        protected ArrowType arrows = ArrowType.End;
        /// <summary>
        /// 默认值是尾箭头。这是考虑到用户的使用习惯。
        /// </summary>
        [Tools.LunarProperty("Arrows", PropertyDateType.ArrowType)]
        public ArrowType Arrows
        {
            get { return arrows; }
            set
            {
                arrows = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute("Arrows", value.ToString());
                }

                RefreshArrows();
            }
        }

        private static Thickness defaultBorderWidth = new Thickness(0);

        /// <summary>
        /// 包围文本区域的Border，目的是支持文本区域的内间距。
        /// </summary>
        protected Border mainBorder = new Border()
        {
            BorderThickness = defaultBorderWidth,
            Padding = defaultBorderWidth,
            BorderBrush = Brushes.Transparent,
            CornerRadius = new CornerRadius(16),
            Cursor = System.Windows.Input.Cursors.Hand,
        };

        public override System.Windows.Rect OuterRect
        {
            get { return new Rect(TopLeft, BottomRight); }
        }

        protected double radius = 0.0;
        /// <summary>
        /// [读写]是否圆角矩形。
        /// </summary>
        [Tools.LunarProperty("Radius", PropertyDateType.Double)]
        public double Radius
        {
            get
            {
                return this.radius;
            }
            set
            {
                radius = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.RadiusTag, radius.ToString());
                }

                this.RefreshRadius(new Rect(Canvas.GetLeft(this.mainBorder),
                Canvas.GetTop(this.mainBorder), this.mainBorder.ActualWidth, this.mainBorder.ActualHeight));
            }
        }

        protected Path startArrowPath = new Path() { Visibility = Visibility.Hidden };//默认情况下不显示。

        public Path StartArrowPath { get { return startArrowPath; } }

        protected Path endArrowPath = new Path();

        public Path EndArrowPath { get { return endArrowPath; } }

        protected PathFigure startArrowPathFigure = new PathFigure();

        protected PathFigure endArrowPathFigure = new PathFigure();

        protected PolyLineSegment startArrowPolyLineSegment = new PolyLineSegment();

        protected PolyLineSegment endArrowPolyLineSegment = new PolyLineSegment();

        protected Thickness widgetPadding = new Thickness(0);
        /// <summary>
        /// [读写]部件内部空隙宽。默认值为0。
        /// </summary>
        [Tools.LunarProperty("WidgetPadding", PropertyDateType.Thickness)]
        public new Thickness WidgetPadding
        {
            get { return widgetPadding; }
            set
            {
                widgetPadding = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.WidgetPaddingTag, Globals.ThickConverter.ConvertToString(value));
                }
                this.RefreshWidgetPadding();

                //会影响圆角。
                this.RefreshRadius(new Rect(Canvas.GetLeft(this.mainBorder),
                    Canvas.GetTop(this.mainBorder), this.mainBorder.ActualWidth, this.mainBorder.ActualHeight));
            }
        }

        #endregion


        #region 方法=========================================================================================================


        public override void Build()
        {
            base.Build();

            if (this.xmlData == null) return;

            //此类是Arrows属性的“最终实现类”。因此调用，
            this.RefreshArrows();

            //因为这一分支的WidgetBackColor只管文本背景，因此，对这一分支来说，
            //此类就是WidgetBackColor属性的“最终实现类”。
            this.RefreshWidgetBackColor();
            this.RefreshWidgetPadding();

            //放在此类中，新添加的线条部件会无效，须放到具体实现类中去。
            //this.mainBorder.InvalidateArrange(); this.mainBorder.UpdateLayout();
            //this.RefreshRadius(new Rect(Canvas.GetLeft(this.mainBorder),
            //    Canvas.GetTop(this.mainBorder), this.mainBorder.ActualWidth, this.mainBorder.ActualHeight));
        }

        /// <summary>
        /// 所有由样式管理的属性均应放在此方法中读取。
        /// </summary>
        protected override void BuildStyleProperties()
        {
            base.BuildStyleProperties();

            if (this.xmlData == null) return;

            WidgetStyle defStyleOfDoc;
            if (this.masterEditor != null && this.masterEditor.MasterManager != null)
            {
                defStyleOfDoc = this.masterEditor.MasterManager.GetDefaultWidgetStyle(this.WidgetClassName);
            }
            else
            {
                defStyleOfDoc = WidgetStyle.FromWidgetClassName(this.WidgetClassName);
            }

            //取箭头状态。
            XmlAttribute attrArrows = this.xmlData.GetAttribute(XmlTags.ArrowsTag);
            if (attrArrows != null)
            {
                this.arrows = (ArrowType)Enum.Parse(typeof(ArrowType), attrArrows.Value);
            }
            else
            {
                this.arrows = defStyleOfDoc.Arrows;
            }

            //到ArrowLineWidget和ContentWidget为止，WidgetStyle中相关属性只有padding还未读取。
            XmlAttribute attrWidgetPadding = this.xmlData.GetAttribute(XmlTags.WidgetPaddingTag);
            if (attrWidgetPadding != null)
            {
                this.widgetPadding = (Thickness)Globals.ThickConverter.ConvertFromString(attrWidgetPadding.Value);
            }
            else
            {
                this.widgetPadding = defStyleOfDoc.WidgetPadding;
            }

            XmlAttribute attrRadius = this.xmlData.GetAttribute(XmlTags.RadiusTag);
            if (attrRadius != null)
            {
                this.radius = double.Parse(attrRadius.Value);
            }
            else
            {
                this.radius = defStyleOfDoc.Radius;
            }
        }

        public override void BuildWidgetStylePropertiesAndRefresh()
        {
            base.BuildWidgetStylePropertiesAndRefresh();

            //到此类为止，只有Arrows最终实现了。（派生类不需要再对Arrows作特殊处理）

            //一系列Refreshxxx();

            //这几个属性的“最终实现类”不是此类。因此不放在此处刷新。
            //this.RefreshWidgetLineColor();
            //this.RefreshWidgetLineWidth();
            //this.RefreshLineDash();

            //LineWidget不需要此属性。
            //this.RefreshWidgetPadding();

            //此类是Arrows的最终实现类。
            this.RefreshArrows();

            //因为这一分支的WidgetBackColor只管文本背景，因此，对这一分支来说，
            //此类就是WidgetBackColor属性的“最终实现类”。
            this.RefreshWidgetBackColor();
            this.RefreshWidgetPadding();

            this.mainBorder.InvalidateArrange(); this.mainBorder.UpdateLayout();//有必要
            this.RefreshRadius(new Rect(Canvas.GetLeft(this.mainBorder),
                Canvas.GetTop(this.mainBorder), this.mainBorder.ActualWidth, this.mainBorder.ActualHeight));

            //这几个的“最终实现类”就是Widget类。因此已在Widget类中调用。
            //this.RefreshWidgetForeColor();
            //this.RefreshWidgetOpacity();
            //this.RefreshIsShadowVisible();
        }

        /// <summary>
        /// 使用EditorManager.FormatBrush来格式化此部件。
        /// 
        /// </summary>
        /// <returns>如果被格式化，说明不是要进行选定状态的改变。</returns>
        public override bool FormatSelf(ref ModifingItem<Action, ModifingInfo> mi, ref ModifingInfo info)
        {
            if (this.masterEditor == null || this.masterEditor.MasterManager == null) return false;

            EditorManager manager = this.masterEditor.MasterManager;
            if (manager.FormatBrush == null) return false;//不在“格式刷”状态

            PageEditor mainPageEditor = this.masterEditor;

            List<Widgets.Widget> destWidgets = new List<Widget>();
            destWidgets.Add(this);//格式化功能只需要自身一个即可。不更改兄弟部件的选定状态。
            if (info == null)
            {
                info = new ModifingInfo();
                info.ModifingDescription = "格式化部件";
                manager.GetSelectedPageEditorStatus(info);
                manager.GetSelectedWidgetStatus_Old(info);
                manager.GetSelectedWidgetStatus_New(info);
            }

            bool shouldReg = false;
            if (mi == null)
            {
                shouldReg = true;
                mi = new ModifingItem<Action, ModifingInfo>(info);
            }

            //设置格式
            Action actOpacity = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetOpacityTag,
                this.WidgetOpacity.ToString(), manager.FormatBrush.DefaultWidgetStyle.WidgetOpacity.ToString());
            this.WidgetOpacity = manager.FormatBrush.DefaultWidgetStyle.WidgetOpacity;

            Action actBackColor = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetBackColor,
                BrushManager.GetName(this.WidgetBackColor),
                BrushManager.GetName(manager.FormatBrush.DefaultWidgetStyle.WidgetBackColor));
            this.WidgetBackColor = manager.FormatBrush.DefaultWidgetStyle.WidgetBackColor;

            Action actForeColor = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetForeColor,
                BrushManager.GetName(this.WidgetForeColor),
                BrushManager.GetName(manager.FormatBrush.DefaultWidgetStyle.WidgetForeColor));
            this.WidgetForeColor = manager.FormatBrush.DefaultWidgetStyle.WidgetForeColor;

            Action actLineDash = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.LineDashTag,
                this.LineDash.ToString(), manager.FormatBrush.DefaultWidgetStyle.LineDash.ToString());
            this.LineDash = manager.FormatBrush.DefaultWidgetStyle.LineDash;

            Action actLineWidth = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetLineWidthTag,
                this.WidgetLineWidth.ToString(), manager.FormatBrush.DefaultWidgetStyle.WidgetLineWidth.ToString());
            this.WidgetLineWidth = manager.FormatBrush.DefaultWidgetStyle.WidgetLineWidth;

            Action actLineColor = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetLineColorTag,
                BrushManager.GetName(this.widgetLineColor),
                BrushManager.GetName(manager.FormatBrush.DefaultWidgetStyle.WidgetLineColor));
            this.WidgetLineColor = manager.FormatBrush.DefaultWidgetStyle.WidgetLineColor;

            Action actLineArrows = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.ArrowsTag,
                this.Arrows.ToString(), manager.FormatBrush.DefaultWidgetStyle.Arrows.ToString());
            this.Arrows = manager.FormatBrush.DefaultWidgetStyle.Arrows;

            Action actIsShadowVisible = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.IsShadowVisibleTag,
                this.IsShadowVisible.ToString(), manager.FormatBrush.DefaultWidgetStyle.IsShadowVisible.ToString());
            this.IsShadowVisible = manager.FormatBrush.DefaultWidgetStyle.IsShadowVisible;

            Action actWidgetPadding = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetPaddingTag,
                this.WidgetPadding.ToString(), manager.FormatBrush.DefaultWidgetStyle.WidgetPadding.ToString());
            this.WidgetPadding = manager.FormatBrush.DefaultWidgetStyle.WidgetPadding;

            Action actRadius = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.RadiusTag,
                this.Radius.ToString(), manager.FormatBrush.DefaultWidgetStyle.Radius.ToString());
            this.Radius = manager.FormatBrush.DefaultWidgetStyle.Radius;

            mi.AddAction(actOpacity); mi.AddAction(actBackColor); mi.AddAction(actForeColor);
            mi.AddAction(actLineArrows); mi.AddAction(actLineWidth);mi.AddAction(actLineColor);
            mi.AddAction(actIsShadowVisible); mi.AddAction(actWidgetPadding); mi.AddAction(actRadius);

            if (shouldReg)
            {
                manager.RegisterModifingItem(mi);
                if (manager.FormatBrush.OnlyFormatOnceTime)
                {
                    masterEditor.RaiseWidgetFormated(new List<Widgets.Widget>() { this });
                }
            }

            return true;
        }

        /// <summary>
        /// 目的是点击选取部件。
        /// </summary>
        void mainBorder_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            mainTextPanel_PreviewMouseLeftButtonUp(sender, e);
            e.Handled = true;
        }

        void mainBorder_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            mainTextPanel_PreviewMouseLeftButtonDown(sender, e);
            e.Handled = true;
        }

        public virtual void RefreshArrows()
        {
            switch (arrows)
            {
                case ArrowType.All:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Visible;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.Start:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Hidden;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.End:
                    {
                        startArrowPath.Visibility = Visibility.Hidden;
                        endArrowPath.Visibility = Visibility.Visible;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.EmptyStart:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Hidden;
                        //StartArrowPath.Fill = Brushes.Transparent;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.RhombStart:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Hidden;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.EmptyRhombStart:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Hidden;
                        //StartArrowPath.Fill = Brushes.Transparent;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.Arrow:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Visible;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
                case ArrowType.EmptyArrow:
                    {
                        startArrowPath.Visibility = Visibility.Visible;
                        endArrowPath.Visibility = Visibility.Visible;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = Brushes.Transparent;
                        break;
                    }
                case ArrowType.OpenArrowEnd:
                    {
                        startArrowPath.Visibility = Visibility.Hidden;
                        endArrowPath.Visibility = Visibility.Visible;
                        //StartArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = Brushes.Transparent;
                        break;
                    }
                default://无箭头
                    {
                        startArrowPath.Visibility = Visibility.Hidden;
                        endArrowPath.Visibility = Visibility.Hidden;
                        //EndArrowPath.Fill = WidgetLineColor;
                        //EndArrowPath.Fill = WidgetLineColor;
                        break;
                    }
            }
        }

        /// <summary>
        /// ArrowLineWidget类的RefreshWidgetPadding()方法会在内部调用RefreshTextPanelLocatin()方法。
        /// </summary>
        public void RefreshWidgetPadding()
        {
            if (this.mainBorder != null)
                this.mainBorder.Padding = this.widgetPadding;

            this.mainBorder.InvalidateArrange();
            this.mainBorder.UpdateLayout();

            this.RefreshTextPanelLocatin();
        }

        public override void RefreshLocation()
        {
            base.RefreshLocation();
        }

        public override void RefreshIsShadowVisible()
        {
            if (isShadowVisible)
            {
                this.mainTextPanel.Effect =
                startArrowPath.Effect = endArrowPath.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.mainTextPanel.Effect =
                startArrowPath.Effect = endArrowPath.Effect = null;
            }
        }

        public void RefreshRadius(Rect rect)
        {
            if (this.mainBorder == null) return;
            if (radius > 0)
            {
                double k;

                if (radius > 1)
                {
                    k = (radius % 1);//取小数。
                }
                else
                {
                    k = radius;
                }

                double r = Math.Min(rect.Width, rect.Height) * k;

                this.mainBorder.CornerRadius = new CornerRadius(r);
            }
            else
            {
                this.mainBorder.CornerRadius = new CornerRadius(0);
            }
        }

        public override void RefreshText()
        {
            base.RefreshText();

            this.mainBorder.InvalidateArrange();
            this.mainBorder.UpdateLayout();

            if (this.mainBorder.ActualHeight > 0 && this.mainBorder.ActualWidth > 0)
            {
                this.RefreshRadius(new Rect(Canvas.GetLeft(this.mainBorder),
                    Canvas.GetTop(this.mainBorder), this.mainBorder.ActualWidth, this.mainBorder.ActualHeight));
            }
        }

        /// <summary>
        /// WidgetBackground属性值改变时会调用此方法刷新部件的背景色。
        /// ——亦可根据需要调用。
        /// ——默认直接改变mainBorder的背景色。派生类可自行定义行为。
        /// </summary>
        public override void RefreshWidgetBackColor()
        {
            if (this.mainBorder != null)
                this.mainBorder.Background = widgetBackColor;

            switch (arrows)
            {
                case ArrowType.Start:
                case ArrowType.End:
                case ArrowType.All:
                case ArrowType.None:
                    this.startArrowPath.Fill = this.endArrowPath.Fill = widgetLineColor;
                    break;
                case ArrowType.EmptyStart:
                    this.startArrowPath.Fill = Brushes.Transparent;
                    this.endArrowPath.Fill = widgetBackColor;
                    break;
                case ArrowType.RhombStart:
                    this.startArrowPath.Fill = this.endArrowPath.Fill = widgetBackColor;
                    break;
                case ArrowType.EmptyRhombStart:
                    this.startArrowPath.Fill = Brushes.Transparent;
                    this.endArrowPath.Fill = widgetBackColor;
                    break;
                case ArrowType.Arrow:
                    this.startArrowPath.Fill = this.endArrowPath.Fill = widgetBackColor;
                    break;
                case ArrowType.EmptyArrow:
                    this.startArrowPath.Fill = Brushes.Transparent;
                    this.endArrowPath.Fill = widgetBackColor;
                    break;
                case ArrowType.OpenArrowEnd:
                    this.startArrowPath.Fill = Brushes.Transparent;
                    this.endArrowPath.Fill = widgetBackColor;
                    break;
            }
        }
        #endregion
    }
}
