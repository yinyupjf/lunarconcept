﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicTriangleWidget
    {

        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.TriangleWidget).Name));
        }

        private void MiSetBackgroundImage_Clicked(object sender, RoutedEventArgs e)
        {
            var mainSelWidget = Globals.MainWindow.EditorManager.GetMainSelectedWidget();
            if (mainSelWidget != null)
            {
                mainSelWidget.LoadImageFromFile();
            }
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void MiTriangle_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.Triangle));
        }

        private void MiPie_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.Pie));
        }

        private void MiSector_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.Sector));
        }

        private void MiBracket_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.Bracket));
        }

        private void MiTLine_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.TLine));
        }

        private void MiYLine_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.YLine));
        }

        private void MiOrgLine_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.OrgLine));
        }

        private void MiDialogBubble_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.DialogBubble));
        }

        private void MiThinkingBubble_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTriangleFormCommand.Execute(Enums.TriangleForm.ThinkingBubble));
        }

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
