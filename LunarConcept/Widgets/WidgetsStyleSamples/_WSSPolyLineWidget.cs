﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.WidgetsStyleSamples
{
    /// <summary>
    /// 创建时间：2012年6月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：创建一个代表对应部件的样式的CheckBox。以便设置部件默认样式。
    /// 
    /// ★注意★：类名必须以“_WSS”开头。
    /// </summary>
    class _WSSPolyLineWidget : WidgetsStyleSamples.WidgetStyleSample
    {
        public _WSSPolyLineWidget(WidgetStyle srcStyle, string localName) :
            base(srcStyle, localName)
        {
            this.mainStyle = WidgetStyle.FromWidgetClassName(typeof(Widgets.PolyLineWidget).Name);

            this.sampleTextBlock.Text =
                (this.mainStyle != null && string.IsNullOrEmpty(this.mainStyle.Text) == false) ?
                    this.mainStyle.Text :
                    this.WidgetClassLocalName;

            this.Children.Add(this.sampleTextBlock);

            Canvas.SetZIndex(sampleTextBlock, 100);

            RefreshTextBlockLocation();

            this.pathFigure.Segments.Add(this.mainPolyLineSegment);
            this.pathGeometry.Figures.Add(this.pathFigure);
            this.mainPath.Data = this.pathGeometry;

            this.Children.Add(this.mainPath);

            double halfHeight = this.Height / 2;
            double rightSide = this.Width - 2;

            this.pathFigure.StartPoint = new Point(10, this.Height - 20);
            this.mainPolyLineSegment.Points = new PointCollection()
            {
                new Point(10,halfHeight),
                new Point(this.Width-10,halfHeight),
                new Point(this.Width-10,20),
            };

            //画首尾箭头
            startArrow.Points = new System.Windows.Media.PointCollection()
            {
                new Point(10,this.Height-10),
                new Point(6,this.Height-20),
                new Point(14,this.Height-20),
            };

            endArrow.Points = new System.Windows.Media.PointCollection()
            {
                new Point(this.Width-10,10),
                new Point(this.Width-14,20),
                new Point(this.Width-6,20),
            };

            this.Children.Add(this.startArrow);
            this.Children.Add(this.endArrow);

            //=========挂接需要的事件。
            this.MainStyle = WidgetStyle.FromWidgetClassName(this.WidgetClassName);

            this.MainStyle.ArrowsChanged += new EventHandler<ArrowsChangedEventArgs>(mainStyle_ArrowsChanged);
            this.MainStyle.IsShadowVisibleChanged += new EventHandler<IsShadowVisibleChangedEventArgs>(mainStyle_IsShadowVisibleChanged);
            this.MainStyle.LineDashChanged += new EventHandler<LineDashChangedEventArgs>(mainStyle_LineDashChanged);

            this.MainStyle.WidgetBackColorChanged += new EventHandler<WidgetBackColorChangedEventArgs>(mainStyle_WidgetBackColorChanged);
            this.mainStyle.WidgetLineColorChanged += new EventHandler<WidgetLineColorChangedEventArgs>(mainStyle_WidgetLineColorChanged);
            this.MainStyle.WidgetLineWidthChanged += new EventHandler<WidgetLineWidthChangedEventArgs>(mainStyle_WidgetLineWidthChanged);

            this.MainStyle.WidgetForeColorChanged += new EventHandler<WidgetForeColorChangedEventArgs>(mainStyle_WidgetForeColorChanged);
            this.MainStyle.WidgetOpacityChanged += new EventHandler<WidgetOpacityChangedEventArgs>(mainStyle_WidgetOpacityChanged);
            //this.mainStyle.WidgetPaddingChanged += new EventHandler<WidgetPaddingChangedEventArgs>(mainStyle_WidgetPaddingChanged);//无意义
        }

        private void RefreshTextBlockLocation()
        {
            Canvas.SetLeft(this.sampleTextBlock, (this.Width - this.sampleTextBlock.Height) / 2);
            Canvas.SetTop(this.sampleTextBlock,
                this.Height / 2 - this.sampleTextBlock.Height - this.mainStyle.WidgetLineWidth / 2 - 2);//折线不同
        }

        //void mainStyle_WidgetPaddingChanged(object sender, WidgetPaddingChangedEventArgs e)
        //{
        //
        //}

        void mainStyle_WidgetOpacityChanged(object sender, WidgetOpacityChangedEventArgs e)
        {
            this.mainPath.Opacity = this.sampleTextBlock.Opacity = e.NewValue;
        }

        void mainStyle_WidgetForeColorChanged(object sender, WidgetForeColorChangedEventArgs e)
        {
            this.sampleTextBlock.Foreground = e.NewValue;
        }

        void mainStyle_WidgetLineWidthChanged(object sender, WidgetLineWidthChangedEventArgs e)
        {
            this.mainPath.StrokeThickness = (e.NewValue < 1) ? 1 : e.NewValue;
            RefreshTextBlockLocation();
        }

        void mainStyle_WidgetLineColorChanged(object sender, WidgetLineColorChangedEventArgs e)
        {
            this.mainPath.Stroke =
                this.startArrow.Stroke = this.startArrow.Fill =
                this.endArrow.Stroke = this.endArrow.Fill =
                e.NewValue;
        }

        void mainStyle_WidgetBackColorChanged(object sender, WidgetBackColorChangedEventArgs e)
        {
            this.sampleTextBlock.Background = e.NewValue;
        }

        void mainStyle_LineDashChanged(object sender, LineDashChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case LineDashType.DashType.Dash:
                    {
                        this.mainPath.StrokeDashArray = LineDashType.dashCollection; break;
                    }
                case LineDashType.DashType.DashDotDot:
                    {
                        this.mainPath.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                    }
                case LineDashType.DashType.Dot:
                    {
                        this.mainPath.StrokeDashArray = LineDashType.dotCollection; break;
                    }
                case LineDashType.DashType.DashDot:
                    {
                        this.mainPath.StrokeDashArray = LineDashType.dashDotCollection; break;
                    }
                default:
                    {
                        this.mainPath.StrokeDashArray = LineDashType.solidCollection; break;
                    }
            }
        }

        void mainStyle_IsShadowVisibleChanged(object sender, IsShadowVisibleChangedEventArgs e)
        {
            if (e.NewValue)
            {
                this.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.Effect = null;
            }
        }

        void mainStyle_ArrowsChanged(object sender, ArrowsChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case Enums.ArrowType.All:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Visible;
                        this.endArrow.Visibility = System.Windows.Visibility.Visible;
                        this.pathFigure.StartPoint = new Point(10, this.Height - 20);
                        this.mainPolyLineSegment.Points = new PointCollection(){
                            new Point(10,this.Height/2),  
                            new Point(this.Width-10,this.Height/2),
                            new Point(this.Width - 10, 20),
                        };
                        break;
                    }
                case Enums.ArrowType.Start:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Visible;
                        this.endArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.pathFigure.StartPoint = new Point(10, this.Height - 20);
                        this.mainPolyLineSegment.Points = new PointCollection(){
                            new Point(10,this.Height/2),  
                            new Point(this.Width-10,this.Height/2),
                            new Point(this.Width - 10, 10),
                        };
                        break;
                    }
                case Enums.ArrowType.End:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.endArrow.Visibility = System.Windows.Visibility.Visible;
                        this.pathFigure.StartPoint = new Point(10, this.Height - 10);
                        this.mainPolyLineSegment.Points = new PointCollection(){
                            new Point(10,this.Height/2),  
                            new Point(this.Width-10,this.Height/2),
                            new Point(this.Width - 10, 20),
                        };
                        break;
                    }
                default:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.endArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.pathFigure.StartPoint = new Point(10, this.Height - 10);
                        this.mainPolyLineSegment.Points = new PointCollection(){
                            new Point(10,this.Height/2),  
                            new Point(this.Width-10,this.Height/2),
                            new Point(this.Width - 10, 10),
                        };
                        break;
                    }
            }
        }

        private PolyLineSegment mainPolyLineSegment = new PolyLineSegment() { IsStroked = true };

        private PathGeometry pathGeometry = new PathGeometry();

        private PathFigure pathFigure = new PathFigure();

        private System.Windows.Shapes.Path mainPath = new Path()
        {
            Stroke = Brushes.Black,
            StrokeThickness = 2,
        };

        private Polygon startArrow = new Polygon()
        {
            Fill = Brushes.Black,
            Stroke = Brushes.Black,
            StrokeThickness = 2,
            StrokeLineJoin = System.Windows.Media.PenLineJoin.Round
        };

        private Polygon endArrow = new Polygon()
        {
            Fill = Brushes.Black,
            Stroke = Brushes.Black,
            StrokeThickness = 2,
            StrokeLineJoin = System.Windows.Media.PenLineJoin.Round
        };
    }
}