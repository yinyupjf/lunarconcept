﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept.Widgets.WidgetsStyleSamples
{
    /// <summary>
    /// 创建时间：2012年6月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：创建一个代表对应部件的样式的CheckBox。以便设置部件默认样式。
    /// 
    /// ★注意★：类名必须以“_WSS”开头。
    /// </summary>
    class _WSSStraitLineWidget : WidgetsStyleSamples.WidgetStyleSample
    {
        public _WSSStraitLineWidget(WidgetStyle srcStyle, string localName) :
            base(srcStyle, localName)
        {
            this.mainStyle = WidgetStyle.FromWidgetClassName(typeof(Widgets.StraitLineWidget).Name);

            this.sampleTextBlock.Text =
                (this.mainStyle != null && string.IsNullOrEmpty(this.mainStyle.Text) == false) ?
                    this.mainStyle.Text :
                    this.WidgetClassLocalName;

            this.Children.Add(this.sampleTextBlock);

            Canvas.SetZIndex(sampleTextBlock, 100);

            Canvas.SetLeft(this.sampleTextBlock, (this.Width - this.sampleTextBlock.Width) / 2);
            Canvas.SetTop(this.sampleTextBlock, (this.Height - this.sampleTextBlock.Height) / 2);

            this.Children.Add(this.mainLine);

            double halfHeight = this.Height / 2;
            double rightSide = this.Width - 2;

            this.mainLine.X1 = 2; this.mainLine.Y1 = halfHeight;
            this.mainLine.X2 = this.Width - 2; this.mainLine.Y2 = this.mainLine.Y1;

            //画首尾箭头
            startArrow.Points = new System.Windows.Media.PointCollection()
            {
                new Point(2,halfHeight),
                new Point(12,halfHeight-4),
                new Point(12,halfHeight+4),
            };

            endArrow.Points = new System.Windows.Media.PointCollection()
            {
                new Point(rightSide,halfHeight),
                new Point(rightSide-10,halfHeight-4),
                new Point(rightSide-10,halfHeight+4),
            };

            this.Children.Add(this.startArrow);
            this.Children.Add(this.endArrow);

            //=========挂接需要的事件。
            this.MainStyle = WidgetStyle.FromWidgetClassName(this.WidgetClassName);

            this.MainStyle.ArrowsChanged += new EventHandler<ArrowsChangedEventArgs>(mainStyle_ArrowsChanged);
            this.MainStyle.IsShadowVisibleChanged += new EventHandler<IsShadowVisibleChangedEventArgs>(mainStyle_IsShadowVisibleChanged);
            this.MainStyle.LineDashChanged += new EventHandler<LineDashChangedEventArgs>(mainStyle_LineDashChanged);

            this.MainStyle.WidgetBackColorChanged += new EventHandler<WidgetBackColorChangedEventArgs>(mainStyle_WidgetBackColorChanged);
            this.mainStyle.WidgetLineColorChanged += new EventHandler<WidgetLineColorChangedEventArgs>(mainStyle_WidgetLineColorChanged);
            this.MainStyle.WidgetLineWidthChanged += new EventHandler<WidgetLineWidthChangedEventArgs>(mainStyle_WidgetLineWidthChanged);

            this.MainStyle.WidgetForeColorChanged += new EventHandler<WidgetForeColorChangedEventArgs>(mainStyle_WidgetForeColorChanged);
            this.MainStyle.WidgetOpacityChanged += new EventHandler<WidgetOpacityChangedEventArgs>(mainStyle_WidgetOpacityChanged);
            //this.mainStyle.WidgetPaddingChanged += new EventHandler<WidgetPaddingChangedEventArgs>(mainStyle_WidgetPaddingChanged);//无意义
        }

        //void mainStyle_WidgetPaddingChanged(object sender, WidgetPaddingChangedEventArgs e)
        //{
        //
        //}

        void mainStyle_WidgetOpacityChanged(object sender, WidgetOpacityChangedEventArgs e)
        {
            this.mainLine.Opacity = this.sampleTextBlock.Opacity = e.NewValue;
        }

        void mainStyle_WidgetForeColorChanged(object sender, WidgetForeColorChangedEventArgs e)
        {
            this.sampleTextBlock.Foreground = e.NewValue;
        }

        void mainStyle_WidgetLineWidthChanged(object sender, WidgetLineWidthChangedEventArgs e)
        {
            this.mainLine.StrokeThickness = (e.NewValue < 1) ? 1 : e.NewValue;
        }

        void mainStyle_WidgetLineColorChanged(object sender, WidgetLineColorChangedEventArgs e)
        {
            this.mainLine.Stroke =
                this.startArrow.Stroke = this.startArrow.Fill =
                this.endArrow.Stroke = this.endArrow.Fill =
                e.NewValue;
        }

        void mainStyle_WidgetBackColorChanged(object sender, WidgetBackColorChangedEventArgs e)
        {
            this.sampleTextBlock.Background = e.NewValue;
        }

        void mainStyle_LineDashChanged(object sender, LineDashChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case LineDashType.DashType.Dash:
                    {
                        this.mainLine.StrokeDashArray = LineDashType.dashCollection; break;
                    }
                case LineDashType.DashType.DashDotDot:
                    {
                        this.mainLine.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                    }
                case LineDashType.DashType.Dot:
                    {
                        this.mainLine.StrokeDashArray = LineDashType.dotCollection; break;
                    }
                case LineDashType.DashType.DashDot:
                    {
                        this.mainLine.StrokeDashArray = LineDashType.dashDotCollection; break;
                    }
                default:
                    {
                        this.mainLine.StrokeDashArray = LineDashType.solidCollection; break;
                    }
            }
        }

        void mainStyle_IsShadowVisibleChanged(object sender, IsShadowVisibleChangedEventArgs e)
        {
            if (e.NewValue)
            {
                this.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.Effect = null;
            }
        }

        void mainStyle_ArrowsChanged(object sender, ArrowsChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case Enums.ArrowType.All:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Visible;
                        this.endArrow.Visibility = System.Windows.Visibility.Visible;
                        this.mainLine.X1 = 12;
                        this.mainLine.X2 = this.Width - 12;
                        break;
                    }
                case Enums.ArrowType.Start:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Visible;
                        this.endArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.mainLine.X1 = 12;
                        this.mainLine.X2 = this.Width - 2;
                        break;
                    }
                case Enums.ArrowType.End:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.endArrow.Visibility = System.Windows.Visibility.Visible;
                        this.mainLine.X1 = 2;
                        this.mainLine.X2 = this.Width - 12;
                        break;
                    }
                default:
                    {
                        this.startArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.endArrow.Visibility = System.Windows.Visibility.Hidden;
                        this.mainLine.X1 = 2;
                        this.mainLine.X2 = this.Width - 2;
                        break;
                    }
            }
        }

        private Line mainLine = new Line()
        {
            Stroke = Brushes.Black,
            StrokeThickness = 2,
        };

        private Polygon startArrow = new Polygon()
        {
            Fill = Brushes.Black,
            Stroke = Brushes.Black,
            StrokeThickness = 2,
            StrokeLineJoin = System.Windows.Media.PenLineJoin.Round
        };

        private Polygon endArrow = new Polygon()
        {
            Fill = Brushes.Black,
            Stroke = Brushes.Black,
            StrokeThickness = 2,
            StrokeLineJoin = System.Windows.Media.PenLineJoin.Round
        };
    }
}
