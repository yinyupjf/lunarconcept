﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.WidgetsStyleSamples
{
    /// <summary>
    /// 创建时间：2012年6月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：创建一个代表对应部件的样式的CheckBox。以便设置部件默认样式。
    /// 
    /// ★注意★：类名必须以“_WSS”开头。
    /// </summary>
    class _WSSTextArea : WidgetsStyleSamples.WidgetStyleSample
    {
        public _WSSTextArea(WidgetStyle srcStyle, string localName) :
            base(srcStyle, localName)
        {
            this.mainStyle = WidgetStyle.FromWidgetClassName(typeof(Widgets.TextArea).Name);

            this.sampleTextBlock.Text =
                (this.mainStyle != null && string.IsNullOrEmpty(this.mainStyle.Text) == false) ?
                    this.mainStyle.Text :
                    this.WidgetClassLocalName;

            this.mainBorder.Child = this.sampleTextBlock;
            this.Children.Add(this.mainBorder);

            Canvas.SetZIndex(mainBorder, 100);

            //=========挂接需要的事件。
            this.MainStyle = WidgetStyle.FromWidgetClassName(this.WidgetClassName);

            //this.mainStyle.ArrowsChanged += new EventHandler<ArrowsChangedEventArgs>(mainStyle_ArrowsChanged);//无意义
            this.MainStyle.IsShadowVisibleChanged += new EventHandler<IsShadowVisibleChangedEventArgs>(mainStyle_IsShadowVisibleChanged);
            //this.mainStyle.LineDashChanged += new EventHandler<LineDashChangedEventArgs>(mainStyle_LineDashChanged);//无意义

            this.MainStyle.WidgetBackColorChanged += new EventHandler<WidgetBackColorChangedEventArgs>(mainStyle_WidgetBackColorChanged);
            this.MainStyle.WidgetLineColorChanged += new EventHandler<WidgetLineColorChangedEventArgs>(mainStyle_WidgetLineColorChanged);
            this.MainStyle.WidgetLineWidthChanged += new EventHandler<WidgetLineWidthChangedEventArgs>(mainStyle_WidgetLineWidthChanged);

            this.MainStyle.WidgetForeColorChanged += new EventHandler<WidgetForeColorChangedEventArgs>(mainStyle_WidgetForeColorChanged);
            this.MainStyle.WidgetOpacityChanged += new EventHandler<WidgetOpacityChangedEventArgs>(mainStyle_WidgetOpacityChanged);
            this.MainStyle.WidgetPaddingChanged += new EventHandler<WidgetPaddingChangedEventArgs>(mainStyle_WidgetPaddingChanged);

            this.mainBorder.SizeChanged += new System.Windows.SizeChangedEventHandler(mainBorder_SizeChanged);
        }

        void mainBorder_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            Canvas.SetLeft(this.mainBorder, (this.Width - this.mainBorder.ActualWidth) / 2);
            Canvas.SetTop(this.mainBorder, (this.Height - this.mainBorder.ActualHeight) / 2);
        }

        void mainStyle_WidgetPaddingChanged(object sender, WidgetPaddingChangedEventArgs e)
        {
            mainBorder.Padding = e.NewValue;
        }

        void mainStyle_WidgetOpacityChanged(object sender, WidgetOpacityChangedEventArgs e)
        {
            mainBorder.Opacity = e.NewValue;
        }

        void mainStyle_WidgetForeColorChanged(object sender, WidgetForeColorChangedEventArgs e)
        {
            sampleTextBlock.Foreground = e.NewValue;
        }

        void mainStyle_WidgetLineWidthChanged(object sender, WidgetLineWidthChangedEventArgs e)
        {
            mainBorder.BorderThickness = new System.Windows.Thickness(e.NewValue);
        }

        void mainStyle_WidgetLineColorChanged(object sender, WidgetLineColorChangedEventArgs e)
        {
            mainBorder.BorderBrush = e.NewValue;
        }

        void mainStyle_WidgetBackColorChanged(object sender, WidgetBackColorChangedEventArgs e)
        {
            mainBorder.Background = e.NewValue;
        }

        //void mainStyle_LineDashChanged(object sender, LineDashChangedEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        void mainStyle_IsShadowVisibleChanged(object sender, IsShadowVisibleChangedEventArgs e)
        {
            if (e.NewValue)
            {
                this.mainBorder.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.mainBorder.Effect = null;
            }
        }

        //void mainStyle_ArrowsChanged(object sender, ArrowsChangedEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        private Border mainBorder = new Border();
    }
}
