﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicPolyLineWidget
    {

        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.PolyLineWidget).Name));
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void CMDicPolyLineWidget_Opened(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            MiHorizontalDirection.IsChecked = MiVerticalDirection.IsChecked = false;
            MiTextAtLeftOrTop.IsChecked = MiTextAtRightOrBottom.IsChecked = false;
            MiLLine.IsChecked = MiCLine.IsChecked =
                MiPolyLine.IsChecked = MiBigFishBone.IsChecked = MiMediumFishBone.IsChecked = false;

            //int pCount = 0;

            foreach (UIElement ue in pe.Children)
            {
                SHomeWorkshop.LunarConcept.Widgets.PolyLineWidget pw =
                    ue as SHomeWorkshop.LunarConcept.Widgets.PolyLineWidget;
                if (pw != null)
                {
                    //pCount++;
                    if (pw.IsMainSelected)
                    {
                        MiPolyLine.IsChecked = (pw.LineForm == Enums.PolyLineForms.PolyLine);
                        MiBigFishBone.IsChecked = (pw.LineForm == Enums.PolyLineForms.BigFishBone);
                        MiMediumFishBone.IsChecked = (pw.LineForm == Enums.PolyLineForms.MediumFishBone);
                        MiCLine.IsChecked = (pw.LineForm == Enums.PolyLineForms.CLine);
                        MiLLine.IsChecked = (pw.LineForm == Enums.PolyLineForms.LLine);

                        switch (pw.LineForm)
                        {
                            case Enums.PolyLineForms.CLine:
                            case Enums.PolyLineForms.LLine:
                            case Enums.PolyLineForms.PolyLine:
                                {
                                    MiHorizontalDirection.IsChecked = (pw.Direction == System.Windows.Controls.Orientation.Horizontal);
                                    MiVerticalDirection.IsChecked = (pw.Direction == System.Windows.Controls.Orientation.Vertical);

                                    MiTextAtLeftOrTop.IsChecked = (pw.IsTextAtLeftOrTop) ? true : false;
                                    MiTextAtRightOrBottom.IsChecked = (pw.IsTextAtLeftOrTop == false) ? true : false;

                                    SeparatorBetweenDirectionAndLineForm.Visibility =
                                    MiHorizontalDirection.Visibility =
                                    MiVerticalDirection.Visibility = Visibility.Visible;
                                    break;
                                }
                            case Enums.PolyLineForms.BigFishBone:
                            case Enums.PolyLineForms.MediumFishBone:
                                {
                                    SeparatorBetweenDirectionAndLineForm.Visibility =
                                    MiHorizontalDirection.Visibility =
                                    MiVerticalDirection.Visibility = Visibility.Collapsed;
                                    break;
                                }
                        }
                    }
                }
            }
        }

        private void MiHorizontalDirection_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetDirectionHorizontalCommand.Execute());
        }

        private void MiVerticalDirection_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetDirectionVerticalCommand.Execute());
        }

        private void MiPolyLine_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPolyLineFormCommand.Execute(Enums.PolyLineForms.PolyLine));
        }

        private void MiLLine_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPolyLineFormCommand.Execute(Enums.PolyLineForms.LLine));
        }

        private void MiCLine_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPolyLineFormCommand.Execute(Enums.PolyLineForms.CLine));
        }

        private void MiBigFishBone_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPolyLineFormCommand.Execute(Enums.PolyLineForms.BigFishBone));
        }

        private void MiMediumFishBone_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPolyLineFormCommand.Execute(Enums.PolyLineForms.MediumFishBone));
        }

        private void MiTextAtLeftOrTop_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTextAtLeftOrTopCommand.Execute());
        }

        private void MiTextAtRightOrBottom_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTextAtRightOrBottomCommand.Execute());
        }

        private void SdRadius_DragCompleted(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRadiusCommand.Execute(SdRadius.Value));
        }

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
