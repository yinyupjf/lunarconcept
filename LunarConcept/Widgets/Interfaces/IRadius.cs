﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Widgets.Interfaces
{
    public interface IRadius
    {
        string Id { get; }

        PageEditor MasterEditor { get; }

        double Radius { get; set; }

        void RefreshRadius(Rect rect);
    }
}
