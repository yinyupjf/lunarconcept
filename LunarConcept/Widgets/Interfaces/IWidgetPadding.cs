﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Widgets.Interfaces
{
    public interface IWidgetPadding
    {
        string Id { get; }

        PageEditor MasterEditor { get; }

        Thickness WidgetPadding { get; set; }

        void RefreshWidgetPadding();
    }
}
