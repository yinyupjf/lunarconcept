﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Widgets.Interfaces
{
    /// <summary>
    /// 创建时间：二○一二年五月三十日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：令直线，曲线，折线三个类支持“自动连接”到某个实现了“ICanBeLinkedWidget”接口的部件类。
    /// ★备注：　此前只有ContentWidget类支持自动连接线，且自动连接线只能是直线。
    /// 
    /// ★注意：　此接口只能用于部件类，否则必定出错！！！
    /// </summary>
    public interface ILinkableLine
    {
        /// <summary>
        /// 连接线必须支持设置箭头指向。
        /// </summary>
        ArrowType Arrows { get; set; }

        ControlDraggingType DraggingType { get; }

        void DrawLine();

        string Id { get; }

        string EndMasterId { get; set; }

        Point EndPoint { get; set; }

        bool IsLinked { get; }

        bool IsShadowVisible { get; set; }

        /// <summary>
        /// 连接线永不应设置为Locked。其内容均应如下：
        /// [Tools.LunarProperty("IsLocked", PropertyDateType.Bool)]
        /// public override bool IsLocked
        /// {
        ///     get
        ///     {
        ///         if (IsLinked) return false;

        ///         return base.IsLocked;
        ///     }
        ///     set
        ///     {
        ///         if (IsLinked) return;//连接线不被设置为锁定。

        ///         base.IsLocked = value;
        ///     }
        /// }
        /// </summary>
        bool IsLocked { get; set; }

        bool IsSelected { get; set; }

        bool IsMainSelected { get; set; }

        LineDashType.DashType LineDash { get; set; }

        double WidgetLineWidth { get; set; }

        Controls.PageEditor MasterEditor { get; }

        string StartMasterId { get; set; }

        Point StartPoint { get; set; }

        double WidgetOpacity { get; set; }

        Brush WidgetBackColor { get; set; }

        Brush WidgetLineColor { get; set; }

        Brush WidgetForeColor { get; set; }

        Thickness WidgetPadding { get; set; }

        XmlNode XmlData { get; set; }

        /// <summary>
        /// 移动挂接的主部件时，与此部件连接的线（就是本接口实现类的某个实例）需要自动相应地移动。
        /// </summary>
        /// <param name="tmpStartMasterRect">如果指定了此参数，则startRect应按此参数来移动。</param>
        /// <param name="tmpEndMasterRect">如果指定了此参数，则endRect应按此参数来移动。</param>
        void MoveWhenDraggingMasterWidget(Rect? tmpStartMasterRect = null, Rect? tmpEndMasterRect = null);

        void NewID();

        void RefreshLocation();

        void SelectOnlySelf();

        void RefreshPresentateAdorners();
    }
}
