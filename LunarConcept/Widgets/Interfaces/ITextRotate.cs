﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.Interfaces
{
    public interface ITextRotate
    {
        string Id { get; }

        PageEditor MasterEditor { get; }

        double TextRotateAngle { get; set; }

        void RefreshTextRotateAngle();
    }
}
