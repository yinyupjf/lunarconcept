﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicBracketWidget
    {


        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.BracketWidget).Name));
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void MiSetHorizontalPointer_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBracketFormCommand.Execute(Enums.BracketLineForms.HorizontalPointer));
        }

        private void MiSetVerticalPointer_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBracketFormCommand.Execute(Enums.BracketLineForms.VerticalPointer));
        }

        private void MiSetBracket_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBracketFormCommand.Execute(Enums.BracketLineForms.Bracket));
        }

        private void MiSetSquare_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBracketFormCommand.Execute(Enums.BracketLineForms.Square));
        }

        private void MiSetBrace_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBracketFormCommand.Execute(Enums.BracketLineForms.Brace));
        }

        private void MiSetBezier_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetBracketFormCommand.Execute(Enums.BracketLineForms.Bezier));
        }

        private void CMDicBracketWidget_Opened(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            MiTextAtClose.IsChecked = MiTextAtOpen.IsChecked = false;
            MiSetBracket.IsChecked = MiSetSquare.IsChecked = MiSetBrace.IsChecked =
                MiSetHorizontalPointer.IsChecked = MiSetVerticalPointer.IsChecked =
                MiSetBezier.IsChecked = false;

            //int bCount = 0;

            foreach (UIElement ue in pe.Children)
            {
                SHomeWorkshop.LunarConcept.Widgets.BracketWidget bw =
                    ue as SHomeWorkshop.LunarConcept.Widgets.BracketWidget;
                if (bw != null)
                {
                    //bCount++;
                    if (bw.IsMainSelected)
                    {
                        MiTextAtClose.IsChecked = (bw.IsTextAtClose) ? true : false;
                        MiTextAtOpen.IsChecked = (bw.IsTextAtClose == false) ? true : false;

                        switch (bw.LineForm)
                        {
                            case Enums.BracketLineForms.Brace:
                                {
                                    MiSetBrace.IsChecked = true; break;
                                }
                            case Enums.BracketLineForms.Bracket:
                                {
                                    MiSetBracket.IsChecked = true; break;
                                }
                            case Enums.BracketLineForms.HorizontalPointer:
                                {
                                    MiSetHorizontalPointer.IsChecked = true; break;
                                }
                            case Enums.BracketLineForms.Square:
                                {
                                    MiSetSquare.IsChecked = true; break;
                                }
                            case Enums.BracketLineForms.VerticalPointer:
                                {
                                    MiSetVerticalPointer.IsChecked = true; break;
                                }
                            case Enums.BracketLineForms.Bezier:
                                {
                                    MiSetBezier.IsChecked = true; break;
                                }
                        }

                        break;//理论上MainSelectedWidget只应该有一个。
                    }
                }
            }
        }

        private void MiTextAtClose_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTextAtLeftOrTopCommand.Execute());
        }

        private void MiTextAtOpen_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetTextAtRightOrBottomCommand.Execute());
        }

        private void SdRadius_DragCompleted(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRadiusCommand.Execute(SdRadius.Value));
        }

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
