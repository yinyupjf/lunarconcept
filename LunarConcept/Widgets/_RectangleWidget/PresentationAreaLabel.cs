﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept.Widgets._RectangleWidget
{
    public class PresentationAreaLabel : ListBoxItem
    {
        public PresentationAreaLabel(RectangleWidget presentationArea)
        {
            this.ToolTip = "双击鼠标左键跳转";
            presentationAreaWidget = presentationArea;

            if (presentationArea != null)
            {
                if (string.IsNullOrEmpty(presentationArea.MarkText))
                {
                    this.Content = "未命名演示区域";
                }
                else
                {
                    this.Content = presentationArea.MarkText;
                }
            }
        }

        private Widgets.RectangleWidget presentationAreaWidget;

        public RectangleWidget PresentationAreaWidget
        {
            get
            {
                return presentationAreaWidget;
            }
        }
    }
}
