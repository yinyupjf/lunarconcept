﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicRectangleWidget
    {
        private void CMRectangleWidget_Opend(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            MiFixTextWidthOff.IsChecked = MiFixTextWidthOn.IsChecked = false;
            MiCollapseMarkText.IsChecked = MiExpandMarkText.IsChecked = false;

            //int rCount = 0;

            Widget mainSelWidget = pe.GetMainSelectedWidget();
            if (mainSelWidget != null)
            {
                RectangleWidget rw = mainSelWidget as RectangleWidget;
                if (rw != null)
                {
                    MiFixTextWidthOn.IsChecked = rw.FixTextWidth;
                    MiFixTextWidthOff.IsChecked = !rw.FixTextWidth;

                    MiCollapseMarkText.IsChecked = rw.IsMarkTextCollapsed;
                    MiExpandMarkText.IsChecked = !rw.IsMarkTextCollapsed;

                    MiIsPresentationArea.IsChecked = rw.IsPresentationArea;
                }
            }
        }

        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.RectangleWidget).Name));
        }

        private void MiSetBackgroundImage_Clicked(object sender, RoutedEventArgs e)
        {
            var mainSelWidget = Globals.MainWindow.EditorManager.GetMainSelectedWidget();
            if (mainSelWidget != null)
            {
                mainSelWidget.LoadImageFromFile();
            }
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void MiFixTextWidthOn_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetFixTextWidthOnCommand.Execute());
        }

        private void MiSetMarkText_Click(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            string oldMarkText = string.Empty;

            Widget mainSel = pe.GetMainSelectedWidget();
            Widgets.RectangleWidget rw = mainSel as Widgets.RectangleWidget;
            if (rw == null) return;

            oldMarkText = rw.MarkText;

            Dialogs.TextInputBox tiBox = new Dialogs.TextInputBox(Globals.MainWindow, 
                "　　请在下框中输入矩形的新标记文本。\r\n\r\n　　★注意：如果此矩形被定义为【演示区域】，则这里标记文本将决定演示的顺序。",
                Globals.AppName, oldMarkText);

            if (tiBox.ShowDialog() == true)
            {
                string result = Commands.TextCommands.RunTextCommand.Run("mk_" + tiBox.TbxInput.Text);
                if (result != string.Empty)
                {
                    MessageBox.Show("　　设置矩形标记文本出错。异常信息如下：\r\n" + result, Globals.AppName,
                         MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void MiExpandMarkText_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetMarkTextExpandCommand.Execute());
        }

        private void MiCollapseMarkText_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetMarkTextCollapseCommand.Execute());
        }

        private void MiFixTextWidthOff_Click(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetFixTextWidthOffCommand.Execute());
        }

        private void MiIsPresentationArea_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as System.Windows.Controls.MenuItem;
            if (menuItem == null) return;

            menuItem.IsChecked = !menuItem.IsChecked;

            LunarMessage.Warning(Commands.SetIsPresentationAreaCommand.Execute(menuItem.IsChecked));
        }

        private void SdRadius_DragCompleted(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRadiusCommand.Execute(SdRadius.Value));
        }

        private void MiPasteIconXamlText_Clicked(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = Commands.SetRectangleIconXamlTextCommand.Execute(Clipboard.GetText());
                if (result != string.Empty)
                {
                    var btn = MessageBox.Show("　　粘贴Xaml失败，原因是：" + result + "\r\n　　要粘贴一个示例吗？",
                        Globals.AppName, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (btn == MessageBoxResult.Yes)
                    {
                        Commands.SetRectangleIconXamlTextCommand.Execute(Properties.Resources.ExampleXaml);
                    }
                    return;
                }

                // 撤销时太麻烦
                //result = Commands.SetWidgetLineWidthCommand.Execute(0);
                //if (result != string.Empty)
                //{
                //    LunarMessage.Warning("　　未能将矩形边框宽度设置为0，原因是：" + result);
                //    return;
                //}

                //result = Commands.SetWidgetBackColorCommand.Execute(System.Windows.Media.Brushes.Transparent);
                //if (result != string.Empty)
                //{
                //    LunarMessage.Warning("　　未能将矩形背景色设置为透明色，原因是：" + result);
                //    return;
                //}
            }
            catch (Exception ex)
            {
                LunarMessage.Warning(ex.Message);
                return;
            }
        }

        private void MiPasteIconXamlTextHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Information("　　此功能配合《Syncfusion Metro Studio 2》使用是极方便的，可直接复制Xaml文本粘贴到矩形中。\r\n" +
                "　　这样可令矩形支持极丰富的图标！\r\n　　另，粘贴成功后，会自动将矩形边框宽度设置为零，将背景色设置为透明。");
        }

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        private void ImageTopLeft_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.TopLeft));
        }

        private void ImageTop_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.Top));
        }

        private void ImageTopRight_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.TopRight));
        }

        private void ImageLeft_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.Left));
        }

        private void ImageCenter_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.Center));
        }

        private void ImageRight_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.Right));
        }

        private void ImageBottomLeft_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.BottomLeft));
        }

        private void ImageBottom_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.Bottom));
        }

        private void ImageBottomRight_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRectangleTextAreaAlignmentCommand.Execute(Enums.TextAreaAlignment.BottomRight));
        }

        #endregion
    }
}
