﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;
using SHomeWorkshop.LunarConcept.Adorners;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Enums;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;
using System.Collections.Generic;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    /// <summary>
    /// 创建时间：2012年1月20日前
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供一个带文本的圆形（椭圆）。
    /// </summary>
    public class EllipseWidget : ShapeWidget, ICanSameSize, ICanBeLinkedWidget, ITextRotate
    {

        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态构造方法]
        /// </summary>
        static EllipseWidget()
        {
            dashArray = new DoubleCollection() { 2, 2 };
            contextMenu = (ContextMenu)Globals.MainWindow.MainGrid.FindResource("CMDicEllipseWidget");
        }

        /// <summary>
        /// 构造方法。
        /// </summary>
        /// <param name="pageEditor">所属的PageEditor。此参数将成为Widget.MasterEditor的值。</param>
        public EllipseWidget(PageEditor pageEditor)
            : base(pageEditor)
        {
            this.ContextMenu = contextMenu;

            widgetType = Enums.WidgetTypes.Ellipse;
            widgetClassLocalName = Widget.GetWidgetClassLocalName(this.GetType().Name);

            mainEllipse.Fill = null;
            mainEllipse.Stroke = WidgetForeColor;
            mainEllipse.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(mainEllipse_PreviewMouseLeftButtonDown);
            mainEllipse.PreviewMouseRightButtonUp += new MouseButtonEventHandler(mainEllipse_PreviewMouseRightButtonUp);

            this.mainCanvas.Children.Add(mainEllipse);

            Canvas.SetZIndex(mainEllipse, 0);

            startCtrl = new LineCtrlAdorner(this.mainEllipse, this, Brushes.Red) { Visibility = Visibility.Hidden };
            endCtrl = new LineCtrlAdorner(this.mainEllipse, this, Brushes.Blue) { Visibility = Visibility.Hidden };
            //中点装饰器。
            this.centerAdorner = new WidgetCenterAdorner(this.mainEllipse, this, Brushes.Black) { ToolTip = "按鼠标左键向外拖动，生成关系节点" };
            this.centerAdorner.PreviewMouseRightButtonUp += new MouseButtonEventHandler(centerAdorner_PreviewMouseRightButtonUp);

            this.commentAdorner = new CommentAdorner(this.mainEllipse, this) { Visibility = System.Windows.Visibility.Collapsed };//默认不显示。
            this.commentAdorner.MouseLeftButtonUp += new MouseButtonEventHandler(commentAdorner_MouseLeftButtonUp);

            this.hyperLinkAdorner = new HyperLinkAdorner(this.mainEllipse, this) { Visibility = Visibility.Collapsed };
            this.hyperLinkAdorner.MouseLeftButtonUp += new MouseButtonEventHandler(hyperLinkAdorner_MouseLeftButtonUp);

            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this.mainCanvas);
            if (adornerLayer == null)
            {
                MessageBox.Show("　　未找到Widget的装饰层！", Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                adornerLayer.Add(this.startCtrl);
                adornerLayer.Add(this.endCtrl);
                adornerLayer.Add(this.centerAdorner);
                adornerLayer.Add(this.commentAdorner);//这个要在各具体部件类中添加。
                adornerLayer.Add(this.hyperLinkAdorner);//这个要在各具体部件类中添加。
            }

            startCtrl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(startCtrl_MouseLeftButtonDown);
            endCtrl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(endCtrl_MouseLeftButtonDown);

            startCtrl.ToolTip = endCtrl.ToolTip = "双击鼠标左键设置备注文本";

            centerAdorner.MouseLeftButtonDown += new MouseButtonEventHandler(centerAdorner_MouseLeftButtonDown);

            //最后添加文本面板。
            this.mainCanvas.Children.Add(this.mainTextPanel);
            Canvas.SetZIndex(this.mainTextPanel, 2);

            this.ControlHandlerDoubleClicked += EllipseWidget_ControlHandlerDoubleClicked;
        }

        private void EllipseWidget_ControlHandlerDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        #endregion


        #region 字段与属性===================================================================================================

        /// <summary>
        /// [只读]返回右下角坐标。
        /// </summary>
        public override Point BottomRight
        {
            get
            {
                double minLeft, minTop, maxRight, maxBottom;

                minLeft = Math.Min(startPoint.X, endPoint.X);
                maxRight = Math.Max(startPoint.X, endPoint.X);

                minTop = Math.Min(startPoint.Y, endPoint.Y);
                maxBottom = Math.Max(startPoint.Y, endPoint.Y);

                return new Point(maxRight, maxBottom);
            }
        }

        protected WidgetCenterAdorner centerAdorner;
        /// <summary>
        /// [只读]用于“拖出自动连接线”的装饰器。一般应在中心位置。
        /// </summary>
        public WidgetCenterAdorner CenterAdorner
        {
            get { return centerAdorner; }
        }

        /// <summary>
        /// 本类通用上下文菜单。
        /// </summary>
        private static ContextMenu contextMenu;

        /// <summary>
        /// 点划线定义。
        /// </summary>
        private static DoubleCollection dashArray;

        private ControlDraggingType draggingType = ControlDraggingType.None;


        private LineCtrlAdorner endCtrl;
        /// <summary>
        /// [只读]尾控制器。
        /// </summary>
        public LineCtrlAdorner EndCtrl
        {
            get { return endCtrl; }
        }

        private Point EndBasePoint
        {
            get
            {
                return new Point(Math.Max(startPoint.X, endPoint.X), Math.Max(startPoint.Y, endPoint.Y));
            }
        }

        private Point endPoint = new Point();
        /// <summary>
        /// [读写]起点。
        /// </summary>
        [Tools.LunarProperty("EndPoint", PropertyDateType.Point)]
        public Point EndPoint
        {
            get { return endPoint; }
            set
            {
                endPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.EndPointTag, value.ToString());
                }

                RefreshLocation();
            }
        }

        /// <summary>
        /// [只读]表示当前部件是否被某个连接线挂接着。
        /// </summary>
        public bool IsLinked
        {
            get
            {
                if (this.masterEditor == null) return false;

                foreach (UIElement ue in this.masterEditor.Children)
                {
                    ILinkableLine linkedLine = ue as ILinkableLine;
                    if (linkedLine == null) continue;

                    if (linkedLine.StartMasterId == this.id || linkedLine.EndMasterId == this.id) return true;
                }

                return false;
            }
        }

        public override bool IsSelected
        {
            get { return base.IsSelected; }
            set
            {
                base.IsSelected = value;

                //显示控制点。
                if (isSelected)
                {
                    startCtrl.Visibility =
                        endCtrl.Visibility = Visibility.Visible;
                }
                else
                {
                    startCtrl.Visibility =
                        endCtrl.Visibility = Visibility.Hidden;
                }
            }
        }

        private Ellipse mainEllipse = new Ellipse() { Cursor = Cursors.Hand };
        /// <summary>
        /// [只读]用以呈现的椭圆。
        /// </summary>
        public Ellipse MainEllipse
        {
            get { return mainEllipse; }
        }

        private Rect movingRect;
        /// <summary>
        /// 部件正在被拖动时的外边框。
        /// </summary>
        public Rect MovingRect
        {
            get
            {
                return movingRect;
            }
        }

        private Point StartBasePoint
        {
            get
            {
                return new Point(Math.Min(startPoint.X, endPoint.X), Math.Min(startPoint.Y, endPoint.Y));
            }
        }

        private LineCtrlAdorner startCtrl;
        /// <summary>
        /// [只读]首控制器。
        /// </summary>
        public LineCtrlAdorner StartCtrl
        {
            get { return startCtrl; }
        }

        private Point startPoint = new Point();
        /// <summary>
        /// [读写]起点。
        /// </summary>
        [Tools.LunarProperty("StartPoint", PropertyDateType.Point)]
        public Point StartPoint
        {
            get { return startPoint; }
            set
            {
                startPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.StartPointTag, value.ToString());
                }

                RefreshLocation();
            }
        }

        private double textRotateAngle = 0;
        /// <summary>
        /// [读写]文本旋转角度。取值范围：[-180,180]。
        /// </summary>
        [Tools.LunarProperty("TextRotateAngle", PropertyDateType.Double)]
        public double TextRotateAngle
        {
            get { return textRotateAngle; }
            set
            {
                if (value > 180)
                {
                    textRotateAngle = 180;
                }
                else if (value < -180)
                {
                    textRotateAngle = -180;
                }
                else
                {
                    textRotateAngle = value;
                }

                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.TextRotateAngleTag, textRotateAngle.ToString());
                }

                this.RefreshTextRotateAngle();
            }
        }

        /// <summary>
        /// [只读]返回圆路径对象的左上角坐标。
        /// </summary>
        public override Point TopLeft
        {
            get
            {
                double minLeft, minTop, maxRight, maxBottom;

                minLeft = Math.Min(startPoint.X, endPoint.X);
                maxRight = Math.Max(startPoint.X, endPoint.X);

                minTop = Math.Min(startPoint.Y, endPoint.Y);
                maxBottom = Math.Max(startPoint.Y, endPoint.Y);

                return new Point(minLeft, minTop);
            }
        }

        #endregion


        #region 方法=========================================================================================================

        public override void Build()
        {
            base.Build();

            if (this.xmlData == null) return;

            XmlAttribute attrStartPoint = this.xmlData.GetAttribute(XmlTags.StartPointTag);
            if (attrStartPoint != null)
            {
                this.startPoint = Point.Parse(attrStartPoint.Value);
            }

            XmlAttribute attrEndPoint = this.xmlData.GetAttribute(XmlTags.EndPointTag);
            if (attrEndPoint != null)
            {
                this.endPoint = Point.Parse(attrEndPoint.Value);
            }

            XmlAttribute attrTextRotateAngle = this.xmlData.GetAttribute(XmlTags.TextRotateAngleTag);
            if (attrTextRotateAngle != null)
            {
                this.textRotateAngle = double.Parse(attrTextRotateAngle.Value);
            }

            this.RefreshTextRotateAngle();
            this.RefreshLocation();

            //此类是下面这几个属性的“最终实现类”。这些属性的值都已在基类确定。因此调用,
            this.RefreshWidgetBackColor();
            this.RefreshWidgetLineColor();
            this.RefreshWidgetLineWidth();
            this.RefreshLineDash();
        }

        protected override void BuildStyleProperties()
        {
            base.BuildStyleProperties();

            //没有必须在此类读取的、与Style相关的Xml特性
        }

        public override void BuildWidgetStylePropertiesAndRefresh()
        {
            base.BuildWidgetStylePropertiesAndRefresh();

            //此类是下面这几个属性的“最终实现类”。这些属性的值都已在基类确定。因此调用,
            this.RefreshWidgetBackColor();
            this.RefreshWidgetLineColor();
            this.RefreshWidgetLineWidth();
            this.RefreshLineDash();


            //无意义
            //this.RefreshArrows();
            //this.RefreshWidgetPadding();

            //这几个在Widget类中已经调用。
            //this.RefreshWidgetForeColor();
            //this.RefreshIsShadowVisible();
            //this.RefreshWidgetOpacity();
        }

        /// <summary>
        /// 关联事件。右击中心装饰器时，活动此部件。
        /// </summary>
        void centerAdorner_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.mainEllipse_PreviewMouseRightButtonUp(sender, e);
        }

        public void DrawLine()
        {
            if (masterEditor == null) return;

            DrawLine(this.startPoint, this.endPoint, false);
        }

        /// <param name="startPoint">首端点。</param>
        /// <param name="endPoint">尾端点。</param>
        /// <param name="isShift">是否按住Shift键拖动。</param>
        private void DrawLine(Point startPoint, Point endPoint, bool isShift)
        {
            Point startBasePt, endBasePt;

            startBasePt = new Point(Math.Min(startPoint.X, endPoint.X),
                   Math.Min(startPoint.Y, endPoint.Y));
            endBasePt = new Point(Math.Max(startPoint.X, endPoint.X),
              Math.Max(startPoint.Y, endPoint.Y));

            if (isShift)
            {
                if (startPoint.Y > endPoint.Y)
                {
                    endBasePt.Y = startBasePt.Y + (endBasePt.X - startBasePt.X);
                }
                else
                {
                    startBasePt.Y = endBasePt.Y - (endBasePt.X - startBasePt.X);
                }
            }

            Canvas.SetLeft(mainEllipse, startBasePt.X);
            Canvas.SetTop(mainEllipse, startBasePt.Y);

            mainEllipse.Width = (endBasePt.X - startBasePt.X);
            mainEllipse.Height = (endBasePt.Y - startBasePt.Y);

            //移动部件控制点时，要刷新连接线的。
            this.movingRect = new Rect(startBasePt, endBasePt);

            //注意：与其它线型部件不同。Ellipse和Rectangle的装饰位置坐标有区别。
            startCtrl.CenterPoint = new Point(startPoint.X - startBasePt.X, startPoint.Y - startBasePt.Y);
            endCtrl.CenterPoint = new Point(endPoint.X - startBasePt.X, endPoint.Y - startBasePt.Y);//这里也用startBasePt

            LocateTextPanel(startBasePt, endBasePt);
        }

        /// <summary>
        /// 重定文本位置。
        /// </summary>
        /// <param name="startBasePt">首基准点。</param>
        /// <param name="endBasePt">尾基准点。</param>
        private void LocateTextPanel(Point startBasePt, Point endBasePt)
        {


            Point center = new Point(startBasePt.X + (endBasePt.X - startBasePt.X) / 2,
                startBasePt.Y + (endBasePt.Y - startBasePt.Y) / 2);

            Point textStart = new Point(center.X - this.mainTextPanel.ActualWidth / 2 - this.mainTextPanel.Margin.Left,
                center.Y - this.mainTextPanel.ActualHeight / 2 - this.mainTextPanel.Margin.Top);

            Canvas.SetLeft(this.mainTextPanel, textStart.X);
            Canvas.SetTop(this.mainTextPanel, textStart.Y);
        }

        /// <summary>
        /// 拖动后，放置部件位置。
        /// ——此方法会造成事实上的位置改变。
        /// </summary>
        /// <param name="mi">记录修改操作信息的条目。用于“撤销”与“重做”。</param>
        /// <param name="mousePoint">新位置。</param>
        public override void DropWidget(ModifingItem<Action, ModifingInfo> mi, Point mousePoint)
        {
            if (mi == null) return;
            if (masterEditor == null) return;

            Point globalLocation = masterEditor.MouseInfo.LeftButtonPreviewPoint;

            Point newStartPoint = new Point(
                startPoint.X - globalLocation.X + mousePoint.X, startPoint.Y - globalLocation.Y + mousePoint.Y);

            Point newEndPoint = new Point(
                endPoint.X - globalLocation.X + mousePoint.X, endPoint.Y - globalLocation.Y + mousePoint.Y);

            #region 自动中心吸附
            bool autoAlignment = Globals.MainWindow.RtbtnAutoAlignment.IsChecked == true;

            if (autoAlignment)
            {
                double hOffset = double.MaxValue, vOffset = double.MaxValue;
                if (autoAlignment)
                {
                    //尝试自动对齐

                    //先尝试中心坐标对齐
                    //需要取出最接近的位置
                    Point center = new Point(newStartPoint.X + this.OuterRect.Width / 2, newStartPoint.Y + this.OuterRect.Height / 2);

                    foreach (UIElement ue in this.MasterEditor.MainCanvas.Children)
                    {
                        Widget widget = ue as ContentWidget;
                        if (widget == null)
                        {
                            widget = ue as ShapeWidget;
                            if (widget == null) continue;
                        }

                        if (widget == this) continue;
                        if (widget.MasterEditor.GetSelectedWidgetsList().Contains(widget)) continue;

                        Point widgetCenter = new Point(widget.OuterRect.Left + widget.OuterRect.Width / 2, widget.OuterRect.Top + widget.OuterRect.Height / 2);
                        double h = double.MaxValue, v = double.MaxValue, tmph = double.MaxValue, tmpv = double.MaxValue;

                        tmph = widgetCenter.X - center.X;
                        tmpv = widgetCenter.Y - center.Y;

                        if (Math.Abs(tmph) < Math.Abs(h)) h = tmph;
                        if (Math.Abs(tmpv) < Math.Abs(v)) v = tmpv;

                        if (Math.Abs(h) < Math.Abs(hOffset)) hOffset = h;
                        if (Math.Abs(v) < Math.Abs(vOffset)) vOffset = v;
                    }
                }

                //自动吸附对齐会造成很迷惑人的效果。取消之。2014年7月26日
                //if (hOffset < double.MaxValue && Math.Abs(hOffset) < 10)
                //{
                //    newStartPoint.X += hOffset;
                //    newEndPoint.X += hOffset;
                //}

                //if (vOffset < double.MaxValue && Math.Abs(vOffset) < 10)
                //{
                //    newStartPoint.Y += vOffset;
                //    newEndPoint.Y += vOffset;
                //}
            }
            #endregion

            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name,
                XmlTags.StartPointTag, startPoint.ToString(),
                newStartPoint.ToString());
            StartPoint = newStartPoint;

            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name,
                XmlTags.EndPointTag, endPoint.ToString(),
                newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.ModifingInfo.NewMainSelectedWidgetID = this.id;
            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 完成拖放控制点。
        /// </summary>
        public override void DropWidgetControler(Point newOutSidePoint, ModifingItem<Action, ModifingInfo> mi)
        {
            if (mi == null || mi.ModifingInfo == null) return;
            if (masterEditor == null || masterEditor.MasterManager == null) return;

            ModifingInfo info = mi.ModifingInfo;

            bool isShift = false;

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            if ((ksLeftShift & KeyStates.Down) > 0 || (ksRightShift & KeyStates.Down) > 0)
            {
                isShift = true;
            }

            switch (draggingType)
            {
                case ControlDraggingType.Start:
                    {
                        info.ModifingDescription = "拖动圆首端点";

                        if (isShift)
                        {
                            if (newOutSidePoint.Y > endPoint.Y)
                            {
                                newOutSidePoint.Y = endPoint.Y + Math.Abs(newOutSidePoint.X - endPoint.X);
                            }
                            else
                            {
                                newOutSidePoint.Y = endPoint.Y - Math.Abs(newOutSidePoint.X - endPoint.X);
                            }
                        }

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.StartPointTag, startPoint.ToString(), newOutSidePoint.ToString());
                        StartPoint = newOutSidePoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
                case ControlDraggingType.End:
                    {
                        info.ModifingDescription = "拖动圆尾端点";

                        if (isShift)
                        {
                            if (newOutSidePoint.Y > startPoint.Y)
                            {
                                newOutSidePoint.Y = startPoint.Y + Math.Abs(newOutSidePoint.X - startPoint.X);
                            }
                            else
                            {
                                newOutSidePoint.Y = startPoint.Y - Math.Abs(newOutSidePoint.X - startPoint.X);
                            }
                        }

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.EndPointTag, endPoint.ToString(), newOutSidePoint.ToString());
                        EndPoint = newOutSidePoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
            }

            //刷新连接线首尾端点的位置。
            List<ILinkableLine> linkedLines = this.GetLinkedLines();
            this.masterEditor.RefreshLinkedLines(mi, linkedLines);
        }

        /// <summary>
        /// 取与自身挂接的连接线。
        /// </summary>
        /// <returns>可能返回null。</returns>
        public List<ILinkableLine> GetLinkedLines()
        {
            if (this.masterEditor == null) return null;

            List<Widget> widgets = new List<Widget>();
            widgets.Add(this);

            return this.masterEditor.GetLinkedLines(widgets);
        }

        public override string GetRelativeOuterXml(Point baseCopyTopLeft)
        {
            if (this.xmlData == null) return string.Empty;

            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X - baseCopyTopLeft.X,
                oldStartPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.StartPointTag, newStartPoint.ToString());

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X - baseCopyTopLeft.X,
                oldEndPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.EndPointTag, newEndPoint.ToString());

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(this.xmlData.OuterXml);

            this.xmlData.SetAttribute(XmlTags.StartPointTag, oldStartPoint.ToString());
            this.xmlData.SetAttribute(XmlTags.EndPointTag, oldEndPoint.ToString());

            return sb.ToString();
        }

        /// <summary>
        /// 这个虚方法是用以查看本部件是否在选定框的内部。
        /// 线型部件，各有各的计算办法。
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public override bool IsInRect(Rect rect)
        {
            //return base.IsInRect(rect);//这个要屏蔽。
            bool isInRect = base.IsInRect(rect);
            if (isInRect)
            {
                return true;
            }
            else
            {
                Rect virtualRect = new Rect(startPoint, endPoint);
                return rect.IntersectsWith(virtualRect);
            }
        }

        /// <summary>
        /// 准备拖出“关系（由两条直线、两个新文本框组成，或由一个文本框、一条直线组成）。
        /// </summary>
        void centerAdorner_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (masterEditor == null) return;
            if (e.ClickCount == 2)
            {
                //防止误拖动部件。
                if (masterEditor != null && masterEditor.MasterManager != null)
                {
                    masterEditor.MouseInfo.DraggingType = masterEditor.MasterManager.MouseDraggingType;
                }

                this.SelectOnlySelf();

                if (Globals.MainWindow.IsPresentatingByPath == false)
                {
                    this.Edit();
                }
                return;
            }

            e.Handled = true;
            if (Globals.MainWindow != null && Globals.MainWindow.EditorManager != null)
            {
                switch (Globals.MainWindow.EditorManager.MouseDraggingType)
                {
                    case PageDraggingType.InsertBezierLine:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertBezierLineRelation;
                            break;
                        }
                    case PageDraggingType.InsertPolyLine:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertPolyLineRelation;
                            break;
                        }
                    default:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertStraitLineRelation;
                            break;
                        }
                }
            }
            else
            {
                masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertStraitLineRelation;
            }

            //Point pt = e.GetPosition(this.masterEditor);//这个不好，会偏
            //pt = new Point(pt.X - this.masterEditor.BorderThickness.Left,
            //    pt.Y - this.masterEditor.BorderThickness.Top);

            Point pt = this.Center;//直接用中心点更好。
            masterEditor.MouseInfo.LeftButtonPreviewPoint = pt;
            masterEditor.MouseInfo.MainSelectedWidget = this;
        }

        /// <summary>
        /// 尺寸向上收缩指定单位（默认为1）。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向上收缩多少个“与设备无关单位”。</param>
        public override void CollapseToTop(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.Y > endPoint.Y)
            {
                Point newStartPoint = new Point(startPoint.X, startPoint.Y - units);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.Y < endPoint.Y)
            {
                Point newEndPoint = new Point(endPoint.X, endPoint.Y - units);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果水平对齐，则不更改。
        }

        /// <summary>
        /// 尺寸向下扩展指定单位（默认为1）。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向下扩展多少个“与设备无关单位”。</param>
        public override void ExpandToBottom(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.Y > endPoint.Y)
            {
                Point newStartPoint = new Point(startPoint.X, startPoint.Y + units);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.Y < endPoint.Y)
            {
                Point newEndPoint = new Point(endPoint.X, endPoint.Y + units);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果水平对齐，则不更改。
        }

        /// <summary>
        /// 尺寸向左收缩指定单位（默认为1）。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向左收缩多少个“与设备无关单位”。</param>
        public override void CollapseToLeft(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.X > endPoint.X)
            {
                Point newStartPoint = new Point(startPoint.X - units, startPoint.Y);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.X < endPoint.X)
            {
                Point newEndPoint = new Point(endPoint.X - units, endPoint.Y);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果垂直对齐，则不更改
        }

        /// <summary>
        /// 尺寸向右扩展指定单位（默认为1）。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向右扩展多少个“与设备无关单位”。</param>
        public override void ExpandToRight(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.X > endPoint.X)
            {
                Point newStartPoint = new Point(startPoint.X + units, startPoint.Y);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.X < endPoint.X)
            {
                Point newEndPoint = new Point(endPoint.X + units, endPoint.Y);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果垂直对齐，则不更改
        }

        /// <summary>
        /// 向上移动部件位置。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向上移动多少个“与设备无关单位”。</param>
        public override void MoveUp(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - units);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - units);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 改变中心点横坐标。
        /// </summary>
        public override void MoveHorizontalCenterTo(ModifingItem<Action, ModifingInfo> mi, double center)
        {
            if (mi == null || double.IsNaN(center) || masterEditor == null) return;

            double offset = 0;
            double curCenter = TopLeft.X + (BottomRight.X - TopLeft.X) / 2;
            offset = curCenter - center;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 改变中心点纵坐标。
        /// </summary>
        public override void MoveVerticalCenterTo(ModifingItem<Action, ModifingInfo> mi, double center)
        {
            if (mi == null || double.IsNaN(center) || masterEditor == null) return;

            double offset = 0;
            double curCenter = TopLeft.Y + (BottomRight.Y - TopLeft.Y) / 2;
            offset = curCenter - center;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveDown不同，这里是指定向下移动到哪个位置（下边缘到哪个位置）。
        /// </summary>
        public override void MoveBottomSiderTo(ModifingItem<Action, ModifingInfo> mi, double bottom)
        {
            if (mi == null || double.IsNaN(bottom) || masterEditor == null) return;

            double offset = 0;
            double maxBottom = BottomRight.Y;
            offset = maxBottom - bottom;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveLeft不同，这里是指定向左移动到哪个位置（左边缘到哪个位置）。
        /// </summary>
        public override void MoveLeftSiderTo(ModifingItem<Action, ModifingInfo> mi, double left)
        {
            if (mi == null || double.IsNaN(left) || masterEditor == null) return;

            double offset = 0;
            double minLeft = TopLeft.X;
            offset = minLeft - left;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveRight不同，这里是指定向右移动到哪个位置（右边缘到哪个位置）。
        /// </summary>
        public override void MoveRightSiderTo(ModifingItem<Action, ModifingInfo> mi, double right)
        {
            if (mi == null || double.IsNaN(right) || masterEditor == null) return;

            double offset = 0;
            double maxRight = BottomRight.X;
            offset = maxRight - right;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveUp不同，这里是指定向上移动到哪个位置（上边缘到哪个位置）。
        /// </summary>
        public override void MoveTopSiderTo(ModifingItem<Action, ModifingInfo> mi, double top)
        {
            if (mi == null || double.IsNaN(top) || masterEditor == null) return;

            double offset = 0;
            double minTop = TopLeft.Y;
            offset = minTop - top;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        void mainEllipse_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //双击编辑文本
            if (e.ClickCount == 2)
            {
                this.SelectOnlySelf();
                if (Globals.MainWindow.IsPresentatingByPath == false)
                {
                    this.Edit();
                }
                return;
            }

            ModifingItem<Action, ModifingInfo> mi = null;
            ModifingInfo info = null;
            if (FormatSelf(ref mi, ref info))
            {
                if (masterEditor != null)
                {
                    masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveWidgets;
                }
                return;//格式刷格式化，不更改选定状态。
            }

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            bool isShift = false;

            if ((ksRightShift & KeyStates.Down) > 0 || (ksLeftShift & KeyStates.Down) > 0) { isShift = true; }

            bool isCtrl = false;
            KeyStates ksRightCtrl = Keyboard.GetKeyStates(Key.RightCtrl);
            KeyStates ksLeftCtrl = Keyboard.GetKeyStates(Key.LeftCtrl);

            if ((ksRightCtrl & KeyStates.Down) > 0 || (ksLeftCtrl & KeyStates.Down) > 0) isCtrl = true;

            if (isShift && isCtrl == false)
            {
                //基本规则：
                //    ①如果未选定，选定，并作为活动部件。
                //    ②如果已选定，看是否是活动部件，如果已经是活动部件，整个取消选定状态；
                //                                      如果不是活动部件，设为活动部件。
                if (this.IsSelected == false)
                {
                    this.IsMainSelected = true;
                }
                else
                {
                    if (this.IsMainSelected == false)
                    {
                        this.IsMainSelected = true;
                    }
                    else
                    {
                        this.IsSelected = false;
                    }
                }
            }
            else
            {
                if (IsSelected == false)
                {
                    this.SelectOnlySelf();
                }
                //如果是选定的部件，那么可能是在拖动。
            }

            //准备拖动。
            if (masterEditor != null)
            {
                masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveWidgets;
                Point pt = e.GetPosition(this.masterEditor);
                pt = new Point(pt.X - this.masterEditor.BorderThickness.Left, pt.Y - this.masterEditor.BorderThickness.Top);
                masterEditor.MouseInfo.LeftButtonPreviewPoint = pt;
            }
        }

        void mainEllipse_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (this.IsSelected == false)
            {
                //如果未选定，就作为活动，
                this.SelectOnlySelf();
            }
            else
            {
                //若已选定，不更改选定范围，只更改“活动部件”为此部件
                if (this.IsMainSelected == false) this.IsMainSelected = true;
            }

            if (this.ContextMenu != null)
            {
                this.ContextMenu.IsOpen = true;
            }
        }

        /// <summary>
        /// 向下移动部件位置。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向下移动多少个“与设备无关单位”。</param>
        public override void MoveDown(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y + units);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y + units);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 向左移动部件位置。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向左移动多少个“与设备无关单位”。</param>
        public override void MoveLeft(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X - units, startPoint.Y);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - units, endPoint.Y);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 向右移动部件位置。
        /// </summary>
        /// <param name="mi">用于“撤销”与“重做”的修改操作记录条目。</param>
        /// <param name="units">向右移动多少个“与设备无关单位”。</param>
        public override void MoveRight(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X + units, startPoint.Y);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X + units, endPoint.Y);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 在拖动控制点时，同步显示拖放效果。
        /// ——此方法不会造成事实上的改动。
        /// </summary>
        /// <param name="newOutSidePoint">控制点的新坐标（通常就是鼠标指针的位置）。</param>
        public override void MoveWhenDraggingControler(Point newOutSidePoint)
        {
            bool isShift = false;

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            if ((ksLeftShift & KeyStates.Down) > 0 || (ksRightShift & KeyStates.Down) > 0)
            {
                isShift = true;
            }

            switch (draggingType)
            {
                case ControlDraggingType.Start:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(newMovingPoint, this.endPoint, isShift);

                        break;
                    }
                case ControlDraggingType.End:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(this.startPoint, newMovingPoint, isShift);

                        break;
                    }
            }
        }

        /// <summary>
        /// 在拖动整个部件时时，同步显示拖放效果。
        /// ——此方法不会造成事实上的改动。
        /// </summary>
        /// <param name="newOutSidePoint">控制点的新坐标（通常就是鼠标指针的位置）。</param>
        public override void MoveWhenDraggingWidget(Point mousePoint)
        {
            if (masterEditor == null || masterEditor.MasterManager == null) return;

            Point newMovingPoint = mousePoint;
            Point globalLocation = masterEditor.MouseInfo.LeftButtonPreviewPoint;

            Point tmpStartPoint = new Point(startPoint.X - globalLocation.X + newMovingPoint.X,
                startPoint.Y - globalLocation.Y + newMovingPoint.Y);
            Point tmpEndPoint = new Point(endPoint.X - globalLocation.X + newMovingPoint.X,
                endPoint.Y - globalLocation.Y + newMovingPoint.Y);

            DrawLine(tmpStartPoint, tmpEndPoint, false);
        }

        /// <summary>
        /// 刷新所有控制点。
        /// </summary>
        public override void RefreshControlers()
        {
            if (isSelected)
            {
                //刷新控制点色彩
                if (masterEditor != null && masterEditor.MasterManager != null)
                {
                    if (startCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetStartControlerBrush)
                        startCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetStartControlerBrush;

                    if (endCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetEndControlerBrush)
                        endCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetEndControlerBrush;
                }

                Point topLeft = TopLeft;
                startCtrl.CenterPoint = new Point(startPoint.X - TopLeft.X, startPoint.Y - topLeft.Y);
                endCtrl.CenterPoint = new Point(endPoint.X - TopLeft.X, endPoint.Y - topLeft.Y);

                startCtrl.Visibility =
                    endCtrl.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                startCtrl.Visibility =
                    endCtrl.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        public override void RefreshLineDash()
        {
            switch (lineDash)
            {
                case LineDashType.DashType.Dash:
                    {
                        this.mainEllipse.StrokeDashArray = LineDashType.dashCollection; break;
                    }
                case LineDashType.DashType.DashDotDot:
                    {
                        this.mainEllipse.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                    }
                case LineDashType.DashType.Dot:
                    {
                        this.mainEllipse.StrokeDashArray = LineDashType.dotCollection; break;
                    }
                case LineDashType.DashType.Solid:
                    {
                        this.mainEllipse.StrokeDashArray = LineDashType.solidCollection; break;
                    }
                default:
                    {
                        this.mainEllipse.StrokeDashArray = LineDashType.dashDotCollection; break;
                    }
            }
        }

        public override void RefreshWidgetLineWidth()
        {
            mainEllipse.StrokeThickness = widgetLineWidth;
        }

        public override void RefreshLocation()
        {
            base.RefreshLocation();

            DrawLine();

            //Point startBasePt = StartBasePoint;
            //Point endBasePt = EndBasePoint;
            //Canvas.SetLeft(mainEllipse, startBasePt.X);
            //Canvas.SetTop(mainEllipse, startBasePt.Y);

            //mainEllipse.Width = (endBasePt.X - startBasePt.X);
            //mainEllipse.Height = (endBasePt.Y - startBasePt.Y);

            //this.movingRect = new Rect(startPoint, endPoint);

            RefreshWidgetLineWidth();
            RefreshControlers();
            this.RefreshTextRotateAngle();
        }

        public override void RefreshPointWhenGroupIn(Point baseTopLeft)
        {
            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X - baseTopLeft.X,
                oldStartPoint.Y - baseTopLeft.Y);
            StartPoint = newStartPoint;

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X - baseTopLeft.X,
                oldEndPoint.Y - baseTopLeft.Y);
            EndPoint = newEndPoint;
        }

        public override void RefreshPointWhenGroupOut(Point baseTopLeft)
        {
            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X + baseTopLeft.X,
                oldStartPoint.Y + baseTopLeft.Y);
            StartPoint = newStartPoint;

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X + baseTopLeft.X,
                oldEndPoint.Y + baseTopLeft.Y);
            EndPoint = newEndPoint;
        }

        public override void RefreshIsShadowVisible()
        {
            //base.RefreshIsShadowVisible();//保持文本清晰

            if (isShadowVisible)
            {
                this.mainEllipse.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.mainEllipse.Effect = null;
            }
        }

        public override void RefreshTextPanelLocatin()
        {
            LocateTextPanel(this.StartBasePoint, this.EndBasePoint);
        }

        /// <summary>
        /// 刷新文本区旋转角度。
        /// </summary>
        public void RefreshTextRotateAngle()
        {
            if (this.mainTextPanel.RenderTransformOrigin != DefaultRenderCenter)
            {
                this.mainTextPanel.RenderTransformOrigin = DefaultRenderCenter;
            }

            if (this.textRotateAngle == 0)
            {
                this.mainTextPanel.RenderTransform = DefaultRotateTransform;
            }
            else
            {
                this.mainTextPanel.RenderTransform = new RotateTransform(textRotateAngle);
            }
        }

        public override void RefreshWidgetBackColor()
        {
            base.RefreshWidgetBackColor();
            if (widgetBackColor == Brushes.Transparent)
            {
                this.mainEllipse.Fill = null;
            }
            else
            {
                this.mainEllipse.Fill = widgetBackColor;
            }
        }

        public override void RefreshWidgetLineColor()
        {
            this.mainEllipse.Stroke = widgetLineColor;
        }

        public override void UpdatePointsWhenPasting(Point basePasteTopLeft)
        {
            Point newStartPoint = new Point(startPoint.X + basePasteTopLeft.X,
               startPoint.Y + basePasteTopLeft.Y);

            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X + basePasteTopLeft.X,
                endPoint.Y + basePasteTopLeft.Y);

            EndPoint = newEndPoint;
        }

        public event EventHandler<MouseButtonEventArgs> ControlHandlerDoubleClicked;

        protected void OnControlHandlerDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (ControlHandlerDoubleClicked != null)
            {
                ControlHandlerDoubleClicked(sender, e);
            }
        }

        void endCtrl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            if (masterEditor == null) return;

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = StartPoint;
            masterEditor.MouseInfo.DraggingType = PageDraggingType.MoveLineWidgetControler;
            draggingType = ControlDraggingType.End;

            endCtrl.Visibility = System.Windows.Visibility.Hidden;
        }

        void startCtrl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            if (masterEditor == null) return;

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = StartPoint;
            masterEditor.MouseInfo.DraggingType = PageDraggingType.MoveLineWidgetControler;
            draggingType = ControlDraggingType.Start;

            startCtrl.Visibility = System.Windows.Visibility.Hidden;
        }
        #endregion


        #region 其它=========================================================================================================


        private enum ControlDraggingType { Start, CenterCP, End, None }

        #endregion
    }
}
