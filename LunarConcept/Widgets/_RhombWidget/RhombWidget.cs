﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;
using SHomeWorkshop.LunarConcept.Adorners;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Enums;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;
using System.Collections.Generic;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    /// <summary>
    /// 创建时间：2012年1月20日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供一个菱形部件。
    /// </summary>
    public class RhombWidget : ShapeWidget, ICanSameSize, ICanBeLinkedWidget, ITextRotate
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态构造方法]
        /// </summary>
        static RhombWidget()
        {
            dashArray = new DoubleCollection() { 2, 2 };
            contextMenu = (ContextMenu)Globals.MainWindow.MainGrid.FindResource("CMDicRhombWidget");
        }

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="pageEditor"></param>
        public RhombWidget(PageEditor editor)
            : base(editor)
        {
            this.ContextMenu = contextMenu;

            widgetType = Enums.WidgetTypes.Rhomb;
            widgetClassLocalName = Widget.GetWidgetClassLocalName(this.GetType().Name);

            mainPolyLine.Fill = null;
            mainPolyLine.Stroke = WidgetForeColor;
            mainPolyLine.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(mainPolygon_PreviewMouseLeftButtonDown);
            mainPolyLine.PreviewMouseRightButtonUp += new MouseButtonEventHandler(mainPolyLine_PreviewMouseRightButtonUp);

            this.mainCanvas.Children.Add(mainPolyLine);

            Canvas.SetZIndex(mainPolyLine, 0);

            startCtrl = new LineCtrlAdorner(this.mainPolyLine, this, Brushes.Red) { Visibility = Visibility.Hidden };
            endCtrl = new LineCtrlAdorner(this.mainPolyLine, this, Brushes.Blue) { Visibility = Visibility.Hidden };

            startCtrl.ToolTip = endCtrl.ToolTip = "双击鼠标左键设置备注文本";

            //中点装饰器。
            this.centerAdorner = new WidgetCenterAdorner(this.mainPolyLine, this, Brushes.Black) { ToolTip = "按鼠标左键向外拖动，生成关系节点" };
            this.centerAdorner.PreviewMouseRightButtonUp += new MouseButtonEventHandler(centerAdorner_PreviewMouseRightButtonUp);
            this.centerAdorner.MouseLeftButtonDown += new MouseButtonEventHandler(centerAdorner_MouseLeftButtonDown);

            this.commentAdorner = new CommentAdorner(this.mainPolyLine, this) { Visibility = System.Windows.Visibility.Collapsed };//默认不显示。
            this.commentAdorner.MouseLeftButtonUp += new MouseButtonEventHandler(commentAdorner_MouseLeftButtonUp);

            this.hyperLinkAdorner = new HyperLinkAdorner(this.mainPolyLine, this) { Visibility = Visibility.Collapsed };
            this.hyperLinkAdorner.MouseLeftButtonUp += new MouseButtonEventHandler(hyperLinkAdorner_MouseLeftButtonUp);

            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this.mainCanvas);
            if (adornerLayer == null)
            {
                MessageBox.Show("　　未找到Widget的装饰层！", Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                adornerLayer.Add(this.startCtrl);
                adornerLayer.Add(this.endCtrl);
                adornerLayer.Add(this.centerAdorner);
                adornerLayer.Add(this.commentAdorner);//这个要在各具体部件类中添加。
                adornerLayer.Add(this.hyperLinkAdorner);//这个要在各具体部件类中添加。
            }

            startCtrl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(startCtrl_MouseLeftButtonDown);
            endCtrl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(endCtrl_MouseLeftButtonDown);


            //最后添加文本面板。
            this.mainCanvas.Children.Add(this.mainTextPanel);
            Canvas.SetZIndex(this.mainTextPanel, 2);

            this.ControlHandlerDoubleClicked += RhombWidget_ControlHandlerDoubleClicked;
        }

        private void RhombWidget_ControlHandlerDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        #endregion


        #region 字段与属性===================================================================================================

        public override Point BottomRight
        {
            get
            {
                double minLeft, minTop, maxRight, maxBottom;

                minLeft = Math.Min(startPoint.X, endPoint.X);
                maxRight = Math.Max(startPoint.X, endPoint.X);

                minTop = Math.Min(startPoint.Y, endPoint.Y);
                maxBottom = Math.Max(startPoint.Y, endPoint.Y);

                return new Point(maxRight, maxBottom);
            }
        }

        protected WidgetCenterAdorner centerAdorner;
        /// <summary>
        /// [只读]用于“拖出自动连接线”的装饰器。一般应在中心位置。
        /// </summary>
        public WidgetCenterAdorner CenterAdorner
        {
            get { return centerAdorner; }
        }

        /// <summary>
        /// 本类通用上下文菜单。
        /// </summary>
        private static ContextMenu contextMenu;

        /// <summary>
        /// 点划线定义。
        /// </summary>
        private static DoubleCollection dashArray;

        /// <summary>
        /// 指明正在拖动的是哪个控制点。
        /// </summary>
        private ControlDraggingType draggingType = ControlDraggingType.None;

        private Point EndBasePoint
        {
            get
            {
                return new Point(Math.Max(startPoint.X, endPoint.X), Math.Max(startPoint.Y, endPoint.Y));
            }
        }

        private LineCtrlAdorner endCtrl;
        /// <summary>
        /// [只读]尾控制点（装饰器）。
        /// </summary>
        public LineCtrlAdorner EndCtrl
        {
            get { return endCtrl; }
        }

        private Point endPoint = new Point();
        /// <summary>
        /// [读写]终点。
        /// </summary>
        [Tools.LunarProperty("EndPoint", PropertyDateType.Point)]
        public Point EndPoint
        {
            get { return endPoint; }
            set
            {
                endPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.EndPointTag, value.ToString());
                }

                RefreshLocation();
            }
        }

        /// <summary>
        /// [只读]表示当前部件是否被某个连接线挂接着。
        /// </summary>
        public bool IsLinked
        {
            get
            {
                if (this.masterEditor == null) return false;

                foreach (UIElement ue in this.masterEditor.Children)
                {
                    ILinkableLine linkedLine = ue as ILinkableLine;
                    if (linkedLine == null) continue;

                    if (linkedLine.StartMasterId == this.id || linkedLine.EndMasterId == this.id) return true;
                }

                return false;
            }
        }

        /// <summary>
        /// [读写]是否处于“被选定”状态。
        /// </summary>
        public override bool IsSelected
        {
            get { return base.IsSelected; }
            set
            {
                base.IsSelected = value;

                //显示控制点。
                if (isSelected)
                {
                    startCtrl.Visibility =
                        endCtrl.Visibility = Visibility.Visible;
                }
                else
                {
                    startCtrl.Visibility =
                        endCtrl.Visibility = Visibility.Hidden;
                }
            }
        }

        private Polygon mainPolyLine = new Polygon()
        {
            Cursor = Cursors.Hand,
            StrokeLineJoin = PenLineJoin.Round,
            SnapsToDevicePixels = true,
        };

        private Rect movingRect;
        /// <summary>
        /// 部件正在被拖动时的外边框。
        /// </summary>
        public Rect MovingRect
        {
            get
            {
                return movingRect;
            }
        }

        private Point StartBasePoint
        {
            get
            {
                return new Point(Math.Min(startPoint.X, endPoint.X), Math.Min(startPoint.Y, endPoint.Y));
            }
        }

        private LineCtrlAdorner startCtrl;
        /// <summary>
        /// [只读]首控制点（装饰器）。
        /// </summary>
        public LineCtrlAdorner StartCtrl
        {
            get { return startCtrl; }
        }

        private Point startPoint = new Point();
        /// <summary>
        /// [读写]起点。
        /// </summary>
        [Tools.LunarProperty("StartPoint", PropertyDateType.Point)]
        public Point StartPoint
        {
            get { return startPoint; }
            set
            {
                startPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.StartPointTag, value.ToString());
                }

                RefreshLocation();
            }
        }

        private double textRotateAngle = 0;
        /// <summary>
        /// [读写]文本旋转角度。取值范围：[-180,180]。
        /// </summary>
        [Tools.LunarProperty("TextRotateAngle", PropertyDateType.Double)]
        public double TextRotateAngle
        {
            get { return textRotateAngle; }
            set
            {
                if (value > 180)
                {
                    textRotateAngle = 180;
                }
                else if (value < -180)
                {
                    textRotateAngle = -180;
                }
                else
                {
                    textRotateAngle = value;
                }

                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.TextRotateAngleTag, textRotateAngle.ToString());
                }

                this.RefreshTextRotateAngle();
            }
        }

        public override Point TopLeft
        {
            get
            {
                double minLeft, minTop, maxRight, maxBottom;

                minLeft = Math.Min(startPoint.X, endPoint.X);
                maxRight = Math.Max(startPoint.X, endPoint.X);

                minTop = Math.Min(startPoint.Y, endPoint.Y);
                maxBottom = Math.Max(startPoint.Y, endPoint.Y);

                return new Point(minLeft, minTop);
            }
        }

        private Enums.WidgetForm widgetForm = Enums.WidgetForm.Rhomb;//默认
        /// <summary>
        /// [读写]部件形态。默认值是Rhomb。
        /// </summary>
        [Tools.LunarProperty("WidgetForm", PropertyDateType.WidgetForm)]
        public Enums.WidgetForm WidgetForm
        {
            get { return widgetForm; }
            set
            {
                widgetForm = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.WidgetFormTag, value.ToString());
                }
                this.DrawLine();
            }
        }


        #endregion


        #region 方法=========================================================================================================

        /// <summary>
        /// 根据XmlData的信息，生成部件内容，设置部件格式。
        /// </summary>
        public override void Build()
        {
            base.Build();

            if (this.xmlData == null) return;

            XmlAttribute attrStartPoint = this.xmlData.GetAttribute(XmlTags.StartPointTag);
            if (attrStartPoint != null)
            {
                this.startPoint = Point.Parse(attrStartPoint.Value);
            }

            XmlAttribute attrEndPoint = this.xmlData.GetAttribute(XmlTags.EndPointTag);
            if (attrEndPoint != null)
            {
                this.endPoint = Point.Parse(attrEndPoint.Value);
            }

            XmlAttribute attrTextRotateAngle = this.xmlData.GetAttribute(XmlTags.TextRotateAngleTag);
            if (attrTextRotateAngle != null)
            {
                this.textRotateAngle = double.Parse(attrTextRotateAngle.Value);
            }

            XmlAttribute attrWidgetForm = this.xmlData.GetAttribute(XmlTags.WidgetFormTag);
            if (attrWidgetForm != null)
            {
                this.widgetForm = (Enums.WidgetForm)Enum.Parse(typeof(Enums.WidgetForm), attrWidgetForm.Value);
            }

            this.RefreshTextRotateAngle();
            this.RefreshLocation();

            //此类是下面这几个属性的“最终实现类”。这些属性的值都已在基类确定。因此调用,
            this.RefreshWidgetBackColor();
            this.RefreshWidgetLineColor();
            this.RefreshWidgetLineWidth();
            this.RefreshLineDash();
        }

        protected override void BuildStyleProperties()
        {
            base.BuildStyleProperties();

            //没有必须在此类读取的、与Style相关的Xml特性
        }

        public override void BuildWidgetStylePropertiesAndRefresh()
        {
            base.BuildWidgetStylePropertiesAndRefresh();

            //此类是下面这几个属性的“最终实现类”。这些属性的值都已在基类确定。因此调用,
            this.RefreshWidgetBackColor();
            this.RefreshWidgetLineColor();
            this.RefreshWidgetLineWidth();
            this.RefreshLineDash();


            //无意义
            //this.RefreshArrows();
            //this.RefreshWidgetPadding();

            //这几个在Widget类中已经调用。
            //this.RefreshWidgetForeColor();
            //this.RefreshIsShadowVisible();
            //this.RefreshWidgetOpacity();
        }

        /// <summary>
        /// 关联事件。右击中心装饰器时，活动此部件。
        /// </summary>
        void centerAdorner_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.mainPolyLine_PreviewMouseRightButtonUp(sender, e);
        }

        public void DrawLine()
        {
            if (masterEditor == null) return;

            DrawLine(this.startPoint, this.endPoint, false);
        }

        /// <summary>
        /// 菱形由两个端点足以决定。
        /// </summary>
        /// <param name="startPoint">首端点。</param>
        /// <param name="endPoint">尾端点。</param>
        /// <param name="isShift">是否按住Shift键。</param>
        private void DrawLine(Point startPoint, Point endPoint, bool isShift)
        {
            Point startBasePt, endBasePt;

            startBasePt = new Point(Math.Min(startPoint.X, endPoint.X),
                   Math.Min(startPoint.Y, endPoint.Y));
            endBasePt = new Point(Math.Max(startPoint.X, endPoint.X),
              Math.Max(startPoint.Y, endPoint.Y));

            if (isShift)
            {
                if (startPoint.Y > endPoint.Y)
                {
                    endBasePt.Y = startBasePt.Y + (endBasePt.X - startBasePt.X);
                }
                else
                {
                    startBasePt.Y = endBasePt.Y - (endBasePt.X - startBasePt.X);
                }
            }

            startBasePt = FormatPoint(startBasePt);
            endBasePt = FormatPoint(endBasePt);

            switch (this.widgetForm)
            {
                case Enums.WidgetForm.Cross:
                    {
                        DrawCross(ref startBasePt, ref endBasePt);
                        break;
                    }
                case Enums.WidgetForm.SixSideShape:
                    {
                        DrawSixSideShape(ref startBasePt, ref endBasePt);
                        break;
                    }
                case Enums.WidgetForm.EightSidedShape:
                    {
                        DrawEightSideShape(ref startBasePt, ref endBasePt);
                        break;
                    }
                case Enums.WidgetForm.LeftParallelogram:
                    {
                        DrawLeftParallelogram(ref startBasePt, ref endBasePt);
                        break;
                    }
                case Enums.WidgetForm.RightParallelogram:
                    {
                        DrawRightParallelogram(ref startBasePt, ref endBasePt);
                        break;
                    }
                case Enums.WidgetForm.LeftFillArrow:
                case Enums.WidgetForm.LeftFletchingArrow:
                case Enums.WidgetForm.LeftTailArrow:
                    {
                        DrawLeftArrow(ref startBasePt, ref endBasePt, this.widgetForm);
                        break;
                    }
                case Enums.WidgetForm.RightFillArrow:
                case Enums.WidgetForm.RightFletchingArrow:
                case Enums.WidgetForm.RightTailArrow:
                    {
                        DrawRightArrow(ref startBasePt, ref endBasePt, this.widgetForm);
                        break;
                    }
                case Enums.WidgetForm.TopFillArrow:
                case Enums.WidgetForm.TopFletchingArrow:
                case Enums.WidgetForm.TopTailArrow:
                    {
                        DrawTopArrow(ref startBasePt, ref endBasePt, this.widgetForm);
                        break;
                    }
                case Enums.WidgetForm.BottomFillArrow:
                case Enums.WidgetForm.BottomFletchingArrow:
                case Enums.WidgetForm.BottomTailArrow:
                    {
                        DrawBottomArrow(ref startBasePt, ref endBasePt, this.widgetForm);
                        break;
                    }
                case Enums.WidgetForm.Star:
                    {
                        DrawStar(ref startBasePt, ref endBasePt);
                        break;
                    }
                case WidgetForm.Rectangle:
                    {
                        DrawRectangle(ref startBasePt, ref endBasePt);
                        break;
                    }
                case WidgetForm.TopTrapezoid:
                case WidgetForm.BottomTrapezoid:
                    {
                        DrawTrapezoid(ref startBasePt, ref endBasePt, this.widgetForm);
                        break;
                    }
                case WidgetForm.LeftTag:
                case WidgetForm.RightTag:
                    {
                        DrawTag(ref startBasePt, ref endBasePt, this.widgetForm);
                        break;
                    }
                default:
                    {
                        DrawRhomb(ref startBasePt, ref endBasePt);
                        break;
                    }
            }
            //this.centerAdorner.InvalidateVisual();//移动过程中无需刷新装饰器。

            //移动部件控制点时，要刷新连接线的。
            this.movingRect = new Rect(startBasePt, endBasePt);

            LocateTextPanel(startBasePt, endBasePt);

            //注意：Rhomb的首尾装饰器与Ellipse和Rectangle的装饰位置坐标有区别。
            startCtrl.CenterPoint = startPoint;
            endCtrl.CenterPoint = endPoint;
        }

        private void DrawTrapezoid(ref Point startBasePt, ref Point endBasePt, WidgetForm form)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = (int)(height / (2 + Math.Sqrt(2)));

            if (offset * 2 > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                if (form == WidgetForm.TopTrapezoid)
                {
                    p1 = new Point(startBasePt.X + offset, startBasePt.Y);
                    p2 = new Point(endBasePt.X - offset, startBasePt.Y);
                    p3 = new Point(endBasePt.X, endBasePt.Y);
                    p4 = new Point(startBasePt.X, endBasePt.Y);
                }
                else
                {
                    p1 = new Point(startBasePt.X, startBasePt.Y);
                    p2 = new Point(endBasePt.X, startBasePt.Y);
                    p3 = new Point(endBasePt.X - offset, endBasePt.Y);
                    p4 = new Point(startBasePt.X + offset, endBasePt.Y);
                }
                mainPolyLine.StrokeLineJoin = PenLineJoin.Round;
                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
        }

        private void DrawTag(ref Point startBasePt, ref Point endBasePt, WidgetForm form)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = (int)(height / (2 + Math.Sqrt(2)));

            if (offset * 2 > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6;   // startBasePt 和 endBasePt 是外框
                if (form == WidgetForm.RightTag)
                {
                    p1 = new Point(endBasePt.X - offset, startBasePt.Y);
                    p2 = new Point(startBasePt.X, startBasePt.Y);
                    p3 = new Point(startBasePt.X, endBasePt.Y);
                    p4 = new Point(endBasePt.X, endBasePt.Y);
                    p5 = new Point(endBasePt.X, startBasePt.Y + offset);
                    p6 = new Point(endBasePt.X - offset, startBasePt.Y + offset);
                }
                else
                {
                    p1 = new Point(startBasePt.X + offset, startBasePt.Y);
                    p2 = new Point(endBasePt.X, startBasePt.Y);
                    p3 = new Point(endBasePt.X, endBasePt.Y);
                    p4 = new Point(startBasePt.X, endBasePt.Y);
                    p5 = new Point(startBasePt.X, startBasePt.Y + offset);
                    p6 = new Point(startBasePt.X + offset, startBasePt.Y + offset);

                }

                mainPolyLine.StrokeLineJoin = PenLineJoin.Round;
                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,p5,p6,p1,p5,
                };
            }
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawStar(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4, p5, p6, p7, p8;

            double halfWidth = Math.Abs(endBasePt.X - startBasePt.X) / 2;
            double halfHeight = Math.Abs(endBasePt.Y - startBasePt.Y) / 2;

            Point center = new Point(startBasePt.X + halfWidth, startBasePt.Y + halfHeight);

            double xOffset = Math.Abs((int)(halfWidth / 4));
            double yOffset = Math.Abs((int)(halfHeight / 4));

            p1 = new Point(center.X - xOffset, center.Y - yOffset);
            p2 = new Point(center.X, startBasePt.Y);
            p3 = new Point(center.X + xOffset, p1.Y);
            p4 = new Point(endBasePt.X, center.Y);
            p5 = new Point(p3.X, center.Y + yOffset);
            p6 = new Point(p2.X, endBasePt.Y);
            p7 = new Point(p1.X, p5.Y);
            p8 = new Point(startBasePt.X, center.Y);

            mainPolyLine.Points = new PointCollection()
            {
                p1,p2,p3,p4,p5,p6,p7,p8,
            };
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawBottomArrow(ref Point startBasePt, ref Point endBasePt, WidgetForm widgetForm)
        {
            Point p1, p2, p3, p4;

            double width = Math.Abs(endBasePt.X - startBasePt.X);
            double offset = Math.Abs((int)(width / 2 / Math.Sqrt(3)));

            if (offset > Math.Abs(endBasePt.Y - startBasePt.Y))
            {
                //如果纵向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6;

                p1 = startBasePt;
                p2 = new Point(startBasePt.X + width / 2, startBasePt.Y + offset);
                p3 = new Point(endBasePt.X, startBasePt.Y);
                p4 = new Point(p3.X, endBasePt.Y - offset);
                p5 = new Point(p2.X, endBasePt.Y);
                p6 = new Point(startBasePt.X, p4.Y);

                switch (widgetForm)
                {
                    case Enums.WidgetForm.BottomFillArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,/*p2,*/p3,p4,p5,p6//就少个p2,
                            };
                            break;
                        }
                    case Enums.WidgetForm.BottomFletchingArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p4,p5,p6,
                            };
                            break;
                        }
                    case Enums.WidgetForm.BottomTailArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p5,
                            };
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawTopArrow(ref Point startBasePt, ref Point endBasePt, WidgetForm widgetForm)
        {
            Point p1, p2, p3, p4;

            double width = Math.Abs(endBasePt.X - startBasePt.X);
            double offset = Math.Abs((int)(width / 2 / Math.Sqrt(3)));

            if (offset > Math.Abs(endBasePt.Y - startBasePt.Y))
            {
                //如果纵向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6;

                p1 = new Point(startBasePt.X + width / 2, startBasePt.Y);
                p2 = new Point(endBasePt.X, startBasePt.Y + offset);
                p3 = endBasePt;
                p4 = new Point(p1.X, endBasePt.Y - offset);
                p5 = new Point(startBasePt.X, endBasePt.Y);
                p6 = new Point(startBasePt.X, p2.Y);

                switch (widgetForm)
                {
                    case Enums.WidgetForm.TopFillArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,/*p4,*/p5,p6//就少个p4,
                            };
                            break;
                        }
                    case Enums.WidgetForm.TopFletchingArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p4,p5,p6,
                            };
                            break;
                        }
                    case Enums.WidgetForm.TopTailArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p3,p4,p5,
                            };
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawLeftArrow(ref Point startBasePt, ref Point endBasePt, WidgetForm widgetForm)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = Math.Abs((int)(height / 2 / Math.Sqrt(3)));

            if (offset > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6;

                p1 = new Point(startBasePt.X + offset, startBasePt.Y);
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = new Point(endBasePt.X - offset, startBasePt.Y + height / 2);
                p4 = endBasePt;
                p5 = new Point(p1.X, endBasePt.Y);
                p6 = new Point(startBasePt.X, p3.Y);

                switch (widgetForm)
                {
                    case Enums.WidgetForm.LeftFillArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,/*p3,*/p4,p5,p6//就少个p3,
                            };
                            break;
                        }
                    case Enums.WidgetForm.LeftFletchingArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p4,p5,p6,
                            };
                            break;
                        }
                    case Enums.WidgetForm.LeftTailArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p2,p3,p4,p6,
                            };
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawRightArrow(ref Point startBasePt, ref Point endBasePt, WidgetForm widgetForm)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = Math.Abs((int)(height / 2 / Math.Sqrt(3)));

            if (offset > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6;

                p1 = startBasePt;
                p2 = new Point(endBasePt.X - offset, startBasePt.Y);
                p3 = new Point(endBasePt.X, startBasePt.Y + height / 2);
                p4 = new Point(p2.X, endBasePt.Y);
                p5 = new Point(p1.X, endBasePt.Y);
                p6 = new Point(startBasePt.X + offset, p3.Y);


                switch (widgetForm)
                {
                    case Enums.WidgetForm.RightFillArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p4,p5,//p6//就少个p6,
                            };
                            break;
                        }
                    case Enums.WidgetForm.RightFletchingArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p4,p5,p6,
                            };
                            break;
                        }
                    case Enums.WidgetForm.RightTailArrow:
                        {
                            mainPolyLine.Points = new PointCollection()
                            {
                                p1,p3,p5,p6,
                            };
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawRectangle(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;
            p1 = new Point(startBasePt.X, startBasePt.Y);
            p2 = new Point(startBasePt.X, endBasePt.Y);
            p3 = new Point(endBasePt.X, endBasePt.Y);
            p4 = new Point(endBasePt.X, startBasePt.Y);
            mainPolyLine.Points = new PointCollection()
            {
                p1,p2,p3,p4,
            };
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawRhomb(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;
            p1 = new Point(startBasePt.X, startBasePt.Y + (endBasePt.Y - startBasePt.Y) / 2);
            p2 = new Point(startBasePt.X + (endBasePt.X - startBasePt.X) / 2, startBasePt.Y);
            p3 = new Point(endBasePt.X, p1.Y);
            p4 = new Point(p2.X, endBasePt.Y);
            mainPolyLine.Points = new PointCollection()
            {
                p1,p2,p3,p4,
            };
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawRightParallelogram(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double hoffset = height / Math.Sqrt(3);

            if (hoffset > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);
            }
            else
            {
                p1 = startBasePt;
                p2 = new Point(endBasePt.X - hoffset, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X + hoffset, endBasePt.Y);
            }

            mainPolyLine.Points = new PointCollection()
                        {
                            p1,p2,p3,p4,
                        };
        }

        /// <summary>
        /// 绘制形态。
        /// </summary>
        private void DrawLeftParallelogram(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double hoffset = height / Math.Sqrt(3);

            if (hoffset > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);
            }
            else
            {
                p1 = new Point(startBasePt.X + hoffset, startBasePt.Y);
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = new Point(endBasePt.X - hoffset, endBasePt.Y);
                p4 = new Point(startBasePt.X, endBasePt.Y);
            }

            mainPolyLine.Points = new PointCollection()
                        {
                            p1,p2,p3,p4,
                        };
        }

        /// <summary>
        /// 绘制“Cross”形态。
        /// </summary>
        /// <param name="startBasePt"></param>
        /// <param name="endBasePt"></param>
        private void DrawCross(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = (int)(height / (2 + Math.Sqrt(2)));

            if (offset * 2 > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,p3,p4,
                            };
            }
            else
            {
                Point p5, p6, p7, p8;
                Point pTR, pBR, pBL, pTL;
                p1 = new Point(startBasePt.X + offset, startBasePt.Y);
                p2 = new Point(endBasePt.X - offset, startBasePt.Y);
                p3 = new Point(endBasePt.X, startBasePt.Y + offset);
                pTR = new Point(p2.X, p3.Y);

                p4 = new Point(endBasePt.X, endBasePt.Y - offset);
                p5 = new Point(p2.X, endBasePt.Y);
                pBR = new Point(p5.X, p4.Y);

                p6 = new Point(p1.X, endBasePt.Y);
                p7 = new Point(startBasePt.X, p4.Y);
                pBL = new Point(p6.X, p7.Y);

                p8 = new Point(startBasePt.X, p3.Y);
                pTL = new Point(p1.X, p8.Y);

                mainPolyLine.Points = new PointCollection()
                            {
                                p1,p2,pTR,p3,p4,pBR,p5,p6,pBL,p7,p8,pTL,
                            };
            }
        }

        /// <summary>
        /// 绘制“SixSideShape”形态。
        /// </summary>
        /// <param name="startBasePt"></param>
        /// <param name="endBasePt"></param>
        private void DrawSixSideShape(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = Math.Abs((int)(height / 2 / Math.Sqrt(3)));

            if (offset * 2 > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6;

                p1 = new Point(startBasePt.X + offset, startBasePt.Y);
                p2 = new Point(endBasePt.X - offset, startBasePt.Y);
                p3 = new Point(endBasePt.X, startBasePt.Y + height / 2);
                p4 = new Point(p2.X, endBasePt.Y);
                p5 = new Point(p1.X, endBasePt.Y);
                p6 = new Point(startBasePt.X, p3.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,p5,p6,
                };
            }
        }

        /// <summary>
        /// 绘制“EightSideShape”形态。
        /// </summary>
        /// <param name="startBasePt"></param>
        /// <param name="endBasePt"></param>
        private void DrawEightSideShape(ref Point startBasePt, ref Point endBasePt)
        {
            Point p1, p2, p3, p4;

            double height = Math.Abs(endBasePt.Y - startBasePt.Y);
            double offset = (int)(height / (2 + Math.Sqrt(2)));

            if (offset * 2 > Math.Abs(endBasePt.X - startBasePt.X))
            {
                //如果横向偏移量过大，则画成矩形
                p1 = startBasePt;
                p2 = new Point(endBasePt.X, startBasePt.Y);
                p3 = endBasePt;
                p4 = new Point(startBasePt.X, endBasePt.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,
                };
            }
            else
            {
                Point p5, p6, p7, p8;
                p1 = new Point(startBasePt.X + offset, startBasePt.Y);
                p2 = new Point(endBasePt.X - offset, startBasePt.Y);
                p3 = new Point(endBasePt.X, startBasePt.Y + offset);
                p4 = new Point(endBasePt.X, endBasePt.Y - offset);
                p5 = new Point(p2.X, endBasePt.Y);
                p6 = new Point(p1.X, endBasePt.Y);
                p7 = new Point(startBasePt.X, p4.Y);
                p8 = new Point(startBasePt.X, p3.Y);

                mainPolyLine.Points = new PointCollection()
                {
                    p1,p2,p3,p4,p5,p6,p7,p8,
                };
            }
        }

        /// <summary>
        /// 重定文本位置。
        /// </summary>
        /// <param name="startPoint">首基准点。</param>
        /// <param name="endPoint">尾基准点。</param>
        private void LocateTextPanel(Point startBasePt, Point endBasePt)
        {
            Point center = new Point(startBasePt.X + (endBasePt.X - startBasePt.X) / 2,
                startBasePt.Y + (endBasePt.Y - startBasePt.Y) / 2);

            Point textStart = new Point(center.X - this.mainTextPanel.ActualWidth / 2 - this.mainTextPanel.Margin.Left,
                center.Y - this.mainTextPanel.ActualHeight / 2 - this.mainTextPanel.Margin.Top);

            Canvas.SetLeft(this.mainTextPanel, textStart.X);
            Canvas.SetTop(this.mainTextPanel, textStart.Y);
        }

        public override void DropWidget(ModifingItem<Action, ModifingInfo> mi, Point mousePoint)
        {
            if (mi == null) return;
            if (masterEditor == null) return;

            Point globalLocation = masterEditor.MouseInfo.LeftButtonPreviewPoint;

            Point newStartPoint = new Point(
                startPoint.X - globalLocation.X + mousePoint.X, startPoint.Y - globalLocation.Y + mousePoint.Y);

            Point newEndPoint = new Point(
                endPoint.X - globalLocation.X + mousePoint.X, endPoint.Y - globalLocation.Y + mousePoint.Y);

            #region 自动中心吸附
            bool autoAlignment = Globals.MainWindow.RtbtnAutoAlignment.IsChecked == true;

            if (autoAlignment)
            {
                double hOffset = double.MaxValue, vOffset = double.MaxValue;
                if (autoAlignment)
                {
                    //尝试自动对齐

                    //先尝试中心坐标对齐
                    //需要取出最接近的位置
                    Point center = new Point(newStartPoint.X + this.OuterRect.Width / 2, newStartPoint.Y + this.OuterRect.Height / 2);

                    foreach (UIElement ue in this.MasterEditor.MainCanvas.Children)
                    {
                        Widget widget = ue as ContentWidget;
                        if (widget == null)
                        {
                            widget = ue as ShapeWidget;
                            if (widget == null) continue;
                        }

                        if (widget == this) continue;
                        if (widget.MasterEditor.GetSelectedWidgetsList().Contains(widget)) continue;

                        Point widgetCenter = new Point(widget.OuterRect.Left + widget.OuterRect.Width / 2, widget.OuterRect.Top + widget.OuterRect.Height / 2);
                        double h = double.MaxValue, v = double.MaxValue, tmph = double.MaxValue, tmpv = double.MaxValue;

                        tmph = widgetCenter.X - center.X;
                        tmpv = widgetCenter.Y - center.Y;

                        if (Math.Abs(tmph) < Math.Abs(h)) h = tmph;
                        if (Math.Abs(tmpv) < Math.Abs(v)) v = tmpv;

                        if (Math.Abs(h) < Math.Abs(hOffset)) hOffset = h;
                        if (Math.Abs(v) < Math.Abs(vOffset)) vOffset = v;
                    }
                }

                //自动吸附对齐会造成很迷惑人的效果。取消之。2014年7月26日
                //if (hOffset < double.MaxValue && Math.Abs(hOffset) < 10)
                //{
                //    newStartPoint.X += hOffset;
                //    newEndPoint.X += hOffset;
                //}

                //if (vOffset < double.MaxValue && Math.Abs(vOffset) < 10)
                //{
                //    newStartPoint.Y += vOffset;
                //    newEndPoint.Y += vOffset;
                //}
            }
            #endregion

            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name,
                XmlTags.StartPointTag, startPoint.ToString(),
                newStartPoint.ToString());
            StartPoint = newStartPoint;

            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name,
                XmlTags.EndPointTag, endPoint.ToString(),
                newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.ModifingInfo.NewMainSelectedWidgetID = this.id;
            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        public override void DropWidgetControler(Point newOutSidePoint, ModifingItem<Action, ModifingInfo> mi)
        {
            if (mi == null || mi.ModifingInfo == null) return;
            if (masterEditor == null || masterEditor.MasterManager == null) return;

            ModifingInfo info = mi.ModifingInfo;

            bool isShift = false;

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            if ((ksLeftShift & KeyStates.Down) > 0 || (ksRightShift & KeyStates.Down) > 0)
            {
                isShift = true;
            }

            switch (draggingType)
            {
                case ControlDraggingType.Start:
                    {
                        info.ModifingDescription = "拖动菱形首端点";

                        if (isShift)
                        {
                            if (newOutSidePoint.Y > endPoint.Y)
                            {
                                newOutSidePoint.Y = endPoint.Y + Math.Abs(newOutSidePoint.X - endPoint.X);
                            }
                            else
                            {
                                newOutSidePoint.Y = endPoint.Y - Math.Abs(newOutSidePoint.X - endPoint.X);
                            }
                        }

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.StartPointTag, startPoint.ToString(), newOutSidePoint.ToString());
                        StartPoint = newOutSidePoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
                case ControlDraggingType.End:
                    {
                        info.ModifingDescription = "拖动菱形尾端点";

                        if (isShift)
                        {
                            if (newOutSidePoint.Y > startPoint.Y)
                            {
                                newOutSidePoint.Y = startPoint.Y + Math.Abs(newOutSidePoint.X - startPoint.X);
                            }
                            else
                            {
                                newOutSidePoint.Y = startPoint.Y - Math.Abs(newOutSidePoint.X - startPoint.X);
                            }
                        }

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.EndPointTag, endPoint.ToString(), newOutSidePoint.ToString());
                        EndPoint = newOutSidePoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
            }

            //刷新连接线首尾端点的位置。
            List<ILinkableLine> linkedLines = this.GetLinkedLines();
            this.masterEditor.RefreshLinkedLines(mi, linkedLines);
        }

        /// <summary>
        /// 这个虚方法是用以查看本部件是否在选定框的内部。
        /// 线型部件，各有各的计算办法。
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public override bool IsInRect(Rect rect)
        {
            //return base.IsInRect(rect);//这个要屏蔽。
            bool isInRect = base.IsInRect(rect);
            if (isInRect)
            {
                return true;
            }
            else
            {
                Rect virtualRect = new Rect(startPoint, endPoint);
                return rect.IntersectsWith(virtualRect);
            }
        }

        void mainPolygon_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //双击编辑文本
            if (e.ClickCount == 2)
            {
                this.SelectOnlySelf();
                if (Globals.MainWindow.IsPresentatingByPath == false)
                {
                    this.Edit();
                }
                return;
            }

            ModifingItem<Action, ModifingInfo> mi = null;
            ModifingInfo info = null;
            if (FormatSelf(ref mi, ref info))
            {
                if (masterEditor != null)
                {
                    masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveWidgets;
                }
                return;//格式刷格式化，不更改选定状态。
            }

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            bool isShift = false;

            if ((ksRightShift & KeyStates.Down) > 0 || (ksLeftShift & KeyStates.Down) > 0) { isShift = true; }

            bool isCtrl = false;
            KeyStates ksRightCtrl = Keyboard.GetKeyStates(Key.RightCtrl);
            KeyStates ksLeftCtrl = Keyboard.GetKeyStates(Key.LeftCtrl);

            if ((ksRightCtrl & KeyStates.Down) > 0 || (ksLeftCtrl & KeyStates.Down) > 0) isCtrl = true;

            if (isShift && isCtrl == false)
            {
                //基本规则：
                //    ①如果未选定，选定，并作为活动部件。
                //    ②如果已选定，看是否是活动部件，如果已经是活动部件，整个取消选定状态；
                //                                      如果不是活动部件，设为活动部件。
                if (this.IsSelected == false)
                {
                    this.IsMainSelected = true;
                }
                else
                {
                    if (this.IsMainSelected == false)
                    {
                        this.IsMainSelected = true;
                    }
                    else
                    {
                        this.IsSelected = false;
                    }
                }
            }
            else
            {
                if (IsSelected == false)
                {
                    this.SelectOnlySelf();
                }
                //如果是选定的部件，那么可能是在拖动。
            }

            //准备拖动。
            if (masterEditor != null)
            {
                masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveWidgets;
                Point pt = e.GetPosition(this.masterEditor);
                pt = new Point(pt.X - this.masterEditor.BorderThickness.Left, pt.Y - this.masterEditor.BorderThickness.Top);
                masterEditor.MouseInfo.LeftButtonPreviewPoint = pt;
            }
        }

        void mainPolyLine_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (this.IsSelected == false)
            {
                //如果未选定，就作为活动，
                this.SelectOnlySelf();
            }
            else
            {
                //若已选定，不更改选定范围，只更改“活动部件”为此部件
                if (this.IsMainSelected == false) this.IsMainSelected = true;
            }

            if (this.ContextMenu != null)
            {
                this.ContextMenu.IsOpen = true;
            }
        }

        /// <summary>
        /// 准备拖出“关系（由两条直线、两个新文本框组成，或由一个文本框、一条直线组成）。
        /// </summary>
        void centerAdorner_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (masterEditor == null) return;
            if (e.ClickCount == 2)
            {
                //防止误拖动部件。
                if (masterEditor != null && masterEditor.MasterManager != null)
                {
                    masterEditor.MouseInfo.DraggingType = masterEditor.MasterManager.MouseDraggingType;
                }

                this.SelectOnlySelf();
                if (Globals.MainWindow.IsPresentatingByPath == false)
                {
                    this.Edit();
                }
                return;
            }

            e.Handled = true;

            if (Globals.MainWindow != null && Globals.MainWindow.EditorManager != null)
            {
                switch (Globals.MainWindow.EditorManager.MouseDraggingType)
                {
                    case PageDraggingType.InsertBezierLine:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertBezierLineRelation;
                            break;
                        }
                    case PageDraggingType.InsertPolyLine:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertPolyLineRelation;
                            break;
                        }
                    default:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertStraitLineRelation;
                            break;
                        }
                }
            }
            else
            {
                masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertStraitLineRelation;
            }

            //Point pt = e.GetPosition(this.masterEditor);//这个不好，会偏
            //pt = new Point(pt.X - this.masterEditor.BorderThickness.Left,
            //    pt.Y - this.masterEditor.BorderThickness.Top);

            Point pt = this.Center;//直接用中心点更好。
            masterEditor.MouseInfo.LeftButtonPreviewPoint = pt;
            masterEditor.MouseInfo.MainSelectedWidget = this;
        }

        public override void CollapseToTop(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.Y > endPoint.Y)
            {
                Point newStartPoint = new Point(startPoint.X, startPoint.Y - units);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.Y < endPoint.Y)
            {
                Point newEndPoint = new Point(endPoint.X, endPoint.Y - units);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果水平对齐，则不更改。
        }

        public override void ExpandToBottom(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.Y > endPoint.Y)
            {
                Point newStartPoint = new Point(startPoint.X, startPoint.Y + units);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.Y < endPoint.Y)
            {
                Point newEndPoint = new Point(endPoint.X, endPoint.Y + units);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果水平对齐，则不更改。
        }

        public override void CollapseToLeft(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.X > endPoint.X)
            {
                Point newStartPoint = new Point(startPoint.X - units, startPoint.Y);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.X < endPoint.X)
            {
                Point newEndPoint = new Point(endPoint.X - units, endPoint.Y);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果垂直对齐，则不更改
        }

        public override void ExpandToRight(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.X > endPoint.X)
            {
                Point newStartPoint = new Point(startPoint.X + units, startPoint.Y);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.X < endPoint.X)
            {
                Point newEndPoint = new Point(endPoint.X + units, endPoint.Y);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果垂直对齐，则不更改
        }

        public override void MoveUp(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - units);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - units);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        public override void MoveDown(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y + units);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y + units);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        public override void MoveLeft(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X - units, startPoint.Y);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - units, endPoint.Y);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        public override void MoveRight(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X + units, startPoint.Y);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X + units, endPoint.Y);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 改变中心点横坐标。
        /// </summary>
        public override void MoveHorizontalCenterTo(ModifingItem<Action, ModifingInfo> mi, double center)
        {
            if (mi == null || double.IsNaN(center) || masterEditor == null) return;

            double offset = 0;
            double curCenter = TopLeft.X + (BottomRight.X - TopLeft.X) / 2;
            offset = curCenter - center;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 改变中心点纵坐标。
        /// </summary>
        public override void MoveVerticalCenterTo(ModifingItem<Action, ModifingInfo> mi, double center)
        {
            if (mi == null || double.IsNaN(center) || masterEditor == null) return;

            double offset = 0;
            double curCenter = TopLeft.Y + (BottomRight.Y - TopLeft.Y) / 2;
            offset = curCenter - center;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveDown不同，这里是指定向下移动到哪个位置（下边缘到哪个位置）。
        /// </summary>
        public override void MoveBottomSiderTo(ModifingItem<Action, ModifingInfo> mi, double bottom)
        {
            if (mi == null || double.IsNaN(bottom) || masterEditor == null) return;

            double offset = 0;
            double maxBottom = BottomRight.Y;
            offset = maxBottom - bottom;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveLeft不同，这里是指定向左移动到哪个位置（左边缘到哪个位置）。
        /// </summary>
        public override void MoveLeftSiderTo(ModifingItem<Action, ModifingInfo> mi, double left)
        {
            if (mi == null || double.IsNaN(left) || masterEditor == null) return;

            double offset = 0;
            double minLeft = TopLeft.X;
            offset = minLeft - left;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveRight不同，这里是指定向右移动到哪个位置（右边缘到哪个位置）。
        /// </summary>
        public override void MoveRightSiderTo(ModifingItem<Action, ModifingInfo> mi, double right)
        {
            if (mi == null || double.IsNaN(right) || masterEditor == null) return;

            double offset = 0;
            double maxRight = BottomRight.X;
            offset = maxRight - right;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveUp不同，这里是指定向上移动到哪个位置（上边缘到哪个位置）。
        /// </summary>
        public override void MoveTopSiderTo(ModifingItem<Action, ModifingInfo> mi, double top)
        {
            if (mi == null || double.IsNaN(top) || masterEditor == null) return;

            double offset = 0;
            double minTop = TopLeft.Y;
            offset = minTop - top;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actEnd);
        }

        public override void MoveWhenDraggingControler(Point newOutSidePoint)
        {
            bool isShift = false;

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            if ((ksLeftShift & KeyStates.Down) > 0 || (ksRightShift & KeyStates.Down) > 0)
            {
                isShift = true;
            }

            switch (draggingType)
            {
                case ControlDraggingType.Start:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(newMovingPoint, this.endPoint, isShift);
                        break;
                    }
                case ControlDraggingType.End:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(this.startPoint, newMovingPoint, isShift);

                        break;
                    }
            }

            this.commentAdorner.Visibility =
                this.hyperLinkAdorner.Visibility =
                this.centerAdorner.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 取与自身挂接的连接线。
        /// </summary>
        /// <returns>可能返回null。</returns>
        public List<ILinkableLine> GetLinkedLines()
        {
            if (this.masterEditor == null) return null;

            List<Widget> widgets = new List<Widget>();
            widgets.Add(this);

            return this.masterEditor.GetLinkedLines(widgets);
        }

        public override string GetRelativeOuterXml(Point baseCopyTopLeft)
        {
            if (this.xmlData == null) return string.Empty;

            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X - baseCopyTopLeft.X,
                oldStartPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.StartPointTag, newStartPoint.ToString());

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X - baseCopyTopLeft.X,
                oldEndPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.EndPointTag, newEndPoint.ToString());

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(this.xmlData.OuterXml);

            this.xmlData.SetAttribute(XmlTags.StartPointTag, oldStartPoint.ToString());
            this.xmlData.SetAttribute(XmlTags.EndPointTag, oldEndPoint.ToString());

            return sb.ToString();
        }

        public override void MoveWhenDraggingWidget(Point mousePoint)
        {
            if (masterEditor == null || masterEditor.MasterManager == null) return;

            Point newMovingPoint = mousePoint;
            Point globalLocation = masterEditor.MouseInfo.LeftButtonPreviewPoint;

            Point tmpStartPoint = new Point(startPoint.X - globalLocation.X + newMovingPoint.X,
                startPoint.Y - globalLocation.Y + newMovingPoint.Y);
            Point tmpEndPoint = new Point(endPoint.X - globalLocation.X + newMovingPoint.X,
                endPoint.Y - globalLocation.Y + newMovingPoint.Y);

            DrawLine(tmpStartPoint, tmpEndPoint, false);

            //还需要禁用中心装饰器。这与矩形、椭圆都不同。
            this.commentAdorner.Visibility =
                this.hyperLinkAdorner.Visibility =
                this.centerAdorner.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 刷新所有控制点。
        /// </summary>
        public override void RefreshControlers()
        {
            if (isSelected)
            {
                //刷新控制点色彩
                if (masterEditor != null && masterEditor.MasterManager != null)
                {
                    if (startCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetStartControlerBrush)
                        startCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetStartControlerBrush;

                    if (endCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetEndControlerBrush)
                        endCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetEndControlerBrush;
                }

                startCtrl.CenterPoint = startPoint;
                endCtrl.CenterPoint = endPoint;

                startCtrl.Visibility =
                    endCtrl.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                startCtrl.Visibility =
                    endCtrl.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        public override void RefreshLineDash()
        {
            switch (lineDash)
            {
                case LineDashType.DashType.Dash:
                    {
                        this.mainPolyLine.StrokeDashArray = LineDashType.dashCollection; break;
                    }
                case LineDashType.DashType.DashDotDot:
                    {
                        this.mainPolyLine.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                    }
                case LineDashType.DashType.Dot:
                    {
                        this.mainPolyLine.StrokeDashArray = LineDashType.dotCollection; break;
                    }
                case LineDashType.DashType.Solid:
                    {
                        this.mainPolyLine.StrokeDashArray = LineDashType.solidCollection; break;
                    }
                default:
                    {
                        this.mainPolyLine.StrokeDashArray = LineDashType.dashDotCollection; break;
                    }
            }
        }

        public override void RefreshWidgetLineWidth()
        {
            mainPolyLine.StrokeThickness = widgetLineWidth;
        }

        public override void RefreshLocation()
        {
            base.RefreshLocation();

            DrawLine();

            //Point p1, p2, p3, p4;

            //p1 = new Point(startPoint.X, startPoint.Y + (endPoint.Y - startPoint.Y) / 2);
            //p2 = new Point(startPoint.X + (endPoint.X - startPoint.X) / 2, startPoint.Y);
            //p3 = new Point(endPoint.X, p1.Y);
            //p4 = new Point(p2.X, endPoint.Y);

            //mainPolyLine.Points = new PointCollection(){
            //    p1,p2,p3,p4,
            //};

            //this.movingRect = new Rect(startPoint, endPoint);

            RefreshWidgetLineWidth();
            RefreshControlers();
            this.RefreshTextRotateAngle();

            this.centerAdorner.Visibility = Visibility.Visible;
            this.centerAdorner.InvalidateVisual();//与椭圆、矩形不同，这里需要手工刷新装饰器。
            this.commentAdorner.InvalidateVisual();
        }

        public override void RefreshPointWhenGroupIn(Point baseTopLeft)
        {
            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X - baseTopLeft.X,
                oldStartPoint.Y - baseTopLeft.Y);
            StartPoint = newStartPoint;

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X - baseTopLeft.X,
                oldEndPoint.Y - baseTopLeft.Y);
            EndPoint = newEndPoint;
        }

        public override void RefreshPointWhenGroupOut(Point baseTopLeft)
        {
            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X + baseTopLeft.X,
                oldStartPoint.Y + baseTopLeft.Y);
            StartPoint = newStartPoint;

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X + baseTopLeft.X,
                oldEndPoint.Y + baseTopLeft.Y);
            EndPoint = newEndPoint;
        }

        public override void RefreshIsShadowVisible()
        {
            //base.RefreshIsShadowVisible();//保持文本清晰

            if (isShadowVisible)
            {
                this.mainPolyLine.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.mainPolyLine.Effect = null;
            }
        }

        public override void RefreshTextPanelLocatin()
        {
            Point center = new Point(this.startPoint.X + (this.endPoint.X - this.startPoint.X) / 2,
                this.startPoint.Y + (this.endPoint.Y - this.startPoint.Y) / 2);

            Point textStart = new Point(center.X - this.mainTextPanel.ActualWidth / 2 - this.mainTextPanel.Margin.Left,
                center.Y - this.mainTextPanel.ActualHeight / 2 - this.mainTextPanel.Margin.Top);

            Canvas.SetLeft(this.mainTextPanel, textStart.X);
            Canvas.SetTop(this.mainTextPanel, textStart.Y);
        }

        public override void RefreshWidgetBackColor()
        {
            base.RefreshWidgetBackColor();
            if (widgetBackColor == Brushes.Transparent)
            {
                this.mainPolyLine.Fill = null;
            }
            else
            {
                this.mainPolyLine.Fill = widgetBackColor;
            }
        }

        public override void RefreshWidgetLineColor()
        {
            this.mainPolyLine.Stroke = widgetLineColor;
        }

        /// <summary>
        /// 刷新文本区旋转角度。
        /// </summary>
        public void RefreshTextRotateAngle()
        {
            if (this.mainTextPanel.RenderTransformOrigin != DefaultRenderCenter)
            {
                this.mainTextPanel.RenderTransformOrigin = DefaultRenderCenter;
            }

            if (this.textRotateAngle == 0)
            {
                this.mainTextPanel.RenderTransform = DefaultRotateTransform;
            }
            else
            {
                this.mainTextPanel.RenderTransform = new RotateTransform(textRotateAngle);
            }
        }

        public override void UpdatePointsWhenPasting(Point basePasteTopLeft)
        {
            Point newStartPoint = new Point(startPoint.X + basePasteTopLeft.X,
               startPoint.Y + basePasteTopLeft.Y);

            StartPoint = newStartPoint;

            Point newEndPoint = new Point(endPoint.X + basePasteTopLeft.X,
                endPoint.Y + basePasteTopLeft.Y);

            EndPoint = newEndPoint;
        }

        public event EventHandler<MouseButtonEventArgs> ControlHandlerDoubleClicked;

        protected void OnControlHandlerDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (ControlHandlerDoubleClicked != null)
            {
                ControlHandlerDoubleClicked(sender, e);
            }
        }

        void endCtrl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            if (masterEditor == null) return;

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = StartPoint;
            masterEditor.MouseInfo.DraggingType = PageDraggingType.MoveLineWidgetControler;
            draggingType = ControlDraggingType.End;

            endCtrl.Visibility = System.Windows.Visibility.Hidden;
        }

        void startCtrl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            if (masterEditor == null) return;

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = StartPoint;
            masterEditor.MouseInfo.DraggingType = PageDraggingType.MoveLineWidgetControler;
            draggingType = ControlDraggingType.Start;

            startCtrl.Visibility = System.Windows.Visibility.Hidden;
        }


        #endregion


    }
}
