﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;
using Microsoft.Win32;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Input;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicPictureBoxWidget
    {
        private void CMDicPictureBoxWidget_Opened(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            MiExpandImage.IsChecked = MiCollapseImage.IsChecked = false;

            //int pCount = 0;

            foreach (UIElement ue in pe.Children)
            {
                SHomeWorkshop.LunarConcept.Widgets.PictureBox pw =
                    ue as SHomeWorkshop.LunarConcept.Widgets.PictureBox;
                if (pw != null)
                {
                    //pCount++;
                    if (pw.IsMainSelected)
                    {
                        MiExpandImage.IsChecked = !pw.IsImageCollapsed;
                        MiCollapseImage.IsChecked = pw.IsImageCollapsed;
                    }
                }
            }
        }

        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.PictureBox).Name));
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void MiLoadImage_Clicked(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;

            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            List<Widgets.Widget> selectedWidgetList = pe.GetSelectedWidgetsList();
            if (selectedWidgetList.Count <= 0) return;

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "更换图像数据" };
            pe.MasterManager.GetSelectedPageEditorStatus(info);
            pe.MasterManager.GetSelectedWidgetStatus_Old(info);
            pe.MasterManager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            string filename = "";

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Title = Globals.AppName + "——请选择需要的图片：";
            ofd.Filter = "支持的所有图片格式(*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff)|*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff|" +
                "PNG或JPEG图片(*.png;*.jpg;*.jpeg)|*.png;*.jpg;*.jpeg|" +
                "联合图像专家组格式(*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                "图形交换格式(*.gif)|*.gif|" +
                "W3C 可移植网络图形(*png)|*png|" +
                "标题图像文件(*.tiff)|*.tiff";

            if (ofd.ShowDialog() != true) return;

            filename = ofd.FileName;

            int skipedInnerIcons = 0;
            foreach (Widget w in selectedWidgetList)
            {
                PictureBox pbox = w as PictureBox;
                if (pbox == null) continue;

                if (pbox.IsInnerIcon)
                {
                    skipedInnerIcons++;
                    continue;
                }

                string oldValue = pbox.XmlData.InnerText;

                pbox.LoadImageFromFile(filename);

                Action actReplaceImage = new Action(pe.Id, pbox.Id, pbox.GetType().Name,
                    "ImageBase64", oldValue, pbox.XmlData.InnerText);

                mi.AddAction(actReplaceImage);
            }

            manager.RegisterModifingItem(mi);
            manager.MasterWindow.MainScrollViewer.Focus();

            if (skipedInnerIcons > 0)
            {
                MessageBox.Show(string.Format(
                    "选定的部件中包括【{0}】个不允许更换图片的内置图标。已忽略对这些内置图标的操作。",
                    skipedInnerIcons), Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MiExpandImage_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetIsImageCollapseOffCommand.Execute());
        }

        private void MiCollapseImage_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetIsImageCollapseOnCommand.Execute());
        }

        private void MiSetXamlIcon_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetXamlIconCommand.Execute());
        }

        private void BtnTextAtTop_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPictureBoxTextDockCommand.Execute(System.Windows.Controls.Dock.Top));
        }

        private void BtnTextAtBottom_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPictureBoxTextDockCommand.Execute(System.Windows.Controls.Dock.Bottom));
        }

        private void BtnTextAtLeft_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPictureBoxTextDockCommand.Execute(System.Windows.Controls.Dock.Left));
        }

        private void BtnTextAtRight_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetPictureBoxTextDockCommand.Execute(System.Windows.Controls.Dock.Right));
        }

        private void sliderOfZoom_Drop(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            double zoomValue = 0;
            if (sliderOfZoom.Value == 0)
            {
                zoomValue = 1;
                ZoomValueTextBlock.Text = "1";
            }
            else if (sliderOfZoom.Value < 0)
            {
                zoomValue = 1 / (Math.Abs(sliderOfZoom.Value) + 1);
                ZoomValueTextBlock.Text = "1/" + (Math.Abs(sliderOfZoom.Value) + 1).ToString();
            }
            else
            {
                zoomValue = sliderOfZoom.Value + 1;
                ZoomValueTextBlock.Text = zoomValue.ToString();
            }

            var result = Commands.SetIsImageZoomCommand.Execute(zoomValue);
            if (result != null && result != string.Empty)
            {
                LunarMessage.Warning(result);
            }
        }

        #region 下面这几个是内容部件公共菜单项目
        private void MiRect_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Rect));
        }

        private void MiRhomb_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Rhomb));
        }

        private void MiEllipse_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Ellipse));
        }

        private void MiRoundRect_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.RoundRect));
        }

        private void MiSixSideShape_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.SixSideShape));
        }

        private void MiEightSideShape_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.EightSidedShape));
        }

        private void MiCross_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Cross));
        }

        private void MiLeftParallelogram_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.LeftParallelogram));
        }

        private void MiRightParallelogram_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.RightParallelogram));
        }

        private void MiFillArrowLeft_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowLeft));
        }

        private void MiFillArrowRight_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowRight));
        }

        private void MiFillArrowTop_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowTop));
        }

        private void MiFillArrowBottom_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowBottom));
        }

        private void MiArrowLeft_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowLeft));
        }

        private void MiArrowRight_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowRight));
        }

        private void MiArrowTop_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowTop));
        }

        private void MiArrowBottom_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowBottom));
        }

        private void MiTag_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Tag));
        }

        private void MiEmptySemicircle_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.EmptySemicircle));
        }

        private void MiSemicircle_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Semicircle));
        }

        private void MiEmptyTriangle_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.EmptyTriangle));
        }

        private void MiTriangle_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Triangle));
        }

        private void MiLeftPencial_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.LeftPencial));
        }

        private void MiRightPencial_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.RightPencial));
        }

        private void MiTrapezoid_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Trapezoid));
        }

        private void MiAntiTrapezoid_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.AntiTrapezoid));
        }

        #endregion

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
