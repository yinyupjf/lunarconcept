﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年4月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用以为ContentWidget提供上、下注释文本。
    /// </summary>
    public class WidgetCommentAdorner : Adorner
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入Widget的主子元素。</param>
        /// <param name="masterWidget">使用此部件装饰器的部件。</param>
        /// <param name="commentLocation">备注文本显示在部件上方还是下侧。</param>
        public WidgetCommentAdorner(UIElement adornedElement,
            Widgets.ContentWidget masterWidget,
            Location commentLocation)
            : base(adornedElement)
        {
            this.masterWidget = masterWidget;
            this.commentLocation = commentLocation;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private Widgets.ContentWidget masterWidget;
        /// <summary>
        /// [只读]使用此装饰器的部件。
        /// </summary>
        public Widgets.ContentWidget MasterWidget
        {
            get { return masterWidget; }
        }

        public enum Location { Top, Bottom }

        private Location commentLocation = Location.Top;

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 绘制装饰器。
        /// </summary>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Visibility != Visibility.Visible) return;

            if (this.masterWidget == null) return;

            if (this.commentLocation == Location.Top &&
                (this.masterWidget.CommentTopText == null || this.masterWidget.CommentTopText.Length <= 0)) return;
            else if (this.commentLocation == Location.Bottom &&
                (this.masterWidget.CommentBottomText == null || this.masterWidget.CommentBottomText.Length <= 0)) return;

            if (this.masterWidget.MasterEditor == null) return;
            if (this.masterWidget.MasterEditor.MasterManager == null) return;
            if (this.masterWidget.MasterEditor.MasterManager.MasterWindow == null) return;

            Rect adornedElementRect = new Rect(this.AdornedElement.RenderSize);

            TextFormattingMode tfm = TextOptions.GetTextFormattingMode(
                this.masterWidget.MasterEditor.MasterWindow.MainScrollViewer);

            if (this.commentLocation == Location.Top)
            {
                drawingContext.DrawText(
                    new FormattedText(
                        this.masterWidget.CommentTopText,
                        new System.Globalization.CultureInfo("zh-CN"),
                        System.Windows.FlowDirection.LeftToRight,
                        new Typeface(
                            Globals.MainWindow.FontFamily,
                            FontStyles.Normal,
                            FontWeights.Normal,
                            FontStretches.Normal
                        ),
                        masterWidget.MasterEditor.MasterManager.DefaultFontSize,
                        masterWidget.WidgetForeColor,
                        null, tfm, VisualTreeHelper.GetDpi(Globals.MainWindow).PixelsPerDip
                    ),
                   new Point(adornedElementRect.TopLeft.X,
                       adornedElementRect.TopLeft.Y - masterWidget.MasterEditor.MasterManager.DefaultFontSize - 6)
                );
            }
            else
            {
                drawingContext.DrawText(
                    new FormattedText(
                        this.masterWidget.CommentBottomText,
                        new System.Globalization.CultureInfo("zh-CN"),
                        System.Windows.FlowDirection.LeftToRight,
                        new Typeface(
                            Globals.MainWindow.FontFamily,
                            FontStyles.Normal,
                            FontWeights.Normal,
                            FontStretches.Normal
                        ),
                        masterWidget.MasterEditor.MasterManager.DefaultFontSize,
                        masterWidget.WidgetForeColor,
                        null, tfm, VisualTreeHelper.GetDpi(Globals.MainWindow).PixelsPerDip
                    ),
                   new Point(adornedElementRect.BottomLeft.X,
                       adornedElementRect.BottomLeft.Y + 4)
                );
            }
        }

        #endregion
    }
}
