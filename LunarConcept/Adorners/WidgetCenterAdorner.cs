﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年1月26日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：为ConetneWidget和ShapeWidget提供一个中心装饰器，用以拖出关系线和另一个节点。
    /// </summary>
    public class WidgetCenterAdorner : Adorner
    {
        /// <summary>
        /// 用于内容部件的控制点。
        /// </summary>
        public WidgetCenterAdorner(UIElement adornedElement, Widgets.Widget masterWidget, Brush brush)
            : base(adornedElement)
        {
            this.masterWidget = masterWidget;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = false;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            this.mainSelectedBrush = brush;
            this.renderPen = new Pen(this.mainSelectedBrush, 1);
            this.renderPen2 = new Pen(this.mainSelectedBrush, 1) { DashCap = PenLineCap.Flat, DashStyle = DashStyles.Dash, };
            this.Opacity = 0.75;
            //this.Cursor = System.Windows.Input.Cursors.SizeAll;

            this.MouseLeftButtonDown +=
                new System.Windows.Input.MouseButtonEventHandler(LineCtrlAdorner_MouseLeftButtonDown);
            this.MouseEnter += new System.Windows.Input.MouseEventHandler(WidgetCenterAdorner_MouseEnter);
            this.MouseLeave += new System.Windows.Input.MouseEventHandler(WidgetCenterAdorner_MouseLeave);
        }

        void WidgetCenterAdorner_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void WidgetCenterAdorner_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void LineCtrlAdorner_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnDoubleClicked(sender, e);
            }
        }

        public event EventHandler<System.Windows.Input.MouseButtonEventArgs> DoubleClicked;

        protected void OnDoubleClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DoubleClicked != null)
            {
                DoubleClicked(sender, e);
            }
        }

        private Brush mainSelectedBrush;

        private Widgets.Widget masterWidget = null;

        private Pen renderPen;

        private Pen renderPen2;

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (masterWidget == null || masterWidget.IsInGroup) return;

            Rect rect;

            if (masterWidget is Widgets.RhombWidget)
            {
                rect = masterWidget.OuterRect;//RhombWidget.RenderSize总是以(0,0)为左上角。
            }
            else if (masterWidget is Widgets.TriangleWidget)
            {
                rect = masterWidget.OuterRect;//TriangleWidget的外框也很特殊。
            }
            else
            {
                rect = new Rect(AdornedElement.RenderSize);
            }

            Point center = new Point(rect.Left + rect.Width / 2, rect.Top + rect.Height / 2);


            if (IsMouseOver)// || masterWidget.IsMainSelected)//这个会导致当前文本块中的文本看不清楚。
            {
                #region 画四叶形
                //PathGeometry baseGeometry = new PathGeometry();
                //PathFigure pfLeaf = new PathFigure() { IsClosed = false, };
                //pfLeaf.StartPoint = rect.TopLeft;
                //QuadraticBezierSegment psLeftBezier = new QuadraticBezierSegment() { IsSmoothJoin = true, IsStroked = true };
                //psLeftBezier.Point1 = new Point(rect.Right, center.Y);
                //psLeftBezier.Point2 = rect.BottomLeft;
                //pfLeaf.Segments.Add(psLeftBezier);

                ////PathFigure pfBottomBezier = new PathFigure() { IsClosed = false, };
                ////pfBottomBezier.StartPoint = rect.BottomLeft;
                //QuadraticBezierSegment psBottomBezier = new QuadraticBezierSegment() { IsSmoothJoin = true, IsStroked = true };
                //psBottomBezier.Point1 = new Point(center.X, rect.Top);
                //psBottomBezier.Point2 = rect.BottomRight;
                //pfLeaf.Segments.Add(psBottomBezier);
                ////geometry.Figures.Add(pfBottomBezier);

                ////PathFigure pfRightBezier = new PathFigure() { IsClosed = false, };
                ////pfRightBezier.StartPoint = rect.TopRight;
                //QuadraticBezierSegment psRightBezier = new QuadraticBezierSegment() { IsSmoothJoin = true, IsStroked = true };
                //psRightBezier.Point1 = new Point(rect.Left, center.Y);
                //psRightBezier.Point2 = rect.TopRight;
                //pfLeaf.Segments.Add(psRightBezier);
                ////geometry.Figures.Add(pfRightBezier);

                ////PathFigure pfTopBezier = new PathFigure() { IsClosed = false, };
                ////pfTopBezier.StartPoint = rect.TopLeft;
                //QuadraticBezierSegment psTopBezier = new QuadraticBezierSegment() { IsSmoothJoin = true, IsStroked = true };
                //psTopBezier.Point1 = new Point(center.X, rect.Bottom);
                //psTopBezier.Point2 = rect.TopLeft;
                //pfLeaf.Segments.Add(psTopBezier);
                ////geometry.Figures.Add(pfTopBezier);

                //baseGeometry.Figures.Add(pfLeaf);
                //drawingContext.DrawGeometry(Brushes.White, renderPen, baseGeometry);
                #endregion

                #region 画风车形

                if (rect.Width > 40 && rect.Height > 40)
                {
                    PathGeometry baseGeometry = new PathGeometry();
                    PathFigure pfLeaf = new PathFigure() { IsClosed = true, IsFilled = true, };
                    pfLeaf.StartPoint = rect.TopLeft;

                    PolyLineSegment psLeftPoly = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                    psLeftPoly.Points.Add(new Point(rect.Right, center.Y));
                    psLeftPoly.Points.Add(rect.BottomLeft);
                    pfLeaf.Segments.Add(psLeftPoly);

                    var psBottomPoly = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                    psBottomPoly.Points.Add(new Point(center.X, rect.Top));
                    psBottomPoly.Points.Add(rect.BottomRight);
                    pfLeaf.Segments.Add(psBottomPoly);

                    var psRightPoly = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                    psRightPoly.Points.Add(new Point(rect.Left, center.Y));
                    psRightPoly.Points.Add(rect.TopRight);
                    pfLeaf.Segments.Add(psRightPoly);

                    var psTopPoly = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                    psTopPoly.Points.Add(new Point(center.X, rect.Bottom));
                    psTopPoly.Points.Add(rect.TopLeft);
                    pfLeaf.Segments.Add(psTopPoly);

                    baseGeometry.Figures.Add(pfLeaf);
                    drawingContext.DrawGeometry(Brushes.LightGray, null, baseGeometry);
                }
                #endregion

                #region 画中心背景圆角矩形
                var centerRect = new Rect(center.X - 9, center.Y - 9, 18, 18);
                drawingContext.DrawRoundedRectangle(mainSelectedBrush, renderPen, centerRect, 5, 5);
                #endregion

                #region 画中心指示器
                PathGeometry geometry = new PathGeometry();

                PathFigure pfUpArrow = new PathFigure();
                pfUpArrow.StartPoint = new Point(center.X - 3, center.Y - 3);
                PolyLineSegment psUpArrow = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                psUpArrow.Points = new PointCollection()
                {
                    new Point(center.X,center.Y - 6),
                    new Point(center.X + 3,center.Y - 3),
                };
                pfUpArrow.Segments.Add(psUpArrow);
                geometry.Figures.Add(pfUpArrow);

                PathFigure pfLeftArrow = new PathFigure();
                pfLeftArrow.StartPoint = new Point(center.X - 3, center.Y + 3);
                PolyLineSegment psLeftArrow = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                psLeftArrow.Points = new PointCollection()
                {
                    new Point(center.X - 6,center.Y),
                    new Point(center.X - 3,center.Y - 3),
                };
                pfLeftArrow.Segments.Add(psLeftArrow);
                geometry.Figures.Add(pfLeftArrow);

                PathFigure pfDownArrow = new PathFigure();
                pfDownArrow.StartPoint = new Point(center.X + 3, center.Y + 3);
                PolyLineSegment psDownArrow = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                psDownArrow.Points = new PointCollection()
                {
                    new Point(center.X,center.Y + 6),
                    new Point(center.X - 3,center.Y + 3),
                };
                pfDownArrow.Segments.Add(psDownArrow);
                geometry.Figures.Add(pfDownArrow);

                PathFigure pfRightArrow = new PathFigure();
                pfRightArrow.StartPoint = new Point(center.X + 3, center.Y - 3);
                PolyLineSegment psRightArrw = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
                psRightArrw.Points = new PointCollection()
                {
                    new Point(center.X + 6,center.Y),
                    new Point(center.X + 3,center.Y + 3),
                };
                pfRightArrow.Segments.Add(psRightArrw);
                geometry.Figures.Add(pfRightArrow);
                drawingContext.DrawGeometry(Brushes.White, renderPen, geometry);

                #endregion
            }
            else
            {
                drawingContext.DrawRoundedRectangle(Brushes.Transparent, new Pen(Brushes.Transparent, 1),
                    new Rect(center.X - 9, center.Y - 9, 18, 18), 5, 5);
            }
        }
    }
}