﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年7月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：TextHidedAdorner用在部件文本被隐藏时显示一个圆角矩形。
    /// </summary>
    public class TextHidedAdorner : Adorner
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入Widget.MainTextPanel。</param>
        public TextHidedAdorner(StackPanel adornedElement)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            //this.SnapsToDevicePixels = true;
            //this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);

            this.textPanel = adornedElement;

            this.ToolTip = tooltip;
            this.Opacity = 0.5;

            this.Cursor = Cursors.Arrow;
            this.MouseEnter += new MouseEventHandler(MaskAdorner_MouseEnter);
            this.MouseLeave += new MouseEventHandler(MaskAdorner_MouseLeave);
        }

        private static readonly string tooltip = "部件文本被隐藏";

        void MaskAdorner_MouseLeave(object sender, MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void MaskAdorner_MouseEnter(object sender, MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        private StackPanel textPanel;

        public StackPanel TextPanel
        {
            get { return textPanel; }
            set { textPanel = value; }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (textPanel == null) return;


            Rect rect = new Rect(this.AdornedElement.RenderSize);
            if (rect.Width <= 0) return;

            //rect.X -= 4; rect.Y -= 4; rect.Width += 8; rect.Height += 8;
            //rect.Width += 1; rect.Height += 1;//改画笔宽为２，就不需要加１了。


            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;

            Brush bsh = manager.WidgetSelectionAdornerBrush;
            Pen pen = new Pen(bsh, 2) { DashStyle = DashStyles.Solid };

            TextFormattingMode tfm = (TextFormattingMode)this.GetValue(TextOptions.TextFormattingModeProperty);
            if (tfm == TextFormattingMode.Display)
            {
                this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            }
            else
            {
                this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            }

            //drawingContext.DrawRoundedRectangle(Brushes.Transparent, pen, rect, 4, 4);

            drawingContext.DrawRectangle(null, pen, rect);
        }
    }
}