﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using System;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2016年1月
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：RectangleWidget使用的标记装饰器。用于将一个矩形定义为一个演示区域。
    /// 
    /// </summary>
    public class PresentationAreaAdorner : Adorner
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入RectangleWidget的Canvas。</param>
        public PresentationAreaAdorner(UIElement adornedElement, Widgets.RectangleWidget rectangleWidget)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);

            this.masterRectangleWidget = rectangleWidget;

            this.Cursor = Cursors.Hand;
        }

        private Widgets.RectangleWidget masterRectangleWidget;

        public Widgets.RectangleWidget MasterRectangleWidget
        {
            get { return masterRectangleWidget; }
            set { masterRectangleWidget = value; }
        }

        public Rect MasterRect { get; internal set; }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (masterRectangleWidget == null) return;

            if (this.masterRectangleWidget.MasterEditor == null) return;
            if (this.masterRectangleWidget.MasterEditor.MasterManager == null) return;
            if (this.masterRectangleWidget.MasterEditor.MasterManager.MasterWindow == null) return;

            double defFontSize = this.masterRectangleWidget.MasterEditor.MasterManager.DefaultFontSize;

            Typeface typeface;

            if (defFontSize > 15)
            {
                typeface = new Typeface(
                            font_YaHei,
                            FontStyles.Normal,
                            FontWeights.Bold,
                            FontStretches.Normal
                        );
            }
            else
            {
                typeface = new Typeface(
                            font_SimSun,
                            FontStyles.Normal,
                            FontWeights.Normal,
                            FontStretches.Normal
                        );
            }

            #region 旧的太复杂，界面混乱不堪。

            //要考虑线宽。
            double lineWidth = this.masterRectangleWidget.WidgetLineWidth;

            var adornedElementRect = this.MasterRect;

            //Brush baseBrush = FindResource("PresentationAdornerBackgroundImageBrush") as Brush;
            //var pen = new Pen(baseBrush, 10);

            //Point newTopLeft = new Point(adornedElementRect.TopLeft.X - 10, adornedElementRect.TopLeft.Y - 10);
            //Point ptTmp = new Point(adornedElementRect.BottomRight.X + 10, adornedElementRect.BottomRight.Y + 10);
            //Rect lineRect = new Rect(newTopLeft, ptTmp);
            //drawingContext.DrawRectangle(null, pen, lineRect);

            ////画些白色的小方格，使之看起来像胶卷
            //double startLeft = 0;
            //double startTop = 0;
            //double w = 0;
            //double h = 0;

            //if (lineRect.Height >= 20)
            //{
            //    var count = (int)((lineRect.Height - 20 + 2) / 6);
            //    var realHeight = count * 6 - 2;
            //    h = (int)((lineRect.Height - 20 - realHeight) / 2);
            //    startTop = lineRect.Top + 10 + h;

            //    var top = startTop;
            //    for (int i = 0; i < count; i++)
            //    {
            //        drawingContext.DrawRectangle(Brushes.White, null, new Rect(lineRect.Right - 3, top, 6, 4));
            //        drawingContext.DrawRectangle(Brushes.White, null, new Rect(lineRect.Left - 3, top, 6, 4));
            //        top += 6;
            //    }
            //}

            //if (lineRect.Width >= 20)
            //{
            //    var count = (int)((lineRect.Width - 20 + 2) / 6);
            //    var realWidth = count * 6 - 2;
            //    w = (int)((lineRect.Width - 20 - realWidth) / 2);
            //    startLeft = lineRect.Left + 10 + w;

            //    var left = startLeft;
            //    for (int i = 0; i < count; i++)
            //    {
            //        drawingContext.DrawRectangle(Brushes.White, null, new Rect(left, lineRect.Top - 3, 4, 6));
            //        drawingContext.DrawRectangle(Brushes.White, null, new Rect(left, lineRect.Bottom - 3, 4, 6));
            //        left += 6;
            //    }
            //}

            //if (lineRect.Height >= 10 && lineRect.Width >= 10)
            //{
            //    drawingContext.DrawRectangle(Brushes.White, null, new Rect(lineRect.Left - 3, lineRect.Top - 3, 6, 6));
            //    drawingContext.DrawRectangle(Brushes.White, null, new Rect(lineRect.Right - 3, lineRect.Top - 3, 6, 6));
            //    drawingContext.DrawRectangle(Brushes.White, null, new Rect(lineRect.Left - 3, lineRect.Bottom - 3, 6, 6));
            //    drawingContext.DrawRectangle(Brushes.White, null, new Rect(lineRect.Right - 3, lineRect.Bottom - 3, 6, 6));
            //}
            #endregion

            // 新方案，画个七彩矩形即可。
            Brush baseBrush = BrushManager.SevenColors;
            var pen = new Pen(baseBrush, 1);
            Point newTopLeft = new Point(adornedElementRect.TopLeft.X - 2, adornedElementRect.TopLeft.Y - 2);
            Point ptTmp = new Point(adornedElementRect.BottomRight.X + 2, adornedElementRect.BottomRight.Y + 2);
            Rect lineRect = new Rect(newTopLeft, ptTmp);
            drawingContext.DrawRectangle(null, pen, lineRect);
        }

        static PresentationAreaAdorner()
        {
            font_SimSun = new FontFamily("SimSun");
            font_YaHei = new FontFamily("Microsoft YaHei");
        }

        private static FontFamily font_YaHei;
        private static FontFamily font_SimSun;
    }
}