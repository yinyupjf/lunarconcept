﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年7月14日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于显示部件的备注。
    /// </summary>
    public class CommentAdorner : Adorner
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement"></param>
        public CommentAdorner(UIElement adornedElement, Widgets.Widget masterWidget)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            //this.SnapsToDevicePixels = true;
            //this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            this.masterWidget = masterWidget;

            this.Cursor = Cursors.Arrow;
            //this.MouseEnter += new MouseEventHandler(MaskAdorner_MouseEnter);
            //this.MouseLeave += new MouseEventHandler(MaskAdorner_MouseLeave);
            this.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(CommentAdorner_PreviewMouseLeftButtonDown);
        }

        void CommentAdorner_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.masterWidget != null && this.masterWidget.MasterEditor != null)
            {
                this.masterWidget.MasterEditor.MouseInfo.DraggingType =
                    PageDraggingType.MoveWidgets;//避免移动插入点。
            }
        }

        //void MaskAdorner_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    this.InvalidateVisual();
        //}

        //void MaskAdorner_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    this.InvalidateVisual();
        //}

        private Widgets.Widget masterWidget;

        public Widgets.Widget MasterWidget
        {
            get { return masterWidget; }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            Rect rect;

            Widgets.ArrowLineWidget alw = masterWidget as Widgets.ArrowLineWidget;
            if (alw != null)
            {
                rect = alw.OuterRect;
            }
            else
            {
                Widgets.ContentWidget cw = masterWidget as Widgets.ContentWidget;
                if (cw != null ||
                    masterWidget is Widgets.EllipseWidget ||
                    masterWidget is Widgets.RectangleWidget)
                {
                    rect = new Rect(this.AdornedElement.RenderSize);
                }
                else
                {
                    rect = masterWidget.OuterRect;//ShapeWidget
                }
            }

            Pen pen = new Pen(Brushes.Black, 0) { DashStyle = DashStyles.Solid };

            Brush bsh = TryFindResource("Pencil") as VisualBrush;

            if (bsh != null)
            {

                double defFontSize = this.MasterWidget.MasterEditor.MasterManager.DefaultFontSize;

                Typeface typeface;

                if (defFontSize > 15)
                {
                    typeface = new Typeface(
                                font_YaHei,
                                FontStyles.Normal,
                                FontWeights.Bold,
                                FontStretches.Normal
                            );
                }
                else
                {
                    typeface = new Typeface(
                                font_SimSun,
                                FontStyles.Normal,
                                FontWeights.Normal,
                                FontStretches.Normal
                            );
                }

                TextFormattingMode tfm = TextFormattingMode.Display;

                FormattedText formattedText =
                            new FormattedText(
                                this.MasterWidget.CommentText,
                                new System.Globalization.CultureInfo("zh-CN"),
                                System.Windows.FlowDirection.LeftToRight,
                                typeface, 16, Brushes.Black, null, tfm, VisualTreeHelper.GetDpi(Globals.MainWindow).PixelsPerDip
                            );

                drawingContext.DrawText(formattedText, new Point(rect.Right + 36, rect.Bottom - 16));
                drawingContext.DrawRectangle(bsh, pen, new Rect(rect.Right + 18, rect.Bottom - 16, 18, 18));
            }

            //drawingContext.DrawRectangle(Brushes.LightYellow, pen,
            //    new Rect(rect.Right + 18, rect.Bottom - 16, 19, 16));

            //drawingContext.DrawLine(pen, new Point(rect.Right + 20, rect.Bottom - 12),
            //    new Point(rect.Right + 34, rect.Bottom - 12));

            //drawingContext.DrawLine(pen, new Point(rect.Right + 20, rect.Bottom - 8),
            //    new Point(rect.Right + 34, rect.Bottom - 8));

            //drawingContext.DrawLine(pen, new Point(rect.Right + 20, rect.Bottom - 4),
            //    new Point(rect.Right + 30, rect.Bottom - 4));
        }

        static CommentAdorner()
        {
            //customFont = new FontFamily(new Uri("pack://application:,,,/"), "./Resources/#SubtleSansRegular");
            font_SimSun = new FontFamily("SimSun");
            font_YaHei = new FontFamily("Microsoft YaHei");
        }

        private static FontFamily font_YaHei;
        private static FontFamily font_SimSun;
    }
}