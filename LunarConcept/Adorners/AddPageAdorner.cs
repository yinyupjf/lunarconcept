﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2018年02月17日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用来添加新页面的装饰器——省去切换Ribbon选项卡。
    /// </summary>
    public class AddPageAdorner : Adorner
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入PageEditor的BaseCanvas。</param>
        /// <param name="masterPageEditor">使用此装饰器的页面编辑器。</param>
        public AddPageAdorner(UIElement adornedElement, PageEditor masterPageEditor)
            : base(adornedElement)
        {
            this.masterPageEditor = masterPageEditor;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private PageEditor masterPageEditor;
        /// <summary>
        /// [只读]使用此装饰器的页面。
        /// </summary>
        public PageEditor MasterPageEditor
        {
            get { return masterPageEditor; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 绘制装饰器。
        /// </summary>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Visibility != Visibility.Visible) return;

            if (this.masterPageEditor.MasterManager == null || this.masterPageEditor == null) return;

            if (this.masterPageEditor.IsMainSelected == false) return;

            Rect adornedElementRect = new Rect(this.AdornedElement.RenderSize);

            Rect outRect;

            var blackPen = new Pen(Brushes.Black, 1);
            switch (this.masterPageEditor.MasterManager.Direction)
            {
                case System.Windows.Controls.Orientation.Horizontal:
                    {
                        outRect = new Rect(adornedElementRect.Right + 1, adornedElementRect.Bottom - 45, 20, 20);
                        drawingContext.DrawRectangle(Brushes.SkyBlue, new Pen(Brushes.Blue, 1), outRect);

                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 7, outRect.Top + 2, 6, 16));
                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 2, outRect.Top + 7, 16, 6));

                        break;
                    }
                default:
                    {
                        outRect = new Rect(adornedElementRect.Right - 45, adornedElementRect.Bottom + 1, 20, 20);
                        drawingContext.DrawRectangle(Brushes.SkyBlue, new Pen(Brushes.Blue, 1), outRect);

                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 7, outRect.Top + 2, 6, 16));
                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 2, outRect.Top + 7, 16, 6));

                        break;
                    }
            }

            var ptStart = new Point(outRect.Left + 8, outRect.Top + 8);   // 用一个segment 会出现结束少一个像素的问题。现在分成四个方括号（围成一个空心加号）来试试。

            var geo = new PathGeometry();
            var figure = new PathFigure();
            figure.StartPoint = ptStart;
            
            var segment1 = new PolyLineSegment();
            segment1.Points.Add(ptStart);
            segment1.Points.Add(new Point(outRect.Left + 8, outRect.Top + 4));
            segment1.Points.Add(new Point(outRect.Left + 12, outRect.Top + 4));
            segment1.Points.Add(new Point(outRect.Left + 12, outRect.Top + 8));
            figure.Segments.Add(segment1);

            var segment2 = new PolyLineSegment();
            segment2.Points.Add(new Point(outRect.Left + 16, outRect.Top + 8));
            segment2.Points.Add(new Point(outRect.Left + 16, outRect.Top + 12));
            segment2.Points.Add(new Point(outRect.Left + 12, outRect.Top + 12));
            figure.Segments.Add(segment2);

            var segment3 = new PolyLineSegment();
            segment3.Points.Add(new Point(outRect.Left + 12, outRect.Top + 16));
            segment3.Points.Add(new Point(outRect.Left + 8, outRect.Top + 16));
            segment3.Points.Add(new Point(outRect.Left + 8, outRect.Top + 12));
            figure.Segments.Add(segment3);

            var segment4 = new PolyLineSegment();
            segment4.Points.Add(new Point(outRect.Left + 4, outRect.Top + 12));
            segment4.Points.Add(new Point(outRect.Left + 4, outRect.Top + 8));
            segment4.Points.Add(new Point(outRect.Left + 8, outRect.Top + 8));
            figure.Segments.Add(segment4);

            figure.IsClosed = true;
            figure.IsFilled = true;
            geo.Figures.Add(figure);

            drawingContext.DrawGeometry(Brushes.White, blackPen, geo);
        }

        #endregion
    }
}
