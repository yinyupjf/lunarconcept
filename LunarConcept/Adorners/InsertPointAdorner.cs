﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2011年12月29日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：为每个PageEditor提供一个“插入点（靶标）”，表示当前“插入点”的位置。
    /// </summary>
    public class InsertPointAdorner : Adorner
    {
        private static ImageSource iSrc;

        static InsertPointAdorner()
        {
            iSrc = new BitmapImage(
                new Uri("pack://Application:,,,/LunarConcept;component/Images/InsertPointHS.png"));
        }

        /// <summary>
        /// 用在主界面的靶标（表示插入点）。
        /// </summary>
        /// <param name="adornedElement">应传入备注文本块。</param>
        /// <param name="win">应传入主窗口。</param>
        public InsertPointAdorner(UIElement adornedElement, PageEditor pageEditor)
            : base(adornedElement)
        {
            this.pageEditor = pageEditor;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            this.ToolTip = "插入点";

            this.MouseDown += new System.Windows.Input.MouseButtonEventHandler(InsertPointAdorner_MouseDown);
        }

        void InsertPointAdorner_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.pageEditor == null) return;
            //TODO: 以插入点为中心滚动主界面还未做。
        }

        private PageEditor pageEditor = null;

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (pageEditor == null) return;

            Point centerPoint = pageEditor.BaseInsertPoint;

            //手绘总是有问题——因为double型在单像素对齐上不准
            drawingContext.DrawImage(iSrc,
                new Rect(centerPoint.X - 15, centerPoint.Y - 15, 15, 15));
        }
    }
}