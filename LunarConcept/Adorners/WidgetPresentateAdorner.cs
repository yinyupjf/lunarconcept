﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2021年3月30日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用以在演示时点击显示连接线。
    /// </summary>
    public class WidgetPresentateAdorner : Adorner
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入Widget的主子元素。</param>
        /// <param name="masterWidget">使用此部件装饰器的部件。</param>
        public WidgetPresentateAdorner(UIElement adornedElement, Widget masterWidget, Brush defaultBrush)
            : base(adornedElement)
        {
            this.masterWidget = masterWidget;
            this.defaultBrush = defaultBrush;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private Widget masterWidget;
        /// <summary>
        /// [只读]使用此装饰器的部件。
        /// </summary>
        public Widget MasterWidget
        {
            get { return masterWidget; }
        }

        private Brush defaultBrush;

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 绘制装饰器。
        /// </summary>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Visibility != Visibility.Visible) return;

            if (this.masterWidget == null || this.masterWidget == null) return;

            if (Globals.MainWindow.IsPresentatingByPath == false) return;

            Point pt;

            if (defaultBrush == Brushes.Red)
            {
                pt = (this.masterWidget as Widgets.Interfaces.ILinkableLine).StartPoint;
                this.Opacity = 0.4;

                pt.X -= 10;
                pt.Y -= 10;

                drawingContext.DrawRectangle(Brushes.Transparent, new Pen(Tools.BrushManager.SevenColors, 2), new Rect(pt.X, pt.Y, 20, 20));
            }
            else
            {
                defaultBrush = Brushes.Transparent;
                pt = (this.masterWidget as Widgets.Interfaces.ILinkableLine).EndPoint;
                this.Opacity = 0.4;

                pt.X -= 14;
                pt.Y -= 14;

                var ptStart = new Point(pt.X + 14, pt.Y);
                //drawingContext.DrawRectangle(defaultBrush, new Pen(Tools.BrushManager.SevenColors, 2), new Rect(pt.X, pt.Y, 20, 20));
                var geo = new PathGeometry();
                var figure = new PathFigure();
                figure.StartPoint = ptStart;
                var segment = new PolyLineSegment();
                segment.Points.Add(ptStart);
                segment.Points.Add(new Point(pt.X, pt.Y + 14));
                segment.Points.Add(new Point(pt.X + 14, pt.Y + 28));
                segment.Points.Add(new Point(pt.X + 28, pt.Y + 14));
                figure.IsClosed = true;
                figure.Segments.Add(segment);
                geo.Figures.Add(figure);

                drawingContext.DrawGeometry(Brushes.Transparent, new Pen(Tools.BrushManager.SevenColors, 2), geo);
            }

        }

        #endregion
    }
}
