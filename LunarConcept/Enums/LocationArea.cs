﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2018年3月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于判断某个点在矩形的四正、四隅位置。
    /// </summary>
    public enum LocationArea
    {
        UnKnown,
        Left,
        LeftTop,
        Top,
        RightTop,
        Right,
        RightBottom,
        Bottom,
        LeftBottom,
        In,
    }
}
