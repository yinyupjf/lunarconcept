﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年9月29日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：只用于三角形。
    /// </summary>
    public enum TriangleForm
    {
        Triangle,           //三角形
        Pie,                //饼形
        Sector,             //扇形
        Bracket,            //花括号，这与 Bracket 部件有很大的区别——Bracket 不支持斜花括号。
        TLine,              //绘制首尾点连线和中控点向首尾点连线的垂线。
        OrgLine,            //类似组织结构图连接线。
        YLine,              //Y形线。
        DialogBubble,       //对话框气泡。2021年4月9日
        ThinkingBubble,           //思考云对话框。2021年4月12日
    }
}
