﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 移植时间：2012年5月30日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于线型部件，区别拖动哪个控制点。
    /// </summary>
    public enum ControlDraggingType { Start, End, CenterCP, StartCP, EndCP, None }
}
