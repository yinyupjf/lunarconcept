﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年1月4日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于线型部件，定义箭头方式。但对圆、矩形没有意义。
    /// </summary>
    public enum ArrowType { 
        /// <summary>
        /// 首箭头。
        /// </summary>
        Start,
        /// <summary>
        /// 尾箭头。
        /// </summary>
        End,
        /// <summary>
        /// 首尾箭头。
        /// </summary>
        All,
        /// <summary>
        /// 无箭头。
        /// </summary>
        None,
        /// <summary>
        /// 空心首箭头。
        /// </summary>
        EmptyStart,
        /// <summary>
        /// 菱形首标记。
        /// </summary>
        RhombStart,
        /// <summary>
        /// 空菱形首标记。
        /// </summary>
        EmptyRhombStart,
        /// <summary>
        /// 尾翼形首标记。
        /// </summary>
        Arrow,
        /// <summary>
        /// 空尾翼形首标记。
        /// </summary>
        EmptyArrow,
        /// <summary>
        /// 只有两条线的箭头。
        /// </summary>
        OpenArrowEnd,
        
    }
}
