﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年7月13日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：目前只用于菱形部件。部件有多个形态。
    /// </summary>
    public enum WidgetForm
    {
        Rhomb,                      //默认值，菱形
        LeftParallelogram,          //左右边向左下倾斜平行四边形
        RightParallelogram,         //左右边向右下倾斜平行四边形
        SixSideShape,               //六边形
        EightSidedShape,            //八边形
        Cross,                      //十字形（八边形四斜边凹进）
        Star,                       //星形

        LeftFletchingArrow,         //四向箭羽形
        RightFletchingArrow,
        TopFletchingArrow,
        BottomFletchingArrow,

        LeftFillArrow,              //另一边为直线的四向箭头
        RightFillArrow,
        TopFillArrow,
        BottomFillArrow,

        LeftTailArrow,              //近似三角形的箭头（非指向边凹进）
        RightTailArrow,
        TopTailArrow,
        BottomTailArrow,

        Rectangle,                  //提供两种矩形是有必要的 2020年3月21日

        LeftTag,                    //左缺角标签
        RightTag,                   //右缺角标签

        TopTrapezoid,               //上梯形
        BottomTrapezoid,            //下梯形

        UnKnown,                    //未知形态
    }
}
