﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2013年2月4日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于括弧部件。定义括弧的呈现方式。
    /// </summary>
    public enum BracketLineForms
    {
        Bracket,                        //正常的花括弧
        Brace,                          //小括弧
        HorizontalPointer,              //横向指针状
        VerticalPointer,                //纵向指针
        Square,                         //方括号
        Bezier,                         //贝塞尔曲线
    };
}
