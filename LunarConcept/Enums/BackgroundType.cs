﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 页面编辑器管理器的默认背景的类型分：图像、纯色。
    /// </summary>
    public enum BackgroundType { ImageBrush, SolidBrush, }
}
