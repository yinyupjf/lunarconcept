﻿using System.Collections.Generic;
using System.Windows.Input;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2011年12月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：自动管理自定义命令列表。
    ///           ——主窗口在初始化时，会遍历这个列表并将其中的所有命令绑定添加到主窗口的CommandBindings集合中！！！
    /// </summary>
    class CmdBindings
    {
        private static List<CommandBinding> mainWindowCmdBindingsList = new List<CommandBinding>();
        /// <summary>
        /// 2013年8月25日添加。较重要更新，将命令分为两大类：全局命令与编辑区命令。
        /// [只读]返回命令绑定列表。主窗口初始化时，
        /// 会将此列表中所有命令绑定都注册到自身的CommandBindings集合中。
        /// </summary>
        public static List<CommandBinding> MainWindowCmdBindingsList
        {
            get { return mainWindowCmdBindingsList; }
        }

        private static List<CommandBinding> editAreaCmdBindingsList = new List<CommandBinding>();
        /// <summary>
        /// [只读]返回命令绑定列表。主窗口在初始化时，
        /// 会将这个列表中的所有命令绑定都注册到【主窗口编辑区】的CommandBindings集合中。
        /// </summary>
        public static List<CommandBinding> EditAreaCmdBindingsList
        {
            get { return editAreaCmdBindingsList; }
        }

        /// <summary>
        /// [静态构造方法]在此将所有自定义命令类中自带的“命令绑定”对象
        /// 添加到“命令绑定列表（CmdBindingsList）”中。以便由主窗口的构造方法注册。
        /// </summary>
        static CmdBindings()
        {
            //TODO: 应将所有命令都添加到此处，以便主窗口的构造方法注册这些命令绑定。
            
            //这些命令分两类，一类是全局的，无论何时均应有效，例如：Ctl+N新建文档（即使焦点在备注区也一样）。
            mainWindowCmdBindingsList.Add(App_ExitCommand.CmdBinding);

            mainWindowCmdBindingsList.Add(NewDocumentCommand.CmdBinding);
            mainWindowCmdBindingsList.Add(NewDocumentFromTemplateCommand.CmdBinding);
            mainWindowCmdBindingsList.Add(OpenDocumentCommand.CmdBinding);

            mainWindowCmdBindingsList.Add(OutportSplitTIFFImagesCommand.CmdBinding);
            mainWindowCmdBindingsList.Add(OutportSplitPNGImagesCommand.CmdBinding);


            mainWindowCmdBindingsList.Add(OutportToImageFileCommand.CmdBinding);
            mainWindowCmdBindingsList.Add(OutportMainPageToImageFileCommand.CmdBinding);

            //这个必须是全局的，否则右工具栏上那个引用此命令的按钮总是处于禁用状态。
            mainWindowCmdBindingsList.Add(CopyToDataBaseCommand.CmdBinding);
            
            //缩放其实放到编辑区也可以，放这里方便一些。快捷键一般也不会与其它界面部分冲突。
            editAreaCmdBindingsList.Add(ZoomInCommand.CmdBinding);
            editAreaCmdBindingsList.Add(ZoomOutCommand.CmdBinding);
            editAreaCmdBindingsList.Add(ZoomResetCommand.CmdBinding);


            //TODO: 以下命令放在ScrollViewer中，这样可避免对其它部分产生影响。
            editAreaCmdBindingsList.Add(AlignListHorizontalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignListVerticalCommand.CmdBinding);

            editAreaCmdBindingsList.Add(AlignWidgetsDistributeHorizontalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsDistributeVerticalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToAllCenterCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToBottomCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToHorizontalCenterCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToLeftCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToRightCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToTopCommand.CmdBinding);
            editAreaCmdBindingsList.Add(AlignWidgetsToVerticalCenterCommand.CmdBinding);
            
            editAreaCmdBindingsList.Add(BuildLineMarksCommand.CmdBinding);

            editAreaCmdBindingsList.Add(ClearTextCommand.CmdBinding);
            editAreaCmdBindingsList.Add(CopyCommand.CmdBinding);
            editAreaCmdBindingsList.Add(CopyPageToImageCommand.CmdBinding);
            editAreaCmdBindingsList.Add(CopyPageToXamlCommand.CmdBinding);
            editAreaCmdBindingsList.Add(CopyToImageCommand.CmdBinding);

            editAreaCmdBindingsList.Add(QuickCopiesCommand.CmdBinding);

            editAreaCmdBindingsList.Add(CreateCopybookCommand.CmdBinding);

            editAreaCmdBindingsList.Add(CutCommand.CmdBinding);
            editAreaCmdBindingsList.Add(DeleteWidgetsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(DeletePageEditorsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(EditCommand.CmdBinding);
            editAreaCmdBindingsList.Add(FindCommand.CmdBinding);

            editAreaCmdBindingsList.Add(FlipHorizontalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(FlipVerticalCommand.CmdBinding);

            editAreaCmdBindingsList.Add(FoamatAsTableCommand.CmdBinding);
            editAreaCmdBindingsList.Add(FormatTitleCommand.CmdBinding);

            editAreaCmdBindingsList.Add(GroupWidgetCommand.CmdBinding);

            editAreaCmdBindingsList.Add(InsertNewPageEditorCommand.CmdBinding);
            editAreaCmdBindingsList.Add(InsertPageCopyEditorCommand.CmdBinding);
            editAreaCmdBindingsList.Add(InsertPictureBoxCommand.CmdBinding);
            editAreaCmdBindingsList.Add(InsertTextAreaCommand.CmdBinding);
            editAreaCmdBindingsList.Add(InsertSimilarWidgetAndEditCommand.CmdBinding);
            editAreaCmdBindingsList.Add(InsertRelationsBetweenCommand.CmdBinding);

            editAreaCmdBindingsList.Add(ListWidgetsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(LockWidgetsCommand.CmdBinding);

            editAreaCmdBindingsList.Add(MovePageEditorToNextCommand.CmdBinding);
            editAreaCmdBindingsList.Add(MovePageEditorToPreviewCommand.CmdBinding);

            //这两个是列表工具
            editAreaCmdBindingsList.Add(CauseOrEffectWidgetsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(OrganiseWidgetsCommand.CmdBinding);

            editAreaCmdBindingsList.Add(PageHeaderAndFooterCommand.CmdBinding);
            editAreaCmdBindingsList.Add(PasteCommand.CmdBinding);

            editAreaCmdBindingsList.Add(RedoCommand.CmdBinding);//Redo、Undo不能全局（否则会出问题，例如正在编辑备注文本）。

            editAreaCmdBindingsList.Add(ReplaceCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SameContentWidgetsSizeCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SameSizeHorizontalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SameSizeVerticalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SaveDocumentCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SaveDocumentAsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SaveDocumentAsTemplateCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SelectAllWidgetsInPageEditorCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SelectLinkedLinesCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SelectLinkedFromLinesCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SelectLinkedToLinesCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SelectLinkedFromWidgetsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SelectLinkedToWidgetsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SelectLinkedWidgetsCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetAssistGridFormCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetCommentBottomTextCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetCommentTopTextCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetRectangleMarkTextCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetEndLinkToCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetStartLinkToCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetCommentTextCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetHyperLinkTextCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetDefaultFontSizeCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetLineWidgetArrowsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetDocumentOptionsCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetPaperHorizontalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetPaperSizeCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetPageTitleCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetPaperVerticalCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetBackColorCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetLineColorCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetForeColorCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetLineWidthCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetLineDashTypeCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetMaskTypeCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetOpacityCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetPaddingWidthCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetIsImageZoomCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetWidgetAsGlassButtonCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetAsBubbleCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetWidgetFillBlankModeOffCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetFillBlankModeOnCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetRhombWidgetFormCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetOutBorderFormCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetTriangleFormCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetWidgetShadowOffCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetShadowOnCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetWidgetTextVisibleOnCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SetWidgetTextVisibleOffCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SizeCollapseToLeftCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SizeCollapseToTopCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SizeExpandToBottomCommand.CmdBinding);
            editAreaCmdBindingsList.Add(SizeExpandToRightCommand.CmdBinding);

            editAreaCmdBindingsList.Add(UndoCommand.CmdBinding);//Redo、Undo不能全局（否则会出问题，例如正在编辑备注文本）。

            editAreaCmdBindingsList.Add(UnGroupWidgetCommand.CmdBinding);
            editAreaCmdBindingsList.Add(WidgetMoveToBottomCommand.CmdBinding);
            editAreaCmdBindingsList.Add(WidgetMoveToLeftCommand.CmdBinding);
            editAreaCmdBindingsList.Add(WidgetMoveToRightCommand.CmdBinding);
            editAreaCmdBindingsList.Add(WidgetMoveToTopCommand.CmdBinding);

            editAreaCmdBindingsList.Add(SetOneTypeOfWisgetStyleCommand.CmdBinding);
            editAreaCmdBindingsList.Add(ResetOneTypeOfWidgetStyleCommand.CmdBinding);
            editAreaCmdBindingsList.Add(ResetAllTypeOfWidgetsStyleCommand.CmdBinding);
        }
    }
}
