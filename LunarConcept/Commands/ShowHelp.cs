﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using WinForm = System.Windows.Forms;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2011年12月28日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：显示帮助文档命令。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class ShowHelpCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static ShowHelpCommand()//类型构造器
        {
            //初始化Chm文件内部层级列表。
            ShowHelpCommand.helpPathInChmFileList = new List<string>();
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Alignmentwidgets/Alignment.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Alignmentwidgets/Flip.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Alignmentwidgets/Flip.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Alignmentwidgets/Lockwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Alignmentwidgets/Samesize.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Autolayout/Causeoreffectwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Autolayout/Listwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Autolayout/Organisewidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Buildlinemarks.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Formatastable.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Getwidgetimagedata.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Maskwidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Assists/Savetodatabase.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Color/Background.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Color/Borderground.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Color/Foreground.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Abouttextrotate.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Fillblank.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Hidewidgettext.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Linetype.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Locationandsize.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetformat/Widgetstyles.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Aboutprinting.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Addtitles.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Linkwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Mainwindow.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Movewidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Pages.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Table.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetgroup.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgetselection.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Allabilities/Widgettext.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Quickstart/Aboutexams.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Quickstart/Createoutline.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Quickstart/Drawconcept.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Quickstart/Drawfishbone.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Quickstart/Drawmindmap.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Else/_tchelp.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Else/_tcmovebaseinsertpoint.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Else/_tcselectallwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Else/_tcsetwidgetcommentatbottom.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Else/_tcsetwidgetcommentattop.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Else/_tcxml.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Find/_tcfind.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Find/_tcfindindatabase.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Find/_tcsearchpresentationareas.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Insertwidget/_tcinsertbackgroundellipse.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Insertwidget/_tcinsertbackgroundrhomb.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Insertwidget/_tcinsertinnericon.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Insertwidget/_tcinsertrectangle.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Layoutwidgets/_tcformatastable.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Layoutwidgets/_tclistwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Layoutwidgets/_tcorganisewidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setpageproties/_tcsetdefaultfontsize.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setpageproties/_tcsetpageleftsidecolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setpageproties/_tcsetpagerightsidecolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setpageproties/_tcsetpagesize.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setpageproties/_tcsetpagetitle.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setpageproties/_tcsetcolorsolution.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcmask.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetrectanglemarktext.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsettextrotateangle.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsettranscolorsatwidgets.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetbackcolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetforecolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetlinecolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetlinewidth.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsbold.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsfontfamily.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsfontsize.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsitalic.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetslinearrows.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetslinedash.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsopacity.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetspaddingwidth.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsshadow.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsstrikeline.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetstextbackcolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetstextforecolor.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Setwidgetproperties/_tcsetwidgetsunderline.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/textcommands.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Straitlinewidget/Anchorstraitlineto.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Bezierlinewidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Bracketwidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Ellipsewidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Groupwidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Picturebox.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Polylinewidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Rectanglewidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Rhombwidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Straitlinewidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Textarea.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Trianglewidget.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Widgetintroduction/Widgettypes.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Aboutconceptmap.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Introduction.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Welcome.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Shortcuts.html");
            ShowHelpCommand.helpPathInChmFileList.Add("Textcommands/Insertwidget/_tcinserttextarea.html");


            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "ShowHelpCommand",
                "ShowHelpCommand",
                typeof(ShowHelpCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);

        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }


        public static List<string> helpPathInChmFileList;
        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            e.CanExecute = true;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            Execute();
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static void Execute()
        {
            string result = ShowHelp();
            if (result != string.Empty)
            {
                MessageBox.Show("　　无法显示帮助文档。错误信息如下：\r\n　　" + result,
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public static string ShowHelp()
        {
            string helpFilePath = Globals.InstalledPath;
            if (helpFilePath.EndsWith("\\") == false)
            {
                helpFilePath += "\\";
            }

            helpFilePath += "Help\\Lunar_Concept_Help.chm";

            if (System.IO.File.Exists(helpFilePath) == false) return "　　未找到帮助文档，无法继续！";

            try
            {
                WinForm.Help.ShowHelp(null, helpFilePath);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return "　　发生意外错误，不能顺利打开帮助文档！\r\n　　" +
                     "异常信息如下：\r\n　　" + ex.Message + "\r\n　　" + ex.StackTrace;
            }
        }

        /// <summary>
        /// 要调用本方法，必须保证帮助文档中存在对应名称的页面。
        /// 
        /// 对于部件的说明，直接应以部件类名为页面ID。
        /// 对于命令的说明，直接应以命令类名为页面ID。
        /// </summary>
        /// <param name="helpPageID">帮助文档中某页面的ID。</param>
        /// <returns></returns>
        public static string ShowHelpPage(string helpPageID)
        {
            if (helpPageID == null || helpPageID.Length <= 0) return "　　需要提供打算显示的帮助文档页面的ID。";

            string helpFilePath = Globals.InstalledPath;
            if (helpFilePath.EndsWith("\\") == false)
            {
                helpFilePath += "\\";
            }

            helpFilePath += "Help\\Lunar_Concept_Help.chm";

            if (System.IO.File.Exists(helpFilePath) == false)
            {
                return "　　未找到帮助文档，无法继续！";
            }

            string htmlString = helpPageID + ".html";//2013年2月5日重新编译帮助文档。现在统一后缀名为html。

            foreach (string s in ShowHelpCommand.helpPathInChmFileList)
            {
                if (s.ToLower().EndsWith(htmlString.ToLower()))
                {
                    htmlString = s;
                    break;
                }
            }

            try
            {
                if (string.IsNullOrEmpty(htmlString) == false)
                {
                    WinForm.Help.ShowHelp(null, helpFilePath,
                         WinForm.HelpNavigator.Topic, htmlString);
                }
                else
                {
                    WinForm.Help.ShowHelp(null, helpFilePath);
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return "　　发生意外错误，不能顺利打开帮助文档！\r\n" +
                     "异常信息如下：\r\n" + ex.Message + "\r\n" + ex.StackTrace;
            }
        }

        #endregion
    }
}
