﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年4月18日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：通过设置内容部件（ContentWidget）的内间距来使选定的各内容部件等尺寸（按最大高及最大宽为准）。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class SameContentWidgetsSizeCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static SameContentWidgetsSizeCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "SameContentWidgetsSizeCommand",
                "SameContentWidgetsSizeCommand",
                typeof(SameContentWidgetsSizeCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。

            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //List<Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            //if (selectedWidgets.Count <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //int contentWidgetsCount = 0;
            //foreach (Widget w in selectedWidgets)
            //{
            //    if (w is Widgets.ContentWidget) contentWidgetsCount++;
            //}

            //if (contentWidgetsCount < 2)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();
            if (selectedWidgets.Count <= 0) return "　　未选定任何部件。";

            int contentWidgetsCount = 0;
            foreach (Widget w in selectedWidgets)
            {
                if (w is Widgets.ContentWidget) contentWidgetsCount++;
            }

            if (contentWidgetsCount < 2)
            {
                return "　　至少选定两个内容部件才能执行此命令。";
            }

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "使选定的各内容部件等尺寸";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            var pages = manager.GetAllPageEditors();
            foreach (var page in pages)
            {
                //先取出最大高、宽。
                double maxWidth = 0;
                double maxHeight = 0;

                List<Widget> selWidgets = page.GetSelectedWidgetsList();

                foreach (Widget w in selWidgets)
                {
                    Widgets.ContentWidget cw = w as Widgets.ContentWidget;
                    if (cw == null) continue;

                    Rect rect = cw.RealRect;

                    if (rect.Width > maxWidth) maxWidth = rect.Width;
                    if (rect.Height > maxHeight) maxHeight = rect.Height;
                }

                foreach (Widget w in selWidgets)
                {
                    Widgets.ContentWidget cw = w as Widgets.ContentWidget;
                    if (cw == null) continue;

                    Action actSetPaddingWidth;

                    //ContentWidget内间距改变，如果被链接，则应考虑保持中心点位置。
                    Point oldTopLeft = cw.TopLeft;
                    Point oldBottomRight = cw.BottomRight;//备用

                    Rect rect = cw.RealRect;
                    if (rect.Height == maxHeight && rect.Width == maxWidth) continue;//最大的一个不需要更改。

                    Thickness oldPadding = cw.WidgetPadding;

                    double halfWidthOffset = (maxWidth - rect.Width) / 2;
                    double halfHeightOffset = (maxHeight - rect.Height) / 2;

                    Thickness newPadding = new Thickness(
                        oldPadding.Left + halfWidthOffset,
                        oldPadding.Top + halfHeightOffset,
                        oldPadding.Right + halfWidthOffset,
                        oldPadding.Bottom + halfHeightOffset);

                    actSetPaddingWidth = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.WidgetPaddingTag,
                    Globals.ThickConverter.ConvertToString(oldPadding), Globals.ThickConverter.ConvertToString(newPadding));

                    cw.WidgetPadding = newPadding;
                    mi.AddAction(actSetPaddingWidth);

                    if (cw.IsLinked)
                    {
                        //保持中心点
                        Point oldCenter = new Point(oldTopLeft.X + (oldBottomRight.X - oldTopLeft.X) / 2,
                            oldTopLeft.Y + (oldBottomRight.Y - oldTopLeft.Y) / 2);
                        cw.InvalidateArrange(); cw.UpdateLayout();

                        Point newTopLeft = cw.TopLeft;
                        Point newBottomRight = cw.BottomRight;
                        Point newLocation = new Point(oldCenter.X - (newBottomRight.X - newTopLeft.X) / 2,
                            oldCenter.Y - (newBottomRight.Y - newTopLeft.Y) / 2);

                        Action actNewLocation = new Action(cw.MasterEditor.Id, cw.Id, cw.GetType().Name, XmlTags.LocationTag,
                            cw.Location.ToString(), newLocation.ToString());
                        cw.Location = newLocation;
                        mi.AddAction(actNewLocation);
                    }
                    else
                    {
                        cw.InvalidateArrange(); cw.UpdateLayout();
                    }

                    foreach (UIElement ue in page.Children)
                    {
                        ILinkableLine linkedLine = ue as ILinkableLine;
                        if (linkedLine == null) continue;

                        if (linkedLine.StartMasterId == cw.Id || linkedLine.EndMasterId == cw.Id)
                        {
                            page.RefreshLinkedLines(mi, linkedLine);
                        }
                    }
                }
            }

            manager.RegisterModifingItem(mi);
            return string.Empty;
        }

        #endregion
    }
}
