﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using WinForm = System.Windows.Forms;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年2月26日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：将选定的各部件按垂直方向顺列对齐，间距固定为2。
    /// 
    /// ★★注意：此操作不会更改各部件的水平坐标！！！！
    ///           这个不是ListWidgets，而是AlignList。
    ///           这个只是“垂直分散对齐”的另一个版本，与“垂直分散对齐”不同，
    ///           这里是使各部件间距总是2，而“垂直分散对齐”虽然间距相等，但间距不固定。
    /// 
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class AlignListVerticalCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static AlignListVerticalCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "AlignListVerticalCommand",
                "AlignListVerticalCommand",
                typeof(AlignListVerticalCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }
            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }
            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null) { e.CanExecute = false; return; }
            //int selCount = mainPe.GetSelectedWidgetsCount();
            //if (selCount < 2) { e.CanExecute = false; return; }

            e.CanExecute = true;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            string result = Execute();
            if (result != string.Empty)
            {
                MessageBox.Show("　　未能顺利排列成垂直列表。错误消息如下：\r\n" + result,
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        /// <param name="padding">纵向间距。默认10。</param>
        /// <param name="alignLeft">是否左对齐。默认false。为真时不会按鱼骨图小骨处理对齐。</param>
        /// <param name="formatFixedRectangles">是否格式化“自适应”的矩形。默认false。</param>
        /// <param name="formatRectanglesWidth">是否将矩形部件的宽度统一。</param>
        public static string Execute(double? padding = 10, bool alignLeft = false, bool formatFixedRectangles = false, bool formatRectanglesWidth = false)
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";
            PageEditor pageEditor = manager.GetMainSelectedPageEditor();
            if (pageEditor == null) return "　　当前没有选定任何页面。";

            List<Widgets.Widget> selectedWidgets = pageEditor.GetSelectedWidgetsList(true);
            if (selectedWidgets.Count < 2) return "　　至少选定两个部件（连接线不算）。";

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "部件对齐为垂直列表" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            selectedWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = selectedWidgets[0];
            Widgets.Widget endWidget = selectedWidgets[selectedWidgets.Count - 1];

            Point startTopLeft = startWidget.TopLeft;
            Point endBottomRight = endWidget.BottomRight;

            double needHeight = startWidget.RealRect.Height;

            for (int i = 1; i < selectedWidgets.Count; i++)//i从1开始
            {
                needHeight += (2 + selectedWidgets[i].RealRect.Height);
            }

            if (needHeight + startTopLeft.Y > pageEditor.EditArea.Bottom)
            {
                return "　　当前页面编辑区没有足够的纵向空间进行纵向列表。";
            }

            if (formatFixedRectangles)
            {
                foreach (Widget w in selectedWidgets)
                {
                    Widgets.RectangleWidget rw = w as Widgets.RectangleWidget;
                    if (rw == null) continue;

                    Widgets.RectangleWidget.ResetRectangleWidgetHeight(mi, rw);
                }
            }

            if (formatRectanglesWidth)
            {
                var maxWidth = -1d;
                foreach (Widget w in selectedWidgets)
                {
                    Widgets.RectangleWidget rw = w as Widgets.RectangleWidget;
                    if (rw == null) continue;

                    maxWidth = Math.Max(maxWidth, rw.RealRect.Width);
                }

                if (maxWidth > 0)
                {
                    foreach (Widget w in selectedWidgets)
                    {
                        Widgets.RectangleWidget rw = w as Widgets.RectangleWidget;
                        if (rw == null) continue;

                        Widgets.RectangleWidget.ResetRectangleWidgetWidth(mi, rw, maxWidth);
                    }
                }
            }

            if (alignLeft)
            {
                for (int i = 1; i < selectedWidgets.Count; i++)//i从1开始
                {
                    selectedWidgets[i].MoveLeftSiderTo(mi, startTopLeft.X);
                }
            }

            List<ILinkableLine> linkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            Rect startRect = new Rect(startWidget.TopLeft, startWidget.BottomRight);
            double offset = startTopLeft.Y + startRect.Height;
            if (padding.HasValue)
            {
                offset += padding.Value;
            }

            if (selectedWidgets.Count > 0)
            {
                var preWidget = selectedWidgets[0];
                for (int i = 1; i < selectedWidgets.Count; i++)//首部件不需要重排。
                {
                    Widgets.Widget w = selectedWidgets[i];
                    if (padding.HasValue == false)
                    {
                        offset -= preWidget.WidgetLineWidth;
                    }
                    w.MoveTopSiderTo(mi, offset);

                    Rect rect = new Rect(w.TopLeft, w.BottomRight);

                    offset = w.TopLeft.Y + rect.Height + (padding.HasValue ? padding.Value : 0);
                    preWidget = w;
                }
            }

            //此参数为真时在前面已处理，只是简单地左对齐。
            if (alignLeft == false)
            {
                //尝试按鱼骨图小骨处理
                //判断是否均有一条折线（且为小鱼骨线形态）连接线，
                //判断这些小鱼骨线的源是否同一部件，
                //如果是，尝试按鱼骨图样式排列（按源部件的相对位置分四个区域），

                bool skipAlighAsSmallFishBone = false;

                List<PolyLineWidget> allSmallFishBones = new List<PolyLineWidget>();

                List<ICanBeLinkedWidget> icwList = new List<ICanBeLinkedWidget>();
                foreach (Widget w in selectedWidgets)
                {
                    ICanBeLinkedWidget icw = w as ICanBeLinkedWidget;
                    if (icw != null)
                    {
                        icwList.Add(icw);
                        List<PolyLineWidget> smallFishBones = GetSmallFishBones(icw);

                        if (smallFishBones == null || smallFishBones.Count != 1)
                        {
                            skipAlighAsSmallFishBone = true;
                            break;
                        }
                        else
                        {
                            allSmallFishBones.Add(smallFishBones[0]);
                        }
                    }
                }

                Widget startMaster = null;

                if (allSmallFishBones.Count <= 1)
                {
                    skipAlighAsSmallFishBone = true;
                }
                else
                {
                    string fstStartMasterID = allSmallFishBones[0].StartMasterId;
                    startMaster = pageEditor.GetWidget(fstStartMasterID);
                    if (startMaster == null)
                    {
                        skipAlighAsSmallFishBone = true;
                    }

                    ICanBeLinkedWidget startICW = null;
                    if (startMaster != null)
                    {
                        startICW = startMaster as ICanBeLinkedWidget;
                        if (startICW == null)
                        {
                            skipAlighAsSmallFishBone = true;
                        }
                    }

                    for (int i = 1; i < allSmallFishBones.Count; i++)//注意：i从1开始。
                    {
                        Widgets.PolyLineWidget pw = allSmallFishBones[i];
                        if (pw.IsLinked == false || pw.StartMasterId != fstStartMasterID)
                        //选定的所有可连接部件都必须指向同一个StartMaster。
                        {
                            skipAlighAsSmallFishBone = true;
                            break;
                        }
                    }
                }

                //选定的所有“可连接部件”均有且仅有一条“小鱼骨线（折线的一种形态）”，
                //这些小鱼骨线的StartMaster均指向同一个现存的“可连接部件”。
                if (skipAlighAsSmallFishBone == false)
                {
                    //按四个区域来排列
                    Rect startMasterRect = startMaster.OuterRect;
                    Point startMasterCenter = new Point(startMasterRect.Left + startMasterRect.Width / 2,
                        startMasterRect.Top + startMasterRect.Height / 2);

                    //根据startRect相对于startMasterRect的位置来决定是按哪个方向排列。
                    Point startCenter = new Point(startRect.Left + startRect.Width / 2,
                        startRect.Top + startRect.Height / 2);

                    if (startCenter.X > startMasterCenter.X)
                    {
                        if (startCenter.Y > startMasterRect.Y)//四区，从startWidget向右下按60°斜角排列左下角。
                        {
                            Point basePt = startWidget.OuterRect.BottomLeft;
                            for (int i = 1; i < icwList.Count; i++)//i from 1
                            {
                                Rect r = icwList[i].OuterRect;
                                Point newPt = new Point(basePt.X + Math.Abs(r.Bottom - basePt.Y) / Math.Sqrt(3),
                                    basePt.Y + 10 + r.Height);

                                icwList[i].MoveLeftSiderTo(mi, newPt.X);

                                basePt = newPt;
                            }
                        }
                        else//一区，从startWidget向左下按60°斜角排列。
                        {
                            Point basePt = startWidget.OuterRect.TopLeft;
                            for (int i = 1; i < icwList.Count; i++)//i from 1
                            {
                                Rect r = icwList[i].OuterRect;
                                Point newPt = new Point(basePt.X - Math.Abs(r.Top - basePt.Y) / Math.Sqrt(3),
                                    basePt.Y + 10 + r.Height);

                                icwList[i].MoveLeftSiderTo(mi, newPt.X);

                                basePt = newPt;
                            }
                        }
                    }
                    else
                    {
                        if (startCenter.Y > startMasterCenter.Y)//三区
                        {
                            Point basePt = startWidget.OuterRect.BottomRight;
                            for (int i = 1; i < icwList.Count; i++)//i from 1
                            {
                                Rect r = icwList[i].OuterRect;
                                Point newPt = new Point(basePt.X - Math.Abs(r.Bottom - basePt.Y) / Math.Sqrt(3),
                                    basePt.Y + 10 + r.Height);

                                icwList[i].MoveRightSiderTo(mi, newPt.X);

                                basePt = newPt;
                            }
                        }
                        else//二区
                        {
                            Point basePt = startWidget.OuterRect.TopRight;
                            for (int i = 1; i < icwList.Count; i++)//i from 1
                            {
                                Rect r = icwList[i].OuterRect;
                                Point newPt = new Point(basePt.X + Math.Abs(r.Top - basePt.Y) / Math.Sqrt(3),
                                    basePt.Y + 10 + r.Height);

                                icwList[i].MoveRightSiderTo(mi, newPt.X);

                                basePt = newPt;
                            }
                        }
                    }
                }
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, linkedLines);
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        #endregion

        private static List<PolyLineWidget> GetSmallFishBones(ICanBeLinkedWidget icw)
        {
            if (icw == null) return null;
            if (icw.MasterEditor == null) return null;

            List<ILinkableLine> linkedLines = icw.GetLinkedLines();
            List<PolyLineWidget> smallFishBones = new List<PolyLineWidget>();
            foreach (ILinkableLine linkedLine in linkedLines)
            {
                Widgets.PolyLineWidget pw = linkedLine as Widgets.PolyLineWidget;
                if (pw != null && pw.LineForm == Enums.PolyLineForms.MediumFishBone)
                {
                    smallFishBones.Add(pw);
                }
            }

            return smallFishBones;
        }

    }
}
