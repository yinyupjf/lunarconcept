﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;
using SHomeWorkshop.LunarConcept.ModifingManager;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年8月21日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：水平翻转线型部件。（改变线型部件的控制点位置。）
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class FlipHorizontalCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static FlipHorizontalCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "FlipHorizontalCommand",
                "FlipHorizontalCommand",
                typeof(FlipHorizontalCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。

            //考虑到性能问题，全部取消此判断。反正执行时会判断。

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。
            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到活动页面。";

            List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();
            List<ICanSameSize> selectedCanSameSizeWidgets = new List<ICanSameSize>();

            foreach (Widgets.Widget w in selectedWidgets)
            {
                ICanSameSize ics = w as ICanSameSize;
                if (ics != null)
                {
                    ILinkableLine lLine = ics as ILinkableLine;
                    if (lLine != null && lLine.IsLinked) continue;//连接线不能翻转。

                    selectedCanSameSizeWidgets.Add(ics);
                }
            }

            if (selectedCanSameSizeWidgets.Count < 1) return "　　至少要选定一个以上线型部件，且不能是连接线，也不能是三角形。";

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "等宽" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            List<ILinkableLine> allLinkedLines = mainPe.GetLinkedLines(selectedWidgets);

            foreach (ICanSameSize iss in selectedCanSameSizeWidgets)
            {
                Point oldStart = iss.StartPoint; Point oldEnd = iss.EndPoint;

                Point newStartPoint = new Point(oldEnd.X, oldStart.Y);
                Action actStart = new Action(iss.MasterEditor.Id, iss.Id, iss.GetType().Name, XmlTags.StartPointTag,
                    oldStart.ToString(), newStartPoint.ToString());
                iss.StartPoint = newStartPoint;

                mi.AddAction(actStart);

                Point newEndPoing = new Point(oldStart.X, oldEnd.Y);
                Action actEnd = new Action(iss.MasterEditor.Id, iss.Id, iss.GetType().Name, XmlTags.EndPointTag,
                    oldEnd.ToString(), newEndPoing.ToString());
                iss.EndPoint = newEndPoing;

                mi.AddAction(actEnd);

                Widgets.BezierLineWidget bLine = iss as Widgets.BezierLineWidget;
                if (bLine != null)
                {
                    Point oldStartCP = bLine.StartCPPoint; Point oldEndCP = bLine.EndCPPoint;

                    Point newStartCPPoint = new Point(oldEndCP.X, oldStartCP.Y);
                    Action actStartCP = new Action(bLine.MasterEditor.Id, bLine.Id, bLine.GetType().Name, XmlTags.StartCPPointTag,
                        oldStartCP.ToString(), newStartCPPoint.ToString());
                    bLine.StartCPPoint = newStartCPPoint;

                    mi.AddAction(actStartCP);

                    Point newEndCPPoing = new Point(oldStartCP.X, oldEndCP.Y);
                    Action actEndCP = new Action(bLine.MasterEditor.Id, bLine.Id, bLine.GetType().Name, XmlTags.EndCPPointTag,
                        oldEndCP.ToString(), newEndCPPoing.ToString());
                    bLine.EndCPPoint = newEndCPPoing;

                    mi.AddAction(actEndCP);
                }
            }

            mainPe.RefreshLinkedLines(mi, allLinkedLines);

            manager.RegisterModifingItem(mi);

            return string.Empty;
        }

        #endregion
    }
}
