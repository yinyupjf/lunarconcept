﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using WinForm = System.Windows.Forms;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年5月5日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：将选定的各部件按水平方向顺列对齐，间距固定为2。
    /// ★★注意：此操作不会更改各部件的垂直坐标！！！！
    ///           这个不是ListWidgets，而是AlignList。
    ///           这个只是“水平分散对齐”的另一个版本，与“水平分散对齐”不同，
    ///           这里是使各部件间距总是2，而“水平分散对齐”虽然间距相等，但间距不固定。
    /// 
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class AlignListHorizontalCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static AlignListHorizontalCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "AlignListHorizontalCommand",
                "AlignListHorizontalCommand",
                typeof(AlignListHorizontalCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }
            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }
            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null) { e.CanExecute = false; return; }
            //int selCount = mainPe.GetSelectedWidgetsCount();
            //if (selCount < 2) { e.CanExecute = false; return; }

            e.CanExecute = true;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            string result = Execute();
            if (result != string.Empty)
            {
                MessageBox.Show("　　未能顺利排列成水平列表。错误消息如下：\r\n" + result,
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        /// <param name="padding">纵向间距。默认10。</param>
        /// <param name="alignTop">是否左对齐。默认false。</param>
        /// <param name="formatFixedRectangles">是否格式化“自适应”的矩形。默认false。</param>
        public static string Execute(double padding = 10, bool alignTop = false, bool formatFixedRectangles = false)
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";
            PageEditor pageEditor = manager.GetMainSelectedPageEditor();
            if (pageEditor == null) return "　　当前没有选定任何页面。";

            List<Widgets.Widget> selectedWidgets = pageEditor.GetSelectedWidgetsList(true);
            if (selectedWidgets.Count < 2) return "　　至少选定两个部件（连接线不算）。";

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "部件对齐为水平列表" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            selectedWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            if (formatFixedRectangles)
            {
                foreach (Widget w in selectedWidgets)
                {
                    Widgets.RectangleWidget rw = w as Widgets.RectangleWidget;
                    if (rw == null) continue;

                    Widgets.RectangleWidget.ResetRectangleWidgetHeight(mi, rw);
                }
            }

            Widgets.Widget startWidget = selectedWidgets[0];
            Widgets.Widget endWidget = selectedWidgets[selectedWidgets.Count - 1];

            Point startTopLeft = startWidget.TopLeft;
            Point endBottomRight = endWidget.BottomRight;

            if (alignTop)
            {
                for (int i = 1; i < selectedWidgets.Count; i++)//i从1开始
                {
                    selectedWidgets[i].MoveTopSiderTo(mi, startTopLeft.Y);
                }
            }

            double needWidth = startWidget.RealRect.Width;

            for (int i = 1; i < selectedWidgets.Count; i++)//i从1开始
            {
                needWidth += (2 + selectedWidgets[i].RealRect.Width);
            }

            if (needWidth + startTopLeft.X > pageEditor.EditArea.Right)
            {
                return "　　当前页面的编辑区没有足够的横向空间进行横向列表。";
            }

            List<ILinkableLine> linkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            Rect startRect = new Rect(startWidget.TopLeft, startWidget.BottomRight);
            double offset = startTopLeft.X + startRect.Width + padding;

            for (int i = 1; i < selectedWidgets.Count; i++)//首部件不需要重排。
            {
                Widgets.Widget w = selectedWidgets[i];
                w.MoveLeftSiderTo(mi, offset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                offset = w.TopLeft.X + rect.Width + padding;
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, linkedLines);
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        #endregion
    }
}
