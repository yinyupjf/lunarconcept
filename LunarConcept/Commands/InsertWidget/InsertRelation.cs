﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls.Ribbon;
using SHomeWorkshop.LunarConcept.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using SHomeWorkshop.LunarConcept.Widgets;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年1月26日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：从当前部件拖出一个新“关系”。
    /// 
    ///           ——一个“关系”由一个直线、一个文本框组成。
    /// </summary>
    public static class InsertRelationCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态][构造方法]
        /// 
        /// ——此方法会初始化并向WPF系统注册一个RoutedUICommand。
        /// </summary>
        static InsertRelationCommand()//类型构造器
        {
            routedUICmd = new RoutedUICommand(
                "InsertRelationCommand",
                "InsertRelationCommand",
                typeof(InsertRelationCommand),//创建RoutedUICommand对象
                null);//本程序考虑支持“命令模式”因此，这些命令完全没有必要直接支持快捷键。

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 判断命令是否可以执行。
        /// ——由于可以直接调用Execute()方法，因此，即使被禁用，也不是不能执行相关功能！！！
        /// </summary>
        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //if (manager.GetMainSelectedPageEditor() == null)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true;
            return;
        }

        /// <summary>
        /// 命令被触发时，会调用本事件处理器方法。
        /// ——本方法实际上是调用ExeCute()这个静态方法来实现特定功能。
        /// </summary>
        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Execute();//此命令不直接调用。
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(MouseLeftButtonDraggingEventArgs e)
        {
            if (e == null || e.MasterPageEditor == null || e.MainSelectedWidget == null)
            {
                return "　　未传入MouseLeftButtonDraggingEventArgs参数，或者未指定页面，或者未传入活动部件。";
            }

            bool isCtrl = ((e.Modifiers & ModifierKeys.Control) > 0);

            string result;

            switch (e.MainSelectedWidget.WidgetType)
            {
                case Enums.WidgetTypes.Ellipse:
                case Enums.WidgetTypes.Rectangle:
                case Enums.WidgetTypes.Rhomb:
                    {
                        result = InsertRelation(e.MasterPageEditor, e.MainSelectedWidget as ICanBeLinkedWidget,
                            e.StartPoint, e.EndPoint, e.DraggingType, isCtrl);
                        break;
                    }
                default:
                    {
                        result = InsertRelation(e.MasterPageEditor, e.MainSelectedWidget as ICanBeLinkedWidget,
                            e.StartPoint, e.EndPoint, e.DraggingType, isCtrl);
                        break;
                    }
            }

            return result;
        }

        /// <summary>
        /// 添加关系线。
        /// </summary>
        /// <param name="masterPageEditor">目标编辑器。</param>
        /// <param name="srcWidget">（拖出关系的源部件，必须实现ICanLinkedWidget接口）从哪个部件拖出新关系。</param>
        /// <param name="startPoint">拖动起点坐标。</param>
        /// <param name="endPoint">拖动终点坐标。</param>
        /// <param name="relationType">关系线类型。（Enums.PageDraggingType.InsertStraitLineRelation为默认值。）</param>
        /// <param name="isCtrl">是否按住Ctrl键拖出。默认为false，此时源为矩形拖出菱形，源为菱形拖出椭圆，源为椭圆拖出矩形；
        /// 如果为true，且源部件为矩形、菱形、椭圆时，源部件是什么类型，拖出的节点就是什么类型。</param>
        /// <returns>返回错误消息。如果正确执行完毕，返回string.Empty。</returns>
        public static string InsertRelation(PageEditor masterPageEditor, ICanBeLinkedWidget srcWidget,
            Point startPoint, Point endPoint,
            Enums.PageDraggingType relationType = Enums.PageDraggingType.InsertStraitLineRelation,
            bool isCtrl = false)
        {
            if (isCtrl && (srcWidget is ContentWidget) == true &&
                relationType == Enums.PageDraggingType.InsertStraitLineRelation)
            {
                return InsertDoubleStraitLineRelation(masterPageEditor,
                    srcWidget as Widget, startPoint, endPoint);
            }

            if (masterPageEditor == null) return "　　未找到指定页面。";

            EditorManager manager = masterPageEditor.MasterManager;
            if (manager == null) return "　　未找到页面管理器。";

            if (srcWidget == null) return "　　未找到活动源部件。";

            XmlNode widgetSetNode = masterPageEditor.WidgetSetNode;
            if (widgetSetNode == null) return "　　未找到WidgetSet节点，无法添加关系。";

            Rect srcRect = srcWidget.OuterRect;
            Rect absorbedRect = new Rect(
                srcRect.Left - 20, srcRect.Top - 20,
                srcRect.Width + 40, srcRect.Height + 40);

            if (absorbedRect.Contains(endPoint)) return string.Empty;//如果拖动距离过小，直接返回。

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "添加关系" };
            masterPageEditor.MasterManager.GetSelectedPageEditorStatus(info);
            masterPageEditor.MasterManager.GetSelectedWidgetStatus_Old(info);
            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            ICanBeLinkedWidget cwAtMouseStop = null;
            for (int i = srcWidget.MasterEditor.Children.Count - 1; i >= 0; i--)
            {
                ICanBeLinkedWidget cw = srcWidget.MasterEditor.Children[i] as ICanBeLinkedWidget;
                if (cw == null) continue;

                if (cw.OuterRect.Contains(endPoint) && cw.OuterRect.IntersectsWith(srcRect) == false)
                {
                    cwAtMouseStop = cw;
                    break;
                }
            }

            ICanBeLinkedWidget destCW;

            bool addNewTextArea = false;

            if (cwAtMouseStop == null)
            {
                addNewTextArea = true;

                XmlNode newXmlData;

                switch (srcWidget.WidgetType)
                {
                    case Enums.WidgetTypes.Rhomb:
                        {
                            //2013年8月25日。调换。感觉原来按Ctrl才添加相同部件比较麻烦，相同部件使用频率更高些，不同类型部件还需要考虑改尺寸。
                            if (isCtrl == false)
                            {
                                newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.RhombXml);
                                RhombWidget newRhomb = new Widgets.RhombWidget(masterPageEditor);

                                destCW = newRhomb;
                                destCW.XmlData = newXmlData;
                                destCW.NewID();
                                destCW.WidgetOpacity = 1; // 2020年3月24日

                                RhombWidget srcRhomb = srcWidget as RhombWidget;
                                if (srcRhomb != null)
                                {
                                    newRhomb.WidgetForm = srcRhomb.WidgetForm;
                                    //按Ctrl键拖出菱形时，保证菱形形态与源菱形形态一致。
                                }
                                //设置为与源部件相同的样式。
                                if (srcWidget != null)
                                {
                                    // destCW.WidgetOpacity = 1;  // 这个还是强制不透明为好！2020年3月24日 srcWidget.WidgetOpacity;
                                    destCW.WidgetLineColor = srcWidget.WidgetLineColor;
                                    destCW.WidgetLineWidth = srcWidget.WidgetLineWidth;
                                    destCW.WidgetForeColor = srcWidget.WidgetForeColor;
                                    destCW.WidgetPadding = srcWidget.WidgetPadding;
                                    destCW.IsShadowVisible = srcWidget.IsShadowVisible;
                                    //destCW.WidgetBackColor = srcWidget.WidgetBackColor;

                                    //Backcolor是特殊的，需要另作处理，因为支持了自定义图像背景
                                    var srcWidgetBackColorName = BrushManager.GetName(srcWidget.WidgetBackColor);
                                    if (string.IsNullOrWhiteSpace(srcWidgetBackColorName) || srcWidgetBackColorName == typeof(ImageBrush).Name)
                                    {
                                        if (string.IsNullOrWhiteSpace(srcWidget.ImageBase64) == false)
                                        {
                                            destCW.ImageBase64 = srcWidget.ImageBase64;
                                        }
                                    }
                                    else
                                    {
                                        if (srcWidget.IsWidgetPropertySetted(XmlTags.WidgetBackColor))
                                        {
                                            destCW.WidgetBackColor = srcWidget.WidgetBackColor;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //菱形下层关系节点自动为椭圆形。
                                newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.EllipseXml);
                                destCW = new Widgets.EllipseWidget(masterPageEditor);
                                destCW.XmlData = newXmlData;
                                destCW.NewID();
                                destCW.WidgetOpacity = 1; // 2020年3月24日

                                //设置为默认样式。
                                //if (srcWidget == null)
                                //{
                                //    destCW.WidgetOpacity = manager.EllipseStyle.WidgetOpacity;
                                //    destCW.WidgetBackColor = manager.EllipseStyle.WidgetBackColor;
                                //    destCW.WidgetLineColor = manager.EllipseStyle.WidgetLineColor;
                                //    destCW.WidgetLineWidth = manager.EllipseStyle.WidgetLineWidth;
                                //    destCW.WidgetForeColor = manager.EllipseStyle.WidgetForeColor;
                                //    destCW.WidgetPadding = manager.EllipseStyle.WidgetPadding;
                                //    destCW.IsShadowVisible = manager.EllipseStyle.IsShadowVisible;
                                //}
                            }
                            break;
                        }
                    case Enums.WidgetTypes.Ellipse:
                        {
                            //2013年8月25日。调换。感觉原来按Ctrl才添加相同部件比较麻烦，相同部件使用频率更高些，不同类型部件还需要考虑改尺寸。
                            if (isCtrl == false)
                            {
                                newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.EllipseXml);
                                destCW = new Widgets.EllipseWidget(masterPageEditor);
                                destCW.XmlData = newXmlData;
                                destCW.NewID();
                                destCW.WidgetOpacity = 1; // 2020年3月24日

                                //设置为与源部件相同的样式。
                                if (srcWidget != null)
                                {
                                    // destCW.WidgetOpacity = 1;  // 这个还是强制不透明为好！2020年3月24日 srcWidget.WidgetOpacity;
                                    destCW.WidgetLineColor = srcWidget.WidgetLineColor;
                                    destCW.WidgetLineWidth = srcWidget.WidgetLineWidth;
                                    destCW.WidgetForeColor = srcWidget.WidgetForeColor;
                                    destCW.WidgetPadding = srcWidget.WidgetPadding;
                                    destCW.IsShadowVisible = srcWidget.IsShadowVisible;
                                    //destCW.WidgetBackColor = srcWidget.WidgetBackColor;

                                    //Backcolor是特殊的，需要另作处理，因为支持了自定义图像背景
                                    var srcWidgetBackColorName = BrushManager.GetName(srcWidget.WidgetBackColor);
                                    if (string.IsNullOrWhiteSpace(srcWidgetBackColorName) || srcWidgetBackColorName == typeof(ImageBrush).Name)
                                    {
                                        if (string.IsNullOrWhiteSpace(srcWidget.ImageBase64) == false)
                                        {
                                            destCW.ImageBase64 = srcWidget.ImageBase64;
                                        }
                                    }
                                    else
                                    {
                                        if (srcWidget.IsWidgetPropertySetted(XmlTags.WidgetBackColor))
                                        {
                                            destCW.WidgetBackColor = srcWidget.WidgetBackColor;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //椭圆形下层关系节点自动为矩形。
                                newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.RectangleXml);
                                destCW = new Widgets.RectangleWidget(masterPageEditor);
                                destCW.XmlData = newXmlData;
                                destCW.NewID();
                                destCW.WidgetOpacity = 1; // 2020年3月24日

                                //设置为默认样式。
                                //if (srcWidget == null)
                                //{
                                //    destCW.WidgetOpacity = manager.RectangleStyle.WidgetOpacity;
                                //    destCW.WidgetBackColor = manager.RectangleStyle.WidgetBackColor;
                                //    destCW.WidgetLineColor = manager.RectangleStyle.WidgetLineColor;
                                //    destCW.WidgetLineWidth = manager.RectangleStyle.WidgetLineWidth;
                                //    destCW.WidgetForeColor = manager.RectangleStyle.WidgetForeColor;
                                //    destCW.WidgetPadding = manager.RectangleStyle.WidgetPadding;
                                //    destCW.IsShadowVisible = manager.RectangleStyle.IsShadowVisible;
                                //}
                            }
                            break;
                        }
                    case Enums.WidgetTypes.Rectangle:
                        {
                            //2013年8月25日。调换。感觉原来按Ctrl才添加相同部件比较麻烦，相同部件使用频率更高些，不同类型部件还需要考虑改尺寸。
                            if (isCtrl == false)
                            {
                                newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.RectangleXml);
                                Widgets.RectangleWidget newRectangle;
                                destCW = newRectangle = new Widgets.RectangleWidget(masterPageEditor);
                                destCW.XmlData = newXmlData;
                                destCW.NewID();
                                destCW.WidgetOpacity = 1; // 2020年3月24日

                                Widgets.RectangleWidget curRectangle = srcWidget as Widgets.RectangleWidget;

                                //设置为与源部件相同的样式。
                                if (srcWidget != null)
                                {
                                    //destCW.WidgetOpacity = 1; // 这个还是强制不透明为好！2020年3月24日 srcWidget.WidgetOpacity;
                                    destCW.WidgetLineColor = srcWidget.WidgetLineColor;
                                    destCW.WidgetLineWidth = srcWidget.WidgetLineWidth;
                                    destCW.WidgetForeColor = srcWidget.WidgetForeColor;
                                    destCW.WidgetPadding = srcWidget.WidgetPadding;
                                    destCW.IsShadowVisible = srcWidget.IsShadowVisible;
                                    //destCW.WidgetBackColor = srcWidget.WidgetBackColor;

                                    //Backcolor是特殊的，需要另作处理，因为支持了自定义图像背景
                                    var srcBackColorName = BrushManager.GetName(srcWidget.WidgetBackColor);
                                    if (string.IsNullOrWhiteSpace(srcBackColorName) || srcBackColorName == typeof(ImageBrush).Name)
                                    {
                                        if (string.IsNullOrWhiteSpace(srcWidget.ImageBase64) == false)
                                        {
                                            destCW.ImageBase64 = srcWidget.ImageBase64;
                                        }
                                    }
                                    else
                                    {
                                        if (srcWidget.IsWidgetPropertySetted(XmlTags.WidgetBackColor))
                                        {
                                            destCW.WidgetBackColor = srcWidget.WidgetBackColor;
                                        }
                                    }

                                    if (curRectangle != null)
                                    {
                                        newRectangle.FixTextWidth = curRectangle.FixTextWidth;
                                    }
                                }
                            }
                            else
                            {
                                //矩形下层关系节点自动为菱形。
                                newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.RhombXml);
                                RhombWidget rhombWidget = new Widgets.RhombWidget(masterPageEditor);

                                destCW = rhombWidget;
                                destCW.XmlData = newXmlData;
                                destCW.NewID();
                                destCW.WidgetOpacity = 1; // 2020年3月24日

                                //拖出的菱形，默认为六边形。
                                rhombWidget.WidgetForm = Enums.WidgetForm.SixSideShape;


                                //设置为默认样式。
                                //if (srcWidget == null)
                                //{
                                //    destCW.WidgetOpacity = manager.RhombStyle.WidgetOpacity;
                                //    destCW.WidgetBackColor = manager.RhombStyle.WidgetBackColor;
                                //    destCW.WidgetLineColor = manager.RhombStyle.WidgetLineColor;
                                //    destCW.WidgetLineWidth = manager.RhombStyle.WidgetLineWidth;
                                //    destCW.WidgetForeColor = manager.RhombStyle.WidgetForeColor;
                                //    destCW.WidgetPadding = manager.RhombStyle.WidgetPadding;
                                //    destCW.IsShadowVisible = manager.RhombStyle.IsShadowVisible;
                                //}
                            }
                            break;
                        }
                    default:
                        {
                            newXmlData = widgetSetNode.AppendXmlAsChild(Properties.Resources.TextAreaXml);
                            destCW = new Widgets.TextArea(masterPageEditor);
                            destCW.XmlData = newXmlData;
                            destCW.NewID();
                            destCW.WidgetOpacity = 1; // 2020年3月24日

                            //设置为与源Content相似的属性。
                            if (srcWidget != null)
                            {
                                ContentWidget cw = srcWidget as ContentWidget;
                                ContentWidget destContentWidget = destCW as ContentWidget;
                                if (cw != null && destContentWidget != null)
                                {
                                    destContentWidget.OuterBorderType = cw.OuterBorderType;
                                }

                                //destCW.WidgetOpacity = 1; // 这个还是不透明为好！2020年3月24日 srcWidget.WidgetOpacity;
                                destCW.WidgetLineColor = srcWidget.WidgetLineColor;
                                destCW.WidgetLineWidth = srcWidget.WidgetLineWidth;
                                destCW.WidgetForeColor = srcWidget.WidgetForeColor;
                                destCW.WidgetPadding = srcWidget.WidgetPadding;
                                destCW.IsShadowVisible = srcWidget.IsShadowVisible;
                                //destCW.WidgetBackColor = srcWidget.WidgetBackColor;

                                var srcContentWidget = srcWidget as ContentWidget;
                                if (srcContentWidget != null)
                                {
                                    switch (srcContentWidget.OuterBorderType)
                                    {
                                        case OuterBorderType.Rhomb:
                                        case OuterBorderType.SixSideShape:
                                        case OuterBorderType.EightSidedShape:   // 多边形，表示“事件”
                                            {
                                                // 指向“物”
                                                destContentWidget.OuterBorderType = OuterBorderType.Trapezoid;
                                                break;
                                            }
                                        case OuterBorderType.EmptySemicircle:
                                        case OuterBorderType.Semicircle:
                                        case OuterBorderType.RoundRect:     // 带半圆装饰表示“人”
                                            {
                                                // 人发动“事”
                                                destContentWidget.OuterBorderType = OuterBorderType.SixSideShape;
                                                break;
                                            }
                                        case OuterBorderType.Trapezoid:
                                        case OuterBorderType.AntiTrapezoid:     // 梯形表示“物”
                                            {
                                                // 物回归“人”
                                                destContentWidget.OuterBorderType = OuterBorderType.RoundRect;
                                                break;
                                            }
                                        default:
                                            {
                                                destContentWidget.OuterBorderType = srcContentWidget.OuterBorderType;
                                                break;
                                            }
                                    }
                                }

                                //Backcolor是特殊的，需要另作处理，因为支持了自定义图像背景
                                var srcWidgetBackColorName = BrushManager.GetName(srcWidget.WidgetBackColor);
                                if (string.IsNullOrWhiteSpace(srcWidgetBackColorName) || srcWidgetBackColorName == typeof(ImageBrush).Name)
                                {
                                    if (string.IsNullOrWhiteSpace(srcWidget.ImageBase64) == false)
                                    {
                                        destCW.ImageBase64 = srcWidget.ImageBase64;
                                    }
                                }
                                else
                                {
                                    if (srcWidget.IsWidgetPropertySetted(XmlTags.WidgetBackColor))
                                    {
                                        destCW.WidgetBackColor = srcWidget.WidgetBackColor;
                                    }
                                }
                            }
                            break;
                        }
                }

                if (srcWidget.TitleLevel < Enums.TitleStyle.T5)
                {
                    destCW.TitleLevel = srcWidget.TitleLevel + 1;
                }

                masterPageEditor.AddWidget(destCW as Widget);
            }
            else
            {
                destCW = cwAtMouseStop;
            }

            XmlNode newNodeLine;
            ILinkableLine newLine;

            switch (relationType)
            {
                case Enums.PageDraggingType.InsertBezierLineRelation:
                    {
                        newNodeLine = widgetSetNode.AppendXmlAsChild(Properties.Resources.BezierLineXml);
                        newLine = new BezierLineWidget(masterPageEditor);
                        break;
                    }
                case Enums.PageDraggingType.InsertPolyLineRelation:
                    {
                        newNodeLine = widgetSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                        newLine = new PolyLineWidget(masterPageEditor);
                        break;
                    }
                default:
                    {
                        newNodeLine = widgetSetNode.AppendXmlAsChild(Properties.Resources.StraitLineXml);
                        newLine = new StraitLineWidget(masterPageEditor);
                        break;
                    }
            }

            newLine.XmlData = newNodeLine;
            newLine.NewID();
            newLine.StartMasterId = srcWidget.Id;
            newLine.EndMasterId = destCW.Id;

            //按默认值设置属性
            newLine.WidgetOpacity = destCW.WidgetOpacity;//保持透明一致。
            //newLine.Arrows = manager.StraitLineStyle.Arrows;
            //newLine.WidgetForeColor = manager.StraitLineStyle.WidgetForeColor;
            //newLine.WidgetBackColor = manager.StraitLineStyle.WidgetBackColor;
            //newLine.WidgetLineWidth = manager.StraitLineStyle.WidgetLineWidth;
            //newLine.LineDash = manager.StraitLineStyle.LineDash;
            //newLine.IsShadowVisible = manager.StraitLineStyle.IsShadowVisible;

            Point srcCenter = new Point(absorbedRect.Left + absorbedRect.Width / 2, absorbedRect.Top + absorbedRect.Height / 2);

            if (cwAtMouseStop == null)//用cwAtMouseStop来判断是不是新添加的部件。因为destCW已必定被赋予了某个实例值。
            {
                #region 定位新节点的位置。

                /* 对于ContentWidget来说，StartPoint只是Location的别名；
                 * EndPoint的返回值与BottomRight相同，给EndPoint赋值不会引起任何后果。
                 * 而对于矩形、菱形、椭圆来说，StartPoint、EndPoint是有意义的，将决定部件位置与尺寸。
                 * 
                 * ★注意：对于矩形、菱形、椭圆形来说，Location属性【并非】StartPoint属性的别名！！！
                 * 　　　　——因为Location总是返回左上角，而这几个部件的左上角很可能是EndPoint而不是StartPoint！！！
                 */

                double destWidth, destHeight;
                switch (srcWidget.WidgetType)
                {
                    case Enums.WidgetTypes.Ellipse:
                        {
                            //if (isCtrl)
                            //{
                            //    destWidth = srcRect.Width;
                            //    destHeight = srcRect.Height;
                            //}
                            //else
                            //{
                            //椭圆形拖出矩形，要缩小(2/(√2))
                            //double k = 2 / Math.Sqrt(2);
                            //缩放会很难看。实际上基本需要手工调整尺寸。

                            destWidth = srcRect.Width;// / k;
                            destHeight = srcRect.Height;// / k;
                            //}
                            break;
                        }
                    case Enums.WidgetTypes.Rectangle:
                        {
                            //if (isCtrl)
                            //{
                            destWidth = srcRect.Width;
                            destHeight = srcRect.Height;
                            //}
                            //else
                            //{
                            //    //矩形拖出菱形，等尺寸
                            //    destWidth = srcRect.Width;
                            //    destHeight = srcRect.Height;
                            //}
                            break;
                        }
                    case Enums.WidgetTypes.Rhomb:
                        {
                            //if (isCtrl)
                            //{
                            destWidth = srcRect.Width;
                            destHeight = srcRect.Height;
                            //}
                            //else
                            //{
                            //    //菱形到椭圆，放大
                            //    double k = 2 / Math.Sqrt(2);
                            //    destWidth = srcRect.Width * k;
                            //    destHeight = srcRect.Height * k;
                            //}
                            break;
                        }
                    default:
                        {
                            destCW.InvalidateArrange(); destCW.UpdateLayout();

                            destWidth = destCW.ActualWidth;
                            destHeight = destCW.ActualHeight;
                            break;
                        }
                }

                if (endPoint.Y > srcCenter.Y - 20 && endPoint.Y < srcCenter.Y + 20)
                {
                    //如果是在横向栏内部偏中间位置。
                    if (endPoint.X > srcCenter.X)//如果在右侧
                    {
                        destCW.StartPoint = new Point(endPoint.X, srcCenter.Y - destHeight / 2);//在源矩形右侧
                        destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                            destCW.StartPoint.Y + destHeight);
                    }
                    else
                    {
                        destCW.StartPoint = new Point(endPoint.X - destWidth,
                            srcCenter.Y - destHeight / 2);//在源矩形左侧
                        destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                            destCW.StartPoint.Y + destHeight);
                    }
                }
                else
                {
                    if (endPoint.X > srcCenter.X - 20 && endPoint.X < srcCenter.X + 20)
                    {
                        //如果是在纵向栏内部偏中间位置。
                        if (endPoint.Y > srcCenter.Y)
                        {
                            destCW.StartPoint = new Point(srcCenter.X - destWidth / 2, endPoint.Y);//在源矩形下侧
                            destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                                destCW.StartPoint.Y + destHeight);
                        }
                        else
                        {
                            destCW.StartPoint = new Point(srcCenter.X - destWidth / 2,
                                endPoint.Y - destHeight);//在源矩形上侧
                            destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                                destCW.StartPoint.Y + destHeight);
                        }
                    }
                    else
                    {
                        if (endPoint.X > startPoint.X)
                        {
                            if (endPoint.Y > startPoint.Y)
                            {
                                //令左上角在e.EndPoint
                                destCW.StartPoint = endPoint;
                                destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                                    destCW.StartPoint.Y + destHeight);
                            }
                            else
                            {
                                //令左下角在e.EndPoint
                                destCW.StartPoint = new Point(endPoint.X, endPoint.Y - destHeight);
                                destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                                    destCW.StartPoint.Y + destHeight);
                            }
                        }
                        else
                        {
                            if (endPoint.Y > startPoint.Y)
                            {
                                //令右上角在e.EndPoint
                                destCW.StartPoint = new Point(endPoint.X - destWidth, endPoint.Y);
                                destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                                    destCW.StartPoint.Y + destHeight);
                            }
                            else
                            {
                                //令右下角在e.EndPoint
                                destCW.StartPoint = new Point(endPoint.X - destWidth,
                                   endPoint.Y - destHeight);
                                destCW.EndPoint = new Point(destCW.StartPoint.X + destWidth,
                                    destCW.StartPoint.Y + destHeight);
                            }
                        }
                    }
                }

                #endregion
            }

            masterPageEditor.AddWidget(newLine as Widget);

            Rect rectStart = new Rect(srcWidget.TopLeft, srcWidget.BottomRight);
            rectStart.X -= 4; rectStart.Y -= 4; rectStart.Width += 8; rectStart.Height += 8;
            Rect rectEnd = new Rect(destCW.TopLeft, destCW.BottomRight);
            rectEnd.X -= 4; rectEnd.Y -= 4; rectEnd.Width += 8; rectEnd.Height += 8;

            Point startCenter = new Point(rectStart.Left + (rectStart.Width / 2), rectStart.Top + (rectStart.Height / 2));
            Point endCenter = new Point(rectEnd.Left + (rectEnd.Width / 2), rectEnd.Top + (rectEnd.Height / 2));

            PointToRect.ArrowPoints aptStart = PointToRect.GetCrossPointToRect(rectStart, endCenter);
            PointToRect.ArrowPoints aptEnd = PointToRect.GetCrossPointToRect(rectEnd, startCenter);

            switch (relationType)
            {
                case Enums.PageDraggingType.InsertBezierLineRelation:
                    {
                        //曲线作连接线，初始首、尾端点与直线默认恰好相同。
                        newLine.StartPoint = aptStart.Top;
                        newLine.EndPoint = aptEnd.Top;

                        Widgets.BezierLineWidget bezierLine = newLine as Widgets.BezierLineWidget;

                        switch (srcWidget.TitleLevel)
                        {
                            case Enums.TitleStyle.MainTitle:
                            case Enums.TitleStyle.T1:
                            case Enums.TitleStyle.T2:
                            case Enums.TitleStyle.T3:
                            case Enums.TitleStyle.T4:
                            case Enums.TitleStyle.T5:
                                {
                                    //bezierLine.IsMindMapLine = true;//2012年8月12日改为不再强迫添加导图线。
                                    Point tmpStartPoint, tmpStartCPPoint, tmpEndCPPoint, tmpEndPoint;

                                    ArrowPoints ap = bezierLine.GetMindLinePoints(ref rectStart, ref rectEnd,
                                        out tmpStartPoint, out tmpStartCPPoint,
                                        out tmpEndCPPoint, out tmpEndPoint);

                                    bezierLine.StartPoint = tmpStartPoint;
                                    bezierLine.StartCPPoint = tmpStartCPPoint;
                                    bezierLine.EndCPPoint = tmpEndCPPoint;
                                    bezierLine.EndPoint = tmpEndPoint;

                                    bezierLine.WidgetLineWidth = 12 - ((int)srcWidget.TitleLevel + 1) * 2;
                                    if (srcWidget.WidgetLineColor != Brushes.Transparent)
                                    {
                                        bezierLine.WidgetLineColor = srcWidget.WidgetLineColor;//自动保持边框色。
                                        bezierLine.WidgetBackColor = srcWidget.WidgetBackColor;
                                        bezierLine.WidgetForeColor = srcWidget.WidgetForeColor;
                                    }
                                    break;
                                }
                            default:
                                {
                                    bezierLine.StartCPPoint = new Point(newLine.StartPoint.X + (newLine.EndPoint.X - newLine.StartPoint.X) / 3,
                                        newLine.StartPoint.Y + (newLine.EndPoint.Y - newLine.StartPoint.Y) / 3);
                                    bezierLine.EndCPPoint = new Point(newLine.EndPoint.X - (newLine.EndPoint.X - newLine.StartPoint.X) / 3,
                                        newLine.EndPoint.Y - (newLine.EndPoint.Y - newLine.StartPoint.Y) / 3);
                                    break;
                                }
                        }
                        break;
                    }
                case Enums.PageDraggingType.InsertPolyLineRelation:
                    {
                        Widgets.PolyLineWidget polyLine = newLine as Widgets.PolyLineWidget;

                        //折线为连接线时，行为较复杂，初始的首尾端点位置与直线、曲线均不同。

                        #region 初始化折线连接线的首尾端点位置。

                        Point resultStartPt;
                        Point resultEndPt;

                        if ((rectStart.Bottom >= rectEnd.Top && rectStart.Bottom <= rectEnd.Bottom) ||
                            (rectStart.Top >= rectEnd.Top && rectStart.Top <= rectEnd.Bottom) ||
                            (rectEnd.Bottom >= rectStart.Top && rectEnd.Bottom <= rectStart.Bottom) ||
                            (rectEnd.Top >= rectStart.Top && rectEnd.Top <= rectStart.Bottom))
                        {
                            //水平方向上至少有部分重叠：
                            if (aptStart.Top.X < startCenter.X)
                            {
                                resultStartPt = new Point(rectStart.Left - 2, startCenter.Y);
                            }
                            else
                            {
                                resultStartPt = new Point(rectStart.Right + 2, startCenter.Y);
                            }

                            if (aptEnd.Top.X < endCenter.X)
                            {
                                resultEndPt = new Point(rectEnd.Left - 2, endCenter.Y);
                            }
                            else
                            {
                                resultEndPt = new Point(rectEnd.Right + 2, endCenter.Y);
                            }

                            polyLine.Direction = Orientation.Vertical;
                        }
                        else
                        {
                            if ((rectStart.Left >= rectEnd.Left && rectStart.Left <= rectEnd.Right) ||
                                (rectStart.Right >= rectEnd.Left && rectStart.Right <= rectEnd.Right) ||
                                (rectEnd.Left >= rectStart.Left && rectEnd.Left <= rectStart.Right) ||
                                (rectEnd.Right >= rectStart.Left && rectEnd.Right <= rectStart.Right))
                            {
                                //垂直方向至少有部分重叠：
                                if (aptStart.Top.Y < startCenter.Y)
                                {
                                    resultStartPt = new Point(startCenter.X, rectStart.Top - 2);
                                }
                                else
                                {
                                    resultStartPt = new Point(startCenter.X, rectStart.Bottom + 2);
                                }

                                if (aptEnd.Top.Y < endCenter.Y)
                                {
                                    resultEndPt = new Point(endCenter.X, rectEnd.Top - 2);
                                }
                                else
                                {
                                    resultEndPt = new Point(endCenter.X, rectEnd.Bottom + 2);
                                }

                                polyLine.Direction = Orientation.Horizontal;
                            }
                            else
                            {
                                //两个矩形没有任何重叠：
                                //此时，根据两个矩形间的水平、垂直距离的大小来决定是连线中段是水平指向还是垂直指向：
                                //如果水平距离大，而垂直距离小，则水平指向；
                                //如果垂直距离大，而水平距离小，则垂直指向；
                                //★初始化是这样，但用户在添加此折线关系线后，可以修改指向！！！
                                //　——这是为满足某些特定的需求而设计的。

                                double minleft, mintop, maxbottom, maxright;
                                minleft = Math.Min(rectStart.Left, rectEnd.Left);
                                mintop = Math.Min(rectStart.Top, rectEnd.Top);
                                maxbottom = Math.Max(rectStart.Bottom, rectEnd.Bottom);
                                maxright = Math.Max(rectStart.Right, rectEnd.Right);

                                double width, height;
                                width = maxright - minleft - rectStart.Width - rectEnd.Width;
                                height = maxbottom - mintop - rectStart.Height - rectEnd.Height;

                                if (width > height)
                                {
                                    polyLine.Direction = Orientation.Horizontal;

                                    if (endCenter.Y > startCenter.Y)
                                    {
                                        resultStartPt = new Point(startCenter.X, rectStart.Bottom + 2);
                                        resultEndPt = new Point(endCenter.X, rectEnd.Top - 2);
                                    }
                                    else
                                    {
                                        resultStartPt = new Point(startCenter.X, rectStart.Top - 2);
                                        resultEndPt = new Point(endCenter.X, rectEnd.Bottom + 2);
                                    }
                                }
                                else
                                {
                                    polyLine.Direction = Orientation.Vertical;

                                    if (endCenter.X > startCenter.X)
                                    {
                                        resultStartPt = new Point(rectStart.Right + 2, startCenter.Y);
                                        resultEndPt = new Point(rectEnd.Left - 2, endCenter.Y);
                                    }
                                    else
                                    {
                                        resultStartPt = new Point(rectStart.Left - 2, startCenter.Y);
                                        resultEndPt = new Point(rectEnd.Right + 2, endCenter.Y);
                                    }
                                }
                            }
                        }

                        #endregion

                        polyLine.StartPoint = resultStartPt;
                        polyLine.EndPoint = resultEndPt;
                        polyLine.CenterCPPoint = new Point(newLine.EndPoint.X - (newLine.EndPoint.X - newLine.StartPoint.X) / 2,
                           newLine.EndPoint.Y - (newLine.EndPoint.Y - newLine.StartPoint.Y) / 2);

                        //折线型连接线还要考虑初始纵、横向的问题。
                        if (Math.Abs(resultEndPt.Y - resultStartPt.Y) > Math.Abs(resultEndPt.X - resultStartPt.X))
                        {
                            polyLine.Direction = Orientation.Vertical;
                        }
                        else
                        {
                            polyLine.Direction = Orientation.Horizontal;
                        }
                        break;
                    }
                default:
                    {
                        //直线作连接线

                        newLine.StartPoint = aptStart.Top;
                        newLine.EndPoint = aptEnd.Top;

                        if (rectEnd.Width > 0 && rectEnd.Height > 0)
                        {
                            double d = rectEnd.Width / rectEnd.Height;
                            if (d > 10 || d < 0.1)
                            {
                                //目标部件外框很窄，自动将尾端点设置为 指向边框 EndLinkTo Border
                                Point newEnd;
                                switch (Globals.GetLocationArea(rectEnd, startCenter))
                                {
                                    case LocationArea.Left:
                                        newEnd = new Point(rectEnd.X, startCenter.Y);
                                        break;
                                    case LocationArea.LeftTop:
                                        newEnd = rectEnd.TopLeft;
                                        break;
                                    case LocationArea.Top:
                                        newEnd = new Point(startCenter.X, rectEnd.Y);
                                        break;
                                    case LocationArea.RightTop:
                                        newEnd = rectEnd.TopRight;
                                        break;
                                    case LocationArea.Right:
                                        newEnd = new Point(rectEnd.X + rectEnd.Width, startCenter.Y);
                                        break;
                                    case LocationArea.RightBottom:
                                        newEnd = rectEnd.BottomRight;
                                        break;
                                    case LocationArea.Bottom:
                                        newEnd = new Point(startCenter.X, rectEnd.Bottom);
                                        break;
                                    case LocationArea.LeftBottom:
                                        newEnd = rectEnd.BottomLeft;
                                        break;
                                    //case LocationArea.Center:
                                    default:
                                        newEnd = endCenter;
                                        break;
                                }
                                var sLine = newLine as StraitLineWidget;
                                if (sLine != null)
                                    sLine.EndLinkTo = LinkToPoint.Border;

                                newLine.EndPoint = newEnd;
                            }
                        }

                        break;

                    }
            }

            //最后将完整的Xml记录下来准备撤销。
            if (cwAtMouseStop == null)
            {
                Action actionAddTextArea = new Action(ActionType.WidgetAdded,
                    masterPageEditor.Id, destCW.Id, null, destCW.XmlData.OuterXml);
                mi.AddAction(actionAddTextArea);
            }

            Action actionAddStraitLine = new Action(ActionType.WidgetAdded,
                masterPageEditor.Id, newLine.Id, null, newLine.XmlData.OuterXml);

            if (addNewTextArea)
            {
                destCW.SelectOnlySelf();
                info.NewMainSelectedWidgetID = destCW.Id;
            }
            else
            {
                newLine.SelectOnlySelf();
                info.NewMainSelectedWidgetID = newLine.Id;
            }

            mi.AddAction(actionAddStraitLine);

            if (newLine.MasterEditor != null)
            {
                //防止线的端点未初始化。
                List<ILinkableLine> newLines = new List<ILinkableLine>();
                newLines.Add(newLine);
                newLine.MasterEditor.RefreshLinkedLines(mi, newLines);
            }

            if (masterPageEditor.MasterManager != null)
            {
                masterPageEditor.MasterManager.RegisterModifingItem(mi);
                masterPageEditor.MasterManager.RefreshAutoNumberStrings();
            }

            return string.Empty;
        }

        /// <summary>
        /// 双节点关系，不支持曲线与折线。2012年5月30日。
        /// </summary>
        /// <param name="masterPageEditor">页面编辑器。</param>
        /// <param name="mainSelectedWidget">从哪个部件开始。</param>
        private static string InsertDoubleStraitLineRelation(PageEditor masterPageEditor, Widget mainSelectedWidget,
            Point startPoint, Point endPoint)
        {
            if (masterPageEditor == null) return "　　未找到目标页面。";

            EditorManager manager = masterPageEditor.MasterManager;
            if (manager == null) return "　　未找到页面管理器。";

            if (mainSelectedWidget == null) return "　　未活动源部件。";

            XmlNode widgetSetNode = masterPageEditor.WidgetSetNode;
            if (widgetSetNode == null) return "　　未找到WidgetSet节点，无法添加关系。";

            Rect absorbedRect = new Rect(mainSelectedWidget.OuterRect.Left - 20,
                mainSelectedWidget.OuterRect.Top - 20, mainSelectedWidget.OuterRect.Width + 40,
                mainSelectedWidget.OuterRect.Height + 40);

            if (absorbedRect.Contains(endPoint)) return "　　距离过小。";//如果拖动距离过小，直接返回。

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "添加双重关系" };
            masterPageEditor.MasterManager.GetSelectedPageEditorStatus(info);
            masterPageEditor.MasterManager.GetSelectedWidgetStatus_Old(info);
            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            ICanBeLinkedWidget cwAtMouseStop = null;
            for (int i = mainSelectedWidget.MasterEditor.Children.Count - 1; i >= 0; i--)
            {
                ICanBeLinkedWidget cw = mainSelectedWidget.MasterEditor.Children[i] as ICanBeLinkedWidget;
                if (cw == null) continue;

                if (cw.OuterRect.Contains(endPoint))
                {
                    cwAtMouseStop = cw;
                    break;
                }
            }

            ICanBeLinkedWidget destCW;

            bool addNewTextArea = false;

            Point srcCenter = new Point(absorbedRect.Left + absorbedRect.Width / 2, absorbedRect.Top + absorbedRect.Height / 2);

            if (cwAtMouseStop == null)
            {
                addNewTextArea = true;

                XmlNode newNodeTextArea = widgetSetNode.AppendXmlAsChild(Properties.Resources.TextAreaXml);
                destCW = new Widgets.TextArea(masterPageEditor);
                destCW.XmlData = newNodeTextArea;
                destCW.NewID();

                //设置为与源Content相似的属性。
                ICanBeLinkedWidget curContentWidget = mainSelectedWidget as ICanBeLinkedWidget;
                if (curContentWidget != null)
                {
                    ContentWidget srcCW = curContentWidget as ContentWidget;
                    ContentWidget destContentWidget = destCW as ContentWidget;
                    if (srcCW != null && destContentWidget != null)
                    {
                        destContentWidget.OuterBorderType = srcCW.OuterBorderType;
                    }

                    destCW.WidgetOpacity = curContentWidget.WidgetOpacity;
                    destCW.WidgetLineColor = curContentWidget.WidgetLineColor;
                    destCW.WidgetLineWidth = curContentWidget.WidgetLineWidth;
                    destCW.WidgetForeColor = curContentWidget.WidgetForeColor;
                    destCW.WidgetPadding = curContentWidget.WidgetPadding;
                    destCW.IsShadowVisible = curContentWidget.IsShadowVisible;
                    //destCW.WidgetBackColor = curContentWidget.WidgetBackColor;

                    //Backcolor是特殊的，需要另作处理，因为支持了自定义图像背景
                    var srcWidgetBackColorName = BrushManager.GetName(curContentWidget.WidgetBackColor);
                    if (string.IsNullOrWhiteSpace(srcWidgetBackColorName) || srcWidgetBackColorName == typeof(ImageBrush).Name)
                    {
                        if (string.IsNullOrWhiteSpace(curContentWidget.ImageBase64) == false)
                        {
                            destCW.ImageBase64 = curContentWidget.ImageBase64;
                        }
                    }
                    else
                    {
                        if (curContentWidget.IsWidgetPropertySetted(XmlTags.WidgetBackColor))
                        {
                            destCW.WidgetBackColor = curContentWidget.WidgetBackColor;
                        }
                    }
                }

                masterPageEditor.AddWidget(destCW as Widget);
                destCW.InvalidateArrange(); destCW.UpdateLayout();

                if (endPoint.Y > srcCenter.Y - 20 && endPoint.Y < srcCenter.Y + 20)
                {
                    //如果是在横向栏内部偏中间位置。
                    if (endPoint.X > srcCenter.X)
                    {
                        destCW.Location = new Point(endPoint.X, srcCenter.Y - destCW.ActualHeight / 2);//在源矩形右侧
                    }
                    else
                    {
                        destCW.Location = new Point(endPoint.X - destCW.ActualWidth,
                            srcCenter.Y - destCW.ActualHeight / 2);//在源矩形左侧
                    }
                }
                else
                {
                    if (endPoint.X > srcCenter.X - 20 && endPoint.X < srcCenter.X + 20)
                    {
                        //如果是在纵向栏内部偏中间位置。
                        if (endPoint.Y > srcCenter.Y)
                        {
                            destCW.Location = new Point(srcCenter.X - destCW.ActualWidth / 2, endPoint.Y);//在源矩形下侧
                        }
                        else
                        {
                            destCW.Location = new Point(srcCenter.X - destCW.ActualWidth / 2,
                                endPoint.Y - destCW.ActualHeight);//在源矩形上侧
                        }
                    }
                    else
                    {
                        if (endPoint.X > startPoint.X)
                        {
                            if (endPoint.Y > startPoint.Y)
                            {
                                //令左上角在e.EndPoint
                                destCW.Location = endPoint;
                            }
                            else
                            {
                                //令左下角在e.EndPoint
                                destCW.Location = new Point(endPoint.X, endPoint.Y - destCW.ActualHeight);
                            }
                        }
                        else
                        {
                            if (endPoint.Y > startPoint.Y)
                            {
                                //令右上角在e.EndPoint
                                destCW.Location = new Point(endPoint.X - destCW.ActualWidth, endPoint.Y);
                            }
                            else
                            {
                                //令右下角在e.EndPoint
                                destCW.Location = new Point(endPoint.X - destCW.ActualWidth,
                                   endPoint.Y - destCW.ActualHeight);
                            }
                        }
                    }
                }
            }
            else
            {
                destCW = cwAtMouseStop;
            }

            Rect destRect = destCW.OuterRect;
            destRect.X -= 4; destRect.Y -= 4; destRect.Width += 8; destRect.Height += 8;
            Point destCenter = new Point(destRect.Left + destRect.Width / 2, destRect.Top + destRect.Height / 2);

            Point middleTextAreaCenter = new Point(srcCenter.X + (destCenter.X - srcCenter.X) / 2,
                srcCenter.Y + (destCenter.Y - srcCenter.Y) / 2);

            XmlNode newNodeOfMiddleTextArea = widgetSetNode.AppendXmlAsChild(Properties.Resources.RelationTextAreaXml);
            Widgets.TextArea newMiddleTextArea = new TextArea(masterPageEditor);
            newMiddleTextArea.XmlData = newNodeOfMiddleTextArea;
            newMiddleTextArea.NewID();
            newMiddleTextArea.WidgetLineColor = Brushes.Transparent;
            newMiddleTextArea.WidgetLineWidth = 0;
            newMiddleTextArea.WidgetPadding = new Thickness(0);
            newMiddleTextArea.WidgetOpacity = destCW.WidgetOpacity;

            masterPageEditor.AddWidget(newMiddleTextArea);
            newMiddleTextArea.InvalidateArrange(); newMiddleTextArea.UpdateLayout();

            newMiddleTextArea.Location = new Point(middleTextAreaCenter.X - newMiddleTextArea.ActualWidth / 2,
                middleTextAreaCenter.Y - newMiddleTextArea.ActualHeight / 2);

            Action actInsertMiddleTextArea = new Action(ActionType.WidgetAdded, masterPageEditor.Id, newMiddleTextArea.Id, null,
                newMiddleTextArea.XmlData.OuterXml);

            mi.AddAction(actInsertMiddleTextArea);

            XmlNode newNodeLine = widgetSetNode.AppendXmlAsChild(Properties.Resources.StraitLineXml);
            Widgets.StraitLineWidget straitLine = new StraitLineWidget(masterPageEditor);
            straitLine.XmlData = newNodeLine;
            straitLine.NewID();
            straitLine.StartMasterId = mainSelectedWidget.Id;
            straitLine.EndMasterId = newMiddleTextArea.Id;
            straitLine.Arrows = Enums.ArrowType.None;

            //按默认值设置属性
            straitLine.WidgetOpacity = destCW.WidgetOpacity;
            //straitLine.WidgetForeColor = manager.StraitLineStyle.WidgetForeColor;
            //straitLine.WidgetBackColor = manager.StraitLineStyle.WidgetBackColor;
            //straitLine.WidgetLineWidth = manager.StraitLineStyle.WidgetLineWidth;
            //straitLine.LineDash = manager.StraitLineStyle.LineDash;
            //straitLine.IsShadowVisible = manager.StraitLineStyle.IsShadowVisible;

            XmlNode anotherNodeLine = widgetSetNode.AppendXmlAsChild(Properties.Resources.StraitLineXml);
            Widgets.StraitLineWidget anotherStraitLine = new StraitLineWidget(masterPageEditor);
            anotherStraitLine.XmlData = anotherNodeLine;
            anotherStraitLine.NewID();
            anotherStraitLine.StartMasterId = newMiddleTextArea.Id;
            anotherStraitLine.EndMasterId = destCW.Id;
            anotherStraitLine.Arrows = Enums.ArrowType.End;

            //按默认值设置属性
            anotherStraitLine.WidgetOpacity = destCW.WidgetOpacity;
            //anotherStraitLine.WidgetForeColor = manager.StraitLineStyle.WidgetForeColor;
            //anotherStraitLine.WidgetBackColor = manager.StraitLineStyle.WidgetBackColor;
            //anotherStraitLine.WidgetLineWidth = manager.StraitLineStyle.WidgetLineWidth;
            //anotherStraitLine.LineDash = manager.StraitLineStyle.LineDash;
            //anotherStraitLine.IsShadowVisible = manager.StraitLineStyle.IsShadowVisible;

            masterPageEditor.AddWidget(anotherStraitLine);

            masterPageEditor.AddWidget(straitLine);

            Rect rectStart = new Rect(mainSelectedWidget.TopLeft, mainSelectedWidget.BottomRight);
            rectStart.X -= 4; rectStart.Y -= 4; rectStart.Width += 8; rectStart.Height += 8;
            Rect rectMiddle = new Rect(newMiddleTextArea.TopLeft, newMiddleTextArea.BottomRight);
            rectMiddle.X -= 4; rectMiddle.Y -= 4; rectMiddle.Width += 8; rectMiddle.Height += 8;
            Rect rectEnd = new Rect(destCW.TopLeft, destCW.BottomRight);
            rectEnd.X -= 4; rectEnd.Y -= 4; rectEnd.Width += 8; rectEnd.Height += 8;

            Point startCenter = new Point(rectStart.Left + (rectStart.Width / 2), rectStart.Top + (rectStart.Height / 2));
            Point endCenter = new Point(rectEnd.Left + (rectEnd.Width / 2), rectEnd.Top + (rectEnd.Height / 2));

            PointToRect.ArrowPoints aptStart = PointToRect.GetCrossPointToRect(rectStart, middleTextAreaCenter);
            PointToRect.ArrowPoints aptMiddleStart = PointToRect.GetCrossPointToRect(rectMiddle, startCenter);
            PointToRect.ArrowPoints aptMiddleEnd = PointToRect.GetCrossPointToRect(rectMiddle, endCenter);
            PointToRect.ArrowPoints aptEnd = PointToRect.GetCrossPointToRect(rectEnd, middleTextAreaCenter);

            straitLine.StartPoint = aptStart.Top;
            straitLine.EndPoint = aptMiddleStart.Top;
            anotherStraitLine.StartPoint = aptMiddleEnd.Top;
            anotherStraitLine.EndPoint = aptEnd.Top;

            //最后将完整的Xml记录下来准备撤销。
            if (cwAtMouseStop == null)
            {
                Action actionAddTextArea = new Action(ActionType.WidgetAdded,
                    masterPageEditor.Id, destCW.Id, null, destCW.XmlData.OuterXml);
                mi.AddAction(actionAddTextArea);
            }

            Action actionAddStraitLine = new Action(ActionType.WidgetAdded,
                masterPageEditor.Id, straitLine.Id, null, straitLine.XmlData.OuterXml);

            Action actionAddAnotherStraitLine = new Action(ActionType.WidgetAdded,
                masterPageEditor.Id, anotherStraitLine.Id, null, anotherStraitLine.XmlData.OuterXml);

            if (addNewTextArea)
            {
                destCW.SelectOnlySelf();
                info.NewMainSelectedWidgetID = destCW.Id;
            }
            else
            {
                straitLine.SelectOnlySelf();
                info.NewMainSelectedWidgetID = straitLine.Id;
            }

            mi.AddAction(actionAddStraitLine);
            mi.AddAction(actionAddAnotherStraitLine);

            if (masterPageEditor.MasterManager != null)
            {
                masterPageEditor.MasterManager.RegisterModifingItem(mi);
            }

            return string.Empty;
        }

        #endregion
    }
}
