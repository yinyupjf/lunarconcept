﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls.Ribbon;
using SHomeWorkshop.LunarConcept.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2011年12月30日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：删除当前选定的各PageEditor（可以是多个）。
    /// </summary>
    public static class DeletePageEditorsCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态][构造方法]
        /// 
        /// ——此方法会初始化并向WPF系统注册一个RoutedUICommand。
        /// </summary>
        static DeletePageEditorsCommand()//类型构造器
        {
            routedUICmd = new RoutedUICommand(
                "DeletePageEditorsCommand",
                "DeletePageEditorsCommand",
                typeof(DeletePageEditorsCommand),//创建RoutedUICommand对象
                null);

            //routedUICmd.InputGestures.Add(new KeyGesture(Key.Delete, ModifierKeys.None, "Delete"));//与“删除部件冲突”

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 判断命令是否可以执行。
        /// ——由于可以直接调用Execute()方法，因此，即使被禁用，也不是不能执行相关功能！！！
        /// </summary>
        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //if (Globals.MainWindow == null || Globals.MainWindow.EditorManager == null) return;

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager.XmlDocument == null || manager.XmlDocument.DocumentElement == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //if (manager.GetSelectedPageEditorCount() <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //XmlNode pageSetNode = manager.XmlDocument.DocumentElement.SelectSingleNode(XmlTags.PageSetTag);

            //if (pageSetNode == null)
            //{
            //    e.CanExecute = false;
            //    return;//根本没有删除的必要。
            //}

            e.CanExecute = true;//可以删除页面。
        }

        /// <summary>
        /// 命令被触发时，会调用本事件处理器方法。
        /// ——本方法实际上是调用ExeCute()这个静态方法来实现特定功能。
        /// </summary>
        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            if (manager.XmlDocument == null || manager.XmlDocument.DocumentElement == null)
                return "　　未找到页面管理器的后台Xml文档或未找到文档根节点。";

            XmlNode pageSetNode = manager.XmlDocument.DocumentElement.SelectSingleNode(XmlTags.PageSetTag);

            if (pageSetNode == null) return "未找到后台Xml文档的【PageSet】节点。";//根本没有删除的必要。

            List<PageEditor> selPagesEditors = manager.GetSelectedPageEditorsList();

            if (selPagesEditors.Count <= 0) return "　　未选定要删除的页面。";

            ModifingInfo info = new ModifingInfo();

            //记录操作前被选定的所有PageEditor的ID。
            List<PageEditor> oldSelectedPageEditorList = manager.GetSelectedPageEditorsList();
            foreach (PageEditor pe in oldSelectedPageEditorList)
            {
                info.AddPageEditorID_OldSelected(pe.Id);
                if (pe.IsMainSelected)
                {
                    info.OldMainSelectedPageEditorID = pe.Id;
                }

                if (pe.IsSelected) pe.IsSelected = false;
            }
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            info.ModifingDescription = "删除页";

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            int minIndex = manager.IndexOf(selPagesEditors[0]);

            for (int i = selPagesEditors.Count - 1; i >= 0; i--)
            {
                PageEditor pe = selPagesEditors[i];
                int index = manager.IndexOf(pe);
                Action actDeletePageEditor = new Action(ActionType.PageEditorDeleted,
                    pe.Id, index, pe.XmlData.OuterXml, null);

                manager.RemoveEditor(pe);
                if (pe.XmlData != null && pe.XmlData.ParentNode != null)
                {
                    pe.XmlData.ParentNode.RemoveChild(pe.XmlData);
                }

                mi.AddAction(actDeletePageEditor);
            }

            if (minIndex >= 0)
            {
                if (minIndex <= manager.Count - 1)
                {
                    PageEditor nextPageEditor = manager[minIndex];
                    if (nextPageEditor != null)
                    {
                        nextPageEditor.IsMainSelected = true;
                        info.NewMainSelectedPageEditorID = nextPageEditor.Id;
                    }
                }
                else
                {
                    int newIndex = minIndex - 1;
                    if (newIndex < manager.Count && newIndex >= 0)
                    {
                        PageEditor nextPageEditor = manager[newIndex];
                        if (nextPageEditor != null)
                        {
                            nextPageEditor.IsMainSelected = true;
                            info.NewMainSelectedPageEditorID = nextPageEditor.Id;
                        }
                    }
                }
            }

            manager.RegisterModifingItem(mi);

            manager.RefreshPaginations();

            manager.MasterWindow.RefreshPageView();

            return string.Empty;
        }

        #endregion
    }
}
