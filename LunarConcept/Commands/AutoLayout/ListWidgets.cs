﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using WinForm = System.Windows.Forms;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年3月7日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：按列表样式排列实体部件（指ShapeWidget和ContentWidget,选定的），并在实体部件之间绘制连接线。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class ListWidgetsCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static ListWidgetsCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "ListWidgetsCommand",
                "ListWidgetsCommand",
                typeof(ListWidgetsCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。

            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //两个条件：⑴有活动部件；⑵除活动部件外，必须还有两个及以上被选定的部件。

            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }

            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null) { e.CanExecute = false; return; }

            //List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();
            //if (selectedWidgets.Count < 3) { e.CanExecute = false; return; }//至少三个部件。

            //Widgets.Widget mainSelWidget = null;
            //int notLineWidgetsCount = 0;
            //foreach (Widgets.Widget w in selectedWidgets)
            //{
            //    Widgets.ArrowLineWidget alw = w as Widgets.ArrowLineWidget;
            //    if (alw != null) continue;

            //    if (w.IsMainSelected) mainSelWidget = w;
            //    notLineWidgetsCount++;
            //}

            //if (mainSelWidget == null || notLineWidgetsCount < 3)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。
            LunarMessage.Warning(Execute(e.Parameter as string));
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(string cmdParameter)
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未选定任何页面。";

            List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();
            if (selectedWidgets.Count < 3) return "　　至少选定三个实体部件。";

            Widgets.Widget mainSelWidget = null;
            List<Widgets.Widget> otherNotLineWidgets = new List<Widgets.Widget>();

            foreach (Widgets.Widget w in selectedWidgets)
            {
                Widgets.ArrowLineWidget alw = w as Widgets.ArrowLineWidget;
                if (alw != null) continue;
                //真正的线型部件不参与。而Shape虽然也是线型部件，但有实体区域，因此此处不算在线型部件。

                if (w.IsMainSelected)
                {
                    mainSelWidget = w;
                }
                else
                {
                    otherNotLineWidgets.Add(w);
                }
            }

            if (mainSelWidget == null || otherNotLineWidgets.Count < 2)//总数不能小于３（包括一个活动内容部件）
                return "　　没有找到活动部件，或选定的实体部件总数目小于３个。";

            string direction = "right";
            if (cmdParameter != null)
            {
                direction = cmdParameter.ToLower();
            }

            switch (direction)
            {
                case "topleft":
                    {
                        return List_TopLeft(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "leftbottom":
                    {
                        return List_LeftBottom(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "topright":
                    {
                        return List_TopRight(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "bottomright":
                    {
                        return List_BottomRight(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "lefttop":
                    {
                        return List_LeftTop(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "righttop":
                    {
                        return List_RightTop(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "bottomleft":
                    {
                        return List_BottomLeft(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
                case "rightbottom":
                    {
                        return List_RightBottom(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets);
                    }
            }

            return "　　指定的方向不是八方向中任意一种。";
        }

        private static string List_RightBottom(EditorManager manager, PageEditor pageEditor, Widget mainSelWidget, List<Widget> selectedWidgets, List<Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "右下列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumWidthOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumWidthOfOthers += w.RealRect.Width;
            }

            double baseOffset = mainSelWidget.BottomRight.X + 10;

            double startOffset = baseOffset;
            double otherNewTop = mainSelWidget.Center.Y + mainSelWidget.RealRect.Height / 4 + 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveLeftSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.X + rect.Width + padding;

                if (w.TopLeft.Y != otherNewTop)
                {
                    w.MoveTopSiderTo(mi, otherNewTop);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Horizontal;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string List_RightTop(EditorManager manager, PageEditor pageEditor, Widget mainSelWidget, List<Widget> selectedWidgets, List<Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "右上列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumWidthOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumWidthOfOthers += w.RealRect.Width;
            }

            double baseOffset = mainSelWidget.BottomRight.X + 10;

            double startOffset = baseOffset;
            double otherNewBottom = mainSelWidget.Center.Y - mainSelWidget.RealRect.Height / 4 - 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveLeftSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.X + rect.Width + padding;

                if (w.BottomRight.Y != otherNewBottom)
                {
                    w.MoveBottomSiderTo(mi, otherNewBottom);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Horizontal;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string List_BottomRight(EditorManager manager, PageEditor pageEditor,
            Widget mainSelWidget, List<Widget> selectedWidgets, List<Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "右下列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumHeightOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumHeightOfOthers += w.RealRect.Height;
            }

            double baseOffset = mainSelWidget.BottomRight.Y + 10;

            double startOffset = baseOffset;
            double otherNewLeft = mainSelWidget.Center.X + mainSelWidget.RealRect.Width / 4 + 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveTopSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.Y + rect.Height + padding;

                if (w.TopLeft.X != otherNewLeft)
                {
                    w.MoveLeftSiderTo(mi, otherNewLeft);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Vertical;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string List_LeftBottom(EditorManager manager, PageEditor pageEditor, Widget mainSelWidget, List<Widget> selectedWidgets, List<Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "右下列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumWidthOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumWidthOfOthers += w.RealRect.Width;
            }

            double baseOffset = mainSelWidget.TopLeft.X - 10 - sumWidthOfOthers;

            double startOffset = baseOffset;
            double otherNewTop = mainSelWidget.Center.Y + mainSelWidget.RealRect.Height / 4 + 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveLeftSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.X + rect.Width + padding;

                if (w.TopLeft.Y != otherNewTop)
                {
                    w.MoveTopSiderTo(mi, otherNewTop);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Horizontal;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        /// <summary>
        /// 指向MainSelWidgets和otherNotLineWidgets中某个部件的连接线才会被删除。
        /// </summary>
        private static void DeleteLinkedLines(PageEditor pe, ModifingItem<Action, ModifingInfo> mi,
            List<ILinkableLine> linkedLines, Widget mainSelWidget, List<Widget> otherNotLineWidgets)
        {
            if (pe == null || mi == null || linkedLines == null ||
                linkedLines.Count == 0 || mainSelWidget == null ||
                otherNotLineWidgets == null || otherNotLineWidgets.Count <= 0) return;

            foreach (ILinkableLine linkedLine in linkedLines)
            {
                if (linkedLine.StartMasterId != mainSelWidget.Id &&
                    linkedLine.EndMasterId != mainSelWidget.Id) continue;

                bool needDelete = false;

                if (linkedLine.StartMasterId == mainSelWidget.Id)
                {
                    //看EndMasterWidget是否在otherNotLineWidgets列表中
                    foreach (Widget w in otherNotLineWidgets)
                    {
                        if (w.Id == linkedLine.EndMasterId)
                        {
                            needDelete = true;
                            break;//需要删除
                        }
                    }
                }
                else if (linkedLine.EndMasterId == mainSelWidget.Id)
                {
                    //看StartMasterWidget是否在otherNotLineWidgets列表中
                    foreach (Widget w in otherNotLineWidgets)
                    {
                        if (w.Id == linkedLine.StartMasterId)
                        {
                            needDelete = true;
                            break;//需要删除
                        }
                    }
                }

                if (needDelete == false) continue;

                Action actDel = new Action(ActionType.WidgetDeleted, pe.Id, linkedLine.Id, linkedLine.XmlData.OuterXml, null);
                if (linkedLine.XmlData.ParentNode != null)
                {
                    linkedLine.XmlData.ParentNode.RemoveChild(linkedLine.XmlData);
                }

                UIElement ue = linkedLine as UIElement;
                if (ue != null)
                {
                    if (pe.Children.Contains(ue))
                    {
                        pe.Children.Remove(ue);
                    }
                }
                mi.AddAction(actDel);
            }
        }

        private static string List_BottomLeft(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "左上列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumHeightOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumHeightOfOthers += w.RealRect.Height;
            }

            double baseOffset = mainSelWidget.BottomRight.Y + 10;

            double startOffset = baseOffset;
            double otherNewRight = mainSelWidget.Center.X - mainSelWidget.RealRect.Width / 4 - 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveTopSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.Y + rect.Height + padding;


                if (w.BottomRight.X != otherNewRight)
                {
                    w.MoveRightSiderTo(mi, otherNewRight);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Vertical;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string List_LeftTop(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "左上列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumWidthOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumWidthOfOthers += w.RealRect.Width;
            }

            double baseOffset = mainSelWidget.TopLeft.X - 10 - sumWidthOfOthers;

            double startOffset = baseOffset;
            double otherNewBottom = mainSelWidget.Center.Y - mainSelWidget.RealRect.Height / 4 - 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveLeftSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.X + rect.Width + padding;

                if (w.BottomRight.Y != otherNewBottom)
                {
                    w.MoveBottomSiderTo(mi, otherNewBottom);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Horizontal;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string List_TopRight(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "右上列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumHeightOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumHeightOfOthers += w.RealRect.Height;
            }

            double baseOffset = mainSelWidget.TopLeft.Y - 10 - sumHeightOfOthers;

            double startOffset = baseOffset;
            double otherNewLeft = mainSelWidget.Center.X + mainSelWidget.RealRect.Width / 4 + 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveTopSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.Y + rect.Height + padding;

                if (w.TopLeft.X != otherNewLeft)
                {
                    w.MoveLeftSiderTo(mi, otherNewLeft);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Vertical;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string List_TopLeft(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "左上列表布局" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double padding = 10;//以后可以考虑修改。

            double sumHeightOfOthers = padding * (otherNotLineWidgets.Count - 1);
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumHeightOfOthers += w.RealRect.Height;
            }

            double baseOffset = mainSelWidget.TopLeft.Y - 10 - sumHeightOfOthers;

            double startOffset = baseOffset;
            double otherNewRight = mainSelWidget.Center.X - mainSelWidget.RealRect.Width / 4 - 60;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveTopSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                startOffset = w.TopLeft.Y + rect.Height + padding;


                if (w.BottomRight.X != otherNewRight)
                {
                    w.MoveRightSiderTo(mi, otherNewRight);
                }
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.None;
                pw.IsSelected = true;//新添加的线处于被选定状态。
                pw.Direction = System.Windows.Controls.Orientation.Vertical;
                pw.StartMasterId = mainSelWidget.Id;
                pw.EndMasterId = w.Id;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        #endregion
    }
}
