﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/xx/linedash/dash/dashdot/dot/dotdash/线型/点划线/",
        Enums.TextCommandType.Appearance, "设置选定的线型部件的线型")]
    public class _TCSetWidgetsLineDash
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetWidgetsLineDash");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            LineDashType.DashType newDashType = LineDashType.DashType.DashDot;//默认为点划线。

            switch (parameters.Trim())
            {
                case "dash":
                case "h":
                case "hx":
                case "划":
                case "划线":
                case "-":
                case "--":
                    {
                        newDashType = LineDashType.DashType.Dash; break;
                    }
                case "dashdot":
                case "hd":
                case "dh":
                case "hdx":
                case "dhx":
                case "点划":
                case "点划线":
                case "划点":
                case "划点线":
                case "-.":
                case ".-":
                    {
                        newDashType = LineDashType.DashType.DashDot; break;
                    }
                case "dashdotdot":
                case "ddh":
                case "ddhx":
                case "hdd":
                case "hddx":
                case "点点划":
                case "点点划线":
                case "划点点":
                case "划点点线":
                case "..-":
                case "-..":
                    {
                        newDashType = LineDashType.DashType.DashDotDot; break;
                    }
                case "dot":
                case "d":
                case "点":
                case "点线":
                case ".":
                case "..":
                case "...":
                    {
                        newDashType = LineDashType.DashType.Dot; break;
                    }
                case "solid":
                case "s":
                case "实":
                case "实线":
                case "---":
                case "----":
                    {
                        newDashType = LineDashType.DashType.Solid; break;
                    }
            }

            return Commands.SetWidgetLineDashTypeCommand.Execute(newDashType);
        }
    }
}
