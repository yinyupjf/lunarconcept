﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月16日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/zz/zzc/msk/mask/fg/gz/罩子/罩住/遮罩/遮罩层/覆盖/覆盖层/盖子/",
        Enums.TextCommandType.Appearance, "设置部件遮罩")]
    public class _TCMask
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCMask");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return 　　"未找到活动页面。";

            List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();

            List<Widgets.ContentWidget> selContentWidgets = new List<Widgets.ContentWidget>();
            foreach (Widgets.Widget w in selectedWidgets)
            {
                Widgets.ContentWidget cw = w as Widgets.ContentWidget;
                if (cw != null)
                {
                    selContentWidgets.Add(cw);
                }
            }

            if (selContentWidgets.Count <= 0) return "　　未选定任何内容部件。";

            string typeString = parameters.Trim().ToLower();
            switch (typeString)
            {
                case "b":
                case "bb":
                case "cbb":
                case "blank":
                case "white":
                case "白板":
                case "纯白":
                case "纯白板":
                case "空白":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.Blank);
                        break;
                    }
                case "w":
                case "cancel":
                case "qx":
                case "无":
                case "取消":
                case "none":
                case "nomask":
                case "无遮罩":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.None);
                        break;
                    }
                case "hx":
                case "xhx":
                case "spx":
                case "xspx":
                case "横线":
                case "水平线":
                case "虚水平线":
                case "虚横线":
                case "hline":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.HLine);
                        break;
                    }
                case "sx":
                case "xsx":
                case "zx":
                case "xzx":
                case "czx":
                case "竖线":
                case "纵线":
                case "垂直线":
                case "虚竖线":
                case "虚纵线":
                case "虚垂直线":
                case "vline":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.VLine);
                        break;
                    }
                case "wg":
                case "grid":
                case "网格":
                case "hvline":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.HVLine);
                        break;
                    }
                case "zxx":
                case "左斜线":
                case "leftbias":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.LeftBias);
                        break;
                    }
                case "yxx":
                case "右斜线":
                case "rightbias":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.RightBias);
                        break;
                    }
                case "shx":
                case "sspx":
                case "实横线":
                case "实水平线":
                case "fillhline":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.FillHLine);
                        break;
                    }
                case "ssx":
                case "szx":
                case "sczx":
                case "实竖线":
                case "实纵线":
                case "实垂直线":
                case "fillvline":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.FillVLine);
                        break;
                    }
                case ""://默认值是实网格
                case "swg":
                case "fillgrid":
                case "实网格":
                case "fillhvline":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.FillHVLine);
                        break;
                    }
                case "szxx":
                case "实左斜线":
                case "fillleftbias":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.FillLeftBias);
                        break;
                    }
                case "syxx":
                case "实右斜线":
                case "fillrightbias":
                    {
                        Commands.SetWidgetMaskTypeCommand.Execute(Enums.MaskType.FillRightBias);
                        break;
                    }
                default:
                    return "　　参数不正确。";
            }


            return string.Empty;
        }
    }
}
