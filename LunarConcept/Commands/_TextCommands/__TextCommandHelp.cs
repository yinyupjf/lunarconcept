﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月24日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：显示关于命令的帮助文档。
    /// </summary>
    public static class TextCommandHelp
    {
        public static void ShowHelp(string cmdClassName)
        {
            if (cmdClassName == null || cmdClassName.Length <= 0)
            {
                MessageBox.Show("　　请提供一个命令的文本，否则无法显示此命令的帮助文档。",
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            string result = Commands.ShowHelpCommand.ShowHelpPage(cmdClassName);
            if (result != string.Empty)
            {
                MessageBox.Show("　　无法显示帮助文档。错误信息如下：\r\n　　" + result,
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
