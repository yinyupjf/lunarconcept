﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Widgets;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using System.Windows.Media;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/wbk/textarea/text/textblock/textbox/tb/文本块/文本区/文本/文本区域/", Enums.TextCommandType.Insert, "插入一个文本块")]
    public class _TCInsertTextArea
    {
        /// <summary>
        /// [静态方法]★注意：此方法的参数不能修改。否则字符串命令无法执行。
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCInsertTextArea");
                return string.Empty;
            }

            Enums.TitleStyle titleStyle;

            if (string.IsNullOrEmpty(parameters))
            {
                titleStyle = Enums.TitleStyle.Normal;
            }
            else
            {
                switch (parameters.ToLower())
                {
                    case "t1":
                    case "1":
                    case "１":
                    case "一":
                    case "一级标题":
                        {
                            titleStyle = Enums.TitleStyle.T1;
                            break;
                        }
                    case "t2":
                    case "2":
                    case "２":
                    case "二":
                    case "二级标题":
                        {
                            titleStyle = Enums.TitleStyle.T2;
                            break;
                        }
                    case "t3":
                    case "3":
                    case "３":
                    case "三":
                    case "三级标题":
                        {
                            titleStyle = Enums.TitleStyle.T3;
                            break;
                        }
                    case "t4":
                    case "4":
                    case "４":
                    case "四":
                    case "四级标题":
                        {
                            titleStyle = Enums.TitleStyle.T4;
                            break;
                        }
                    case "t5":
                    case "5":
                    case "５":
                    case "五":
                    case "五级标题":
                        {
                            titleStyle = Enums.TitleStyle.T5;
                            break;
                        }
                    case "主标题":
                    case "m":
                    case "0":
                    case "maintitle":
                        {
                            titleStyle = Enums.TitleStyle.MainTitle;
                            break;
                        }
                    default:
                        {
                            titleStyle = Enums.TitleStyle.Normal;
                            break;
                        }
                }
            }
            return InsertTextAreaWidget(titleStyle);
        }

        /// <summary>
        /// 具本执行插入。
        /// </summary>
        /// <param name="parameters">字符串参数。</param>
        /// <param name="titleStyle">标题层级。此参数只有在添加“full”型矩形时才生效。</param>
        /// <param name="editAfterInserted">是否添加后立即进入编辑状态。</param>
        /// <param name="baseTitleStyle">基准标题层级。在按Ctrl+6添加“正文级时”新添加正文级文本块的总是比baseTitleStyle再缩进一级。
        /// 例如，在一级标题上按Ctrl+6，应该添加一个正文，但缩进级为二级标题缩进级。如果为空，则按默认正文级缩进。</param>
        /// <param name="widgetLineWith">如果提供值，则优先使用此值，不再使用文本块部件默认样式中定义的默认值。</param>
        /// <returns></returns>
        internal static string InsertTextAreaWidget(Enums.TitleStyle titleStyle = Enums.TitleStyle.Normal,
            bool editAfterInserted = true, Enums.TitleStyle? baseTitleStyle = null, double? widgetLineWith = null)
        {
            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            LunarConcept.Controls.PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到当前被选定的页面。";

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "添加文本块" };

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//要选定的。

            XmlNode widgetSetNode = mainPe.WidgetSetNode;
            if (widgetSetNode == null)
            {
                return "　　未找到当前页的“WidgetSet”节点。";
            }

            Point basePoint;
            Widget mainSelWidget = mainPe.GetMainSelectedWidget();
            if (mainSelWidget != null)
            {
                if (mainSelWidget is Widgets.RectangleWidget)
                {
                    basePoint = new Point(mainSelWidget.TopLeft.X, mainSelWidget.BottomRight.Y + 2);
                }
                else
                {
                    basePoint = new Point(mainSelWidget.TopLeft.X, mainSelWidget.BottomRight.Y + 10);
                }
            }
            else
            {
                basePoint = mainPe.BaseInsertPoint;
            }

            if (basePoint.Y - 20 > mainPe.EditArea.Bottom) return "　　没有足够的垂直空间添加新部件。请插入一个新页面。";

            WidgetStyle defStyleOfDoc = manager.GetDefaultWidgetStyle(typeof(Widgets.TextArea).Name);

            TextArea tw = new TextArea(mainPe);

            switch (titleStyle)
            {
                case Enums.TitleStyle.MainTitle:
                case Enums.TitleStyle.T1:
                case Enums.TitleStyle.T2:
                case Enums.TitleStyle.T3:
                case Enums.TitleStyle.T4:
                case Enums.TitleStyle.T5:
                    {
                        XmlNode newNode;

                        newNode = widgetSetNode.AppendXmlAsChild(Properties.Resources.TextAreaXml);
                        tw.XmlData = newNode;
                        Point? newStartPoint = TitleManager.GetTitleStartPoint(manager, mainPe, basePoint, titleStyle);
                        if (newStartPoint != null && newStartPoint.HasValue)
                        {
                            tw.StartPoint = newStartPoint.Value;
                        }

                        tw.NewID();

                        if (widgetLineWith != null && widgetLineWith.HasValue)
                        {
                            tw.WidgetLineWidth = widgetLineWith.Value;
                        }
                        else
                        {
                            tw.WidgetLineWidth = 0;//标题边框默认还是0比较好。2014年7月23日
                        }

                        //tw.WidgetLineColor = defStyleOfDoc.WidgetLineColor;
                        //tw.WidgetForeColor = defStyleOfDoc.WidgetForeColor;

                        break;
                    }
                default://正文（不缩进）。
                    {
                        XmlNode newNode = widgetSetNode.AppendXmlAsChild(Properties.Resources.TextAreaXml);

                        tw.XmlData = newNode;
                        tw.NewID();
                        //tw.WidgetLineWidth = 1;

                        if (widgetLineWith != null && widgetLineWith.HasValue)
                        {
                            tw.WidgetLineWidth = widgetLineWith.Value;
                        }
                        else
                        {
                            tw.WidgetLineWidth = defStyleOfDoc.WidgetLineWidth;
                        }

                        //tw.WidgetLineColor = defStyleOfDoc.WidgetLineColor;
                        //tw.WidgetForeColor = defStyleOfDoc.WidgetForeColor;

                        if (baseTitleStyle == null || baseTitleStyle.HasValue == false)
                        {
                            Point? newStartPoint = TitleManager.GetTitleStartPoint(manager, mainPe, basePoint, titleStyle);
                            if (newStartPoint != null && newStartPoint.HasValue)
                            {
                                tw.StartPoint = newStartPoint.Value;
                            }
                        }
                        else
                        {
                            switch (baseTitleStyle.Value)
                            {
                                case Enums.TitleStyle.Normal://6
                                    {
                                        Point? newStartPoint = TitleManager.GetTitleStartPoint(manager, mainPe, basePoint, baseTitleStyle.Value);
                                        if (newStartPoint != null && newStartPoint.HasValue)
                                        {
                                            tw.StartPoint = newStartPoint.Value;
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        Point? newStartPoint = TitleManager.GetTitleStartPoint(manager, mainPe, basePoint, baseTitleStyle.Value + 1);
                                        if (newStartPoint != null && newStartPoint.HasValue)
                                        {
                                            tw.StartPoint = newStartPoint.Value;
                                        }
                                        break;
                                    }
                            }
                        }

                        break;
                    }
            }

            //初始化相关属性。
            tw.WidgetLineColor = defStyleOfDoc.WidgetLineColor;
            tw.WidgetForeColor = defStyleOfDoc.WidgetForeColor;
            tw.IsShadowVisible = defStyleOfDoc.IsShadowVisible;
            tw.WidgetBackColor = defStyleOfDoc.WidgetBackColor;

            tw.TitleLevel = titleStyle;//否则自动编号无效。

            if (tw == null) return "　　未能添加指定类型的文本块部件。";

            mainPe.AddWidget(tw);

            info.NewMainSelectedWidgetID = tw.Id;
            tw.SelectOnlySelf();

            Action actInsert = new Action(ActionType.WidgetAdded, mainPe.Id, tw.Id, null, tw.XmlData.OuterXml);
            mi.AddAction(actInsert);

            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();

            if (editAfterInserted)
            {
                return Commands.EditCommand.Execute();
            }

            return string.Empty;
        }
    }
}
