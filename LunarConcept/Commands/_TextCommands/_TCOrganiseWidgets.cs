﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年3月5日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/og/org/organise/zzjgt/organization/组织结构图/",
            Enums.TextCommandType.Alignment, "排列成组织结构图样式")]
    public class _TCOrganiseWidgets
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCOrganiseWidgets");
                return string.Empty;
            }

            string paText = parameters.Trim().ToLower();

            switch (paText)
            {
                case "l":
                case "left":
                case "z":
                case "左":
                    {
                        return Commands.OrganiseWidgetsCommand.Execute("left");
                    }
                case ""://默认为右。
                case "r":
                case "right":
                case "y":
                case "右":
                    {
                        return Commands.OrganiseWidgetsCommand.Execute("right");
                    }
                case "t":
                case "s":
                case "u":
                case "top":
                case "up":
                case "上":
                    {
                        return Commands.OrganiseWidgetsCommand.Execute("top");
                    }
                case "x":
                case "b":
                case "d":
                case "bottom":
                case "down":
                case "底":
                case "下":
                    {
                        return Commands.OrganiseWidgetsCommand.Execute("bottom");
                    }
            }

            return "　　指定的方位不是上、下、左、右中的任意一种。";
        }
    }
}
