﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年6月13日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/wbxz/xzwb/xz/xzbj/bjxz/rotate/textrotate/angle/textangle/文本旋转/旋转/旋转文本/部件文本旋转/部件旋转/旋转部件文本/",
        Enums.TextCommandType.Appearance, "设置选定部件的文本旋转角度")]
    public class _TCSetTextRotateAngle
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetTextRotateAngle");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";


            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            if (selectedWidgets.Count <= 0) return "　　未选定部件。";

            FontStyle newFontStyle = FontStyles.Italic;

            string valueStr = parameters.Trim();

            if (string.IsNullOrEmpty(valueStr) == false)
            {
                try
                {
                    double newAngle = double.Parse(valueStr);
                    return Commands.SetTextRotateAngleCommand.Execute(newAngle);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            else
            {
                return Commands.SetTextRotateAngleCommand.Execute(0);//无参数即为取消。
            }
        }
    }
}
