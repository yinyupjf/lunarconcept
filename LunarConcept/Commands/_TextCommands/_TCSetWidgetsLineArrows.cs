﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/jt/arrow/arrows/箭头/",
        Enums.TextCommandType.Appearance, "设置选定的线型部件的箭头")]
    public class _TCSetWidgetsLineArrows
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetWidgetsLineArrows");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            ArrowType newArrowType = ArrowType.End;//默认为尾箭头。

            switch (parameters.Trim())
            {
                case "start":
                case "s":
                case "sjt":
                case "首":
                case "首箭头":
                    {
                        newArrowType = ArrowType.Start; break;
                    }
                case "end":
                case "e":
                case "w":
                case "wjt":
                case "尾":
                case "尾箭头":
                    {
                        newArrowType = ArrowType.End; break;
                    }
                case "a":
                case "all":
                case "d":
                case "dy":
                case "q":
                case "qy":
                case "swjt":
                case "ljt":
                case "都":
                case "都有":
                case "全":
                case "全有":
                case "两箭头":
                case "两端箭头":
                case "全箭头":
                case "全有箭头":
                    {
                        newArrowType = ArrowType.All; break;
                    }
                case "no":
                case "none":
                case "my":
                case "myjt":
                case "n":
                case "没有":
                case "没有箭头":
                case "无箭头":
                    {
                        newArrowType = ArrowType.None; break;
                    }
                case "em":
                case "es":
                case "empty":
                case "emptystart":
                case "空箭头":
                case "空首箭头":
                    {
                        newArrowType = ArrowType.EmptyStart; break;
                    }
                case "r":
                case "rh":
                case "rhomb":
                case "rhombstart":
                case "菱形":
                case "菱形首箭头":
                    {
                        newArrowType = ArrowType.RhombStart; break;
                    }
                case "er":
                case "emptyrhomb":
                case "空菱形":
                case "空菱形首箭头":
                    {
                        newArrowType = ArrowType.EmptyRhombStart; break;
                    }
                case "op":
                case "open":
                case "openarrow":
                case "开口箭头":
                    {
                        newArrowType = ArrowType.OpenArrowEnd; break;
                    }
                case "arr":
                case "arrow":
                case "箭":
                    {
                        newArrowType = ArrowType.Arrow; break;
                    }
                case "earr":
                case "earrow":
                case "emptyarrow":
                case "空箭":
                    {
                        newArrowType = ArrowType.EmptyArrow; break;
                    }
            }

            return Commands.SetLineWidgetArrowsCommand.Execute(newArrowType);
        }
    }
}
