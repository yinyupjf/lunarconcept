﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/ip/insertpoint/crd/插入点/", Enums.TextCommandType.Move, "移动当前页的插入点")]
    public class _TCMoveBaseInsertPoint
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCMoveBaseInsertPoint");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            LunarConcept.Controls.PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到当前被选定的页面。";

            string parameter = parameters.Trim();

            try
            {
                parameter = parameter.Replace("，", ",");//转换一下。
                Point newPoint = Point.Parse(parameter);

                #region 限制在编辑区内部
                if (newPoint.X > mainPe.EditArea.Right)
                {
                    newPoint.X = mainPe.EditArea.Right;
                }

                if (newPoint.X < mainPe.EditArea.Left)
                {
                    newPoint.X = mainPe.EditArea.Left;
                }

                if (newPoint.Y > mainPe.EditArea.Bottom)
                {
                    newPoint.Y = mainPe.EditArea.Bottom;
                }

                if (newPoint.Y < mainPe.EditArea.Top)
                {
                    newPoint.Y = mainPe.EditArea.Top;
                }
                #endregion

                if (mainPe.PaperDirection == System.Windows.Controls.Orientation.Horizontal)
                {
                    if (newPoint.X > mainPe.PaperSize.LongSideWPFUnit ||
                        newPoint.Y > mainPe.PaperSize.ShortSideWPFUnit)
                        return "　　指定的位置不在页面范围内。最大坐标是[" +
                            mainPe.PaperSize.LongSideWPFUnit + "," +
                            mainPe.PaperSize.ShortSideWPFUnit + "]";
                }
                else
                {
                    if (newPoint.X > mainPe.PaperSize.ShortSideWPFUnit ||
                        newPoint.Y > mainPe.PaperSize.LongSideWPFUnit)
                        return "　　指定的位置不在页面范围内。最大坐标是[" +
                            mainPe.PaperSize.ShortSideWPFUnit + "," +
                            mainPe.PaperSize.LongSideWPFUnit + "]";
                }

                mainPe.BaseInsertPoint = newPoint;

                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
