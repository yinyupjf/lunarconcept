﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Tools;
using System.Data;
using System.Data.OleDb;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年2月3日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：复制部件到数据库命令。不允许跨页面复制。
    ///           ——这样就可以提供“用户自定义”部件功能了。
    /// 
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class CopyToDataBaseCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static CopyToDataBaseCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "CopyToDataBaseCommand",
                "CopyToDataBaseCommand",
                typeof(CopyToDataBaseCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。

            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }

            //List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();
            //if (selectedWidgets.Count <= 0) { e.CanExecute = false; return; }

            //否则用户可能认为这是Bug。
            //PageEditor peFst = widgets[0].MasterEditor;
            //for (int i = 1; i < widgets.Count; i++)
            //{
            //    if (widgets[i].MasterEditor != peFst)
            //    {
            //        e.CanExecute = false;
            //        return;
            //    }
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            string copiedWidgetTexts = string.Empty;
            string copiedXmlText = null;

            string result = CopyCommand.Execute(ref copiedWidgetTexts, ref copiedXmlText);
            if (result != string.Empty)
            {
                return result;
            }

            if (string.IsNullOrEmpty(copiedXmlText)) return "　　未能取出部件或页面Xml文本。";

            if (Globals.PathOfDataBase == null)
            {
                return "　　未找到数据库文件，此功能不可用！";
            }

            Dialogs.WidgetRecordInfo wiDlg = new Dialogs.WidgetRecordInfo() { Owner = Globals.MainWindow };

            bool? resultOfDialog = wiDlg.ShowDialog();

            if (resultOfDialog != true) return null;//取消入库操作。

            Globals.GlobalConnection.Open();
            DataTable exTable = Globals.GlobalDataSetIn.Tables["WidgetsSet"];

            string insertStr = "INSERT INTO WidgetsSet (Title,Description,InnerText,ContentText,Comment,DefaultFontSize) VALUES ('"
                        + wiDlg.WidgetTitle + "','" + wiDlg.Description + "','" + copiedWidgetTexts + "','"
                        + copiedXmlText + "','" + wiDlg.Comment + "','" +
                        Globals.MainWindow.EditorManager.DefaultFontSize.ToString() + "')";

            OleDbCommand myCommand = new OleDbCommand(insertStr, Globals.GlobalConnection);

            try
            {
                myCommand.ExecuteNonQuery();

                myCommand.CommandText = "SELECT MAX(ID) FROM WidgetsSet";
                int maxNum = (int)myCommand.ExecuteScalar();
                DataRow r = exTable.NewRow();
                r.BeginEdit();
                r["ID"] = maxNum;
                r["Title"] = wiDlg.WidgetTitle;
                r["InnerText"] = copiedWidgetTexts;
                r["Description"] = wiDlg.Description;
                r["ContentText"] = copiedXmlText;
                r["Comment"] = wiDlg.Comment;
                r["DefaultFontSize"] = Globals.MainWindow.EditorManager.DefaultFontSize;
                r.EndEdit();
                exTable.Rows.Add(r);

                Globals.GlobalConnection.Close();
            }
            finally
            {
                myCommand.Dispose();
            }
            return string.Empty;
        }

        #endregion
    }
}
