﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Converters;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年1月19日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：将当前活动页面复制为图像数据。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class CopyPageToXamlCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static CopyPageToXamlCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "CopyPageToXamlCommand",
                "CopyPageToXamlCommand",
                typeof(CopyPageToXamlCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。


            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }

            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            Execute();
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static void Execute()
        {
            if (Globals.MainWindow == null || Globals.MainWindow.EditorManager == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;

            var mainSelPage = manager.GetMainSelectedPageEditor();
            if (mainSelPage == null)
            {
                MessageBox.Show("　　请先选定一个页面。", Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            try
            {
                //2015年10月8日，本来不需要下面这两行，但为了与《LunarMind》互通，需要给出这个值。
                //所以不需要考虑撤销列表啥的。
                mainSelPage.XmlData.SetAttribute(XmlTags.AssistGridStrokeTag, BrushManager.GetName(manager.WidgetSelectionAdornerBrush));
                var documentBackgroundName = BrushManager.GetName(mainSelPage.MasterManager.DocumentBackground);
                mainSelPage.XmlData.SetAttribute(XmlTags.PageBackground, documentBackgroundName);
                mainSelPage.XmlData.SetAttribute(XmlTags.DefaultFontSizeTag, mainSelPage.MasterManager.DefaultFontSize.ToString());
                mainSelPage.XmlData.SetAttribute(XmlTags.DefaultFontNameTag, mainSelPage.MasterManager.DefaultFontName);

                mainSelPage.XmlData.SetAttribute(XmlTags.PaperDirectionTag, mainSelPage.PaperDirection.ToString());

                //还需要考虑各部件的样式，需要在复制的各部件的xmlData中写入对应的特性。
                //由于这些特性会对当前程序的运行产生影响，所以需要在特性名上动点手脚——在前面加上“ws_”前缀。
                foreach (var w in mainSelPage.GetAllWidgets())
                {
                    w.WriteWidgetStyleAttributes();
                    w.WriteAutoNumberString();
                }

                Clipboard.SetData(DataFormats.UnicodeText, "<LunarConcept>" + mainSelPage.XmlData.OuterXml + "</LunarConcept>");
            }
            catch (Exception ex)
            {
                MessageBox.Show("　　发生意外，未能向剪贴板写入当前页面的Xaml数据！系统剪贴板可能被其它程序占用。" +
                    "异常信息如下：\r\n" +
                    ex.Message + "\r\n" + ex.StackTrace, Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            //MessageBox.Show("　　已将当前页面的Xaml数据复制到剪贴板！", Globals.AppName,
            //    MessageBoxButton.OK, MessageBoxImage.Information);
        }

        #endregion
    }
}
