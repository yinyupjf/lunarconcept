﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.ModifingManager
{
    /// <summary>
    /// 创建时间：2011年12月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：一个类实现此接口就意味着此类需要使用并支持“撤销与重做功能”。
    /// </summary>
    /// <typeparam name="TAction">表示“动作（Action）”的自定义类型。</typeparam>
    /// <typeparam name="TInfo">表示与一次“修改”有关的信息（自定义的一个信息类）。</typeparam>
    public interface IModifingManager<TAction, TInfo>
    {
        /// <summary>
        /// [只读]一个非空列表。承载用于进行“撤销”与“删除”操作的项目列表。
        /// </summary>
        ModifingList<TAction, TInfo> ModifingItemsList { get; }

        /// <summary>
        /// [读写]表示当前文档是否已经被修改的属性。
        /// </summary>
        bool IsModified { get; set; }

        /// <summary>
        /// 应是对ModifingsList类中同名方法的封装。
        /// </summary>
        /// <param name="mi"></param>
        void RegisterModifingItem(ModifingItem<TAction, TInfo> mi);
    }
}
