﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.ModifingManager
{
    /// <summary>
    /// 创建时间：2011年12月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：此类与Action类是实际使用的记录“修改操作信息”的类。
    ///           在使用泛型的ImodifingManager/ModifingList等类时，需要提供两个实际可用的类：
    ///               ①与“修改”有关的信息类；②与“修改”中的“动作”有关的信息类。
    ///           
    ///           本程序中，与“修改”有关的信息类就是此类；与“修改”中某个“动作”有关的信息类是Action类。
    /// </summary>
    public class ModifingInfo
    {
        #region 构造方法=====================================================================================================

        public ModifingInfo()
        {

        }

        #endregion

        #region 字段与属性===================================================================================================

        private string modifingDescription = null;
        /// <summary>
        /// 此次进行的“修改”操作的描述性文本。务求简短。
        /// ——例如：“设置部件边框色”、“删除文本区部件”等等说明文本。
        /// </summary>
        public string ModifingDescription
        {
            get { return modifingDescription; }
            set { modifingDescription = value; }
        }

        private List<string> newSelectedPageEditorIDList;
        /// <summary>
        /// 此次修改后处于被选定状态的PageEditor的ID列表。
        /// </summary>
        public List<string> NewSelectedPageEditorIDList
        {
            get { return newSelectedPageEditorIDList; }
        }

        private string newMainSelectedPageEditorID = null;
        /// <summary>
        /// 表示此次修改后处于“活动”状态的被选定PageEditor的ID。
        /// </summary>
        public string NewMainSelectedPageEditorID
        {
            get { return newMainSelectedPageEditorID; }
            set
            {
                newMainSelectedPageEditorID = value;
                if (this.newSelectedPageEditorIDList == null)
                {
                    this.newSelectedPageEditorIDList = new List<string>();
                }

                if (this.newSelectedPageEditorIDList.Contains(value) == false)
                {
                    this.newSelectedPageEditorIDList.Add(value);
                }
            }
        }

        private string newMainSelectedWidgetID = null;
        /// <summary>
        /// 表示此次修改后处于“活动”状态的被选定Widget的ID。
        /// </summary>
        public string NewMainSelectedWidgetID
        {
            get { return newMainSelectedWidgetID; }
            set
            {
                newMainSelectedWidgetID = value;
                if (this.newSelectedWidgetIDList == null)
                {
                    this.newSelectedWidgetIDList = new List<string>();
                }

                if (this.newSelectedWidgetIDList.Contains(value) == false)
                {
                    this.newSelectedWidgetIDList.Add(value);
                }
            }
        }

        private List<string> newSelectedWidgetIDList;
        /// <summary>
        /// 表示此次修改后所有选定的Widget的ID集合。
        /// </summary>
        public List<string> NewSelectedWidgetIDList
        {
            get { return newSelectedWidgetIDList; }
        }

        private List<string> oldSelectedPageEditorIDList;
        /// <summary>
        /// 修改前处于选定状态的PageEditor的ID列表。
        /// </summary>
        public List<string> OldSelectedPageEditorIDList
        {
            get { return oldSelectedPageEditorIDList; }
        }

        private string oldMainSelectedPageEditorID;
        /// <summary>
        /// 表示此次修改前处于“活动”状态的被选定PageEditor的ID。
        /// </summary>
        public string OldMainSelectedPageEditorID
        {
            get { return oldMainSelectedPageEditorID; }
            set { oldMainSelectedPageEditorID = value; }
        }

        private string oldMainSelectedWidgetID;
        /// <summary>
        /// 表示此次修改前处于“活动”状态的被选定Widget的ID。
        /// </summary>
        public string OldMainSelectedWidgetID
        {
            get { return oldMainSelectedWidgetID; }
            set { oldMainSelectedWidgetID = value; }
        }

        private List<string> oldSelectedWidgetIDList;
        /// <summary>
        /// 表示此次修改前所有选定的Widget的ID集合。
        /// </summary>
        public List<string> OldSelectedWidgetIDList
        {
            get { return oldSelectedWidgetIDList; }
        }

        #endregion


        #region 方法=========================================================================================================

        /// <summary>
        /// 向newSelectedPageEditorIDList中添加一个PageEditor的ID。
        /// </summary>
        /// <param name="newPageID">此次修改后处于选定状态的某个PageEditor的ID。</param>
        public void AddPageEditorID_NewSelected(string newPageEditorID)
        {
            if (newPageEditorID == null) return;

            if (newSelectedPageEditorIDList == null)
                newSelectedPageEditorIDList = new List<string>();

            newSelectedPageEditorIDList.Add(newPageEditorID);
        }

        /// <summary>
        /// 向oldSelectedPageEditorIDList中添加一个PageEditor的ID。
        /// </summary>
        /// <param name="oldPageID">此次修改前处于选定状态的某个PageEditor的ID。</param>
        public void AddPageEditorID_OldSelected(string oldPageEditorID)
        {
            if (oldPageEditorID == null) return;

            if (oldSelectedPageEditorIDList == null)
                oldSelectedPageEditorIDList = new List<string>();

            oldSelectedPageEditorIDList.Add(oldPageEditorID);
        }

        /// <summary>
        /// 向oldSelectedWidgetIDList中添加一个部件的ID。
        /// </summary>
        /// <param name="oldID">修改前选定的某个Widget的ID。</param>
        public void AddWidgetID_OldSelected(string oldID)
        {
            if (oldID == null) return;

            if (oldSelectedWidgetIDList == null)
                oldSelectedWidgetIDList = new List<string>();

            oldSelectedWidgetIDList.Add(oldID);
        }

        /// <summary>
        /// 向newSelectedWidgetIDList中添加一个部件的ID。
        /// </summary>
        /// <param name="newID">修改后选定的某个Widget的ID。</param>
        public void AddWidgetID_NewSelected(string newID)
        {
            if (newID == null) return;

            if (newSelectedWidgetIDList == null)
                newSelectedWidgetIDList = new List<string>();

            newSelectedWidgetIDList.Add(newID);
        }

        /// <summary>
        /// 向newSelectedPageEditorIDList中添加一个PageEditor的ID。
        /// 
        /// 异常：
        ///     IndexOutOfRangeException
        /// </summary>
        /// <param name="oldID">修改后选定的某个PageEditor的ID。</param>
        /// <param name="index"></param>
        public void InsertPageEditorID_NewSelected(string newPageEditorID, int index)
        {
            if (newPageEditorID == null) return;

            if (0 > index)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertPageEditorID_NewSelected()方法的“index”参数值越界。");

            if (newSelectedPageEditorIDList == null)
            {
                newSelectedPageEditorIDList = new List<string>();
            }

            if (index > newSelectedPageEditorIDList.Count)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertPageEditorID_NewSelected()方法的“index”参数值越界。");

            newSelectedPageEditorIDList.Insert(index, newPageEditorID);
        }

        /// <summary>
        /// 向oldSelectedPageEditorIDList中添加一个PageEditor的ID。
        /// 
        /// 异常：
        ///     IndexOutOfRangeException
        /// </summary>
        /// <param name="oldID">修改前选定的某个PageEditor的ID。</param>
        /// <param name="index"></param>
        public void InsertPageEditorID_OldSelected(string oldPageEditorID, int index)
        {
            if (oldPageEditorID == null) return;

            if (0 > index)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertPageEditorID_OldSelected()方法的“index”参数值越界。");

            if (oldSelectedPageEditorIDList == null)
            {
                oldSelectedPageEditorIDList = new List<string>();
            }

            if (index > oldSelectedPageEditorIDList.Count)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertPageEditorID_OldSelected()方法的“index”参数值越界。");

            oldSelectedPageEditorIDList.Insert(index, oldPageEditorID);
        }

        /// <summary>
        /// 向oldSelectedWidgetIDList中插入一个部件的ID。
        /// 
        /// 异常：
        ///     IndexOutOfRangeException
        /// </summary>
        /// <param name="oldID">修改前选定的某个Widget的ID。</param>
        /// <param name="index"></param>
        public void InsertWidgetID_OldSelected(string oldID, int index)
        {
            if (oldID == null) return;

            if (0 > index)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertWidgetID_OldSelected()方法的“index”参数值越界。");

            if (oldSelectedWidgetIDList == null)
            {
                oldSelectedWidgetIDList = new List<string>();
            }

            if (index > oldSelectedWidgetIDList.Count)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertWidgetID_OldSelected()方法的“index”参数值越界。");

            oldSelectedWidgetIDList.Insert(index, oldID);
        }

        /// <summary>
        /// 向newSelectedWidgetIDList中添加一个部件的ID。
        /// 
        /// 异常：
        ///     IndexOutOfRangeException
        /// </summary>
        /// <param name="newID">修改前选定的某个Widget的ID。</param>
        /// <param name="index"></param>
        public void InsertWidgetID_NewSelected(string newID, int index)
        {
            if (newID == null) return;

            if (0 > index)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertWidgetID_NewSelected()方法的“index”参数值越界。");

            if (newSelectedWidgetIDList == null)
            {
                newSelectedWidgetIDList = new List<string>();
            }

            if (index > newSelectedWidgetIDList.Count)
                throw new ArgumentOutOfRangeException("ModifingItems.InsertWidgetID_NewSelected()方法的“index”参数值越界。");

            newSelectedWidgetIDList.Insert(index, newID);
        }

        #endregion
    }
}
