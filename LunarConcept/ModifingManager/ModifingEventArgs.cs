﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.ModifingManager
{
    /// <summary>
    /// 创建时间：2011年12月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于ModifingList类中的UndoExcuted和RedoExcuted事件。
    /// </summary>
    public class ModifingEventArgs<TAction, TInfo> : EventArgs
    {
        /// <summary>
        /// [构造方法]要传入修改项的数据。
        /// </summary>
        /// <param name="mi">修改项。</param>
        public ModifingEventArgs(ModifingItem<TAction, TInfo> modifingItem)
        {
            this.modifingItem = modifingItem;
        }

        private ModifingItem<TAction, TInfo> modifingItem;
        /// <summary>
        /// “撤销”与“重做”时表示一次修改的“修改项”，其中包括多个“动作（Action）”。
        /// </summary>
        public ModifingItem<TAction, TInfo> ModifingItem
        {
            get { return modifingItem; }
            set { modifingItem = value; }
        }
    }
}
