﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace SHomeWorkshop.LunarConcept.Tools
{
    /// <summary>
    /// 创建时间：取部件所在的层次，按倒序排列。
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：便于撤销“删除操作”后保持部件的层级关系。
    /// </summary>
    public class WidgetLayerZACompareClass : IComparer<Widgets.Widget>
    {
        public int Compare(Widgets.Widget x, Widgets.Widget y)
        {
            if (x == y) return 0;

            int xLayerIndex = Canvas.GetZIndex(x);
            int yLayerIndex = Canvas.GetZIndex(y);

            if (xLayerIndex > yLayerIndex) return -1;
            else if (xLayerIndex < yLayerIndex) return 1;
            else
            {
                Canvas parentX = x.Parent as Canvas;
                Canvas parentY = y.Parent as Canvas;

                if (parentY != null && parentY != null && parentX == parentY)
                {
                    int xIndex = parentY.Children.IndexOf(x);
                    int yIndex = parentY.Children.IndexOf(y);

                    if (xIndex > yIndex) return -1;
                    else return 1;
                }

                return 0;
            }
        }
    }
}
