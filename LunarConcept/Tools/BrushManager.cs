﻿using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace SHomeWorkshop.LunarConcept.Tools
{
    /// <summary>
    /// 创建时间：2011年12月27日
    /// 创 建 者：　杨震宇
    /// 
    /// 主要用途：提供中、英文色彩名称到Brushe实例的转换。
    /// ★★★★★此类最有价值的是静态构造方法中的《色彩名称中英文对照表》！
    /// </summary>
    public static class BrushManager
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]用于初始化brushesList列表，以便查找。
        /// 
        /// ——此类【最有价值】的部分就在此处！！！
        /// </summary>
        static BrushManager()
        {
            //生成自定义的玻璃效果的VisualBrush。
            glass = BuildGlassVisualBrush();

            streamer = BuildStreamerBrush();
            antiStreamer = BuildAntiStreamerBrush();
            sevenColors = BuildSevenColorsBrush();
            underLine = BuildUnderLineBrush();

            BuildRoundGlassButtonBrushes();
            BuildRectangleGlassButtonBrushes();
            BuildBubbles();
            BuildHollowBubbles();
            BuildSocialBrushes();
            BuildGrids();

            brushesList = new List<BrushItem>();

            brushesList.Add(new BrushItem(glass, "Glass", "玻璃"));
            brushesList.Add(new BrushItem(streamer, "Streamer", "彩带"));
            brushesList.Add(new BrushItem(antiStreamer, "AntiStreamer", "反色彩带"));
            brushesList.Add(new BrushItem(sevenColors, "SevenColors", "七彩"));

            #region 添加玻璃按钮画刷
            brushesList.Add(new BrushItem(roundGlassButton_Black, "RoundGlassButton_Black", "圆玻璃按钮黑"));
            brushesList.Add(new BrushItem(roundGlassButton_Blue, "RoundGlassButton_Blue", "圆玻璃按钮蓝"));
            brushesList.Add(new BrushItem(roundGlassButton_Brown, "RoundGlassButton_Brown", "圆玻璃按钮棕"));
            brushesList.Add(new BrushItem(roundGlassButton_Gold, "RoundGlassButton_Gold", "圆玻璃按钮金"));
            brushesList.Add(new BrushItem(roundGlassButton_Gray, "RoundGlassButton_Gray", "圆玻璃按钮灰"));
            brushesList.Add(new BrushItem(roundGlassButton_Green, "RoundGlassButton_Green", "圆玻璃按钮绿"));
            brushesList.Add(new BrushItem(roundGlassButton_Orange, "RoundGlassButton_Orange", "圆玻璃按钮橙"));
            brushesList.Add(new BrushItem(roundGlassButton_Red, "RoundGlassButton_Red", "圆玻璃按钮红"));
            brushesList.Add(new BrushItem(roundGlassButton_Violet, "RoundGlassButton_Violet", "圆玻璃按钮紫"));
            brushesList.Add(new BrushItem(roundGlassButton_DeepPink, "RoundGlassButton_DeepPink", "圆玻璃按钮深粉红"));

            brushesList.Add(new BrushItem(rectangleGlassButton_Black, "RectangleGlassButton_Black", "方玻璃按钮黑"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Blue, "RectangleGlassButton_Blue", "方玻璃按钮蓝"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Brown, "RectangleGlassButton_Brown", "方玻璃按钮棕"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Gold, "RectangleGlassButton_Gold", "方玻璃按钮金"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Gray, "RectangleGlassButton_Gray", "方玻璃按钮灰"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Green, "RectangleGlassButton_Green", "方玻璃按钮绿"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Orange, "RectangleGlassButton_Orange", "方玻璃按钮橙"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Red, "RectangleGlassButton_Red", "方玻璃按钮红"));
            brushesList.Add(new BrushItem(rectangleGlassButton_Violet, "RectangleGlassButton_Violet", "方玻璃按钮紫"));
            brushesList.Add(new BrushItem(rectangleGlassButton_DeepPink, "RectangleGlassButton_DeepPink", "方玻璃按钮深粉红"));

            brushesList.Add(new BrushItem(bubbleRedAtBottomLeft, "BubbleRedAtBottomLeft", "左下红底气泡"));
            brushesList.Add(new BrushItem(bubbleDeepPinkAtBottomLeft, "BubbleDeepPinkAtBottomLeft", "左下红底气泡"));
            brushesList.Add(new BrushItem(bubbleBlueAtBottomLeft, "BubbleBlueAtBottomLeft", "左下红底气泡"));
            brushesList.Add(new BrushItem(bubbleGreenAtBottomLeft, "BubbleGreenAtBottomLeft", "左下红底气泡"));
            brushesList.Add(new BrushItem(bubbleBrownAtBottomLeft, "BubbleBrownAtBottomLeft", "左下红底气泡"));
            brushesList.Add(new BrushItem(bubbleBlackAtBottomLeft, "BubbleBlackAtBottomLeft", "左下红底气泡"));

            brushesList.Add(new BrushItem(bubbleRedAtBottomRight, "BubbleRedAtBottomRight", "右下红底气泡"));
            brushesList.Add(new BrushItem(bubbleDeepPinkAtBottomRight, "BubbleDeepPinkAtBottomRight", "右下红底气泡"));
            brushesList.Add(new BrushItem(bubbleBlueAtBottomRight, "BubbleBlueAtBottomRight", "右下红底气泡"));
            brushesList.Add(new BrushItem(bubbleGreenAtBottomRight, "BubbleGreenAtBottomRight", "右下红底气泡"));
            brushesList.Add(new BrushItem(bubbleBrownAtBottomRight, "BubbleBrownAtBottomRight", "右下红底气泡"));
            brushesList.Add(new BrushItem(bubbleBlackAtBottomRight, "BubbleBlackAtBottomRight", "右下红底气泡"));

            brushesList.Add(new BrushItem(bubbleRedAtTopLeft, "BubbleRedAtTopLeft", "左上红底气泡"));
            brushesList.Add(new BrushItem(bubbleDeepPinkAtTopLeft, "BubbleDeepPinkAtTopLeft", "左上深粉红底气泡"));
            brushesList.Add(new BrushItem(bubbleBlueAtTopLeft, "BubbleBlueAtTopLeft", "左上蓝底气泡"));
            brushesList.Add(new BrushItem(bubbleGreenAtTopLeft, "BubbleGreenAtTopLeft", "左上绿底气泡"));
            brushesList.Add(new BrushItem(bubbleBrownAtTopLeft, "BubbleBrownAtTopLeft", "左上棕底气泡"));
            brushesList.Add(new BrushItem(bubbleBlackAtTopLeft, "BubbleBlackAtTopLeft", "左上黑底气泡"));

            brushesList.Add(new BrushItem(bubbleRedAtTopRight, "BubbleRedAtTopRight", "右上红底气泡"));
            brushesList.Add(new BrushItem(bubbleDeepPinkAtTopRight, "BubbleDeepPinkAtTopRight", "右上深粉红底气泡"));
            brushesList.Add(new BrushItem(bubbleBlueAtTopRight, "BubbleBlueAtTopRight", "右上蓝底气泡"));
            brushesList.Add(new BrushItem(bubbleGreenAtTopRight, "BubbleGreenAtTopRight", "右上绿底气泡"));
            brushesList.Add(new BrushItem(bubbleBrownAtTopRight, "BubbleBrownAtTopRight", "右上棕底气泡"));
            brushesList.Add(new BrushItem(bubbleBlackAtTopRight, "BubbleBlackAtTopRight", "右上黑底气泡"));

            #endregion

            #region 空心气泡

            brushesList.Add(new BrushItem(hollowBubbleRedAtBottomLeft, "HollowBubbleRedAtBottomLeft", "左下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleDeepPinkAtBottomLeft, "HollowBubbleDeepPinkAtBottomLeft", "左下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlueAtBottomLeft, "HollowBubbleBlueAtBottomLeft", "左下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleGreenAtBottomLeft, "HollowBubbleGreenAtBottomLeft", "左下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBrownAtBottomLeft, "HollowBubbleBrownAtBottomLeft", "左下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlackAtBottomLeft, "HollowBubbleBlackAtBottomLeft", "左下红气泡"));

            brushesList.Add(new BrushItem(hollowBubbleRedAtBottomRight, "HollowBubbleRedAtBottomRight", "右下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleDeepPinkAtBottomRight, "HollowBubbleDeepPinkAtBottomRight", "右下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlueAtBottomRight, "HollowBubbleBlueAtBottomRight", "右下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleGreenAtBottomRight, "HollowBubbleGreenAtBottomRight", "右下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBrownAtBottomRight, "HollowBubbleBrownAtBottomRight", "右下红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlackAtBottomRight, "HollowBubbleBlackAtBottomRight", "右下红气泡"));

            brushesList.Add(new BrushItem(hollowBubbleRedAtTopLeft, "HollowBubbleRedAtTopLeft", "左上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleDeepPinkAtTopLeft, "HollowBubbleDeepPinkAtTopLeft", "左上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlueAtTopLeft, "HollowBubbleBlueAtTopLeft", "左上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleGreenAtTopLeft, "HollowBubbleGreenAtTopLeft", "左上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBrownAtTopLeft, "HollowBubbleBrownAtTopLeft", "左上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlackAtTopLeft, "HollowBubbleBlackAtTopLeft", "左上红气泡"));

            brushesList.Add(new BrushItem(hollowBubbleRedAtTopRight, "HollowBubbleRedAtTopRight", "右上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleDeepPinkAtTopRight, "HollowBubbleDeepPinkAtTopRight", "右上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlueAtTopRight, "HollowBubbleBlueAtTopRight", "右上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleGreenAtTopRight, "HollowBubbleGreenAtTopRight", "右上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBrownAtTopRight, "HollowBubbleBrownAtTopRight", "右上红气泡"));
            brushesList.Add(new BrushItem(hollowBubbleBlackAtTopRight, "HollowBubbleBlackAtTopRight", "右上红气泡"));
            #endregion

            #region 社工常用几种图例背景
            brushesList.Add(new BrushItem(slash, "Slash", "斜线"));
            brushesList.Add(new BrushItem(antiSlash, "AntiSlash", "反斜线"));
            brushesList.Add(new BrushItem(crossSlash, "CrossSlash", "交叉斜线"));
            brushesList.Add(new BrushItem(topBlock, "TopBlock", "上实心块"));
            brushesList.Add(new BrushItem(leftBlock, "LeftBlock", "左实心块"));
            brushesList.Add(new BrushItem(bottomBlock, "BottomBlock", "下实心块"));
            brushesList.Add(new BrushItem(rightBlock, "RightBlock", "右实心块"));
            brushesList.Add(new BrushItem(rightTopBlock, "RightTopBlock", "右上实心块"));
            brushesList.Add(new BrushItem(rightBottomBlock, "RightBottomBlock", "右下实心块"));
            brushesList.Add(new BrushItem(leftTopBlock, "LeftTopBlock", "左上实心块"));
            brushesList.Add(new BrushItem(leftBottomBlock, "LeftBottomBlock", "左下实心块"));

            #endregion

            #region 网格画刷

            //brushesList.Add(new BrushItem(miziBlack, "MiziBlack", "米字黑"));
            brushesList.Add(new BrushItem(tianziBlack, "TianziBlack", "田字黑"));
            brushesList.Add(new BrushItem(gridBlack, "GridBlack", "网格黑"));
            brushesList.Add(new BrushItem(huiziBlack, "HuiziBlack", "回宫黑"));
            brushesList.Add(new BrushItem(huitianBlack, "HuitianBlack", "回田黑"));

            #endregion

            brushesList.Add(new BrushItem(Brushes.AliceBlue, "AliceBlue", "爱丽丝蓝"));
            brushesList.Add(new BrushItem(Brushes.AntiqueWhite, "AntiqueWhite", "古董白"));
            brushesList.Add(new BrushItem(Brushes.Aqua, "Aqua", "水绿"));
            brushesList.Add(new BrushItem(Brushes.Aquamarine, "Aquamarine", "碧玉绿"));
            brushesList.Add(new BrushItem(Brushes.Azure, "Azure", "蔚蓝"));
            brushesList.Add(new BrushItem(Brushes.Beige, "Beige", "米色"));
            brushesList.Add(new BrushItem(Brushes.Bisque, "Bisque", "乳脂色"));
            brushesList.Add(new BrushItem(Brushes.Black, "Black", "黑色"));
            brushesList.Add(new BrushItem(Brushes.BlanchedAlmond, "BlanchedAlmond", "白杏色"));
            brushesList.Add(new BrushItem(Brushes.Blue, "Blue", "蓝色"));
            brushesList.Add(new BrushItem(Brushes.BlueViolet, "BlueViolet", "蓝紫罗兰色"));
            brushesList.Add(new BrushItem(Brushes.Brown, "Brown", "棕色"));
            brushesList.Add(new BrushItem(Brushes.BurlyWood, "BurlyWood", "坚树色"));
            brushesList.Add(new BrushItem(Brushes.CadetBlue, "CadetBlue", "军校蓝"));
            brushesList.Add(new BrushItem(Brushes.Chartreuse, "Chartreuse", "查特酒绿"));
            brushesList.Add(new BrushItem(Brushes.Chocolate, "Chocolate", "巧克力色"));
            brushesList.Add(new BrushItem(Brushes.Coral, "Coral", "珊瑚色"));
            brushesList.Add(new BrushItem(Brushes.CornflowerBlue, "CornflowerBlue", "矢车菊蓝"));
            brushesList.Add(new BrushItem(Brushes.Cornsilk, "Cornsilk", "玉米色"));
            brushesList.Add(new BrushItem(Brushes.Crimson, "Crimson", "猩红"));
            brushesList.Add(new BrushItem(Brushes.Cyan, "Cyan", "青色"));
            brushesList.Add(new BrushItem(Brushes.DarkBlue, "DarkBlue", "深蓝"));
            brushesList.Add(new BrushItem(Brushes.DarkCyan, "DarkCyan", "深青"));
            brushesList.Add(new BrushItem(Brushes.DarkGoldenrod, "DarkGoldenrod", "深金菊色"));
            brushesList.Add(new BrushItem(Brushes.DarkGray, "DarkGray", "深灰"));
            brushesList.Add(new BrushItem(Brushes.DarkGreen, "DarkGreen", "深绿"));
            brushesList.Add(new BrushItem(Brushes.DarkKhaki, "DarkKhaki", "深卡其布色"));
            brushesList.Add(new BrushItem(Brushes.DarkMagenta, "DarkMagenta", "深洋红"));
            brushesList.Add(new BrushItem(Brushes.DarkOliveGreen, "DarkOliveGreen", "深橄榄绿"));
            brushesList.Add(new BrushItem(Brushes.DarkOrange, "DarkOrange", "深橙色"));
            brushesList.Add(new BrushItem(Brushes.DarkOrchid, "DarkOrchid", "深兰花紫"));
            brushesList.Add(new BrushItem(Brushes.DarkRed, "DarkRed", "深红"));
            brushesList.Add(new BrushItem(Brushes.DarkSalmon, "DarkSalmon", "鲑鱼色"));
            brushesList.Add(new BrushItem(Brushes.DarkSeaGreen, "DarkSeaGreen", "深海绿"));
            brushesList.Add(new BrushItem(Brushes.DarkSlateBlue, "DarkSlateBlue", "深石板蓝"));
            brushesList.Add(new BrushItem(Brushes.DarkSlateGray, "DarkSlateGray", "深石板灰"));
            brushesList.Add(new BrushItem(Brushes.DarkTurquoise, "DarkTurquoise", "深宝石绿"));
            brushesList.Add(new BrushItem(Brushes.DarkViolet, "DarkViolet", "深紫罗兰色"));
            brushesList.Add(new BrushItem(Brushes.DeepPink, "DeepPink", "深粉色"));
            brushesList.Add(new BrushItem(Brushes.DeepSkyBlue, "DeepSkyBlue", "深天蓝"));
            brushesList.Add(new BrushItem(Brushes.DimGray, "DimGray", "暗灰"));
            brushesList.Add(new BrushItem(Brushes.DodgerBlue, "DodgerBlue", "道奇蓝"));
            brushesList.Add(new BrushItem(Brushes.Firebrick, "Firebrick", "耐火砖色"));
            brushesList.Add(new BrushItem(Brushes.FloralWhite, "FloralWhite", "花白"));
            brushesList.Add(new BrushItem(Brushes.ForestGreen, "ForestGreen", "森林绿"));
            brushesList.Add(new BrushItem(Brushes.Fuchsia, "Fuchsia", "灯笼海棠色"));
            brushesList.Add(new BrushItem(Brushes.Gainsboro, "Gainsboro", "庚斯博罗灰"));
            brushesList.Add(new BrushItem(Brushes.GhostWhite, "GhostWhite", "幽灵白"));
            brushesList.Add(new BrushItem(Brushes.Gold, "Gold", "金色"));
            brushesList.Add(new BrushItem(Brushes.Goldenrod, "Goldenrod", "金菊黄"));
            brushesList.Add(new BrushItem(Brushes.Gray, "Gray", "灰色"));
            brushesList.Add(new BrushItem(Brushes.Green, "Green", "绿色"));
            brushesList.Add(new BrushItem(Brushes.GreenYellow, "GreenYellow", "绿黄"));
            brushesList.Add(new BrushItem(Brushes.Honeydew, "Honeydew", "蜜色"));
            brushesList.Add(new BrushItem(Brushes.HotPink, "HotPink", "热粉色"));
            brushesList.Add(new BrushItem(Brushes.IndianRed, "IndianRed", "印度红"));
            brushesList.Add(new BrushItem(Brushes.Indigo, "Indigo", "靛青"));
            brushesList.Add(new BrushItem(Brushes.Ivory, "Ivory", "象牙色"));
            brushesList.Add(new BrushItem(Brushes.Khaki, "Khaki", "卡其布色"));
            brushesList.Add(new BrushItem(Brushes.Lavender, "Lavender", "熏衣草色"));
            brushesList.Add(new BrushItem(Brushes.LavenderBlush, "LavenderBlush", "熏衣草红"));
            brushesList.Add(new BrushItem(Brushes.LawnGreen, "LawnGreen", "草坪绿"));
            brushesList.Add(new BrushItem(Brushes.LemonChiffon, "LemonChiffon", "柠檬绸色"));
            brushesList.Add(new BrushItem(Brushes.LightBlue, "LightBlue", "浅蓝"));
            brushesList.Add(new BrushItem(Brushes.LightCoral, "LightCoral", "浅珊瑚色"));
            brushesList.Add(new BrushItem(Brushes.LightCyan, "LightCyan", "浅青色"));
            brushesList.Add(new BrushItem(Brushes.LightGoldenrodYellow, "LightGoldenrodYellow", "浅金菊黄"));
            brushesList.Add(new BrushItem(Brushes.LightGray, "LightGray", "浅灰"));
            brushesList.Add(new BrushItem(Brushes.LightGreen, "LightGreen", "浅绿"));
            brushesList.Add(new BrushItem(Brushes.LightPink, "LightPink", "浅粉色"));
            brushesList.Add(new BrushItem(Brushes.LightSalmon, "LightSalmon", "浅鲑鱼肉色"));
            brushesList.Add(new BrushItem(Brushes.LightSeaGreen, "LightSeaGreen", "浅海绿"));
            brushesList.Add(new BrushItem(Brushes.LightSkyBlue, "LightSkyBlue", "浅海蓝"));
            brushesList.Add(new BrushItem(Brushes.LightSlateGray, "LightSlateGray", "浅石板灰"));
            brushesList.Add(new BrushItem(Brushes.LightSteelBlue, "LightSteelBlue", "浅钢蓝"));
            brushesList.Add(new BrushItem(Brushes.LightYellow, "LightYellow", "浅黄"));
            brushesList.Add(new BrushItem(Brushes.Lime, "Lime", "酸橙色"));
            brushesList.Add(new BrushItem(Brushes.LimeGreen, "LimeGreen", "酸橙绿"));
            brushesList.Add(new BrushItem(Brushes.Linen, "Linen", "亚麻布色"));
            brushesList.Add(new BrushItem(Brushes.Magenta, "Magenta", "洋红"));
            brushesList.Add(new BrushItem(Brushes.Maroon, "Maroon", "栗色"));
            brushesList.Add(new BrushItem(Brushes.MediumAquamarine, "MediumAquamarine", "中碧玉绿"));
            brushesList.Add(new BrushItem(Brushes.MediumBlue, "MediumBlue", "中蓝"));
            brushesList.Add(new BrushItem(Brushes.MediumOrchid, "MediumOrchid", "中兰花紫"));
            brushesList.Add(new BrushItem(Brushes.MediumPurple, "MediumPurple", "中紫"));
            brushesList.Add(new BrushItem(Brushes.MediumSeaGreen, "MediumSeaGreen", "中海绿"));
            brushesList.Add(new BrushItem(Brushes.MediumSlateBlue, "MediumSlateBlue", "中石板蓝"));
            brushesList.Add(new BrushItem(Brushes.MediumSpringGreen, "MediumSpringGreen", "中春绿"));
            brushesList.Add(new BrushItem(Brushes.MediumTurquoise, "MediumTurquoise", "中宝石绿"));
            brushesList.Add(new BrushItem(Brushes.MediumVioletRed, "MediumVioletRed", "中紫罗兰红"));
            brushesList.Add(new BrushItem(Brushes.MidnightBlue, "MidnightBlue", "午夜蓝"));
            brushesList.Add(new BrushItem(Brushes.MintCream, "MintCream", "薄荷奶油色"));
            brushesList.Add(new BrushItem(Brushes.MistyRose, "MistyRose", "薄雾玫瑰色"));
            brushesList.Add(new BrushItem(Brushes.Moccasin, "Moccasin", "鹿皮色"));
            brushesList.Add(new BrushItem(Brushes.NavajoWhite, "NavajoWhite", "纳瓦白/土著白"));
            brushesList.Add(new BrushItem(Brushes.Navy, "Navy", "海军蓝"));
            brushesList.Add(new BrushItem(Brushes.OldLace, "OldLace", "旧蕾丝色"));
            brushesList.Add(new BrushItem(Brushes.Olive, "Olive", "橄榄色"));
            brushesList.Add(new BrushItem(Brushes.OliveDrab, "OliveDrab", "橄榄褐"));
            brushesList.Add(new BrushItem(Brushes.Orange, "Orange", "橙色"));
            brushesList.Add(new BrushItem(Brushes.OrangeRed, "OrangeRed", "橙红"));
            brushesList.Add(new BrushItem(Brushes.Orchid, "Orchid", "兰花紫"));
            brushesList.Add(new BrushItem(Brushes.PaleGoldenrod, "PaleGoldenrod", "灰金菊色"));
            brushesList.Add(new BrushItem(Brushes.PaleGreen, "PaleGreen", "灰绿"));
            brushesList.Add(new BrushItem(Brushes.PaleTurquoise, "PaleTurquoise", "灰宝石绿"));
            brushesList.Add(new BrushItem(Brushes.PaleVioletRed, "PaleVioletRed", "灰紫罗兰红"));
            brushesList.Add(new BrushItem(Brushes.PapayaWhip, "PapayaWhip", "番木瓜色"));
            brushesList.Add(new BrushItem(Brushes.PeachPuff, "PeachPuff", "桃色"));
            brushesList.Add(new BrushItem(Brushes.Peru, "Peru", "秘鲁色"));
            brushesList.Add(new BrushItem(Brushes.Pink, "Pink", "粉红"));
            brushesList.Add(new BrushItem(Brushes.Plum, "Plum", "李色"));
            brushesList.Add(new BrushItem(Brushes.PowderBlue, "PowderBlue", "粉蓝"));
            brushesList.Add(new BrushItem(Brushes.Purple, "Purple", "紫色"));
            brushesList.Add(new BrushItem(Brushes.Red, "Red", "红色"));
            brushesList.Add(new BrushItem(Brushes.RosyBrown, "RosyBrown", "玫瑰棕色"));
            brushesList.Add(new BrushItem(Brushes.RoyalBlue, "RoyalBlue", "皇室蓝"));
            brushesList.Add(new BrushItem(Brushes.SaddleBrown, "SaddleBrown", "重褐色"));
            brushesList.Add(new BrushItem(Brushes.Salmon, "Salmon", "鲑鱼肉色"));
            brushesList.Add(new BrushItem(Brushes.SandyBrown, "SandyBrown", "沙棕色"));
            brushesList.Add(new BrushItem(Brushes.SeaGreen, "SeaGreen", "海绿"));
            brushesList.Add(new BrushItem(Brushes.SeaShell, "SeaShell", "海贝色"));
            brushesList.Add(new BrushItem(Brushes.Sienna, "Sienna", "黄土赭"));
            brushesList.Add(new BrushItem(Brushes.Silver, "Silver", "银白"));
            brushesList.Add(new BrushItem(Brushes.SkyBlue, "SkyBlue", "天蓝"));
            brushesList.Add(new BrushItem(Brushes.SlateBlue, "SlateBlue", "石板蓝"));
            brushesList.Add(new BrushItem(Brushes.SlateGray, "SlateGray", "石板灰"));
            brushesList.Add(new BrushItem(Brushes.Snow, "Snow", "雪色"));
            brushesList.Add(new BrushItem(Brushes.SpringGreen, "SpringGreen", "春绿"));
            brushesList.Add(new BrushItem(Brushes.SteelBlue, "SteelBlue", "钢蓝"));
            brushesList.Add(new BrushItem(Brushes.Tan, "Tan", "茶色"));
            brushesList.Add(new BrushItem(Brushes.Teal, "Teal", " 水鸭绿"));
            brushesList.Add(new BrushItem(Brushes.Thistle, "Thistle", "蓟色"));
            brushesList.Add(new BrushItem(Brushes.Tomato, "Tomato", "番茄色"));
            brushesList.Add(new BrushItem(Brushes.Transparent, "Transparent", "透明色"));
            brushesList.Add(new BrushItem(Brushes.Turquoise, "Turquoise", "宝石绿"));
            brushesList.Add(new BrushItem(Brushes.Violet, "Violet", "紫罗兰色"));
            brushesList.Add(new BrushItem(Brushes.Wheat, "Wheat", "小麦色"));
            brushesList.Add(new BrushItem(Brushes.White, "White", "白色"));
            brushesList.Add(new BrushItem(Brushes.WhiteSmoke, "WhiteSmoke", "白烟色"));
            brushesList.Add(new BrushItem(Brushes.Yellow, "Yellow", "黄色"));
            brushesList.Add(new BrushItem(Brushes.YellowGreen, "YellowGreen", "黄绿"));

            //生成一个默认的图像画刷。
            defaultImageSource = new BitmapImage(
                new Uri("pack://application:,,,/LunarConcept;component/Images/EditorBackground.jpg"));
            defaultImageBrush = new ImageBrush(defaultImageSource)
            {
                TileMode = TileMode.FlipXY,
                ViewportUnits = BrushMappingMode.Absolute,
                Viewport = new Rect(64, 64, 64, 64),
                Stretch = Stretch.UniformToFill,
            };
            brushesList.Add(new BrushItem(DefaultImageBrush, "DefaultImageBrush", "默认图像背景"));
        }

        private static void BuildRoundGlassButtonBrushes()
        {
            roundGlassButton_Black = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Black");
            roundGlassButton_Blue = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Blue");
            roundGlassButton_Brown = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Brown");
            roundGlassButton_Gold = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Gold");
            roundGlassButton_Gray = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Gray");
            roundGlassButton_Green = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Green");
            roundGlassButton_Orange = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Orange");
            roundGlassButton_Red = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Red");
            roundGlassButton_Violet = (VisualBrush)Application.Current.FindResource("RoundGlassButton_Violet");
            roundGlassButton_DeepPink = (VisualBrush)Application.Current.FindResource("RoundGlassButton_DeepPink");
        }

        private static void BuildRectangleGlassButtonBrushes()
        {
            rectangleGlassButton_Black = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Black");
            rectangleGlassButton_Blue = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Blue");
            rectangleGlassButton_Brown = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Brown");
            rectangleGlassButton_Gold = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Gold");
            rectangleGlassButton_Gray = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Gray");
            rectangleGlassButton_Green = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Green");
            rectangleGlassButton_Orange = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Orange");
            rectangleGlassButton_Red = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Red");
            rectangleGlassButton_Violet = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_Violet");
            rectangleGlassButton_DeepPink = (VisualBrush)Application.Current.FindResource("RectangleGlassButton_DeepPink");
        }

        private static void BuildBubbles()
        {
            bubbleRedAtBottomLeft = (VisualBrush)Application.Current.FindResource("BubbleRedAtBottomLeft");
            bubbleDeepPinkAtBottomLeft = (VisualBrush)Application.Current.FindResource("BubbleDeepPinkAtBottomLeft");
            bubbleBlueAtBottomLeft = (VisualBrush)Application.Current.FindResource("BubbleBlueAtBottomLeft");
            bubbleGreenAtBottomLeft = (VisualBrush)Application.Current.FindResource("BubbleGreenAtBottomLeft");
            bubbleBrownAtBottomLeft = (VisualBrush)Application.Current.FindResource("BubbleBrownAtBottomLeft");
            bubbleBlackAtBottomLeft = (VisualBrush)Application.Current.FindResource("BubbleBlackAtBottomLeft");

            bubbleRedAtBottomRight = (VisualBrush)Application.Current.FindResource("BubbleRedAtBottomRight");
            bubbleDeepPinkAtBottomRight = (VisualBrush)Application.Current.FindResource("BubbleDeepPinkAtBottomRight");
            bubbleBlueAtBottomRight = (VisualBrush)Application.Current.FindResource("BubbleBlueAtBottomRight");
            bubbleGreenAtBottomRight = (VisualBrush)Application.Current.FindResource("BubbleGreenAtBottomRight");
            bubbleBrownAtBottomRight = (VisualBrush)Application.Current.FindResource("BubbleBrownAtBottomRight");
            bubbleBlackAtBottomRight = (VisualBrush)Application.Current.FindResource("BubbleBlackAtBottomRight");

            bubbleRedAtTopLeft = (VisualBrush)Application.Current.FindResource("BubbleRedAtTopLeft");
            bubbleDeepPinkAtTopLeft = (VisualBrush)Application.Current.FindResource("BubbleDeepPinkAtTopLeft");
            bubbleBlueAtTopLeft = (VisualBrush)Application.Current.FindResource("BubbleBlueAtTopLeft");
            bubbleGreenAtTopLeft = (VisualBrush)Application.Current.FindResource("BubbleGreenAtTopLeft");
            bubbleBrownAtTopLeft = (VisualBrush)Application.Current.FindResource("BubbleBrownAtTopLeft");
            bubbleBlackAtTopLeft = (VisualBrush)Application.Current.FindResource("BubbleBlackAtTopLeft");

            bubbleRedAtTopRight = (VisualBrush)Application.Current.FindResource("BubbleRedAtTopRight");
            bubbleDeepPinkAtTopRight = (VisualBrush)Application.Current.FindResource("BubbleDeepPinkAtTopRight");
            bubbleBlueAtTopRight = (VisualBrush)Application.Current.FindResource("BubbleBlueAtTopRight");
            bubbleGreenAtTopRight = (VisualBrush)Application.Current.FindResource("BubbleGreenAtTopRight");
            bubbleBrownAtTopRight = (VisualBrush)Application.Current.FindResource("BubbleBrownAtTopRight");
            bubbleBlackAtTopRight = (VisualBrush)Application.Current.FindResource("BubbleBlackAtTopRight");

        }

        /// <summary>
        /// 社工常用几种图例背景
        /// </summary>
        private  static void BuildSocialBrushes()
        {
            slash = (VisualBrush)Application.Current.FindResource("Slash");
            antiSlash = (VisualBrush)Application.Current.FindResource("AntiSlash");
            crossSlash = (VisualBrush)Application.Current.FindResource("CrossSlash");
            topBlock = (VisualBrush)Application.Current.FindResource("TopBlock");
            leftBlock = (VisualBrush)Application.Current.FindResource("LeftBlock");
            bottomBlock = (VisualBrush)Application.Current.FindResource("BottomBlock");
            rightBlock = (VisualBrush)Application.Current.FindResource("RightBlock");
            leftTopBlock = (VisualBrush)Application.Current.FindResource("LeftTopBlock");
            leftBottomBlock = (VisualBrush)Application.Current.FindResource("LeftBottomBlock");
            rightTopBlock = (VisualBrush)Application.Current.FindResource("RightTopBlock");
            rightBottomBlock = (VisualBrush)Application.Current.FindResource("RightBottomBlock");
        }

        private static void BuildHollowBubbles()
        {
            hollowBubbleRedAtBottomLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleRedAtBottomLeft");
            hollowBubbleDeepPinkAtBottomLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleDeepPinkAtBottomLeft");
            hollowBubbleBlueAtBottomLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleBlueAtBottomLeft");
            hollowBubbleGreenAtBottomLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleGreenAtBottomLeft");
            hollowBubbleBrownAtBottomLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleBrownAtBottomLeft");
            hollowBubbleBlackAtBottomLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleBlackAtBottomLeft");

            hollowBubbleRedAtBottomRight = (VisualBrush)Application.Current.FindResource("HollowBubbleRedAtBottomRight");
            hollowBubbleDeepPinkAtBottomRight = (VisualBrush)Application.Current.FindResource("HollowBubbleDeepPinkAtBottomRight");
            hollowBubbleBlueAtBottomRight = (VisualBrush)Application.Current.FindResource("HollowBubbleBlueAtBottomRight");
            hollowBubbleGreenAtBottomRight = (VisualBrush)Application.Current.FindResource("HollowBubbleGreenAtBottomRight");
            hollowBubbleBrownAtBottomRight = (VisualBrush)Application.Current.FindResource("HollowBubbleBrownAtBottomRight");
            hollowBubbleBlackAtBottomRight = (VisualBrush)Application.Current.FindResource("HollowBubbleBlackAtBottomRight");

            hollowBubbleRedAtTopLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleRedAtTopLeft");
            hollowBubbleDeepPinkAtTopLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleDeepPinkAtTopLeft");
            hollowBubbleBlueAtTopLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleBlueAtTopLeft");
            hollowBubbleGreenAtTopLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleGreenAtTopLeft");
            hollowBubbleBrownAtTopLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleBrownAtTopLeft");
            hollowBubbleBlackAtTopLeft = (VisualBrush)Application.Current.FindResource("HollowBubbleBlackAtTopLeft");

            hollowBubbleRedAtTopRight = (VisualBrush)Application.Current.FindResource("HollowBubbleRedAtTopRight");
            hollowBubbleDeepPinkAtTopRight = (VisualBrush)Application.Current.FindResource("HollowBubbleDeepPinkAtTopRight");
            hollowBubbleBlueAtTopRight = (VisualBrush)Application.Current.FindResource("HollowBubbleBlueAtTopRight");
            hollowBubbleGreenAtTopRight = (VisualBrush)Application.Current.FindResource("HollowBubbleGreenAtTopRight");
            hollowBubbleBrownAtTopRight = (VisualBrush)Application.Current.FindResource("HollowBubbleBrownAtTopRight");
            hollowBubbleBlackAtTopRight = (VisualBrush)Application.Current.FindResource("HollowBubbleBlackAtTopRight");
        }

        private static void BuildGrids()
        {
            gridBlack = (DrawingBrush)Application.Current.FindResource("GridBlack");
            tianziBlack = (DrawingBrush)Application.Current.FindResource("TianziBlack");
            //miziBlack = (DrawingBrush)Application.Current.FindResource("MiziBlack");
            huiziBlack = (DrawingBrush)Application.Current.FindResource("HuiziBlack");
            huitianBlack = (DrawingBrush)Application.Current.FindResource("HuitianBlack");
        }

        private static LinearGradientBrush BuildUnderLineBrush()
        {
            LinearGradientBrush underLineBrush = (LinearGradientBrush)Application.Current.FindResource("UnderLineBrush");
            return underLineBrush;
        }

        private static LinearGradientBrush BuildAntiStreamerBrush()
        {
            LinearGradientBrush lineBrush = (LinearGradientBrush)Application.Current.FindResource("AntiStreamerBrush");
            return lineBrush;
        }

        private static LinearGradientBrush BuildSevenColorsBrush()
        {
            LinearGradientBrush lineBrush = (LinearGradientBrush)Application.Current.FindResource("SevenColorsBrush");
            return lineBrush;
        }

        private static LinearGradientBrush BuildStreamerBrush()
        {
            LinearGradientBrush lineBrush = (LinearGradientBrush)Application.Current.FindResource("StreamerBrush");
            return lineBrush;
        }

        private static VisualBrush BuildGlassVisualBrush()
        {
            //资源字典由Application.Xaml文件引用。
            VisualBrush visualBrush = (VisualBrush)Application.Current.FindResource("GlassVisualBrush");
            return visualBrush;
        }

        /// <summary>
        /// 随机数种子。创建多个种子可能造成返回同一个随机数。
        /// 而使用同一个随机数种子，返回同一个随机数的概率很低。
        /// </summary>
        private static Random random;

        /// <summary>
        /// 随机取纯色。若取不出，默认返回黑色。不会返回null。
        /// </summary>
        /// <returns>纯色画刷。</returns>
        public static SolidColorBrush GetRandomSolidBrush()
        {
            if (BrushManager.brushesList == null || BrushManager.brushesList.Count <= 0)
            {
                return Brushes.Black;
            }

            if (BrushManager.random == null)
            {
                BrushManager.random = new Random();
            }

            int newIndex;

            SolidColorBrush scb = null;
            do
            {
                newIndex = random.Next(75, 214);
                if (newIndex >= 0 && newIndex < BrushManager.brushesList.Count)
                {
                    scb = BrushManager.brushesList[newIndex].Brush as SolidColorBrush;
                }
            } while (scb == null);

            return scb;
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static LinearGradientBrush antiStreamer;
        /// <summary>
        /// [只读]自定义渐变画刷。与Streamer相对存在。色彩对立。
        /// </summary>
        public static LinearGradientBrush AntiStreamer
        {
            get { return BrushManager.antiStreamer; }
        }

        private static LinearGradientBrush sevenColors;
        /// <summary>
        /// [只读]自定义渐变。七彩色渐变，由左上到右下。
        /// </summary>
        public static LinearGradientBrush SevenColors
        {
            get { return BrushManager.sevenColors; }
        }

        private static VisualBrush glass;
        /// <summary>
        /// [只读]自定义VisualBrush。仿制的玻璃按钮效果。
        /// </summary>
        public static VisualBrush Glass
        {
            get { return BrushManager.glass; }
        }

        private static LinearGradientBrush streamer;
        /// <summary>
        /// [只读]自定义渐变画刷。飘带状。彩条。
        /// </summary>
        public static LinearGradientBrush Streamer
        {
            get { return BrushManager.streamer; }
        }

        private static LinearGradientBrush underLine;
        /// <summary>
        /// [只读]自定义渐变画刷。下划线状。
        /// </summary>
        public static LinearGradientBrush UnderLine
        {
            get { return BrushManager.underLine; }
        }

        /// <summary>
        /// 内部使用，不公开。在静态构造方法中初始化其中的内容。
        /// </summary>
        private static List<BrushItem> brushesList;

        private static ImageBrush defaultImageBrush;
        /// <summary>
        /// [静态只读]默认图像画刷。
        /// </summary>
        public static ImageBrush DefaultImageBrush
        {
            get { return BrushManager.defaultImageBrush; }
        }

        /// <summary>
        /// 默认画刷的源。
        /// </summary>
        private static ImageSource defaultImageSource;

        #region 圆玻璃按钮画刷
        private static VisualBrush roundGlassButton_Red;
        /// <summary>
        /// [只读]红色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Red
        {
            get { return roundGlassButton_Red; }
        }

        private static VisualBrush roundGlassButton_Orange;
        /// <summary>
        /// [只读]橙色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Orange
        {
            get { return roundGlassButton_Orange; }
        }

        private static VisualBrush roundGlassButton_DeepPink;
        /// <summary>
        /// [只读]深粉红色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_DeepPink
        {
            get { return roundGlassButton_DeepPink; }
        }

        private static VisualBrush roundGlassButton_Green;
        /// <summary>
        /// [只读]绿色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Green
        {
            get { return roundGlassButton_Green; }
        }

        private static VisualBrush roundGlassButton_Brown;
        /// <summary>
        /// [只读]棕色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Brown
        {
            get { return roundGlassButton_Brown; }
        }

        private static VisualBrush roundGlassButton_Blue;
        /// <summary>
        /// [只读]蓝色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Blue
        {
            get { return roundGlassButton_Blue; }
        }

        private static VisualBrush roundGlassButton_Violet;
        /// <summary>
        /// [只读]紫色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Violet
        {
            get { return roundGlassButton_Violet; }
        }

        private static VisualBrush roundGlassButton_Gold;
        /// <summary>
        /// [只读]金色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Gold
        {
            get { return roundGlassButton_Gold; }
        }

        private static VisualBrush roundGlassButton_Gray;
        /// <summary>
        /// [只读]灰色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Gray
        {
            get { return roundGlassButton_Gray; }
        }

        private static VisualBrush roundGlassButton_Black;
        /// <summary>
        /// [只读]黑色玻璃圆按钮画刷。
        /// </summary>
        public static VisualBrush RoundGlassButton_Black
        {
            get { return roundGlassButton_Black; }
        }
        #endregion

        #region 方玻璃按钮画刷

        private static VisualBrush rectangleGlassButton_Red;
        /// <summary>
        /// [只读]红色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Red
        {
            get { return rectangleGlassButton_Red; }
        }

        private static VisualBrush rectangleGlassButton_Orange;
        /// <summary>
        /// [只读]橙色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Orange
        {
            get { return rectangleGlassButton_Orange; }
        }

        private static VisualBrush rectangleGlassButton_DeepPink;
        /// <summary>
        /// [只读]深粉红色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_DeepPink
        {
            get { return rectangleGlassButton_DeepPink; }
        }

        private static VisualBrush rectangleGlassButton_Green;
        /// <summary>
        /// [只读]绿色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Green
        {
            get { return rectangleGlassButton_Green; }
        }

        private static VisualBrush rectangleGlassButton_Brown;
        /// <summary>
        /// [只读]棕色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Brown
        {
            get { return rectangleGlassButton_Brown; }
        }

        private static VisualBrush rectangleGlassButton_Blue;
        /// <summary>
        /// [只读]蓝色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Blue
        {
            get { return rectangleGlassButton_Blue; }
        }

        private static VisualBrush rectangleGlassButton_Violet;
        /// <summary>
        /// [只读]紫色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Violet
        {
            get { return rectangleGlassButton_Violet; }
        }

        private static VisualBrush rectangleGlassButton_Gold;
        /// <summary>
        /// [只读]金色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Gold
        {
            get { return rectangleGlassButton_Gold; }
        }

        private static VisualBrush rectangleGlassButton_Gray;
        /// <summary>
        /// [只读]灰色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Gray
        {
            get { return rectangleGlassButton_Gray; }
        }

        private static VisualBrush rectangleGlassButton_Black;
        /// <summary>
        /// [只读]黑色玻璃方按钮画刷。
        /// </summary>
        public static VisualBrush RectangleGlassButton_Black
        {
            get { return rectangleGlassButton_Black; }
        }

        #endregion

        #region 气泡画刷

        private static VisualBrush bubbleRedAtBottomLeft;
        /// <summary>
        /// [只读]红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleRedAtBottomLeft
        {
            get { return BrushManager.bubbleRedAtBottomLeft; }
        }

        private static VisualBrush bubbleDeepPinkAtBottomLeft;
        /// <summary>
        /// [只读]深粉红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleDeepPinkAtBottomLeft
        {
            get { return BrushManager.bubbleDeepPinkAtBottomLeft; }
        }

        private static VisualBrush bubbleBlueAtBottomLeft;
        /// <summary>
        /// [只读]蓝色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlueAtBottomLeft
        {
            get { return BrushManager.bubbleBlueAtBottomLeft; }
        }

        private static VisualBrush bubbleGreenAtBottomLeft;
        /// <summary>
        /// [只读]绿色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleGreenAtBottomLeft
        {
            get { return BrushManager.bubbleGreenAtBottomLeft; }
        }

        private static VisualBrush bubbleBrownAtBottomLeft;
        /// <summary>
        /// [只读]棕色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBrownAtBottomLeft
        {
            get { return BrushManager.bubbleBrownAtBottomLeft; }
        }

        private static VisualBrush bubbleBlackAtBottomLeft;
        /// <summary>
        /// [只读]黑色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlackAtBottomLeft
        {
            get { return BrushManager.bubbleBlackAtBottomLeft; }
        }


        private static VisualBrush bubbleRedAtBottomRight;
        /// <summary>
        /// [只读]红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleRedAtBottomRight
        {
            get { return BrushManager.bubbleRedAtBottomRight; }
        }

        private static VisualBrush bubbleDeepPinkAtBottomRight;
        /// <summary>
        /// [只读]深粉红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleDeepPinkAtBottomRight
        {
            get { return BrushManager.bubbleDeepPinkAtBottomRight; }
        }

        private static VisualBrush bubbleBlueAtBottomRight;
        /// <summary>
        /// [只读]蓝色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlueAtBottomRight
        {
            get { return BrushManager.bubbleBlueAtBottomRight; }
        }

        private static VisualBrush bubbleGreenAtBottomRight;
        /// <summary>
        /// [只读]绿色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleGreenAtBottomRight
        {
            get { return BrushManager.bubbleGreenAtBottomRight; }
        }

        private static VisualBrush bubbleBrownAtBottomRight;
        /// <summary>
        /// [只读]棕色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBrownAtBottomRight
        {
            get { return BrushManager.bubbleBrownAtBottomRight; }
        }

        private static VisualBrush bubbleBlackAtBottomRight;
        /// <summary>
        /// [只读]黑色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlackAtBottomRight
        {
            get { return BrushManager.bubbleBlackAtBottomRight; }
        }

        private static VisualBrush bubbleRedAtTopLeft;
        /// <summary>
        /// [只读]红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleRedAtTopLeft
        {
            get { return BrushManager.bubbleRedAtTopLeft; }
        }

        private static VisualBrush bubbleDeepPinkAtTopLeft;
        /// <summary>
        /// [只读]深粉红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleDeepPinkAtTopLeft
        {
            get { return BrushManager.bubbleDeepPinkAtTopLeft; }
        }

        private static VisualBrush bubbleBlueAtTopLeft;
        /// <summary>
        /// [只读]蓝色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlueAtTopLeft
        {
            get { return BrushManager.bubbleBlueAtTopLeft; }
        }

        private static VisualBrush bubbleGreenAtTopLeft;
        /// <summary>
        /// [只读]绿色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleGreenAtTopLeft
        {
            get { return BrushManager.bubbleGreenAtTopLeft; }
        }

        private static VisualBrush bubbleBrownAtTopLeft;
        /// <summary>
        /// [只读]棕色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBrownAtTopLeft
        {
            get { return BrushManager.bubbleBrownAtTopLeft; }
        }

        private static VisualBrush bubbleBlackAtTopLeft;
        /// <summary>
        /// [只读]黑色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlackAtTopLeft
        {
            get { return BrushManager.bubbleBlackAtTopLeft; }
        }

        private static VisualBrush bubbleRedAtTopRight;
        /// <summary>
        /// [只读]红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleRedAtTopRight
        {
            get { return BrushManager.bubbleRedAtTopRight; }
        }

        private static VisualBrush bubbleDeepPinkAtTopRight;
        /// <summary>
        /// [只读]深粉红色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleDeepPinkAtTopRight
        {
            get { return BrushManager.bubbleDeepPinkAtTopRight; }
        }

        private static VisualBrush bubbleBlueAtTopRight;
        /// <summary>
        /// [只读]蓝色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlueAtTopRight
        {
            get { return BrushManager.bubbleBlueAtTopRight; }
        }

        private static VisualBrush bubbleGreenAtTopRight;
        /// <summary>
        /// [只读]绿色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleGreenAtTopRight
        {
            get { return BrushManager.bubbleGreenAtTopRight; }
        }

        private static VisualBrush bubbleBrownAtTopRight;
        /// <summary>
        /// [只读]棕色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBrownAtTopRight
        {
            get { return BrushManager.bubbleBrownAtTopRight; }
        }

        private static VisualBrush bubbleBlackAtTopRight;
        /// <summary>
        /// [只读]黑色玻璃按钮式气泡画刷。
        /// </summary>
        public static VisualBrush BubbleBlackAtTopRight
        {
            get { return BrushManager.bubbleBlackAtTopRight; }
        }


        #endregion

        #region 社工用的几个图例

        private static VisualBrush slash;
        /// <summary>
        /// 斜线背景
        /// </summary>
        public static VisualBrush Slash
        {
            get { return BrushManager.slash; }
        }

        private static VisualBrush antiSlash;
        /// <summary>
        /// 反斜线背景
        /// </summary>
        public static VisualBrush AntiSlash
        {
            get { return antiSlash; }
        }

        private static VisualBrush crossSlash;
        /// <summary>
        /// 交叉斜线背景
        /// </summary>
        public static VisualBrush CrossSlash
        {
            get { return crossSlash; }
        }

        private static VisualBrush topBlock;
        /// <summary>
        /// 上侧实心块
        /// </summary>
        public static VisualBrush TopBlock
        {
            get { return topBlock; }
        }

        private static VisualBrush bottomBlock;
        /// <summary>
        /// 下侧实心块
        /// </summary>
        public static VisualBrush BottomBlock
        {
            get { return bottomBlock; }
        }

        private static VisualBrush leftBlock;
        /// <summary>
        /// 左侧实心块
        /// </summary>
        public static VisualBrush LeftBlock
        {
            get { return leftBlock; }
        }

        private static VisualBrush rightBlock;
        /// <summary>
        /// 右侧实心块
        /// </summary>
        public static VisualBrush RightBlock
        {
            get { return rightBlock; }
        }

        private static VisualBrush leftTopBlock;
        /// <summary>
        /// 左上角实心块
        /// </summary>
        public static VisualBrush LeftTopBlock
        {
            get { return leftTopBlock; }
        }

        private static VisualBrush leftBottomBlock;
        /// <summary>
        /// 左下角实心块
        /// </summary>
        public static VisualBrush LeftBottomBlock
        {
            get { return leftBottomBlock; }
        }

        private static VisualBrush rightTopBlock;
        /// <summary>
        /// 右上角实心块
        /// </summary>
        public static VisualBrush RightTopBlock
        {
            get { return rightTopBlock; }
        }

        private static VisualBrush rightBottomBlock;
        /// <summary>
        /// 右下角实心块
        /// </summary>
        public static VisualBrush RightBottomBlock
        {
            get { return rightBottomBlock; }
        }
        #endregion

        #region 空心气泡画刷

        private static VisualBrush hollowBubbleRedAtBottomLeft;
        /// <summary>
        /// [只读]红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleRedAtBottomLeft
        {
            get { return BrushManager.hollowBubbleRedAtBottomLeft; }
        }

        private static VisualBrush hollowBubbleDeepPinkAtBottomLeft;
        /// <summary>
        /// [只读]深粉红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleDeepPinkAtBottomLeft
        {
            get { return BrushManager.hollowBubbleDeepPinkAtBottomLeft; }
        }

        private static VisualBrush hollowBubbleBlueAtBottomLeft;
        /// <summary>
        /// [只读]蓝色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlueAtBottomLeft
        {
            get { return BrushManager.hollowBubbleBlueAtBottomLeft; }
        }

        private static VisualBrush hollowBubbleGreenAtBottomLeft;
        /// <summary>
        /// [只读]绿色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleGreenAtBottomLeft
        {
            get { return BrushManager.hollowBubbleGreenAtBottomLeft; }
        }

        private static VisualBrush hollowBubbleBrownAtBottomLeft;
        /// <summary>
        /// [只读]棕色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBrownAtBottomLeft
        {
            get { return BrushManager.hollowBubbleBrownAtBottomLeft; }
        }

        private static VisualBrush hollowBubbleBlackAtBottomLeft;
        /// <summary>
        /// [只读]黑色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlackAtBottomLeft
        {
            get { return BrushManager.hollowBubbleBlackAtBottomLeft; }
        }


        private static VisualBrush hollowBubbleRedAtBottomRight;
        /// <summary>
        /// [只读]红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleRedAtBottomRight
        {
            get { return BrushManager.hollowBubbleRedAtBottomRight; }
        }

        private static VisualBrush hollowBubbleDeepPinkAtBottomRight;
        /// <summary>
        /// [只读]深粉红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleDeepPinkAtBottomRight
        {
            get { return BrushManager.hollowBubbleDeepPinkAtBottomRight; }
        }

        private static VisualBrush hollowBubbleBlueAtBottomRight;
        /// <summary>
        /// [只读]蓝色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlueAtBottomRight
        {
            get { return BrushManager.hollowBubbleBlueAtBottomRight; }
        }

        private static VisualBrush hollowBubbleGreenAtBottomRight;
        /// <summary>
        /// [只读]绿色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleGreenAtBottomRight
        {
            get { return BrushManager.hollowBubbleGreenAtBottomRight; }
        }

        private static VisualBrush hollowBubbleBrownAtBottomRight;
        /// <summary>
        /// [只读]棕色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBrownAtBottomRight
        {
            get { return BrushManager.hollowBubbleBrownAtBottomRight; }
        }

        private static VisualBrush hollowBubbleBlackAtBottomRight;
        /// <summary>
        /// [只读]黑色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlackAtBottomRight
        {
            get { return BrushManager.hollowBubbleBlackAtBottomRight; }
        }

        private static VisualBrush hollowBubbleRedAtTopLeft;
        /// <summary>
        /// [只读]红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleRedAtTopLeft
        {
            get { return BrushManager.hollowBubbleRedAtTopLeft; }
        }

        private static VisualBrush hollowBubbleDeepPinkAtTopLeft;
        /// <summary>
        /// [只读]深粉红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleDeepPinkAtTopLeft
        {
            get { return BrushManager.hollowBubbleDeepPinkAtTopLeft; }
        }

        private static VisualBrush hollowBubbleBlueAtTopLeft;
        /// <summary>
        /// [只读]蓝色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlueAtTopLeft
        {
            get { return BrushManager.hollowBubbleBlueAtTopLeft; }
        }

        private static VisualBrush hollowBubbleGreenAtTopLeft;
        /// <summary>
        /// [只读]绿色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleGreenAtTopLeft
        {
            get { return BrushManager.hollowBubbleGreenAtTopLeft; }
        }

        private static VisualBrush hollowBubbleBrownAtTopLeft;
        /// <summary>
        /// [只读]棕色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBrownAtTopLeft
        {
            get { return BrushManager.hollowBubbleBrownAtTopLeft; }
        }

        private static VisualBrush hollowBubbleBlackAtTopLeft;
        /// <summary>
        /// [只读]黑色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlackAtTopLeft
        {
            get { return BrushManager.hollowBubbleBlackAtTopLeft; }
        }


        private static VisualBrush hollowBubbleRedAtTopRight;
        /// <summary>
        /// [只读]红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleRedAtTopRight
        {
            get { return BrushManager.hollowBubbleRedAtTopRight; }
        }

        private static VisualBrush hollowBubbleDeepPinkAtTopRight;
        /// <summary>
        /// [只读]深粉红色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleDeepPinkAtTopRight
        {
            get { return BrushManager.hollowBubbleDeepPinkAtTopRight; }
        }

        private static VisualBrush hollowBubbleBlueAtTopRight;
        /// <summary>
        /// [只读]蓝色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlueAtTopRight
        {
            get { return BrushManager.hollowBubbleBlueAtTopRight; }
        }

        private static VisualBrush hollowBubbleGreenAtTopRight;
        /// <summary>
        /// [只读]绿色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleGreenAtTopRight
        {
            get { return BrushManager.hollowBubbleGreenAtTopRight; }
        }

        private static VisualBrush hollowBubbleBrownAtTopRight;
        /// <summary>
        /// [只读]棕色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBrownAtTopRight
        {
            get { return BrushManager.hollowBubbleBrownAtTopRight; }
        }

        private static VisualBrush hollowBubbleBlackAtTopRight;
        /// <summary>
        /// [只读]黑色空心气泡画刷。
        /// </summary>
        public static VisualBrush HollowBubbleBlackAtTopRight
        {
            get { return BrushManager.hollowBubbleBlackAtTopRight; }
        }

        #endregion

        private static DrawingBrush gridBlack;
        /// <summary>
        /// [只读]黑色网格画刷。
        /// </summary>
        public static DrawingBrush GridBlack
        {
            get { return BrushManager.gridBlack; }
        }

        //private static DrawingBrush miziBlack;
        ///// <summary>
        ///// [只读]黑色米字格画刷。
        ///// </summary>
        //public static DrawingBrush MiziBlack
        //{
        //    get { return BrushManager.miziBlack; }
        //}

        private static DrawingBrush huiziBlack;
        /// <summary>
        /// [只读]黑色回宫格画刷。
        /// </summary>
        public static DrawingBrush HuiziBlack
        {
            get { return BrushManager.huiziBlack; }
        }

        private static DrawingBrush huitianBlack;
        /// <summary>
        /// [只读]黑色回宫田字格（回宫格+田字格）画刷。
        /// </summary>
        public static DrawingBrush HuitianBlack
        {
            get { return BrushManager.huitianBlack; }
        }

        private static DrawingBrush tianziBlack;
        /// <summary>
        /// [只读]黑色田字格画刷。
        /// </summary>
        public static DrawingBrush TianziBlack
        {
            get { return BrushManager.tianziBlack; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// GetBrushByEnglishName(string enName)方法的别名。
        /// </summary>
        /// <param name="enName">画刷的英文名称。</param>
        /// <returns>如果找到，就返回画刷；否则返回null。</returns>
        public static Brush GetBrush(string enName)
        {
            return GetBrushByEnglishName(enName);
        }

        /// <summary>
        /// [静态方法]根据给出的英文名称取色彩Brush对象（Brushes类的某个属性）。
        /// 如果不支持所给的名称，会返回null。
        /// </summary>
        /// <param name="enName">Brush的英文名称。</param>
        /// <returns>返回Brush。</returns>
        public static Brush GetBrushByEnglishName(string enName)
        {
            if (enName == null) return null;

            if (enName.StartsWith("sc#"))
            {
                try
                {
                    Color color = (Color)ColorConverter.ConvertFromString(enName);
                    SolidColorBrush newColorBrush = new SolidColorBrush(color);
                    return newColorBrush;
                }
                catch
                {
                    return null;
                }
            }

            foreach (BrushItem bi in brushesList)
            {
                if (bi.EnName.ToLower() == enName.ToLower()) return bi.Brush;
            }

            return null;
        }

        /// <summary>
        /// [静态方法]根据所给出的中文名称取色彩Brush对象（Brushes类的某个属性）。
        /// 如果不支持所给的名称，会返回null。
        /// </summary>
        /// <param name="cnName">Brushes的中文名称（必须是在本类静态构造方法中定义的名称）。</param>
        /// <returns>返回Brush。</returns>
        public static Brush GetBrushByChineseName(string cnName)
        {
            if (cnName == null) return null;

            if (cnName.StartsWith("sc#"))
            {
                try
                {
                    Color color = (Color)ColorConverter.ConvertFromString(cnName);
                    SolidColorBrush newColorBrush = new SolidColorBrush(color);
                    return newColorBrush;
                }
                catch
                {
                    return null;
                }
            }

            foreach (BrushItem bi in brushesList)
            {
                if (bi.CnName == cnName) return bi.Brush;
            }

            return null;
        }

        /// <summary>
        /// [静态方法]取Brush的中文名称。
        /// 名称修改自网上的中英文色彩名称对照表。
        /// </summary>
        /// <param name="brush">只能传入Brushes类中定义的Brush。</param>
        /// <returns></returns>
        public static string GetChineseName(Brush brush)
        {
            if (brush == null) return null;
            if (brush is ImageBrush) return "图像画刷";

            foreach (BrushItem bi in brushesList)
            {
                if (brush == bi.Brush) return bi.CnName;
            }

            SolidColorBrush sbsh = brush as SolidColorBrush;
            if (sbsh != null)
            {
                return sbsh.Color.ToString();//返回#00FFFFFF之类字串。
            }

            return null;
        }

        /// <summary>
        /// [静态方法]根据Brush的英文名称取Brush的中文名称。
        /// 失败会返回null。
        /// </summary>
        /// <param name="brushEnName">只能传入Brushes类中定义的Brush的英文名称。</param>
        /// <returns>失败会返回null。</returns>
        public static string GetChineseName(string brushEnName)
        {
            if (brushEnName == null) return null;

            if (brushEnName.StartsWith("sc#")) return brushEnName;//原样返回。

            foreach (BrushItem bi in brushesList)
            {
                if (brushEnName == bi.EnName) return bi.CnName;
            }

            return null;
        }

        /// <summary>
        /// [静态方法]即GetEnglishName(Brush brush)方法。
        /// </summary>
        public static string GetName(Brush brush)
        {
            return GetEnglishName(brush);
        }

        /// <summary>
        /// [静态方法]取Brush的英文名称。是.net Brushes类内置的属性名。
        /// ——提供的名称必须是Brushes类支持的色彩名称。如果失败，返回null。
        /// </summary>
        /// <param name="brush">只能传入Brushes类中定义的Brush。</param>
        /// <returns>返回Brush对象。失败返回null。</returns>
        public static string GetEnglishName(Brush brush)
        {
            if (brush == null) return null;
            if (brush is ImageBrush) return typeof(ImageBrush).Name;
            
            foreach (BrushItem bi in brushesList)
            {
                if (brush == bi.Brush) return bi.EnName;
            }

            SolidColorBrush sbsh = brush as SolidColorBrush;
            if (sbsh != null)
            {
                return sbsh.Color.ToString();//返回#00FFFFFF之类字串。
            }

            return null;
        }

        /// <summary>
        /// [静态方法]根据中文名称取Brush的英文名称。
        /// </summary>
        /// <param name="brushCnName">只能传入Brushes类中定义的Brush的中文名称。</param>
        /// <returns>失败会返回null。</returns>
        public static string GetEnglishName(string brushCnName)
        {
            if (brushCnName == null) return null;

            if (brushCnName.StartsWith("sc#")) return brushCnName;//原样返回。

            foreach (BrushItem bi in brushesList)
            {
                if (brushCnName == bi.CnName) return bi.EnName;
            }

            return null;
        }

        /// <summary>
        /// 根据提供的此类中定义的VisualBrush来返回一个基本色（纯色）。
        /// </summary>
        /// <param name="vbsh">本类中定义的VisualBrush。</param>
        /// <returns>如果指定的不是本类中定义的VisualBrush，则返回Btushe.White。</returns>
        public static Brush GetVisualBrushBaseBrush(VisualBrush vbsh)
        {
            if (vbsh == null) return Brushes.White;

            if (vbsh == roundGlassButton_Black || vbsh == rectangleGlassButton_Black ||
                vbsh == bubbleBlackAtBottomLeft || vbsh == bubbleBlackAtBottomRight ||
                vbsh == bubbleBlackAtTopLeft || vbsh == bubbleBlackAtTopRight) return Brushes.Black;

            if (vbsh == roundGlassButton_Blue || vbsh == rectangleGlassButton_Blue ||
                vbsh == bubbleBlueAtBottomLeft || vbsh == bubbleBlueAtBottomRight ||
                vbsh == bubbleBlueAtTopLeft || vbsh == bubbleBlueAtTopRight) return Brushes.Blue;

            if (vbsh == roundGlassButton_Brown || vbsh == rectangleGlassButton_Brown ||
                vbsh == bubbleBrownAtBottomLeft || vbsh == bubbleBrownAtBottomRight ||
                vbsh == bubbleBrownAtTopLeft || vbsh == bubbleBrownAtTopRight) return Brushes.Brown;

            if (vbsh == roundGlassButton_DeepPink || vbsh == rectangleGlassButton_DeepPink ||
                vbsh == bubbleDeepPinkAtBottomLeft || vbsh == bubbleDeepPinkAtBottomRight ||
                vbsh == bubbleDeepPinkAtTopLeft || vbsh == bubbleDeepPinkAtTopRight) return Brushes.DeepPink;

            if (vbsh == roundGlassButton_Gold || vbsh == rectangleGlassButton_Gold) return Brushes.Gold;

            if (vbsh == roundGlassButton_Gray || vbsh == rectangleGlassButton_Gray) return Brushes.Gray;

            if (vbsh == roundGlassButton_Green || vbsh == rectangleGlassButton_Green ||
                vbsh == bubbleGreenAtBottomLeft || vbsh == bubbleGreenAtBottomRight ||
                vbsh == bubbleGreenAtTopLeft || vbsh == bubbleGreenAtTopRight) return Brushes.Green;

            if (vbsh == roundGlassButton_Orange || vbsh == rectangleGlassButton_Orange) return Brushes.Orange;

            if (vbsh == roundGlassButton_Red || vbsh == rectangleGlassButton_Red ||
                vbsh == bubbleRedAtBottomLeft || vbsh == bubbleRedAtBottomRight ||
                vbsh == bubbleRedAtTopLeft || vbsh == bubbleRedAtTopRight) return Brushes.Red;

            if (vbsh == roundGlassButton_Violet || vbsh == rectangleGlassButton_Violet) return Brushes.Violet;


            return Brushes.White;
        }

        /// <summary>
        /// 检查传入的Brush是否合法的Brush（Brushes类预定的某个SolidBrush）或者此类自定义的某些画刷。
        /// </summary>
        public static bool IsValidateBrush(Brush brush)
        {
            if (brush == null) return false;

            SolidColorBrush sbsh = brush as SolidColorBrush;
            if (sbsh != null)
            {
                return true;
            }

            foreach (BrushItem bi in brushesList)
            {
                if (bi.Brush == brush) return true;
            }

            return false;
        }

        /// <summary>
        /// 将提交的字符串转换为Brush对象。
        /// 
        /// 如果字符串以“s”开头，则转换为SolicColorBrush；★这里有个特例：sTransparent，它会直接返回默认的图像画刷。
        /// 如果字符串以“i”开头，则转换为ImageBrush。
        /// 
        /// 如果字符串不合法，则返回默认的图像画刷。
        /// </summary>
        /// <param name="sourceText"></param>
        /// <returns></returns>
        public static Brush Parse(string sourceText)
        {
            if (sourceText == null || sourceText.Length <= 0) return defaultImageBrush;

            if (sourceText == "sTransparent" || sourceText == "Transparent" || sourceText == "iTransparent")
            {
                return defaultImageBrush;
            }

            if (sourceText.StartsWith("i"))
            {
                string imgDataText = sourceText.Substring(1);
                System.Windows.Media.Imaging.BitmapImage bmp = new System.Windows.Media.Imaging.BitmapImage();

                if (imgDataText != null && imgDataText.Length > 0)
                {
                    byte[] b = Convert.FromBase64String(imgDataText);
                    bmp.BeginInit();
                    bmp.StreamSource = new MemoryStream(b);
                    bmp.EndInit();

                    ImageBrush iBsh = new ImageBrush(bmp) { Stretch = System.Windows.Media.Stretch.UniformToFill };
                    iBsh.ViewportUnits = System.Windows.Media.BrushMappingMode.Absolute;
                    iBsh.Viewport = new System.Windows.Rect(0, 0, iBsh.ImageSource.Width, iBsh.ImageSource.Height);
                    return iBsh;

                }
                else return defaultImageBrush;
            }

            if (sourceText.StartsWith("s"))
            {
                return BrushManager.GetBrushByEnglishName(sourceText.Substring(1)); ;
            }

            return defaultImageBrush;
        }

        /// <summary>
        /// 将指定的图片文件转换成为一个Base64的字符串，并以“i”开头。
        /// </summary>
        /// <param name="imageFilePath">图片文件的完整路径。</param>
        /// <returns>图片转换成的Base64字符串（以“i”开头）。</returns>
        public static string ParseToText(string imageFilePath, out ImageBrush iBsh, TileMode tileMode = TileMode.FlipXY)
        {
            string filename = imageFilePath;

            System.IO.FileInfo fileInfo = new FileInfo(filename);

            if (fileInfo.Exists == false)
            {
                System.Windows.MessageBox.Show("　　发生意外，指定的图片文件不存在！", Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                iBsh = null;
                return string.Empty;
            }

            if (fileInfo.Length <= 10485760)//将图片限制在10MB以内
            {
                System.Drawing.Image fileImg = System.Drawing.Image.FromFile(filename);
                using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                {
                    try
                    {
                        if (fileInfo.Extension.ToLower() == ".png")
                        {
                            fileImg.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        }
                        else if (fileInfo.Extension.ToLower() == ".jpg" || fileInfo.Extension.ToLower() == ".jpeg")
                        {
                            fileImg.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        else if (fileInfo.Extension.ToLower() == ".gif")
                        {
                            fileImg.Save(stream, System.Drawing.Imaging.ImageFormat.Gif);
                        }
                        else if (fileInfo.Extension.ToLower() == ".tiff")
                        {
                            fileImg.Save(stream, System.Drawing.Imaging.ImageFormat.Tiff);
                        }
                        else if (fileInfo.Extension.ToLower() == ".bmp")
                        {
                            fileImg.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                        }
                        else
                        {
                            iBsh = null;
                            return string.Empty;//不能识别的文件格式。
                        }

                        byte[] b = stream.GetBuffer();
                        string imgDataText = Convert.ToBase64String(b);

                        ImageBrush newLoadedImageBrush = new ImageBrush()
                        {
                            ImageSource = new BitmapImage(new Uri(filename, UriKind.Relative)),
                            ViewportUnits = System.Windows.Media.BrushMappingMode.Absolute,
                            Viewport = new Rect(0, 0, fileImg.Width, fileImg.Height),
                        };

                        newLoadedImageBrush.TileMode = tileMode;

                        iBsh = newLoadedImageBrush;

                        return "i" + imgDataText;
                    }
                    catch (System.Runtime.InteropServices.ExternalException ex)
                    {
                        MessageBox.Show("　　该图片文件后缀名有问题，或图片被损坏。" +
                            "该图像以错误的图像格式后缀名保存！\r\n" + ex.Message + ex.StackTrace,
                            Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
                        iBsh = null;
                        return string.Empty;
                    }
                }
            }
            else
            {
                MessageBox.Show("　　所选择的图片文件过大！" +
                    "为防止文档体积过分庞大，本图片框只支持文件尺寸在 2MB 以下的图片！" +
                    "请将图片加工一下或另选一张图片。", Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                iBsh = null;
                return string.Empty;
            }
        }

        /// <summary>
        /// SolidColorBrush转换成相应的字符串。注意：以“s”开头。
        /// 
        /// 如果不合法，生成空字符串。
        /// </summary>
        /// <param name="srcBrush">画刷。</param>
        /// <returns>生成的文本。</returns>
        public static string ParseToText(Brush srcBrush)
        {
            SolidColorBrush scb = srcBrush as SolidColorBrush;
            if (scb != null)
            {
                return "s" + BrushManager.GetName(scb);
            }

            return string.Empty;
        }

        #endregion
    }

    /// <summary>
    /// 此类只用于BrushManager。
    /// </summary>
    internal class BrushItem
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="bsh">Brush。</param>
        /// <param name="enName">Brush的中文名称。</param>
        /// <param name="cnName">Brush的英文名称。</param>
        public BrushItem(Brush bsh, string enName, string cnName)
        {
            this.brush = bsh;
            this.enName = enName;
            this.cnName = cnName;
        }

        private Brush brush = Brushes.Transparent;

        /// <summary>
        /// 默认是透明色。
        /// </summary>
        public Brush Brush
        {
            get { return brush; }
            set { brush = value; }
        }

        private string enName = "Transparent";
        /// <summary>
        /// Brush的英文名称。
        /// </summary>
        public string EnName
        {
            get { return enName; }
        }

        private string cnName = "透明色";
        /// <summary>
        /// Brush的中文名称。这些名称是参考网络上的色彩名称的译法修改而来。
        /// </summary>
        public string CnName
        {
            get { return cnName; }
        }
    }
}
