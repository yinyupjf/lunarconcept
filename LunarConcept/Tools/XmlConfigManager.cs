﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SHomeWorkshop.LunarConcept
{
    public class XmlConfigManager
    {
        private string fileName;

        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        private XmlDocument xmlDocument;

        /// <summary>
        /// 打开配置文件（xml）。
        /// </summary>
        /// <returns>文件存在并打开，返回true；文件不存在并创建，返回false。</returns>
        public bool Open()
        {
            try
            {
                xmlDocument.Load(fileName);
            }
            catch (Exception)
            {
                //如果文件不存在，就创建一个新文件。
                XmlTextWriter xmlWriter = new XmlTextWriter(fileName, System.Text.Encoding.UTF8);
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                xmlWriter.WriteStartElement("Root");

                //如果像上面这样使用WriteProcessingInstruction，
                //这里就不使用WriteEndElement()
                //xmlWriter.WriteEndElement();
                //这会导致<Root></Root>变成<Root />
                xmlWriter.Close();
                xmlDocument.Load(fileName);
                return false;
            }
            return true;
        }

        public XmlConfigManager(string fileName)
        {
            this.fileName = fileName;
            xmlDocument = new XmlDocument();
            this.Open();
        }

        /// <summary>
        /// 难得看到的析构函数。
        /// </summary>
        ~XmlConfigManager()
        {
            Save();
        }

        public void Save()
        {
            xmlDocument.Save(fileName);
        }

        /// <summary>
        /// 如果不存在，返回null。
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string Get(string name)
        {
            XmlNode node = xmlDocument.SelectSingleNode("Root").SelectSingleNode(name);
            if (node == null)
            {
                return null;
            }
            else
            {
                return node.InnerText;
            }
        }

        public void Set(string name, string value)
        {
            XmlNode root = xmlDocument.SelectSingleNode("Root");
            XmlNode node = root.SelectSingleNode(name);
            if (node != null)
            {
                node.InnerText = value;
            }
            else
            {
                XmlElement element = xmlDocument.CreateElement(name);
                element.InnerText = value;
                root.AppendChild(element);
            }
        }
    }
}
