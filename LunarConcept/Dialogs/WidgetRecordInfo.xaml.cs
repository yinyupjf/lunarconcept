﻿using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using System;

namespace SHomeWorkshop.LunarConcept.Dialogs
{
    /// <summary>
    /// Interaction logic for ExRecordInfo.xaml
    /// </summary>
    public partial class WidgetRecordInfo : Window
    {
        /// <summary>
        /// 创建时间：2012年2月3日
        /// 创建者：  杨震宇
        /// 
        /// 主要用途：创建一个记录（部件）信息对话框。用以将部件存入数据库。
        /// </summary>
        public WidgetRecordInfo()
        {
            InitializeComponent();

            tbxTitle.SelectAll();
            tbxTitle.Focus();
        }

        private void btnSaveToDataBase_Click(object sender, RoutedEventArgs e)
        {
            if (tbxTitle.Text.Length <= 0)
            {
                MessageBox.Show("　　请输入标题。", Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (tbxDescription.Text.Length <= 0)
            {
                tbxDescription.Text = "<未设置>";
            }

            if (tbxComment.Text.Length <= 0)
            {
                tbxComment.Text = "<未设置>";
            }

            this.DialogResult = true;
            this.Close();
        }

        public string WidgetTitle { get { return tbxTitle.Text; } }
        
        public string Description { get { return tbxDescription.Text; } }

        public string Comment { get { return tbxComment.Text; } }

        private void tbxComment_GotFocus(object sender, RoutedEventArgs e)
        {
            tbxComment.SelectAll();
        }

        private void tbxAuthorInfo_GotFocus(object sender, RoutedEventArgs e)
        {
            tbxDescription.SelectAll();
        }

        private void tbxKeyWord_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    {
                        btnSaveToDataBase_Click(null, null);
                        break;
                    }
                case Key.Escape:
                    {
                        this.DialogResult = false;
                        this.Close();
                        break;
                    }
            }
        }

        private void tbxDescription_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    {
                        btnSaveToDataBase_Click(null, null);
                        break;
                    }
                case Key.Escape:
                    {
                        this.DialogResult = false;
                        this.Close();
                        break;
                    }
            }
        }

        private void tbxComment_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    {
                        btnSaveToDataBase_Click(null, null);
                        break;
                    }
                case Key.Escape:
                    {
                        this.DialogResult = false;
                        this.Close();
                        break;
                    }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
