﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// 创建时间：2012年2月22日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：自定义的关于框。
    ///           AboutBox.xaml 的交互逻辑
    /// </summary>
    public partial class AboutBox : Window
    {
        public AboutBox(Window owner)
        {
            InitializeComponent();

            this.Owner = owner;

            tbSizeInfo.Text = $"MainWindow: {Globals.MainWindow.ActualWidth} * {Globals.MainWindow.ActualHeight}(Viewport: {Globals.MainWindow.MainScrollViewer.ViewportWidth} * {Globals.MainWindow.MainScrollViewer.ViewportHeight})";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    /// <summary>
    /// 主要用途：利用反射取出需要呈现在“关于”框中的几个属性值。然后在关于框中可以直接利用x:Static 绑定。
    /// </summary>
    public class AssemblyInfoWrap
    {
        /// <summary>
        /// [静态构造方法]利用反射取出相关属性的值。
        /// </summary>
        static AssemblyInfoWrap()
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            if (attributes.Length > 0)
            {
                company = (attributes[0] as AssemblyCompanyAttribute).Company;
            }

            attributes = assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            if (attributes.Length > 0)
            {
                copyright = (attributes[0] as AssemblyCopyrightAttribute).Copyright;
            }

            attributes = assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                description = (attributes[0] as AssemblyDescriptionAttribute).Description;
            }

            attributes = assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length > 0)
            {
                product = (attributes[0] as AssemblyProductAttribute).Product;
            }

            attributes = assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            if (attributes.Length > 0)
            {
                title = (attributes[0] as AssemblyTitleAttribute).Title;
            }

            attributes = assembly.GetCustomAttributes(typeof(AssemblyTrademarkAttribute), false);
            if (attributes.Length > 0)
            {
                trademark = (attributes[0] as AssemblyTrademarkAttribute).Trademark;
            }

            attributes = assembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);
            if (attributes.Length > 0)
            {
                version = (attributes[0] as AssemblyFileVersionAttribute).Version;
            }
        }

        private static string company;
        /// <summary>
        /// [只读]返回公司名。
        /// </summary>
        public static string Company
        {
            get { return AssemblyInfoWrap.company; }
        }

        private static string copyright;
        /// <summary>
        /// [只读]返回版权声明。
        /// </summary>
        public static string Copyright
        {
            get { return AssemblyInfoWrap.copyright; }
        }

        private static string description;
        /// <summary>
        /// [只读]返回描述。
        /// </summary>
        public static string Description
        {
            get { return AssemblyInfoWrap.description; }
        }

        private static string product;
        /// <summary>
        /// [只读]返回产品名。
        /// </summary>
        public static string Product
        {
            get { return AssemblyInfoWrap.product; }
        }

        private static string title;
        /// <summary>
        /// [只读]返回程序标题。
        /// </summary>
        public static string Title
        {
            get { return AssemblyInfoWrap.title; }
        }

        private static string trademark;
        /// <summary>
        /// [只读]返回商标名。
        /// </summary>
        public static string Trademark
        {
            get { return AssemblyInfoWrap.trademark; }
        }

        private static string version;
        /// <summary>
        /// [只读]返回程序版本号字符串。
        /// </summary>
        public static string Version
        {
            get { return AssemblyInfoWrap.version; }
        }
    }
}
