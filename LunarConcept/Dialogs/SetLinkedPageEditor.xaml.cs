﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;

namespace SHomeWorkshop.LunarConcept.Dialogs
{
    /// <summary>
    /// 创建时间：2012年2月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置某个ContentWidget（一般用文本框即可）链接到某个页面。
    ///           SetLinkedPageEditor.xaml 的交互逻辑
    /// </summary>
    public partial class SetLinkedPageEditor : Window
    {
        public SetLinkedPageEditor(Window owner, Widgets.ContentWidget destWidget)
        {
            InitializeComponent();

            this.Owner = owner;
            this.destWidget = destWidget;

            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            LboxPageEditor.Items.Add(new ListBoxItem()
            {
                Content = "..取消链接..",
                Tag = "DelLink",
                HorizontalAlignment = HorizontalAlignment.Center,
            });
            LboxPageEditor.Items.Add(new Separator());

            if (Globals.MainWindow != null && Globals.MainWindow.EditorManager != null)
            {
                EditorManager manager = Globals.MainWindow.EditorManager;
                if (manager != null)
                {
                    for (int i = 0; i < manager.Children.Count; i++)
                    {
                        PageEditor pe = manager.Children[i] as PageEditor;
                        if (pe == null) continue;

                        PageListItem pli = new PageListItem(i + 1, pe.PageTitle, pe.Id);
                        pli.MouseDoubleClick += new MouseButtonEventHandler(pli_MouseDoubleClick);
                        LboxPageEditor.Items.Add(pli);
                    }
                }
            }

            if (destWidget == null)
            {
                BtnOk.IsEnabled = false;//没有指定部件。
            }
            else
            {
                for (int i = 0; i < LboxPageEditor.Items.Count; i++)
                {
                    PageListItem pli = LboxPageEditor.Items[i] as PageListItem;
                    if (pli == null)//可能是分隔线
                        continue;

                    if (pli.PageEditorId == destWidget.LinkedPageEditorId)
                    {
                        LboxPageEditor.SelectedItem = pli; break;
                    }
                }

                LboxPageEditor.Focus();
            }
        }

        void pli_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (BtnOk.IsEnabled)
            {
                BtnOk_Click(sender, e);
            }
        }

        private Widgets.ContentWidget destWidget;

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            //指定链接目标页。
            if (destWidget == null || destWidget.MasterEditor == null || destWidget.MasterEditor.MasterManager == null) return;

            EditorManager manager = destWidget.MasterEditor.MasterManager;

            if (LboxPageEditor.SelectedItem == null)
            {
                MessageBox.Show("　　请选定一个页面作为链接目标！", Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            PageListItem pli = LboxPageEditor.SelectedItem as PageListItem;
            if (pli == null)
            {
                ListBoxItem lbi = LboxPageEditor.SelectedItem as ListBoxItem;
                if (lbi == null) return;

                if ((lbi.Tag as string) == "DelLink")
                {
                    //删除链接
                    DeleteLink(manager);
                }
                return;
            }

            if (pli.PageEditorId == destWidget.MasterEditor.Id)
            {
                MessageBoxResult r = MessageBox.Show("　　链接到部件所在页面没有什么意义！\r\n\r\n　　要继续吗？",
                    Globals.AppName, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (r != MessageBoxResult.Yes) return;
            }

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "链接到页面" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            Action act = new Action(destWidget.MasterEditor.Id, destWidget.Id, destWidget.GetType().Name,
                XmlTags.LinkedPageEditorIdTag, destWidget.LinkedPageEditorId, pli.PageEditorId);
            destWidget.LinkedPageEditorId = pli.PageEditorId;

            mi.AddAction(act);
            manager.RegisterModifingItem(mi);

            this.Close();
        }

        private void DeleteLink(EditorManager manager)
        {
            ModifingInfo info = new ModifingInfo() { ModifingDescription = "取消页面链接" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            Action act = new Action(destWidget.MasterEditor.Id, destWidget.Id, destWidget.GetType().Name,
                XmlTags.LinkedPageEditorIdTag, destWidget.LinkedPageEditorId, string.Empty);
            destWidget.LinkedPageEditorId = string.Empty;

            mi.AddAction(act);
            manager.RegisterModifingItem(mi);

            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LboxPageEditor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LboxPageEditor.SelectedIndex < 0)
            {
                BtnOk.IsEnabled = false;
            }
            else
            {
                BtnOk.IsEnabled = true;
            }
        }
    }

    class PageListItem : ListBoxItem
    {
        public PageListItem(int pageIndex, string pageTitleText, string pageEditorId)
        {
            this.pageTitleText = pageTitleText;
            this.pageIndex = pageIndex;
            this.pageEditorId = pageEditorId;

            this.Content = string.Format("第{0,4} 页：{1}", this.pageIndex,
                (this.pageTitleText == null || pageTitleText == string.Empty) ? "..无标题.." : this.pageTitleText);
        }

        private int pageIndex;

        public int PageIndex
        {
            get { return pageIndex; }
        }

        private string pageTitleText;

        public string PageTitleText
        {
            get { return pageTitleText; }
        }

        private string pageEditorId;

        public string PageEditorId
        {
            get { return pageEditorId; }
        }

    }
}
