﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept.Dialogs
{
    /// <summary>
    /// OutLineDialog.xaml 的交互逻辑
    /// </summary>
    public partial class OutLineDialog : Window
    {
        public OutLineDialog(Window owner)
        {
            InitializeComponent();

            this.Owner = owner;
        }

        /// <summary>
        /// 应在窗口显示之前将所有段落添加进来。
        /// </summary>
        /// <param name="newParagraph"></param>
        public void AddParagraph(Paragraph newParagraph)
        {
            if (newParagraph == null) return;

            this.MainDoc.Blocks.Add(newParagraph);
        }

        private void btnCopyAsRtf_Click(object sender, RoutedEventArgs e)
        {
            MainRTB.SelectAll();
            MainRTB.Copy();

            this.DialogResult = true;
            this.Close();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
