﻿using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHomeWorkshop.LunarConcept.Comparers
{
    class UserStyleListItemComparer : IComparer<UserStyleListItem>
    {
        public int Compare(UserStyleListItem x, UserStyleListItem y)
        {
            return x.WidgetClassName.CompareTo(y.WidgetClassName);
        }
    }
}
