﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept
{
    public static class XmlTags
    {
        private static readonly string arrowsTag = "Arrows";
        /// <summary>
        /// "Arrows"标签名。
        /// </summary>
        public static string ArrowsTag
        {
            get { return XmlTags.arrowsTag; }
        }

        private static readonly string backColorTag = "BackColor";
        /// <summary>
        /// "BackColor"标签名。
        /// </summary>
        public static string BackColorTag
        {
            get { return XmlTags.backColorTag; }
        }

        private static readonly string centerCPPointTag = "CenterCPPoint";
        /// <summary>
        /// "CenterCPPoint"标签名。
        /// </summary>
        public static string CenterCPPointTag
        {
            get { return XmlTags.centerCPPointTag; }
        }

        private static readonly string commentBottomTextTag = "CommentBottomText";
        /// <summary>
        /// "CommentBottomText"标签名。
        /// </summary>
        public static string CommentBottomTextTag
        {
            get { return XmlTags.commentBottomTextTag; }
        }

        private static readonly string commentTopTextTag = "CommentTopText";
        /// <summary>
        /// "CommentTopText"标签名。
        /// </summary>
        public static string CommentTopTextTag
        {
            get { return XmlTags.commentTopTextTag; }
        }

        private static readonly string defaultBackTileModeTag = "DefaultBackTileMode";
        /// <summary>
        /// "DefaultBackTileMode"标签名。
        /// </summary>
        public static string DefaultBackTileModeTag
        {
            get { return XmlTags.defaultBackTileModeTag; }
        }

        private static readonly string defaultFontSizeTag = "DefaultFontSize";
        /// <summary>
        /// "DefaultFontSize"标签名。
        /// </summary>
        public static string DefaultFontSizeTag
        {
            get { return XmlTags.defaultFontSizeTag; }
        }

        private static readonly string defaultFontNameTag = "DefaultFontName";
        /// <summary>
        /// "DefaultFontName"标签名。
        /// </summary>
        public static string DefaultFontNameTag
        {
            get { return XmlTags.defaultFontNameTag; }
        }

        private static readonly string defaultForegroundTag = "DefaultForeground";
        /// <summary>
        /// "DefaultForeground"标签名。
        /// </summary>
        public static string DefaultForegroundTag
        {
            get { return XmlTags.defaultForegroundTag; }
        }

        private static readonly string directionTag = "Direction";
        /// <summary>
        /// [只读]"Direction"标签名。
        /// </summary>
        public static string DirectionTag
        {
            get { return XmlTags.directionTag; }
        }

        private static readonly string documentBackgroundTag = "DocumentBackground";
        /// <summary>
        /// "DocumentBackground"标签名。
        /// </summary>
        public static string DocumentBackgroundTag
        {
            get { return XmlTags.documentBackgroundTag; }
        }

        private static readonly string endCPPointTag = "EndCPPoint";
        /// <summary>
        /// "EndCPPoint"标签名。
        /// </summary>
        public static string EndCPPointTag
        {
            get { return XmlTags.endCPPointTag; }
        }

        private static readonly string endMasterIdTag = "EndMasterId";
        /// <summary>
        /// "EndMasterId"标签名。
        /// </summary>
        public static string EndMasterIdTag
        {
            get { return XmlTags.endMasterIdTag; }
        }

        private static readonly string endPointTag = "EndPoint";
        /// <summary>
        /// "EndPoint"标签名。
        /// </summary>
        public static string EndPointTag
        {
            get { return XmlTags.endPointTag; }
        }

        private static readonly string endLinkToTag = "EndLinkTo";
        /// <summary>
        /// "EndLinkTo"标签名。
        /// </summary>
        public static string EndLinkToTag
        {
            get { return XmlTags.endLinkToTag; }
        }

        private static readonly string fontNameTag = "FontName";
        /// <summary>
        /// "FontName"标签。
        /// </summary>
        public static string FontNameTag
        {
            get { return XmlTags.fontNameTag; }
        }

        private static readonly string fontSizeTag = "FontSize";
        /// <summary>
        /// "FontSize"标签名。
        /// </summary>
        public static string FontSizeTag
        {
            get { return XmlTags.fontSizeTag; }
        }

        private static readonly string fontStyleTag = "FontStyle";
        /// <summary>
        /// "FontStyle"标签名。
        /// </summary>
        public static string FontStyleTag
        {
            get { return XmlTags.fontStyleTag; }
        }

        private static readonly string fontWeightTag = "FontWeight";
        /// <summary>
        /// "FontWeight"标签名。
        /// </summary>
        public static string FontWeightTag
        {
            get { return XmlTags.fontWeightTag; }
        }

        private static readonly string foreColorTag = "ForeColor";
        /// <summary>
        /// "ForeColor"标签名。
        /// </summary>
        public static string ForeColorTag
        {
            get { return XmlTags.foreColorTag; }
        }

        private static readonly string docTag = "LunarConcept.Document";
        /// <summary>
        /// 文档根元素的标签。
        /// </summary>
        public static string DocTag
        {
            get { return XmlTags.docTag; }
        }

        private static readonly string dockTag = "Dock";
        /// <summary>
        /// "Dock"标签名。
        /// </summary>
        public static string DockTag
        {
            get { return XmlTags.dockTag; }
        }

        private static readonly string fillBlankModeTag = "FillBlankMode";
        /// <summary>
        /// "FillBlankMode"标签名。
        /// </summary>
        public static string FillBlankModeTag
        {
            get { return XmlTags.fillBlankModeTag; }
        }

        private static readonly string fixTextWidthTag = "FixTextWidth";
        /// <summary>
        /// "FixTextWidth"标签名。
        /// </summary>
        public static string FixTextWidthTag
        {
            get { return XmlTags.fixTextWidthTag; }
        }

        private static readonly string idTag = "ID";
        /// <summary>
        /// "ID"的标签名。
        /// </summary>
        public static string IdTag
        {
            get { return XmlTags.idTag; }
        }

        private static readonly string imageBase64Tag = "ImageBase64";
        /// <summary>
        /// "ImageBase64"标签名。
        /// </summary>
        public static string ImageBase64Tag
        {
            get { return XmlTags.imageBase64Tag; }
        }

        private static readonly string xamlIconTextTag = "XamlIconText";
        /// <summary>
        /// "XamlIconText"标签名。
        /// 完全忘记了 矩形部件 之前已经实现粘贴 Xaml 图标功能，又在 图片框 上重做了个类似的功能。2021年4月14日
        /// </summary>
        public static string XamlIconTextTag
        {
            get { return XmlTags.xamlIconTextTag; }
        }

        //private static readonly string backgroundImageBase64Tag = "BackBase64";
        ///// <summary>
        ///// "BackBase64"标签名。
        ///// </summary>
        //public static string BackgroundImageBase64Tag
        //{
        //    get { return XmlTags.backgroundImageBase64Tag; }
        //}

        private static readonly string innerIconNameTag = "InnerIconName";
        /// <summary>
        /// "InnerIconName"标签名。
        /// </summary>
        public static string InnerIconNameTag
        {
            get { return XmlTags.innerIconNameTag; }
        }

        private static readonly string isImageCollapsedTag = "IsImageCollapsed";
        /// <summary>
        /// "IsImageCollapsed"标签名。
        /// </summary>
        public static string IsImageCollapsedTag
        {
            get { return XmlTags.isImageCollapsedTag; }
        }

        private static readonly string isLockedTag = "IsLocked";
        /// <summary>
        /// "IsLocked"标签名。
        /// </summary>
        public static string IsLockedTag
        {
            get { return XmlTags.isLockedTag; }
        }

        private static readonly string isMarkTextCollapsedTag = "IsMarkTextCollapsed";
        /// <summary>
        /// "IsMarkTextCollapsed"标签名。
        /// </summary>
        public static string IsMarkTextCollapsedTag
        {
            get { return XmlTags.isMarkTextCollapsedTag; }
        }

        private static readonly string isShadowVisibleTag = "IsShadowVisible";
        /// <summary>
        /// "IsShadowVisible"标签名。
        /// </summary>
        public static string IsShadowVisibleTag
        {
            get { return XmlTags.isShadowVisibleTag; }
        }

        private static readonly string isTextAtLeftOrTopTag = "IsTextAtLeftOrTop";
        /// <summary>
        /// "IsTextAtLeftOrTop"标签名。
        /// </summary>
        public static string IsTextAtLeftOrTopTag
        {
            get { return XmlTags.isTextAtLeftOrTopTag; }
        }

        private static readonly string isTextAtCloseTag = "IsTextAtClose";
        /// <summary>
        /// "IsTextAtClose"标签名。
        /// </summary>
        public static string IsTextAtCloseTag
        {
            get { return XmlTags.isTextAtCloseTag; }
        }

        private static readonly string isTextVisibleTag = "IsTextVisible";
        /// <summary>
        /// "IsTextVisible"标签名。
        /// </summary>
        public static string IsTextVisibleTag
        {
            get { return XmlTags.isTextVisibleTag; }
        }

        private static readonly string layerIndexTag = "LayerIndex";
        /// <summary>
        /// "LayerIndex"标签名。
        /// </summary>
        public static string LayerIndexTag
        {
            get { return XmlTags.layerIndexTag; }
        }

        private static readonly string lineDashTag = "LineDash";
        /// <summary>
        /// "actLineDash"标签名。
        /// </summary>
        public static string LineDashTag
        {
            get { return XmlTags.lineDashTag; }
        }

        private static readonly string lineWidthTag = "LineWidth";
        /// <summary>
        /// "LineWidth"标签名。
        /// </summary>
        public static string LineWidthTag
        {
            get { return XmlTags.lineWidthTag; }
        }

        private static readonly string linkedPageEditorIdTag = "LinkedPageEditorId";
        /// <summary>
        /// "LinkedPageEditorId"标签名。
        /// </summary>
        public static string LinkedPageEditorIdTag
        {
            get { return XmlTags.linkedPageEditorIdTag; }
        }

        private static readonly string locationTag = "Location";
        /// <summary>
        /// "Location"标签名。
        /// </summary>
        public static string LocationTag
        {
            get { return XmlTags.locationTag; }
        }

        private static readonly string markTextTag = "MarkText";
        /// <summary>
        /// "MarkText"标签名。
        /// </summary>
        public static string MarkTextTag
        {
            get { return XmlTags.markTextTag; }
        }

        private static readonly string maskTag = "Mask";
        /// <summary>
        /// "Mask"标签名。
        /// </summary>
        public static string MaskTag
        {
            get { return XmlTags.maskTag; }
        }

        private static readonly string pageTag = "Page";
        /// <summary>
        /// "Page"的标签名。
        /// </summary>
        public static string PageTag
        {
            get { return pageTag; }
        }

        private static readonly string pageBackground = "PageBackground";
        /// <summary>
        /// "PageBackground"标签名。
        /// </summary>
        public static string PageBackground
        {
            get { return XmlTags.pageBackground; }
        }

        private static readonly string pageFooterTag = "PageFooter";
        /// <summary>
        /// "PageFooter"标签名。
        /// </summary>
        public static string PageFooterTag
        {
            get { return XmlTags.pageFooterTag; }
        }

        private static readonly string pageHeaderTag = "PageHeader";
        /// <summary>
        /// "PageHeader"标签名。
        /// </summary>
        public static string PageHeaderTag
        {
            get { return XmlTags.pageHeaderTag; }
        }

        private static readonly string isShowPageNumberTag = "ShowPageNumber";
        /// <summary>
        /// "ShowPageNumber"标签名。
        /// </summary>
        public static string IsShowPageNumberTag
        {
            get { return isShowPageNumberTag; }
        }

        private static readonly string pageSetTag = "PageSet";
        /// <summary>
        /// "PageSet"标签的名称。
        /// </summary>
        public static string PageSetTag
        {
            get { return XmlTags.pageSetTag; }
        }

        private static readonly string pageTitleTag = "PageTitle";
        /// <summary>
        /// "PageTitle"标签名。
        /// </summary>
        public static string PageTitleTag
        {
            get { return XmlTags.pageTitleTag; }
        }

        private static readonly string paperDirectionTag = "PaperDirection";
        /// <summary>
        /// "PaperDirection"标签名。
        /// </summary>
        public static string PaperDirectionTag
        {
            get { return XmlTags.paperDirectionTag; }
        }

        private static readonly string pageLongSideMultipleTag = "PageLongSideMultipleTag";
        /// <summary>
        /// "PageLongSideMultipleTag"标签名。
        /// </summary>
        public static string PageLongSideMultipleTag
        {
            get { return XmlTags.pageLongSideMultipleTag; }
        }

        private static readonly string pageShortSideMultipleTag = "PageShortSideMultipleTag";
        /// <summary>
        /// "PageShortSideMultipleTag"标签名。
        /// </summary>
        public static string PageShortSideMultipleTag
        {
            get { return XmlTags.pageShortSideMultipleTag; }
        }

        private static readonly string paperSizeTag = "PaperSize";
        /// <summary>
        /// "PaperSize"标签的名称。
        /// </summary>
        public static string PaperSizeTag
        {
            get { return XmlTags.paperSizeTag; }
        }

        private static readonly string radiusTag = "Radius";
        /// <summary>
        /// "Radius"标签名。
        /// </summary>
        public static string RadiusTag
        {
            get { return XmlTags.radiusTag; }
        }

        private static readonly string startCPPointTag = "StartCPPoint";
        /// <summary>
        /// "CenterCPPoint"标签名。
        /// </summary>
        public static string StartCPPointTag
        {
            get { return XmlTags.startCPPointTag; }
        }

        private static readonly string startMasterIdTag = "StartMasterId";
        /// <summary>
        /// "StartMasterId"标签名。
        /// </summary>
        public static string StartMasterIdTag
        {
            get { return XmlTags.startMasterIdTag; }
        }

        private static readonly string startPointTag = "StartPoint";
        /// <summary>
        /// "StartPoint"标签名。
        /// </summary>
        public static string StartPointTag
        {
            get { return XmlTags.startPointTag; }
        }

        private static readonly string startLinkToTag = "StartLinkTo";
        /// <summary>
        /// "StartLinkTo"标签名。
        /// </summary>
        public static string StartLinkToTag
        {
            get { return XmlTags.startLinkToTag; }
        }

        private static string strikeLineTag = "StrikeLine";
        /// <summary>
        /// "StrikeLine"标签名。
        /// </summary>
        public static string StrikeLineTag
        {
            get { return XmlTags.strikeLineTag; }
            set { XmlTags.strikeLineTag = value; }
        }

        private static readonly string textTag = "Text";
        /// <summary>
        /// "Text"标签名。
        /// </summary>
        public static string TextTag
        {
            get { return XmlTags.textTag; }
        }

        private static readonly string textAreaTag = "TextArea";
        /// <summary>
        /// "TextWidget"部件的Xml标签名。
        /// </summary>
        public static string TextAreaTag
        {
            get { return XmlTags.textAreaTag; }
        }

        private static readonly string textRotateAngleTag = "TextRotateAngle";
        /// <summary>
        /// "TextRotateAngle"标签名。
        /// </summary>
        public static string TextRotateAngleTag
        {
            get { return XmlTags.textRotateAngleTag; }
        }

        private static readonly string textAreaAlignmentTag = "TextAreaAlignment";
        /// <summary>
        /// "TextAreaAlignment"标签名。
        /// </summary>
        public static string TextAreaAlignmentTag
        {
            get { return XmlTags.textAreaAlignmentTag; }
        }

        private static readonly string tileModeTag = "TileMode";
        /// <summary>
        /// "TileMode"标签名。
        /// </summary>
        public static string TileModeTag
        {
            get { return XmlTags.tileModeTag; }
        }

        private static readonly string titleLevelTag = "TitleLevel";
        /// <summary>
        /// "TitleLevel"标签名。
        /// </summary>
        public static string TitleLevelTag
        {
            get { return XmlTags.titleLevelTag; }
        }

        private static readonly string underLineTag = "UnderLine";
        /// <summary>
        /// "UnderLine"标签名。
        /// </summary>
        public static string UnderLineTag
        {
            get { return XmlTags.underLineTag; }
        }

        private static readonly string widgetTag = "Widget";
        /// <summary>
        /// "Widget"标签名。
        /// </summary>
        public static string WidgetTag
        {
            get { return XmlTags.widgetTag; }
        }

        private static readonly string widgetBorderColor = "WidgetBorderColor";
        /// <summary>
        /// "WidgetBorderColor"标签名。
        /// </summary>
        public static string WidgetBorderColor
        {
            get { return XmlTags.widgetBorderColor; }
        }

        private static readonly string widgetBorderWidthTag = "WidgetBorderWidth";
        /// <summary>
        /// "WidgetBorderWidth"标签名。
        /// </summary>
        public static string WidgetBorderWidthTag
        {
            get { return XmlTags.widgetBorderWidthTag; }
        }

        private static readonly string widgetBackColor = "WidgetBackColor";
        /// <summary>
        /// "WidgetBackColor"标签名。
        /// </summary>
        public static string WidgetBackColor
        {
            get { return XmlTags.widgetBackColor; }
        }

        private static readonly string widgetForeColor = "WidgetForeColor";
        /// <summary>
        /// "WidgetForeColor"标签名。
        /// </summary>
        public static string WidgetForeColor
        {
            get { return XmlTags.widgetForeColor; }
        }

        private static readonly string widgetLineColorTag = "WidgetLineColor";
        /// <summary>
        /// "WidgetLineColor"标签名。
        /// </summary>
        public static string WidgetLineColorTag
        {
            get { return XmlTags.widgetLineColorTag; }
        }

        private static readonly string widgetLineWidthTag = "WidgetLineWidth";
        /// <summary>
        /// "WidgetLineWidth"标签名。
        /// </summary>
        public static string WidgetLineWidthTag
        {
            get { return XmlTags.widgetLineWidthTag; }
        }

        private static readonly string widgetPadding = "WidgetPadding";
        /// <summary>
        /// "WidgetPadding"标签名。
        /// </summary>
        public static string WidgetPaddingTag
        {
            get { return XmlTags.widgetPadding; }
        }

        private static readonly string widgetStartControlerBrush = "WidgetStartControlerBrush";
        /// <summary>
        /// "WidgetStartControlerBrush"标签名。——用于指代线型部件首控制点色彩。
        /// </summary>
        public static string WidgetStartControlerBrush
        {
            get { return XmlTags.widgetStartControlerBrush; }
        }

        private static readonly string widgetCenterControlerBrush = "WidgetCenterControlerBrush";
        /// <summary>
        /// "WidgetCenterControlerBrush"标签名。——用于指代线型部件中控制点色彩。
        /// </summary>
        public static string WidgetCenterControlerBrush
        {
            get { return XmlTags.widgetCenterControlerBrush; }
        }

        private static readonly string widgetEndControlerBrush = "WidgetEndControlerBrush";
        /// <summary>
        /// "WidgetEndControlerBrush"标签名。——用于指代线型部件尾控制点色彩。
        /// </summary>
        public static string WidgetEndControlerBrush
        {
            get { return XmlTags.widgetEndControlerBrush; }
        }

        private static readonly string widgetSelectionAdornerBrush = "WidgetSelectionAdornerBrush";
        /// <summary>
        /// "WidgetSelectionAdornerBrush"标签名。——用于指代部件选定框的色彩。
        /// </summary>
        public static string WidgetSelectionAdornerBrush
        {
            get { return XmlTags.widgetSelectionAdornerBrush; }
        }

        private static readonly string widgetSetTag = "WidgetSet";
        /// <summary>
        /// "WidgetSet"的标签名。
        /// </summary>
        public static string WidgetSetTag
        {
            get { return XmlTags.widgetSetTag; }
        }

        private static readonly string paragraphTag = "Paragraph";
        /// <summary>
        /// "Paragraph"标签名。
        /// </summary>
        public static string ParagraphTag
        {
            get { return XmlTags.paragraphTag; }
        }

        private static readonly string paragraphSetTag = "ParagraphSet";
        /// <summary>
        /// "ParagraphSet"标签名。
        /// </summary>
        public static string ParagraphSetTag
        {
            get { return XmlTags.paragraphSetTag; }
        }

        private static readonly string xmlDataInnerXml = "XmlDataInnerXml";
        /// <summary>
        /// "XmlDataInnerXml"标签名。
        /// </summary>
        public static string XmlDataInnerXml
        {
            get { return xmlDataInnerXml; }
        }

        private static readonly string widgetOpacityTag = "WidgetOpacity";

        public static string WidgetOpacityTag
        {
            get { return XmlTags.widgetOpacityTag; }
        }

        private static readonly string textAreaStyleTag = "TextAreaStyle";
        public static string TextAreaStyleTag { get { return textAreaStyleTag; } }

        private static readonly string pictureBoxStyleTag = "PictureBoxStyle";
        public static string PictureBoxStyleTag { get { return pictureBoxStyleTag; } }

        private static readonly string groupStyleTag = "GroupStyle";
        public static string GroupStyleTag { get { return groupStyleTag; } }

        private static readonly string straitLineStyleTag = "StraitLineStyle";
        public static string StraitLineStyleTag { get { return straitLineStyleTag; } }

        private static readonly string bezierLineStyleTag = "BezierLineStyle";
        public static string BezierLineStyleTag { get { return bezierLineStyleTag; } }

        private static readonly string polyLineStyleTag = "PolyLineStyle";
        public static string PolyLineStyleTag { get { return polyLineStyleTag; } }

        private static readonly string ellipseStyleTag = "EllipseStyle";
        public static string EllipseStyleTag { get { return ellipseStyleTag; } }

        private static readonly string rectangleStyleTag = "RectangleStyle";
        public static string RectangleStyleTag { get { return rectangleStyleTag; } }

        private static readonly string rhombStyleTag = "RhombStyle";
        public static string RhombStyleTag { get { return rhombStyleTag; } }

        private static readonly string triangleStyleTag = "TriangleStyle";
        public static string TriangleStyleTag { get { return triangleStyleTag; } }

        private static readonly string bracketStyleTag = "BracketStyle";
        public static string BracketStyleTag { get { return bracketStyleTag; } }


        private static readonly string textAlignmentTag = "TextAlignment";
        /// <summary>
        /// "TextAlignment"标签名。
        /// </summary>
        public static string TextAlignmentTag
        {
            get { return XmlTags.textAlignmentTag; }
        }

        private static readonly string streamerTag = "Streamer";
        /// <summary>
        /// "Streamer"标签名。
        /// </summary>
        public static string StreamerTag
        {
            get { return XmlTags.streamerTag; }
        }

        private static readonly string baseLineAlignmentTag = "BaseLineAlignment";
        /// <summary>
        /// "BaseLineAlignment"标签名。
        /// </summary>
        public static string BaseLineAlignmentTag
        {
            get { return XmlTags.baseLineAlignmentTag; }
        }



        private static readonly string boldTag = "Bold";
        /// <summary>
        /// "Bold"标签名。
        /// </summary>
        public static string BoldTag
        {
            get { return XmlTags.boldTag; }
        }


        private static readonly string italicTag = "Italic";
        /// <summary>
        /// "Italic"标签名。
        /// </summary>
        public static string ItalicTag
        {
            get { return XmlTags.italicTag; }
        }

        private static readonly string widgetFormTag = "WidgetForm";
        /// <summary>
        /// "WidgetForm"标签名。
        /// </summary>
        public static string WidgetFormTag
        {
            get { return XmlTags.widgetFormTag; }
        }

        private static readonly string isMindMapLineTag = "IsMindMapLine";
        /// <summary>
        /// "IsMindMapLine"标签名。
        /// </summary>
        public static string IsMindMapLineTag
        {
            get { return XmlTags.isMindMapLineTag; }
        }

        private static readonly string commentTextTag = "CommentText";
        /// <summary>
        /// "CommentText"标签名。
        /// </summary>
        public static string CommentTextTag
        {
            get { return XmlTags.commentTextTag; }
        }

        private static readonly string hyperLinkTextTag = "HyperLinkText";
        /// <summary>
        /// "HyperLinkText"标签名。
        /// </summary>
        public static string HyperLinkTextTag
        {
            get { return XmlTags.hyperLinkTextTag; }
        }

        private static readonly string lineFormTag = "LineForm";
        /// <summary>
        /// "LineForm"标签名。
        /// </summary>
        public static string LineFormTag
        {
            get { return XmlTags.lineFormTag; }
        }

        private static readonly string assistGridFormTag = "AssistGridForm";
        /// <summary>
        /// "AssistGridForm"标签名。
        /// </summary>
        public static string AssistGridFormTag
        {
            get { return XmlTags.assistGridFormTag; }
        }

        private static readonly string pageRightSideBackColorTag = "PageRightSideBackColor";
        /// <summary>
        /// "PageRightSideBackColor"标签名。
        /// </summary>
        public static string PageRightSideBackColorTag
        {
            get { return XmlTags.pageRightSideBackColorTag; }
        }

        private static readonly string pageLeftSideBackColorTag = "PageLeftSideBackColor";
        /// <summary>
        /// "PageLeftSideBackColor"标签名。
        /// </summary>
        public static string PageLeftSideBackColorTag
        {
            get { return XmlTags.pageLeftSideBackColorTag; }
        }

        private static readonly string outerBorderTypeTag = "OuterBorderType";
        /// <summary>
        /// "OuterBorderType"标称名。
        /// </summary>
        public static string OuterBorderTypeTag
        {
            get { return XmlTags.outerBorderTypeTag; }
        }

        private static readonly string triangleFormTag = "TriangleForm";
        /// <summary>
        /// "TriangleForm"标签名。
        /// </summary>
        public static string TriangleFormTag
        {
            get { return XmlTags.triangleFormTag; }
        }

        private static readonly string iconXamlTextTag = "IconXamlText";
        /// <summary>
        /// "IconXaml"标签名。
        /// </summary>
        public static string IconXamlTextTag
        {
            get { return XmlTags.iconXamlTextTag; }
        }

        private static readonly string zoomValueTag = "ZoomValue";
        /// <summary>
        /// "ZoomValue"标签名。
        /// </summary>
        public static string ZoomValueTag
        {
            get { return XmlTags.zoomValueTag; }
        }

        private static readonly string textDockTag = "TextDock";
        /// <summary>
        /// "TextDock"标签名。
        /// </summary>
        public static string TextDockTag
        {
            get { return XmlTags.textDockTag; }
        }

        private static readonly string assistGridStrokeTag = "AssistGridStroke";
        /// <summary>
        /// "AssistGridStroke"标签名。
        /// </summary>
        public static string AssistGridStrokeTag
        {
            get
            {
                return assistGridStrokeTag;
            }
        }

        private static readonly string isPresentationAreaTag = "IsPresentationArea";
        /// <summary>
        /// "IsPresentationArea"标签名。
        /// </summary>
        public static string IsPresentationAreaTag { get { return isPresentationAreaTag; } }

        private static readonly string autoNumberTextTag = "AutoNumberText";
        /// <summary>
        /// AutoNumberText"标签名。
        /// </summary>
        public static string AutoNumberTextTag { get { return autoNumberTextTag; } }

        private static readonly string typeTag = "Type";
        /// <summary>
        /// "Type"标签名。
        /// </summary>
        public static string TypeTag { get { return typeTag; } }

        private static readonly string imageBackColorTag = "ImageBackColor";
        /// <summary>
        /// "ImageBackColor"标签名。
        /// </summary>
        public static string ImageBackColorTag { get { return imageBackColorTag; } }

        private static string paperSizeTextTag = "PaperSizeText";
        /// <summary>
        /// "PaperTextSize"标签名。
        /// </summary>
        public static string PaperSizeTextTag { get { return paperSizeTextTag; } }
    }
}
