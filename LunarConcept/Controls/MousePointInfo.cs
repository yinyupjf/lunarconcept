﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Controls
{
    /// <summary>
    /// 创建时间：2012年1月1日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用在PageEditor类中，以便解决PageEditor的拖动分类控制问题。
    /// </summary>
    public class MousePointInfo
    {
        private PageDraggingType draggingType = PageDraggingType.None;
        /// <summary>
        /// 正在使用鼠标进行何种操作。
        /// </summary>
        public PageDraggingType DraggingType
        {
            get { return draggingType; }
            set { draggingType = value; }
        }

        public Point LeftButtonPreviewPoint { get; set; }

        public Point LeftButtonPoint { get; set; }

        public Point MiddleButtonPreviewPoint { get; set; }

        public Point MiddleButtonPoint { get; set; }

        public Point RightButtonPreviewPoint { get; set; }

        public Point RightButtonPoint { get; set; }


        public Widgets.Widget MainSelectedWidget { get; set; }
    }
}
