﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept.Controls
{
    /// <summary>
    /// MagnifilerControlPanel.xaml 的交互逻辑
    /// </summary>
    public partial class MagnifilerControlPanel : UserControl
    {
        public MagnifilerControlPanel(EditorManager masterEditorManager)
        {
            InitializeComponent();

            this.masterEditorManager = masterEditorManager;
        }

        private void cbZoomFactor_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (cbZoomFactor.SelectedItem == null) return;
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;

            this.masterEditorManager.Magnifier.ZoomFactor = 1 / double.Parse((cbZoomFactor.SelectedItem as ComboBoxItem).Content.ToString().Replace("倍", ""));
        }

        private void tbtnShowMagnifier_Checked_1(object sender, RoutedEventArgs e)
        {
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;

            this.masterEditorManager.Magnifier.Visibility = System.Windows.Visibility.Visible;

            System.Windows.Controls.Primitives.Popup popup = this.Parent as System.Windows.Controls.Primitives.Popup;
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }

        private void tbtnShowMagnifier_Unchecked_1(object sender, RoutedEventArgs e)
        {
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;

            this.masterEditorManager.Magnifier.Visibility = System.Windows.Visibility.Collapsed;

            System.Windows.Controls.Primitives.Popup popup = this.Parent as System.Windows.Controls.Primitives.Popup;
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }

        private EditorManager masterEditorManager;
        /// <summary>
        /// 主文档管理器。
        /// </summary>
        public EditorManager MasterEditorManager
        {
            get { return this.masterEditorManager; }
        }

        private void cbFrameType_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (cbFrameType.SelectedItem == null) return;
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;

            if ((cbFrameType.SelectedItem as ComboBoxItem).Content.ToString() == "圆形")
            {
                this.masterEditorManager.Magnifier.FrameType = Xceed.Wpf.Toolkit.FrameType.Circle;
                
                lblRadius.Visibility = sliderRadius.Visibility = System.Windows.Visibility.Visible;

                lblHeight.Visibility = lblWidth.Visibility = sliderHeight.Visibility = sliderWidth.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.masterEditorManager.Magnifier.FrameType = Xceed.Wpf.Toolkit.FrameType.Rectangle;

                lblRadius.Visibility = sliderRadius.Visibility = System.Windows.Visibility.Collapsed;

                lblHeight.Visibility = lblWidth.Visibility = sliderHeight.Visibility = sliderWidth.Visibility = System.Windows.Visibility.Visible;
            }

        }

        private void sliderRadius_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;

            this.masterEditorManager.Magnifier.Radius = sliderRadius.Value;
        }

        private void sliderWidth_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;
            this.masterEditorManager.Magnifier.Width = sliderWidth.Value;
        }

        private void sliderHeight_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.masterEditorManager == null || this.masterEditorManager.Magnifier == null) return;
            this.masterEditorManager.Magnifier.Height = sliderHeight.Value;
        }
    }
}
