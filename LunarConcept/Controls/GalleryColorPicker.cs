﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Ribbon;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Controls
{
    /// <summary>
    /// 创建时间：2012年2月23日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供一个Ribbon可用的拾色器。
    ///           原来那个使用PopUp的版本不能使用在Ribbon上（在Ribbon处于最小化、弹出状态时无法使用）。
    /// </summary>
    public class GalleryColorPicker : RibbonGallery
    {
        public GalleryColorPicker()
        {
            this.Background = Brushes.LightBlue;

            //禁用纵向滚动条
            ScrollViewer.SetVerticalScrollBarVisibility(this, ScrollBarVisibility.Hidden);
            this.MinColumnCount = this.MaxColumnCount = 13;//计13列不变。

            //添加标准色块
            AddStandardColors();

            //添加命名色块
            AddNamedColors();

            //添加过渡色块（带过渡色阶）
            AddTransitionColors();
        }

        private void AddStandardColors()
        {
            ColorItem ciTransparent = new ColorItem(Brushes.Transparent);
            ciTransparent.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciTransparent);

            ColorItem ciPlacement = new ColorItem(null);
            this.standardColorsCategory.Items.Add(ciPlacement);
            //ColorItem ciGlass = new ColorItem(BrushManager.Glass);
            //ciGlass.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            //this.standardColorsCategory.Items.Add(ciGlass);//不再作占位符2012年6月13日

            ColorItem ciBlack = new ColorItem(Brushes.Black);
            ciBlack.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciBlack);

            ColorItem ciWhite = new ColorItem(Brushes.White);
            ciWhite.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciWhite);

            ColorItem ciRed = new ColorItem(Brushes.Red);
            ciRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciRed);

            ColorItem ciOrange = new ColorItem(Brushes.Orange);
            ciOrange.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciOrange);

            ColorItem ciYellow = new ColorItem(Brushes.Yellow);
            ciYellow.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciYellow);

            ColorItem ciGreen = new ColorItem(Brushes.Green);
            ciGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciGreen);

            ColorItem ciBrown = new ColorItem(Brushes.Brown);
            ciBrown.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciBrown);

            ColorItem ciBlue = new ColorItem(Brushes.Blue);
            ciBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciBlue);

            ColorItem ciPink = new ColorItem(Brushes.Pink);
            ciPink.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciPink);

            ColorItem ciGray = new ColorItem(Brushes.Gray);
            ciGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciGray);

            ColorItem ciGold = new ColorItem(Brushes.Gold);
            ciGold.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.standardColorsCategory.Items.Add(ciGold);

            this.Items.Add(this.standardColorsCategory);
        }

        private void AddNamedColors()
        {
            ColorItem ciAliceBlue = new ColorItem(Brushes.AliceBlue);
            ciAliceBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciAliceBlue);

            ColorItem ciAntiqueWhite = new ColorItem(Brushes.AntiqueWhite);
            ciAntiqueWhite.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciAntiqueWhite);

            ColorItem ciAquamarine = new ColorItem(Brushes.Aquamarine);
            ciAquamarine.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciAquamarine);

            ColorItem ciAzure = new ColorItem(Brushes.Azure);
            ciAzure.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciAzure);

            ColorItem ciBeige = new ColorItem(Brushes.Beige);
            ciBeige.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciBeige);

            ColorItem ciBisque = new ColorItem(Brushes.Bisque);
            ciBisque.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciBisque);

            ColorItem ciBlanchedAlmond = new ColorItem(Brushes.BlanchedAlmond);
            ciBlanchedAlmond.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciBlanchedAlmond);

            ColorItem ciBlueViolet = new ColorItem(Brushes.BlueViolet);
            ciBlueViolet.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciBlueViolet);

            ColorItem ciBurlyWood = new ColorItem(Brushes.BurlyWood);
            ciBurlyWood.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciBurlyWood);

            ColorItem ciCadetBlue = new ColorItem(Brushes.CadetBlue);
            ciCadetBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciCadetBlue);

            ColorItem ciChartreuse = new ColorItem(Brushes.Chartreuse);
            ciChartreuse.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciChartreuse);

            ColorItem ciChocolate = new ColorItem(Brushes.Chocolate);
            ciChocolate.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciChocolate);

            ColorItem ciCoral = new ColorItem(Brushes.Coral);
            ciCoral.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciCoral);

            ColorItem ciCornflowerBlue = new ColorItem(Brushes.CornflowerBlue);
            ciCornflowerBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciCornflowerBlue);

            ColorItem ciCornsilk = new ColorItem(Brushes.Cornsilk);
            ciCornsilk.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciCornsilk);

            ColorItem ciCrimson = new ColorItem(Brushes.Crimson);
            ciCrimson.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciCrimson);

            ColorItem ciDarkBlue = new ColorItem(Brushes.DarkBlue);
            ciDarkBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkBlue);

            ColorItem ciDarkCyan = new ColorItem(Brushes.DarkCyan);
            ciDarkCyan.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkCyan);

            ColorItem ciDarkGoldenrod = new ColorItem(Brushes.DarkGoldenrod);
            ciDarkGoldenrod.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkGoldenrod);

            ColorItem ciDarkGray = new ColorItem(Brushes.DarkGray);
            ciDarkGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkGray);

            ColorItem ciDarkGreen = new ColorItem(Brushes.DarkGreen);
            ciDarkGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkGreen);

            ColorItem ciDarkKhaki = new ColorItem(Brushes.DarkKhaki);
            ciDarkKhaki.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkKhaki);

            ColorItem ciDarkMagenta = new ColorItem(Brushes.DarkMagenta);
            ciDarkMagenta.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkMagenta);

            ColorItem ciDarkOliveGreen = new ColorItem(Brushes.DarkOliveGreen);
            ciDarkOliveGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkOliveGreen);

            ColorItem ciDarkOrange = new ColorItem(Brushes.DarkOrange);
            ciDarkOrange.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkOrange);

            ColorItem ciDarkOrchid = new ColorItem(Brushes.DarkOrchid);
            ciDarkOrchid.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkOrchid);

            ColorItem ciDarkSalmon = new ColorItem(Brushes.DarkSalmon);
            ciDarkSalmon.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkSalmon);

            ColorItem ciDarkSeaGreen = new ColorItem(Brushes.DarkSeaGreen);
            ciDarkSeaGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkSeaGreen);

            ColorItem ciDarkSlateGray = new ColorItem(Brushes.DarkSlateGray);
            ciDarkSlateGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkSlateGray);

            ColorItem ciDarkTurquoise = new ColorItem(Brushes.DarkTurquoise);
            ciDarkTurquoise.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkTurquoise);

            ColorItem ciDarkViolet = new ColorItem(Brushes.DarkViolet);
            ciDarkViolet.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDarkViolet);

            ColorItem ciDeepPink = new ColorItem(Brushes.DeepPink);
            ciDeepPink.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDeepPink);

            ColorItem ciDeepSkyBlue = new ColorItem(Brushes.DeepSkyBlue);
            ciDeepSkyBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDeepSkyBlue);

            ColorItem ciDimGray = new ColorItem(Brushes.DimGray);
            ciDimGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDimGray);

            ColorItem ciDodgerBlue = new ColorItem(Brushes.DodgerBlue);
            ciDodgerBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciDodgerBlue);

            ColorItem ciFloralWhite = new ColorItem(Brushes.FloralWhite);
            ciFloralWhite.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciFloralWhite);

            ColorItem ciForestGreen = new ColorItem(Brushes.ForestGreen);
            ciForestGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciForestGreen);

            ColorItem ciGainsboro = new ColorItem(Brushes.Gainsboro);
            ciGainsboro.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciGainsboro);

            ColorItem ciGhostWhite = new ColorItem(Brushes.GhostWhite);
            ciGhostWhite.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciGhostWhite);

            ColorItem ciGoldenrod = new ColorItem(Brushes.Goldenrod);
            ciGoldenrod.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciGoldenrod);

            ColorItem ciGreenYellow = new ColorItem(Brushes.GreenYellow);
            ciGreenYellow.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciGreenYellow);

            ColorItem ciHoneydew = new ColorItem(Brushes.Honeydew);
            ciHoneydew.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciHoneydew);

            ColorItem ciIndianRed = new ColorItem(Brushes.IndianRed);
            ciIndianRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciIndianRed);

            ColorItem ciIvory = new ColorItem(Brushes.Ivory);
            ciIvory.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciIvory);

            ColorItem ciKhaki = new ColorItem(Brushes.Khaki);
            ciKhaki.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciKhaki);

            ColorItem ciLavender = new ColorItem(Brushes.Lavender);
            ciLavender.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLavender);

            ColorItem ciLavenderBlush = new ColorItem(Brushes.LavenderBlush);
            ciLavenderBlush.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLavenderBlush);

            ColorItem ciLawnGreen = new ColorItem(Brushes.LawnGreen);
            ciLawnGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLawnGreen);

            ColorItem ciLemonChiffon = new ColorItem(Brushes.LemonChiffon);
            ciLemonChiffon.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLemonChiffon);

            ColorItem ciLightBlue = new ColorItem(Brushes.LightBlue);
            ciLightBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightBlue);

            ColorItem ciLightCoral = new ColorItem(Brushes.LightCoral);
            ciLightCoral.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightCoral);

            ColorItem ciLightCyan = new ColorItem(Brushes.LightCyan);
            ciLightCyan.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightCyan);

            ColorItem ciLightGoldenrodYellow = new ColorItem(Brushes.LightGoldenrodYellow);
            ciLightGoldenrodYellow.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightGoldenrodYellow);

            ColorItem ciLightGray = new ColorItem(Brushes.LightGray);
            ciLightGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightGray);

            ColorItem ciLightGreen = new ColorItem(Brushes.LightGreen);
            ciLightGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightGreen);

            ColorItem ciLightPink = new ColorItem(Brushes.LightPink);
            ciLightPink.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightPink);

            ColorItem ciLightSalmon = new ColorItem(Brushes.LightSalmon);
            ciLightSalmon.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightSalmon);

            ColorItem ciLightSeaGreen = new ColorItem(Brushes.LightSeaGreen);
            ciLightSeaGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightSeaGreen);

            ColorItem ciLightSkyBlue = new ColorItem(Brushes.LightSkyBlue);
            ciLightSkyBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightSkyBlue);

            ColorItem ciLightSlateGray = new ColorItem(Brushes.LightSlateGray);
            ciLightSlateGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightSlateGray);

            ColorItem ciLightSteelBlue = new ColorItem(Brushes.LightSteelBlue);
            ciLightSteelBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightSteelBlue);

            ColorItem ciLightYellow = new ColorItem(Brushes.LightYellow);
            ciLightYellow.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLightYellow);

            ColorItem ciLime = new ColorItem(Brushes.Lime);
            ciLime.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLime);

            ColorItem ciLimeGreen = new ColorItem(Brushes.LimeGreen);
            ciLimeGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLimeGreen);

            ColorItem ciLinen = new ColorItem(Brushes.Linen);
            ciLinen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciLinen);

            ColorItem ciMediumAquamarine = new ColorItem(Brushes.MediumAquamarine);
            ciMediumAquamarine.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumAquamarine);

            ColorItem ciMediumOrchid = new ColorItem(Brushes.MediumOrchid);
            ciMediumOrchid.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumOrchid);

            ColorItem ciMediumPurple = new ColorItem(Brushes.MediumPurple);
            ciMediumPurple.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumPurple);

            ColorItem ciMediumSeaGreen = new ColorItem(Brushes.MediumSeaGreen);
            ciMediumSeaGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumSeaGreen);

            ColorItem ciMediumSlateBlue = new ColorItem(Brushes.MediumSlateBlue);
            ciMediumSlateBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumSlateBlue);

            ColorItem ciMediumSpringGreen = new ColorItem(Brushes.MediumSpringGreen);
            ciMediumSpringGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumSpringGreen);

            ColorItem ciMediumTurquoise = new ColorItem(Brushes.MediumTurquoise);
            ciMediumTurquoise.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumTurquoise);

            ColorItem ciMediumVioletRed = new ColorItem(Brushes.MediumVioletRed);
            ciMediumVioletRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMediumVioletRed);

            ColorItem ciMintCream = new ColorItem(Brushes.MintCream);
            ciMintCream.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMintCream);

            ColorItem ciMistyRose = new ColorItem(Brushes.MistyRose);
            ciMistyRose.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMistyRose);

            ColorItem ciMoccasin = new ColorItem(Brushes.Moccasin);
            ciMoccasin.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciMoccasin);

            ColorItem ciNavajoWhite = new ColorItem(Brushes.NavajoWhite);
            ciNavajoWhite.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciNavajoWhite);

            ColorItem ciOldLace = new ColorItem(Brushes.OldLace);
            ciOldLace.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciOldLace);

            ColorItem ciOlive = new ColorItem(Brushes.Olive);
            ciOlive.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciOlive);

            ColorItem ciOliveDrab = new ColorItem(Brushes.OliveDrab);
            ciOliveDrab.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciOliveDrab);

            ColorItem ciOrange = new ColorItem(Brushes.Orange);
            ciOrange.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciOrange);

            ColorItem ciOrangeRed = new ColorItem(Brushes.OrangeRed);
            ciOrangeRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciOrangeRed);

            ColorItem ciOrchid = new ColorItem(Brushes.Orchid);
            ciOrchid.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciOrchid);

            ColorItem ciPaleGoldenrod = new ColorItem(Brushes.PaleGoldenrod);
            ciPaleGoldenrod.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPaleGoldenrod);

            ColorItem ciPaleGreen = new ColorItem(Brushes.PaleGreen);
            ciPaleGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPaleGreen);

            ColorItem ciPaleTurquoise = new ColorItem(Brushes.PaleTurquoise);
            ciPaleTurquoise.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPaleTurquoise);

            ColorItem ciPaleVioletRed = new ColorItem(Brushes.PaleVioletRed);
            ciPaleVioletRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPaleVioletRed);

            ColorItem ciPapayaWhip = new ColorItem(Brushes.PapayaWhip);
            ciPapayaWhip.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPapayaWhip);

            ColorItem ciPeachPuff = new ColorItem(Brushes.PeachPuff);
            ciPeachPuff.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPeachPuff);

            ColorItem ciPeru = new ColorItem(Brushes.Peru);
            ciPeru.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPeru);

            ColorItem ciPink = new ColorItem(Brushes.Pink);
            ciPink.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPink);

            ColorItem ciPlum = new ColorItem(Brushes.Plum);
            ciPlum.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPlum);

            ColorItem ciPowderBlue = new ColorItem(Brushes.PowderBlue);
            ciPowderBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPowderBlue);

            ColorItem ciPurple = new ColorItem(Brushes.Purple);
            ciPurple.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciPurple);

            ColorItem ciRed = new ColorItem(Brushes.Red);
            ciRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciRed);

            ColorItem ciRosyBrown = new ColorItem(Brushes.RosyBrown);
            ciRosyBrown.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciRosyBrown);

            ColorItem ciRoyalBlue = new ColorItem(Brushes.RoyalBlue);
            ciRoyalBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciRoyalBlue);

            ColorItem ciSaddleBrown = new ColorItem(Brushes.SaddleBrown);
            ciSaddleBrown.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSaddleBrown);

            ColorItem ciSalmon = new ColorItem(Brushes.Salmon);
            ciSalmon.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSalmon);

            ColorItem ciSandyBrown = new ColorItem(Brushes.SandyBrown);
            ciSandyBrown.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSandyBrown);

            ColorItem ciSeaGreen = new ColorItem(Brushes.SeaGreen);
            ciSeaGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSeaGreen);

            ColorItem ciSeaShell = new ColorItem(Brushes.SeaShell);
            ciSeaShell.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSeaShell);

            ColorItem ciSienna = new ColorItem(Brushes.Sienna);
            ciSienna.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSienna);

            ColorItem ciSilver = new ColorItem(Brushes.Silver);
            ciSilver.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSilver);

            ColorItem ciSkyBlue = new ColorItem(Brushes.SkyBlue);
            ciSkyBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSkyBlue);

            ColorItem ciSlateBlue = new ColorItem(Brushes.SlateBlue);
            ciSlateBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSlateBlue);

            ColorItem ciSlateGray = new ColorItem(Brushes.SlateGray);
            ciSlateGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSlateGray);

            ColorItem ciSnow = new ColorItem(Brushes.Snow);
            ciSnow.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSnow);

            ColorItem ciSpringGreen = new ColorItem(Brushes.SpringGreen);
            ciSpringGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSpringGreen);

            ColorItem ciSteelBlue = new ColorItem(Brushes.SteelBlue);
            ciSteelBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciSteelBlue);

            ColorItem ciTan = new ColorItem(Brushes.Tan);
            ciTan.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciTan);

            ColorItem ciTeal = new ColorItem(Brushes.Teal);
            ciTeal.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciTeal);

            ColorItem ciThistle = new ColorItem(Brushes.Thistle);
            ciThistle.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciThistle);

            ColorItem ciTomato = new ColorItem(Brushes.Tomato);
            ciTomato.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciTomato);

            ColorItem ciTurquoise = new ColorItem(Brushes.Turquoise);
            ciTurquoise.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciTurquoise);

            ColorItem ciWheat = new ColorItem(Brushes.Wheat);
            ciWheat.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciWheat);

            ColorItem ciWhiteSmoke = new ColorItem(Brushes.WhiteSmoke);
            ciWhiteSmoke.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
            this.namedColorsCategory.Items.Add(ciWhiteSmoke);

            this.Items.Add(this.namedColorsCategory);
        }

        private void AddTransitionColors()
        {
            for (int i = 10; i >= 1; i--)
            {
                Color clrWhite = Brushes.White.Color * i * 0.1f; clrWhite.A = 255;
                ColorItem ciWhite = new ColorItem(new SolidColorBrush(clrWhite));
                ciWhite.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciWhite);//01

                Color clrDarkGray = Brushes.DarkGray.Color * i * 0.1f; clrDarkGray.A = 255;
                ColorItem ciDarkGray = new ColorItem(new SolidColorBrush(clrDarkGray));
                ciDarkGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciDarkGray);//02

                Color clrGray = Brushes.Gray.Color * i * 0.1f; clrGray.A = 255;
                ColorItem ciGray = new ColorItem(new SolidColorBrush(clrGray));
                ciGray.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciGray);//03                

                Color clrTan = Brushes.Tan.Color * i * 0.1f; clrTan.A = 255;
                ColorItem ciTan = new ColorItem(new SolidColorBrush(clrTan));
                ciTan.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciTan);//04

                Color clrRed = Brushes.Red.Color * i * 0.1f; clrRed.A = 255;
                ColorItem ciRed = new ColorItem(new SolidColorBrush(clrRed));
                ciRed.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciRed);//05

                Color clrOrange = Brushes.Orange.Color * i * 0.1f; clrOrange.A = 255;
                ColorItem ciOrange = new ColorItem(new SolidColorBrush(clrOrange));
                ciOrange.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciOrange);//06

                Color clrYellow = Brushes.Yellow.Color * i * 0.1f; clrYellow.A = 255;
                ColorItem ciYellow = new ColorItem(new SolidColorBrush(clrYellow));
                ciYellow.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciYellow);//07

                Color clrYellowGreen = Brushes.YellowGreen.Color * i * 0.1f; clrYellowGreen.A = 255;
                ColorItem ciYellowGreen = new ColorItem(new SolidColorBrush(clrYellowGreen));
                ciYellowGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciYellowGreen);//08

                Color clrLightGreen = Brushes.LightGreen.Color * i * 0.1f; clrLightGreen.A = 255;
                ColorItem ciLightGreen = new ColorItem(new SolidColorBrush(clrLightGreen));
                ciLightGreen.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciLightGreen);//09

                Color clrCyan = Brushes.Cyan.Color * i * 0.1f; clrCyan.A = 255;
                ColorItem ciCyan = new ColorItem(new SolidColorBrush(clrCyan));
                ciCyan.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciCyan);//11

                Color clrSkyBlue = Brushes.SkyBlue.Color * i * 0.1f; clrSkyBlue.A = 255;
                ColorItem ciSkyBlue = new ColorItem(new SolidColorBrush(clrSkyBlue));
                ciSkyBlue.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciSkyBlue);//12

                Color clrViolet = Brushes.Violet.Color * i * 0.1f; clrViolet.A = 255;
                ColorItem ciViolet = new ColorItem(new SolidColorBrush(clrViolet));
                ciViolet.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciViolet);//13

                Color clrPink = Brushes.Pink.Color * i * 0.1f; clrPink.A = 255;
                ColorItem ciPink = new ColorItem(new SolidColorBrush(clrPink));
                ciPink.ColorPicked += new EventHandler<ColorPickedEventArgs>(ci_ColorPicked);
                this.transitionColorsCategory.Items.Add(ciPink);//13
            }

            this.Items.Add(this.transitionColorsCategory);
        }

        void ci_ColorPicked(object sender, ColorPickedEventArgs e)
        {
            OnColorPicked(this, e);
        }

        public event EventHandler<ColorPickedEventArgs> ColorPicked;

        /// <summary>
        /// 触发事件。
        /// </summary>
        protected void OnColorPicked(object sender, ColorPickedEventArgs e)
        {
            if (ColorPicked != null)
            {
                ColorPicked(sender, e);
            }
        }

        private RibbonGalleryCategory standardColorsCategory = new RibbonGalleryCategory()
        {
            Header = "标准色彩",
        };

        private RibbonGalleryCategory namedColorsCategory = new RibbonGalleryCategory()
        {
            Header = "命名色彩",
        };

        private RibbonGalleryCategory transitionColorsCategory = new RibbonGalleryCategory()
        {
            Header = "过渡色彩",
        };
    }

    public class ColorItem : RibbonGalleryItem
    {
        /// <summary>
        /// 注意，参数必须传入BrushManager类支持的画刷。否则会出现意外。
        /// </summary>
        /// <param name="fill">BrushManager类支持的画刷。为null时提供一个占位符。</param>
        public ColorItem(Brush fill)
        {
            this.Padding = defMargin;

            if (fill == null)
            {
                this.Background =
                this.BorderBrush =
                this.CheckedBackground =
                this.CheckedBorderBrush =
                this.Foreground =
                this.MouseOverBackground =
                this.MouseOverBorderBrush =
                this.mainBorder.Background =
                this.mainBorder.BorderBrush = Brushes.Transparent;
                this.IsEnable = false;
                this.ToolTip = "无效，只是占位符";
            }
            else if (fill == Brushes.Transparent)
            {
                Image transImg = new Image()
                {
                    Height = 14,
                    Width = 14,
                    Source = transSource,
                };
                this.mainBorder.Background = fill;
                this.ToolTip = string.Format("{0}/{1}",
                    BrushManager.GetChineseName(this.mainBorder.Background),
                    BrushManager.GetName(this.mainBorder.Background));
                this.mainBorder.Child = transImg;
            }
            else
            {
                this.mainBorder.Background = fill;

                string enName = BrushManager.GetChineseName(this.mainBorder.Background);

                if (enName.StartsWith("sc#"))
                {
                    this.ToolTip = string.Format("过渡色/{0}", enName);
                }
                else
                {
                    this.ToolTip = string.Format("{0}/{1}", enName,
                        BrushManager.GetName(this.mainBorder.Background));
                }
            }
            this.Content = this.mainBorder;

            this.mainBorder.MouseLeftButtonDown +=
                new System.Windows.Input.MouseButtonEventHandler(mainBorder_MouseLeftButtonDown);
            this.mainBorder.MouseRightButtonDown +=
                new System.Windows.Input.MouseButtonEventHandler(mainBorder_MouseRightButtonDown);
        }

        void mainBorder_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (BrushManager.IsValidateBrush(this.mainBorder.Background) == false)
            {
                MessageBox.Show("这个不是内置配色，不能取其名称到剪贴板！", Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                Clipboard.SetData(DataFormats.UnicodeText, BrushManager.GetName(this.mainBorder.Background));
            }
            catch (Exception ex)
            {
                MessageBox.Show("　　发生意外，未能向剪贴板写入文本！系统剪贴板可能被其它程序占用。" +
                    "异常信息如下：\r\n" +
                    ex.Message + "\r\n" + ex.StackTrace, Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        void mainBorder_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (isEnable == false) return;//此时只作占位符。

            OnColorPicked(this, new ColorPickedEventArgs() { PickedColor = mainBorder.Background });
        }

        private bool isEnable = true;
        /// <summary>
        /// [私有]构造方法中传入的fill为null时，此项目只是个占位符，不提供任何功能。
        /// </summary>
        private bool IsEnable
        {
            get { return isEnable; }
            set
            {
                isEnable = value;
                if (isEnable == false)
                {
                    Foreground = Background = CheckedBackground = CheckedBorderBrush =
                    MouseOverBackground = MouseOverBorderBrush = Brushes.Transparent;
                }
            }
        }

        private static BitmapImage transSource = new BitmapImage(
            new Uri("pack://Application:,,,/LunarConcept;component/Images/EditorIcons/Transparent.png"));

        /// <summary>
        /// 默认情况下，mainBorder不在正中，得加点偏移量。
        /// </summary>
        private static Thickness defMargin = new Thickness(0);

        /// <summary>
        /// 默认边框宽度。
        /// </summary>
        private static Thickness defBorderThickness = new Thickness(1);

        private Border mainBorder = new Border()
        {
            BorderBrush = Brushes.Black,
            BorderThickness = defBorderThickness,
            Margin = defMargin,
            Height = 14,
            Width = 14,
        };

        public event EventHandler<ColorPickedEventArgs> ColorPicked;

        /// <summary>
        /// 触发事件。
        /// </summary>
        protected void OnColorPicked(object sender, ColorPickedEventArgs e)
        {
            if (ColorPicked != null)
            {
                ColorPicked(sender, e);
            }
        }
    }
}
