﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// </summary>
    public partial class ColorPicker : UserControl
    {
        public ColorPicker()
        {
            InitializeComponent();
        }

        public enum Destination { ForeColor, BackColor, LineColor, Other }

        public Destination DestAction { get; set; }

        private void Rectangle_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            Brush selectedColor = (sender as Rectangle).Fill;
            OnColorPicked(sender,
                new ColorPickedEventArgs() { PickedColor = selectedColor });
        }

        public event EventHandler<ColorPickedEventArgs> ColorPicked;

        public event EventHandler<EventArgs> Closed;

        protected void OnClosed(object sender, EventArgs e)
        {
            colorName.Text = "";
            if (Closed != null)
            {
                Closed(this, e);
            }
        }

        protected void OnColorPicked(object sender, ColorPickedEventArgs e)
        {
            if (ColorPicked != null)
            {
                ColorPicked(this, e);//注意，这里不用“sender”！！！
            }
        }

        /// <summary>
        /// 注意：只取Brushes类中支持的SolidBrush。一般应使用BrushManager类的GetBrush方法。
        /// </summary>
        /// <param name="brushName">Brushes类中支持的Brush的英文名称。</param>
        /// <returns>如果Brushes类中未定义，会返回null。</returns>
        public static Brush GetBrushByNameFromBrushesClass(string brushName)
        {
            Type type = typeof(Brushes);
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (pi.Name == brushName)
                {
                    return pi.GetValue(null, null) as Brush;
                }
            }
            return null;
        }

        /// <summary>
        /// 从Brushes类中取指定Brush的英文名称。注意：只支持Brushes类中定义的SolidBrush。
        /// </summary>
        /// <param name="brush">要取名称的画刷。</param>
        /// <returns>如果要取名称的画刷不是Brushes类支持的画刷。返回“UnKnow”。</returns>
        public static string GetBrushNameFromBrushesClass(Brush brush)
        {
            if (brush == null) return "UnKnow";

            PropertyInfo[] properties = typeof(Brushes).GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (brush == pi.GetValue(null, null) as Brush)
                {
                    return pi.Name;
                }
            }

            return "UnKnow";
        }

        private void Image_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OnColorPicked(sender,
                new ColorPickedEventArgs() { PickedColor = Brushes.Transparent });
        }

        private void Rectangle_MouseEnter(object sender, MouseEventArgs e)
        {
            Brush fill = (sender as Rectangle).Fill;
            colorName.Text = string.Format("{0}/{1}",
                BrushManager.GetChineseName(fill),
                BrushManager.GetName(fill));
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            colorName.Text = BrushManager.GetChineseName(Brushes.Transparent);
        }

        private void closeImage_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OnClosed(this, e);
        }

        private void Image_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Clipboard.SetData(DataFormats.UnicodeText, ColorPicker.GetBrushNameFromBrushesClass(Brushes.Transparent));
            }
            catch (Exception ex)
            {
                MessageBox.Show("　　发生意外，未能向剪贴板写入文本！系统剪贴板可能被其它程序占用。" +
                    "异常信息如下：\r\n" +
                    ex.Message + "\r\n" + ex.StackTrace, Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            OnClosed(this, e);
        }

        private void Rectangle_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Clipboard.SetData(DataFormats.UnicodeText, ColorPicker.GetBrushNameFromBrushesClass((sender as Rectangle).Fill));
            }
            catch (Exception ex)
            {
                MessageBox.Show("　　发生意外，未能向剪贴板写入文本！系统剪贴板可能被其它程序占用。" +
                    "异常信息如下：\r\n" +
                    ex.Message + "\r\n" + ex.StackTrace, Globals.AppName,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            OnClosed(this, e);
        }

        private void WrapPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            colorName.Text = "";
        }

        private void DockPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            colorName.Text = "";
        }
    }

    public class ColorPickedEventArgs : EventArgs
    {
        public Brush PickedColor { get; set; }
    }
}
